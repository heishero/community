package com.community;

import com.beefe.picker.PickerViewPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.reactnativenavigation.NavigationApplication;
import com.reactnativenavigation.react.NavigationReactNativeHost;
import com.reactnativenavigation.react.ReactGateway;
import com.rnfs.RNFSPackage;
import com.theweflex.react.WeChatPackage;

import org.devio.rn.splashscreen.SplashScreenReactPackage;

import java.util.Arrays;
import java.util.List;

import cn.jiguang.imui.messagelist.ReactIMUIPackage;
import cn.jpush.reactnativejpush.JPushPackage;
import cn.qiuxiang.react.geolocation.AMapGeolocationPackage;
import io.jchat.android.JMessageReactPackage;

public class MainApplication extends NavigationApplication {

    // 设置为 true 将不会弹出 toast
    private boolean SHUTDOWN_TOAST = true;
    // 设置为 true 将不会打印 log
    private boolean SHUTDOWN_LOG = false;

    @Override
    protected ReactGateway createReactGateway() {
        ReactNativeHost host = new NavigationReactNativeHost(this, isDebug(), createAdditionalReactPackages()) {
            @Override
            protected String getJSMainModuleName() {
                return "index";
            }
        };
        return new ReactGateway(this, isDebug(), host);
    }

    @Override
    public boolean isDebug() {
        return BuildConfig.DEBUG;
    }

    protected List<ReactPackage> getPackages() {
        // Add additional packages you require here
        // No need to add RnnPackage and MainReactPackage
        return Arrays.<ReactPackage>asList(
                new SplashScreenReactPackage(),
                new VectorIconsPackage(),
                new PickerPackage(),
                new JPushPackage(SHUTDOWN_TOAST, SHUTDOWN_LOG),
                new JMessageReactPackage(SHUTDOWN_TOAST),
                new AMapGeolocationPackage(),
                new MainReactPackage(),
                new WeChatPackage(),
                new PickerViewPackage(),
                new RNFSPackage(),
                (ReactPackage) new ReactIMUIPackage()
                // eg. new VectorIconsPackage()
        );
    }

    @Override
    public List<ReactPackage> createAdditionalReactPackages() {
        return getPackages();
    }

}