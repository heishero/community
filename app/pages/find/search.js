/*
 * @Author: yixin
 * @Date: 2018-10-19 23:40:27
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-22 22:29:47
 * 发现
 */
import React from 'react';
import { View, TouchableOpacity, TextInput } from 'react-native';
import { createStyles, isIphoneX } from '../../utils/view';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../utils/page';
import CircleList from '../circle/components/circleList';
import { connect } from 'react-redux';
import { actions as CircleActions } from '../../controllers/circle';


class Search extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      search: '',
    };
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <View style={styles.header}>
          <TouchableOpacity onPress={() => { PageFn.pop(this.props.componentId) }} style={styles.back}><Icon name="fanhui" style={styles.backIcon} /></TouchableOpacity>
          <TextInput
            clearButtonMode="while-editing"
            onChangeText={(text) => { this.setState({ search: text }) }}
            placeholder="搜索圈子"
            style={styles.search}
            value={this.state.search}
            onSubmitEditing={() => {
              this.props.GetGroups({ search: this.state.search });
            }}
          />
        </View>
        <CircleList />
      </View>
    );
  }
}

let styles = {
  wrapper: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    ios: {
      height: isIphoneX ? 152 : 128,
      paddingTop: isIphoneX ? 64 : 40,
    },
    android: {
      height: 128,
      paddingTop: 30
    },
    backgroundColor: 'white',
  },
  back: {
    position: 'absolute',
    left: 28,
    paddingRight: 30,
    android: {
      top: 40,
      paddingTop: 26,
    },
    ios: {
      top: 105,
    }
  },
  backIcon: {
    fontSize: 32,
    color: '#333'
  },
  search: {
    marginTop: 20,
    width: 580,
    height: 60,
    paddingVertical: 0,
    backgroundColor: '#F5F5F5',
    borderRadius: 30,
    paddingHorizontal: 30,
    marginLeft: 40,
    fontSize: 24,
    color: '#646464',
  },
  submit: {
    width: 100,
    height: 60,
    backgroundColor: '#fff',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  box: {
    marginTop: 58
  },
  title: {
    height: 32,
    paddingHorizontal: 26,
  },
  titleIcon: {
    color: '#AEAEAE',
    fontSize: 31
  },
  list: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    paddingLeft: 27,
  },
  item: {
    height: 60,
    width: 160,
    borderRadius: 30,
    borderWidth: 2,
    borderColor: '#AEAEAE',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
    marginRight: 16,
  }
};
styles = createStyles(styles);

export default connect(Store => {
  return {};
}, { GetGroups: CircleActions.GetGroups })(Search);