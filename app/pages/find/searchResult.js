/*
 * @Author: yixin
 * @Date: 2018-10-19 23:40:27
 * @Last Modified by: yixin
 * @Last Modified time: 2019-03-23 19:13:25
 * 搜索详情
 */
import React from 'react';
import { View, ScrollView } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import { createStyles } from '../../utils/view';
import Circle from '../circle/components/circle';
import { getGroupList } from '../../service/group';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../utils/page';
import Storage from '../../utils/storage';

class SearchResult extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      groupList: []
    };
  }

  componentDidMount() {
    this.init();
  }

  async init() {
    const token = await Storage.load('token');
    const groupData = await getGroupList({ search: this.props.search, token });
    this.setState({ groupList: groupData.data });
  }

  toPage=(path) => {
    PageFn.push(this.props.componentId, path);
  }

  render() {
    const { groupList } = this.state;
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}>
        搜索结果
        </NormalHeader>
        <ScrollView style={{ flex: 1 }}>
          <Flex flexDirection="column" justifyContent="flex-start" alignItems="center">
            {
              groupList.map((data, index) => (
                <Circle componentId={this.props.componentId} key={data.id} findData={data} />
              ))
            }
          </Flex>
        </ScrollView>
      </View>
    );
  }
}

let styles = {
  wrapper: {
    flex: 1,
    backgroundColor: '#F7F7F7'
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    ios: {
      height: 128,
      paddingTop: 35,
    },
    android: {
      height: 90,
    },
    backgroundColor: '#FDAC42',
  },
  headerControl: {
    position: 'absolute',
    right: 26,
    height: 53,
    width: 50,
    top: 58,
  },
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
  search: {
    width: 493,
    height: 60,
    borderRadius: 10,
    backgroundColor: '#fff',
    fontSize: 24,
    paddingHorizontal: 19,
  },
  menu: {
    height: 70,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  menuItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  menuBorder: {
    borderBottomWidth: 1,
    borderColor: '#FDAC42',
    height: 70,
    justifyContent: 'center',
    alignItems: 'center'
  }
};
styles = createStyles(styles);

export default SearchResult;