/*
 * @Author: yixin
 * @Date: 2018-10-19 23:40:35
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-19 23:31:59
 * 群
 */
import React from 'react';
import { View, TouchableOpacity, FlatList, Alert, BackHandler } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles, Rem } from '../../utils/view';
import { Navigation } from 'react-native-navigation';
import Circle from '../find/components/circle';
import { connect } from 'react-redux';
import { checkVersion } from '../../utils/version';
import circleAction from '../../controllers/circle/actions';
import { PageFn } from '../../utils/page';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';

class FindRec extends React.Component {

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  componentDidMount() {
    checkVersion();
  }

  onBackAndroid = () => {
    let res = true;
    Alert.alert('提示', '确定退出APP？', [
      {
        text: '确定', onPress: () => {
          BackHandler.exitApp();
        }
      },
      {
        text: '取消', onPress: () => {
          res = true;
        }
      },
    ]);
    return res;
  };

  async componentDidAppear() {
    // BackHandler.addEventListener('hardwareBackPress', this.onBackAndroid);
    this.init();
  }

  componentDidDisappear() {
    // BackHandler.removeEventListener('hardwareBackPress', this.onBackAndroid);
  }

  init() {
    const { getClassify, getGroups } = this.props;
    getClassify();
    getGroups();
  }

  usefulRefresh(index) {
    let newNews = [].concat(this.props.circleData.newsList);
    newNews[index].useful++;
    this.props.renderData({ newsList: newNews });
  }

  changeMenu = (id) => {
    this.props.getGroups({ id });
  }

  render() {
    const { circleData, getGroupsMore } = this.props;
    const { groupsList, hasMore, menuList, menuActive } = circleData;
    console.log('menuList', menuList);
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          rightContent={<Icon name="fangdajing" style={{ fontSize: Rem(35) }} />}
          onRightPress={() => { PageFn.push(this.props.componentId, 'Search') }}
        >所有圈子</NormalHeader>
        <View style={styles.menu}>
          {
            menuList && menuList.length > 0 && menuList.map((data, index) => (
              <TouchableOpacity key={data.id} style={styles.menuItem} onPress={() => { this.changeMenu(data.id) }}>
                <View style={(data.id === menuActive) && styles.menuBorder}>
                  <CText color={data.id === menuActive ? '#565656' : '#AEAEAE'} size="24">{data.name}</CText>
                </View>
              </TouchableOpacity>
            ))
          }
        </View>
        <Flex style={{ flex: 1 }} direction="column" justifyContent="flex-start" alignItems="center">
          {groupsList.length > 0 && <FlatList
            renderItem={({ item, index, section }) => { if (item.id) { return (<Circle path="GroupTab" findData={item} componentId={this.props.componentId} />) } }}
            data={groupsList}
            showsVerticalScrollIndicator={false}
            keyExtractor={(item, index) => item.id}
            onEndReachedThreshold={0.1}
            onEndReached={() => { if (hasMore) { getGroupsMore() } }}
          />}
        </Flex>
      </View>
    );
  }
}

let styles = {
  wrapper: {
    flex: 1,
    backgroundColor: '#F7F7F7'
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    ios: {
      height: 128,
      paddingTop: 35,
    },
    android: {
      height: 90,
    },
    backgroundColor: '#fff',
  },
  headerTab: {
    width: 336,
    height: 60,
    overflow: 'hidden',
  },
  tabItem: {
    flex: 1,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerControl: {
    position: 'absolute',
    right: 26,
    height: 60,
    top: 68,
  },
  menu: {
    height: 70,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  menuItem: {
    width: 187.5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  menuBorder: {
    borderBottomWidth: 1,
    borderColor: '#565656',
    height: 70,
    justifyContent: 'center',
    alignItems: 'center'
  },
};
styles = createStyles(styles);

export default connect((Store) => {
  return { circleData: Store.CircleRenderData };
}, {
  renderData: circleAction.RenderData,
  changeTab: circleAction.ChangeTab,
  getGroups: circleAction.GetGroups,
  getGroupsMore: circleAction.GetGroupsMore,
  getClassify: circleAction.GetClassify
})(FindRec);