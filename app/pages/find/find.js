/*
 * @Author: yixin
 * @Date: 2018-10-19 23:40:35
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-19 23:44:05
 * 群
 */
import React from 'react';
import { View } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { Flex } from 'antd-mobile-rn';
import { connect } from 'react-redux';
import { actions as CircleActions } from '../../controllers/circle';
import { CText, WingBlank } from '../../components';
import { createStyles, Rem } from '../../utils/view';
import { checkVersion } from '../../utils/version';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import FindMenu from './components/findMenu';
import CircleList from '../circle/components/circleList';
import { PageFn } from '../../utils/page';
import Bottom from '../common/bottom';

class Find extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      },
    };
  }

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  componentDidMount() {
    checkVersion();
    this.props.getCrowdClassify();
  }

  componentDidAppear() {
    this.init();
  }

  init() {
    this.props.getGroups({ p_cid: 0 });
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <NormalHeader>发现</NormalHeader>
        <WingBlank>
          <Flex onPress={() => {
            PageFn.push(this.props.componentId, 'Search'); }} justify="center" align="center" style={styles.search}>
            <Icon style={{ fontSize: Rem(30), color: '#868686' }} name="sosuo" />
            <CText color="#868686" size="30"> 搜索</CText>
          </Flex>
        </WingBlank>
        {/* 头部menu菜单 */}
        <FindMenu />
        <CircleList />
        <Bottom index="1" />
      </View>
    );
  }
}

let styles = {
  wrapper: {
    flex: 1,
  },
  search: {
    height: 70,
    borderRadius: 20,
    backgroundColor: '#F3F3F3',
  }
};
styles = createStyles(styles);

export default connect((Store) => {
  return { };
}, {
  getCrowdClassify: CircleActions.GetClassify,
  getGroups: CircleActions.GetGroups,
})(Find);