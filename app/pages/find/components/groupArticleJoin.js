import React from 'react';
import { View, TouchableOpacity, Image } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../../components/CText';
import { createStyles, Rem } from '../../../utils/view';
import PageFn from '../../../utils/page';

const GroupArticleJoin = ({ componentId, data }) => {
  return (
    <TouchableOpacity onPress={() => { if (componentId) { PageFn.push(componentId, 'ArticleDetail', { id: data.id }) } }} style={styles.wrapper}>
      <Image style={styles.img} source={{ uri: data.headimgurl }} />
      <View style={styles.content}>
        <CText color="#414141" size="30">{data.title}</CText>
        <Flex style={{ width: Rem(405) }} justify="space-between" alignItems="center">
          <CText size="24" color="#AAAAAA">{data.created_at}</CText>
          <CText size="24" color="#AAAAAA">{data.views}人阅读</CText>
        </Flex>
      </View>
    </TouchableOpacity>
  );
};

let styles = {
  wrapper: {
    width: 750,
    height: 251,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: 26,
    padding: 26,
    borderRadius: 20,
  },
  img: {
    width: 200,
    height: 170,
    borderRadius: 20,
    backgroundColor: '#F7F7F7',
    marginRight: 28,
  },
  content: {
    flex: 1,
    height: 170,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  }
};
styles = createStyles(styles);
export default GroupArticleJoin;
