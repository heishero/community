import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../../components/CText';
import { createStyles, Rem } from '../../../utils/view';
import Icon from 'react-native-vector-icons/icomoon';

const GroupTitle = ({ title, toPage }) => {
  return (
    <TouchableOpacity style={styles.wrapper} onPress={() => { toPage({ screen: 'CommentList' }) }}>
      <CText color="#3D3D3D" size="30">{title}</CText>
      {toPage && <View style={{ justifyContent: 'center', alignItems: 'center' }}><CText color="#FDAC42" size="24">更多 <Icon name="xiang-you" /></CText></View>}
    </TouchableOpacity>
  );
};

let styles = {
  wrapper: {
    height: 60,
    width: 750,
    paddingHorizontal: 26,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    marginBottom: 26
  }
};
styles = createStyles(styles);
export default GroupTitle;
