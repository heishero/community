import React from 'react';
import { View, TouchableOpacity, Image, Text } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../../components/CText';
import { createStyles, Rem } from '../../../utils/view';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../../utils/page';

const DynamicItem = ({ componentId, data, useful, index }) => {
  if (index === 0) { console.log(data.useful) }
  return (
    <TouchableOpacity onPress={() => { PageFn.push(componentId, 'DynamicDetail', { id: data.id }) }} style={styles.wrapper}>
      <Image source={{ uri: data.headimgurl }} style={styles.img} />
      <View style={styles.content}>
        <Flex style={{ width: Rem(540), marginBottom: Rem(10) }} direction="row" justify="space-between" align="center">
          <CText size="30" color="#414141">{data.nick_name}</CText>
          <CText color="#7F7F7F" size="22">圈子活跃度：<Text style={{ color: '#FDAC42' }}>LEV{data.active}</Text></CText>
        </Flex>
        <Text style={styles.comment}>{data.content}</Text>
      </View>
      <Flex style={styles.bottom}>
        <Flex style={{ marginRight: Rem(48) }} direction="row" justify="flex-end" alignItems="center">
          <Icon style={styles.icon} name="taolun" />
          <CText color="#C6C6C6" size="24">讨论</CText>
        </Flex>
        <Flex style={{ marginRight: Rem(48) }} onPress={() => { useful({ topic_id: data.id, index }) }} direction="row" justify="flex-end" alignItems="center">
          <Icon style={[styles.icon, data.useful > 1 && styles.iconActive]} name="jiali" />
          <CText color="#C6C6C6" size="24">加力</CText>
        </Flex>
      </Flex>
    </TouchableOpacity>
  );
};

let styles = {
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginBottom: 24,
    paddingHorizontal: 24,
    borderRadius: 20,
    paddingTop: 30,
    paddingBottom: 30,
    borderBottomWidth: 1,
    borderColor: '#DEDEDE'
  },
  img: {
    width: 120,
    height: 120,
    borderRadius: 60,
    marginRight: 27,
  },
  content: {
    width: 550,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  comment: {
    fontSize: 22,
    color: '#7f7f7f',
    lineHeight: 30,
    marginBottom: 26,
  },
  bottom: {
    position: 'absolute',
    height: 30,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    bottom: 25,
    right: -10,
  },
  icon: {
    fontSize: 26,
    color: '#C6C6C6',
    marginRight: 10
  },
  iconActive: {
    color: '#FDAC42'
  }
};
styles = createStyles(styles);
export default DynamicItem;
