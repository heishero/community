/*
 * @Author: yixin
 * @Date: 2018-10-27 16:22:57
 * @Last Modified by: yixin
 * @Last Modified time: 2019-02-24 17:21:40
 * 圈子详情介绍
 */
import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../../components/CText';
import { createStyles, Rem } from '../../../utils/view';

const GroupDesc = ({ data }) => {
  return (
    <View style={styles.wrapper}>
      <View style={styles.title}>
        <CText color="#3D3D3D" size="30">{data.name}</CText>
      </View>
      <Flex style={styles.param}>
        <CText color="#3D3D3D" size="24">圈子人数：{data.member}人</CText>
        <View style={styles.paramMargin}><CText color="#3D3D3D" size="24">分类：{data.classify_name}</CText></View>
        <CText color="#3D3D3D" size="24">圈子等级：LEV{data.grade}</CText>
      </Flex>
      <View style={styles.desc}>
        <Text style={styles.descText}>圈子简介：{data.introduce}</Text>
      </View>
      <Flex style={styles.bottom} justify="flex-end" align="center">
        <TouchableOpacity style={[styles.bottomBtn, { color: '#FDAC42', marginRight: Rem(41) }]}><CText size="30" color="#3D3D3D">加入</CText></TouchableOpacity>
        <TouchableOpacity style={[styles.bottomBtn, { color: '#9F9F9F' }]}><CText size="30" color="#9F9F9F">收藏</CText></TouchableOpacity>
      </Flex>
    </View>
  );
};

let styles = {
  wrapper: {
    width: 698,
    height: 321,
    padding: 23,
    borderRadius: 20,
    backgroundColor: '#fff',
    marginTop: 26,
    marginBottom: 30
  },
  title: { marginBottom: 25 },
  param: { marginBottom: 25 },
  paramMargin: { marginHorizontal: 49 },
  descText: {
    fontSize: 22,
    color: '#7F7F7F',
    lineHeight: 30,
  },
  bottom: {
    height: 50,
    position: 'absolute',
    bottom: 26,
    left: 0,
    right: 50,
  },
  bottomBtn: {
    width: 120,
    height: 50,
    borderWidth: 1,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center'
  }
};
styles = createStyles(styles);

export default GroupDesc;
