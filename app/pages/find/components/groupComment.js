import React from 'react';
import { View, TouchableOpacity, Image, Text } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../../components/CText';
import { createStyles, Rem } from '../../../utils/view';

const GroupComment = ({ data }) => {
  return (
    <TouchableOpacity style={styles.wrapper} >
      <Image source={{ uri: data.headimgurl }} style={styles.img} />
      <View style={styles.content}>
        <Flex style={{ width: Rem(550), marginBottom: Rem(26) }} direction="row" justify="space-between" align="center">
          <CText size="30" color="#414141">{data.nick_name}</CText>
          <CText color="#7F7F7F" size="22">{data.created_at}</CText>
        </Flex>
        <Text style={styles.comment}>{data.content}</Text>
      </View>
    </TouchableOpacity>
  );
};

let styles = {
  wrapper: {
    width: 698,
    // height: 210,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: '#fff',
    marginBottom: 26,
    padding: 26,
    borderRadius: 20,
    shadowOffset: { width: 6, height: 6 },
    shadowColor: '#000',
    shadowOpacity: 0.14,
    shadowRadius: 8,
  },
  img: {
    width: 70,
    height: 70,
    borderRadius: 35,
    backgroundColor: '#F7F7F7',
    marginRight: 27,
  },
  content: {
    width: 550,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  comment: {
    fontSize: 22,
    color: '#7f7f7f',
    lineHeight: 30,
  }
};
styles = createStyles(styles);
export default GroupComment;
