/*
 * @Author: yixin
 * @Date: 2019-03-23 12:08:47
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-22 22:34:59
 * 发现页面头部菜单
 */

import React from 'react';
import { View, Image } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import { connect } from 'react-redux';
import { createStyles, Rem } from '../../../utils/view';
import { CText, WingBlank } from '../../../components';
import { PageFn } from '../../../utils/page';

class FindMenu extends React.PureComponent {
  render() {
    if (this.props.menu.length === 0) return null;
    return [
      <WingBlank key={1}>
        <Flex style={styles.wrapper}>
          {this.props.menu.map(data => (
            <Flex style={{ width: '20%', marginTop: Rem(20) }} onPress={() => { PageFn.push('Find', 'FindClassify', { data }) }} key={data.id} direction="column" >
              <Image style={styles.img} resizeMode="cover" source={{ uri: data.icon }} />
              <CText color="#3B3B3B" size="22">{data.name}</CText>
            </Flex>
          ))}
        </Flex>
      </WingBlank>,
      <View key={2} style={styles.bottom}></View>
    ];
  }
}

let styles = {
  wrapper: {
    marginTop: 10,
    marginBottom: 20,
    flexWrap: 'wrap'
  },
  img: {
    width: 88,
    height: 88,
    borderRadius: 44,
    marginBottom: 10,
  },
  bottom: {
    height: 20,
    backgroundColor: '#F6F6F6',
  }
};
styles = createStyles(styles);

export default connect((Store) => {
  return { menu: Store.CircleRenderData.menu };
})(FindMenu);
