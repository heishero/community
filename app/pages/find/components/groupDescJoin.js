/*
 * @Author: yixin
 * @Date: 2018-10-27 16:22:57
 * @Last Modified by: yixin
 * @Last Modified time: 2019-02-24 17:21:51
 * 圈子详情介绍已加入
 */
import React from 'react';
import { View, Image, Text } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../../components/CText';
import { createStyles, Rem } from '../../../utils/view';

const GroupDescJoin = ({ data }) => {
  return (
    <View style={styles.wrapper}>
      <Image style={styles.groupImg} source={{ uri: data.headimgurl }} />
      {/* 圈子昵称 */}
      <CText size="30" color="#fff">{data.name}</CText>
      {/* 圈子人数等级 */}
      <Flex justify="center" align="center" style={styles.groupNumBox}>
        <Flex style={{ flex: 1, marginRight: Rem(100) }} justify="flex-end"><CText size="24" color="#fff">圈子人数：{data.member}</CText></Flex>
        <Flex style={{ flex: 1, marginLefr: Rem(100) }} justify="flex-start"><CText size="24" color="#fff">圈子等级：LEV{data.grade}</CText></Flex>
      </Flex>
      <Text style={styles.introduce}>圈子介绍：今年以来A股弱势震荡，权益类基金销售遇冷，但跟踪指数的ETF规模却一路逆市大增。数据显示，年内ETF基金净值（50字已内）</Text>
    </View>
  );
};

let styles = {
  wrapper: {
    width: 560,
    marginTop: -60,
    marginBottom: 30,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  groupImg: {
    width: 128,
    height: 128,
    borderRadius: 64,
    marginBottom: 10,
  },
  groupNumBox: {
    marginBottom: 10
  },
  introduce: {
    fontSize: 20,
    lineHeight: 30,
    color: '#fff',
  },
};
styles = createStyles(styles);

export default GroupDescJoin;
