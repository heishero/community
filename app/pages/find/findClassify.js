/*
 * @Author: yixin
 * @Date: 2019-03-23 16:33:10
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-22 22:24:37
 * 圈子二级分类页面
 */

import React from 'react';
import { View, ScrollView, Text } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import { createStyles, Rem } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../utils/page';
import CircleList from '../circle/components/circleList';
import { getClassify } from '../../service/group';
import { connect } from 'react-redux';
import { actions as CircleActions } from '../../controllers/circle';

class FindClassify extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
        runNum: false
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      currentId: null,
      classify: [],
    };
    this.props.clear();
  }

  componentDidMount() {
    this.init();
  }

  async init() {
    try {
      const { data } = await getClassify({ pid: this.props.data.id });
      this.setState({
        classify: data,
        currentId: data[0].id
      });
      this.props.getGroups({ p_cid: this.props.data.id, s_cid: data[0].id });
    } catch (error) {

    }
  }

  changeMenu = (id) => {
    this.setState({ currentId: id });
    this.props.getGroups({ p_cid: this.props.data.id, s_cid: id });
  }

  toSearch = () => {
    PageFn.push(this.props.componentId, 'Search');
  }

  render() {
    const { name } = this.props.data;
    const { classify, currentId } = this.state;
    return (
      <View style={{ backgroundColor: '#fff' }}>
        <NormalHeader
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}
          rightContent={<Icon style={styles.headerIcon_right} name="sosuo" />}
          onRightPress={() => { this.toSearch() }}
        >
          {name}
        </NormalHeader>
        {classify && <ScrollView
          style={styles.menu}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          nestedScrollEnabled={true}>
          {
            classify.map(data => (
              <Flex onPress={() => this.changeMenu(data.id)} justify="center" align="center" key={data.id} style={{ width: Rem(187.5) }}>
                <Text style={{
                  color: currentId === data.id ? '#2AB6F3' : '#727272',
                  fontSize: Rem(30),
                  borderBottomWidth: currentId === data.id ? 2 : 0,
                  borderColor: '#2AB6F3',
                  lineHeight: Rem(66)
                }}>{data.name}</Text>
              </Flex>
            ))
          }
        </ScrollView>}
        {
          currentId !== null && <CircleList />
        }
      </View>
    );
  }
}

let styles = {
  headerIcon: {
    fontSize: 34,
    paddingRight: 100,
  },
  headerIcon_right: {
    fontSize: 34,
    paddingLeft: 100,
  },
  menu: {
    borderBottomWidth: 1,
    borderColor: '#C9C9C9',
    height: 88,
  },
};
styles = createStyles(styles);

export default connect((Store) => {
  return { };
}, {
  getCrowdClassify: CircleActions.GetClassify,
  getGroups: CircleActions.GetGroups,
  clear: CircleActions.Clear
})(FindClassify);