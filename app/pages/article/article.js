/*
 * @Author: yixin
 * @Date: 2018-10-19 23:42:34
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-17 22:22:35
 * 干货
 */
import React from 'react';
import { View, FlatList } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import { createStyles, Rem } from '../../utils/view';
import ArtList from './components/artList';
import { getGroupArticle } from '../../service/article';

class Article extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      article: [],
      page: 1,
      pageSize: 10,
      refreshing: false,
    };
  }

  componentDidMount() {
    this.init();
  }

  init = async () => {
    try {
      this.setState({ refreshing: true });
      const { page, pageSize } = this.state;
      const { data } = await getGroupArticle({ crowd_id: this.props.id, page, pageSize });
      this.setState({
        article: data,
        refreshing: false,
      });
    } catch (error) {
      this.setState({
        refreshing: false,
      });
    }
  }

  render() {
    const { article, refreshing } = this.state;
    return (
      <View style={styles.wrapper}>
        <Flex style={{ flex: 1 }} direction="column" justify="flex-start" align="center">
          <FlatList
            style={{ flex: 1, marginTop: Rem(26) }}
            data={article}
            keyExtractor={(item, index) => (String(item.id))}
            showsVerticalScrollIndicator={false}
            renderItem={({ item }) => (<ArtList data={item} componentId={this.props.componentId} />)}
            refreshing={refreshing}
            onRefresh={this.init}
          />
        </Flex>
      </View>
    );
  }
}

let styles = {
  wrapper: {
    flex: 1,
    backgroundColor: '#F7F7F7',
  },
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
};
styles = createStyles(styles);

export default Article;