/*
 * @Author: yixin
 * @Date: 2018-12-07 17:18:56
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-17 22:24:12
 * 文章评论
 */
import React from 'react';
import { View, TextInput } from 'react-native';
import { Flex, Toast } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../utils/page';
import { articleComment } from '../../service/article';
import Storage from '../../utils/storage';

class ReleaseComment extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      content: ''
    };
  }

  comment = async () => {
    try {
      const token = await Storage.load('token');
      Toast.loading('发布中', 8);
      await articleComment({ article_id: this.props.id, token: token, content: this.state.content });
      Toast.success('评价成功');
      PageFn.pop(this.props.componentId);
    } catch (error) {
      Toast.hide();
      console.log(error);
    }
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}
          rightContent={<CText size="30" color="#000">发布</CText>}
          onRightPress={() => { this.comment() }}
        >
          发布评价
        </NormalHeader>
        <Flex justify="center">
          <TextInput onChangeText={(text) => { this.setState({ content: text }) }} value={this.state.content} style={styles.textArea} multiline={true} placeholder="请输入您的评论内容" numberOfLines={8} />
        </Flex>
      </View>
    );
  }}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#F7F7F7' },
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
  textArea: {
    width: 697,
    backgroundColor: '#fff',
    marginTop: 30,
    textAlignVertical: 'top',
    padding: 28
  }
};
styles = createStyles(styles);

export default ReleaseComment;