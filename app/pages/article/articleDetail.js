import React from 'react';
import { View, ScrollView, Text, Image, Alert } from 'react-native';
import { Flex, Toast } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles, Rem } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import GroupTitle from '../find/components/groupTitle';
import GroupArticle from '../find/components/groupArticle';
import GroupComment from '../find/components/groupComment';
import { getArticleDetail, getArticleComment, getRecommendArticle, articleUseful } from '../../service/article';
import PageFn from '../../utils/page';
import Storage from '../../utils/storage';
import HTMLView from 'react-native-htmlview';
import * as WeChat from 'react-native-wechat';

class ArticleDetail extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      article: [],
      comment: [],
      recArticle: [],
    };
  }

  componentDidMount() {
    this.init();
  }

  async init() {
    try {
      const token = await Storage.load('token');
      const { data } = await getArticleDetail({ id: this.props.id, token });
      this.setState({ article: data });
      const comment = await getArticleComment({ article_id: this.props.id, page: 1, pageSize: 3 });
      const recArticle = await getRecommendArticle(this.props.id);
      this.setState({ comment: comment.data, recArticle: recArticle.data });
    } catch (error) {
      console.log(error);
    }
  }

toPage = () => {
  PageFn.push(this.props.componentId, 'CommentList', { id: this.props.id });
}

renderNode(node, index, siblings, parent, defaultRenderer) {
  if (node.name === 'img') {
    const a = node.attribs;
    return (<Image key={index} style={{ width: Rem(750), height: Rem(300), padding: 0, margin: 0 }} source={{ uri: a.src }} />);
  }
}

share = () => {
  Alert.alert(
    '分享',
    '',
    [
      { text: '分享到朋友圈', onPress: () => this.shareTimeLine() },
      { text: '分享给朋友', onPress: () => this.shareSession() },
      { text: '取消', onPress: () => console.log('OK Pressed'), style: 'cancel' },
    ],
    { cancelable: false }
  );
}

shareTimeLine = () => {
  WeChat.isWXAppInstalled().then((isInstalled) => {
    if (isInstalled) {
      WeChat.shareToTimeline({
        title: this.state.article.title,
        description: this.state.article.title,
        thumbImage: 'http://community.img.guoxiaoge.cn/icon.png',
        type: 'news',
        webpageUrl: `https://community.guoxiaoge.cn/h5/article.html?id=${this.state.article.id}`,
      })
        .catch((error) => { console.log('wx:' + error); Toast.error(error.message) }); } else {
      Toast.error('没有安装微信软件，请您安装微信之后再试');
    } });
}

shareSession = () => {
  WeChat.isWXAppInstalled().then((isInstalled) => {
    if (isInstalled) {
      WeChat.shareToSession({
        type: 'news',
        title: this.state.article.title,
        description: this.state.article.title,
        thumbImage: 'http://community.img.guoxiaoge.cn/icon.png',
        webpageUrl: `https://community.guoxiaoge.cn/h5/article.html?id=${this.state.article.id}`,
      })
        .catch((error) => { console.log('wx:' + error); Toast.error(error.message) }); } else {
      Toast.error('没有安装微信软件，请您安装微信之后再试');
    } });
}

// 点赞
useful = async () => {
  if (this.state.article.is_useful) return null;
  try {
    const token = await Storage.load('token');
    await articleUseful({ token, article_id: this.state.article.id });
    this.init();
  } catch (error) {
    Toast.fail('已经点赞');
  }
}

render() {
  const { article, comment, recArticle } = this.state;
  return (
    <View style={styles.wrapper}>
      <NormalHeader
        leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
        onLeftPress={() => { PageFn.pop(this.props.componentId) }}
        rightContent={<Icon style={styles.headerIcon} name="fenxiang" />}
        onRightPress={() => { this.share() }}
      >文化详情</NormalHeader>
      <ScrollView>
        <View style={{ fontSize: Rem(42), fontWeight: 'bold', marginLeft: Rem(12), marginRight: Rem(12), marginTop: Rem(30) }}>
          {article.title && <Text style={{ fontSize: Rem(36), fontWeight: 'bold', marginLeft: Rem(12), marginRight: Rem(12), marginBottom: 15 }}>{article.title}</Text>}
        </View>
        <View style={{ height: this.state.height, marginLeft: Rem(30), marginRight: Rem(30) }}>
          {article.content && <HTMLView value={article.content} renderNode={this.renderNode} />}
        </View>
        <Flex justify="center" align="center" onPress={this.useful}><Icon name="tui-jian" style={[styles.hotIcon, { color: article.is_useful ? '#FDAC42' : '#aaa' }]} /></Flex>
        <Flex justify="space-between" align="center" style={styles.articleBottom}>
          <CText color="#7F7F7F" size="24">发布时间: {article.updated_at}</CText>
          <CText color="#7F7F7F" size="24">{article.useful} 人觉得很赞</CText>
        </Flex>
        {/* 评价 */}
        <Flex direction="column" justify="flex-start" align="center">
          <GroupTitle title="评价" toPage={this.toPage} />
          {comment.map(data => {
            return (<GroupComment key={data.id} data={data} />);
          })}
        </Flex>
        {/* 相关推荐 */}
        <Flex direction="column" justify="flex-start" align="center">
          <GroupTitle title="相关推荐" />
          {recArticle && recArticle.map(data => {
            return (<GroupArticle key={data.id} data={data} componentId={this.props.componentId} />);
          })}
        </Flex>
      </ScrollView>
    </View>
  );
}}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#F7F7F7' },
  headerIcon: {
    fontSize: 32,
    color: '#000',
    paddingHorizontal: 10
  },
  articleBottom: {
    marginTop: 20,
    paddingHorizontal: 26,
    marginBottom: 20,
  },
  toDetail: {
    backgroundColor: '#FDAC42',
    opacity: 0.9,
    justifyContent: 'center',
    alignItems: 'center',
    width: 120,
    height: 120,
    borderRadius: 60,
    position: 'absolute',
    top: 698,
    right: 26,
  },
  hotIcon: {
    fontSize: 50,
    marginTop: 10
  }
};
styles = createStyles(styles);

export default ArticleDetail;