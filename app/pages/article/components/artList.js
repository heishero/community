/*
 * @Author: yixin
 * @Date: 2018-10-25 16:22:02
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-23 22:28:24
 * 干货文章列表
 */

import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text } from 'react-native';
import CText from '../../../components/CText';
import { createStyles } from '../../../utils/view';
import PageFn from '../../../utils/page';

class ArtList extends Component {
  render() {
    const { data } = this.props;
    return (
      <TouchableOpacity onPress={() => {
        PageFn.push(this.props.componentId, 'ArticleDetail', { id: data.id });
      }} style={styles.wrapper}>
        <Image source={{ uri: data.headimgurl }} style={styles.img} />
        <View style={styles.desc}>
          <View style={styles.title}><CText size="28" color="#414141">{data.title}</CText></View>
          <Text numberOfLines={1} style={styles.desc_text}>{data.introduce}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

let styles = {
  wrapper: {
    backgroundColor: '#fff',
    width: 698,
    // height: 343,
    borderRadius: 20,
    shadowOffset: { width: 6, height: 6 },
    shadowColor: '#000',
    shadowOpacity: 0.14,
    shadowRadius: 8,
    paddingBottom: 10,
    marginBottom: 26
  },
  img: {
    height: 242,
    width: 698,
    borderRadius: 20,
  },
  hot: {
    height: 43,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 26,
    backgroundColor: 'rgba(0, 0, 0, 0.23)',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
  },
  hotIcon: {
    fontSize: 25,
    color: '#FEFEFE',
    marginRight: 12,
  },
  hotText: {
    fontSize: 18,
    color: '#FEFEFE'
  },
  desc: {
    paddingVertical: 10,
    paddingHorizontal: 23,
  },
  title: {
    marginBottom: 6
  },
  desc_text: {
    fontSize: 20,
    color: '#8B8B8B'
  }
};
styles = createStyles(styles);

export default ArtList;