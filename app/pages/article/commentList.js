/*
 * @Author: yixin
 * @Date: 2018-11-05 14:43:40
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-17 22:19:47
 * 干货详情评价
 */
import React from 'react';
import { View, ScrollView, TouchableOpacity } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import { Navigation } from 'react-native-navigation';
import CText from '../../components/CText';
import { createStyles } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import GroupComment from '../find/components/groupComment';
import { getArticleComment } from '../../service/article';
import PageFn from '../../utils/page';
import Storage from '../../utils/storage';

class CommentList extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
    this.state = {
      list: [],
      token: '',
    };
  }

  componentDidAppear() {
    this.init();
  }

  async init() {
    try {
      const token = await Storage.load('token');
      this.setState({ token: token });
      const { data } = await getArticleComment({ article_id: this.props.id, pageSize: 20, page: 1 });
      this.setState({ list: data });
    } catch (error) {
    }
  }

  render() {
    const { list, token } = this.state;
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}>
        评价
        </NormalHeader>
        <ScrollView style={styles.list}>
          <Flex direction="column" align="center">
            {
              list.map(data => (
                <GroupComment key={data.id} data={data} />

              ))
            }
          </Flex>
        </ScrollView>
        {!!token && <TouchableOpacity style={styles.create} onPress={() => {
          PageFn.push(this.props.componentId, 'ReleaseComment', { id: this.props.id });
        }}>
          <CText size="30" color="#fff">发 布</CText>
          <CText size="30" color="#fff">评 价</CText>
        </TouchableOpacity>}
      </View>
    );
  }}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#F7F7F7' },
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
  list: {
    marginTop: 22
  },
  create: {
    height: 120,
    width: 120,
    borderRadius: 60,
    backgroundColor: '#FDAC42',
    position: 'absolute',
    bottom: 194,
    right: 50,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 0,
  }
};
styles = createStyles(styles);

export default CommentList;