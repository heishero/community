import React from 'react';
import { View, Text, Image, ScrollView } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles, Rem } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../utils/page';
import { goodsMenu, goodsList } from '../../service/shop';
import { Navigation } from 'react-native-navigation';
import Storage from '../../utils/storage';

class Shop extends React.PureComponent {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
    this.state = {
      list: [],
    };
  }

  componentDidMount() {
    this.init();
  }

  init = async () => {
    try {
      const token = await Storage.load('token');
      const list = await goodsList({ crowd_id: this.props.id, token });
      this.setState({ list: list.data });
    } catch (error) {

    }
  }

  goodsList = () => {
    const { list } = this.state;
    return (
      <Flex direction="row" wrap="wrap" style={styles.goodsList}>
        {
          list.map((data) => (
            <Flex onPress={() => { PageFn.push(this.props.componentId, 'GoodsDetail', { id: data.id }) }} key={data.id} style={styles.goodsItem} direction="row" justify="start">
              <Image style={styles.goodsImg} source={{ uri: data.small_image }} />
              <Flex direction="column" style={styles.goodsInfo} align="start">
                <Text style={styles.goodsName}>{data.name}</Text>
                <Flex direction="row" style={{ marginTop: Rem(10) }}>
                  <Text style={styles.xl}>销量: {data.salenum}</Text>
                  <Text style={styles.kc}>库存: {data.inventory}</Text>
                </Flex>
                <Text style={styles.sjsj}>上架时间：  {data.on_off_time}</Text>
                <Text style={styles.jg}>¥{data.out_price}元</Text>
              </Flex>
            </Flex>
          ))
        }
      </Flex>
    );
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <ScrollView style={styles.container} justify="flex-start" align="flex-start">
          {this.goodsList()}
        </ScrollView>
      </View>
    );
  }}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#fff' },
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
  container: {
    flex: 1,
  },
  list: {
    width: 150,
    height: 1000,
    backgroundColor: '#F7F7F7'
  },
  listItem: {
    width: 150,
    height: 100,
    flexWrap: 'nowrap'
  },
  classifyName: {
    color: '#333',
    fontSize: 28,
  },
  goodsItem: {
    borderWidth: 1,
    borderColor: '#F1F1F1',
    paddingTop: 20,
    paddingBottom: 20,
  },
  goodsImg: {
    height: 210,
    width: 210,
    borderRadius: 16,
    marginLeft: 26,
  },
  goodsName: {
    color: '#0C0C0C',
    fontWeight: '400',
    fontSize: 30,
  },
  sjsj: {
    color: '#999999',
    fontSize: 24,
    marginTop: 10
  },
  goodsInfo: {
    marginLeft: 20,
    marginRight: 43,
    flex: 1
  },
  xl: {
    color: '#999999',
    fontSize: 24,
  },
  kc: {
    color: '#999999',
    fontSize: 24,
    marginLeft: 50,
  },
  goodsList: {
    flex: 1,
  },
  jg: {
    fontSize: 34,
    color: '#FF7200',
    marginTop: 15,
  }
};
styles = createStyles(styles);

export default Shop;