import React from 'react';
import { View, Text, Image, FlatList } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import { createStyles, Rem } from '@/utils/view';
import PageFn from '@/utils/page';
import { goodsRec } from '@/service/shop';
import { Navigation } from 'react-native-navigation';
import Storage from '@/utils/storage';

class GoodsRec extends React.PureComponent {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
    this.state = {
      list: [],
    };
  }

  componentDidMount() {
    this.init();
  }

  init = async () => {
    try {
      const token = await Storage.load('token');
      const { data } = await goodsRec({ crowd_id: this.props.id, token });
      this.setState({ list: data.list });
    } catch (error) {

    }
  }

  renderItem = ({ item }) => {
    return (
      <Flex onPress={() => { PageFn.push(this.props.componentId, 'GoodsDetail', { id: item.id }) }} style={styles.goodsItem} direction="row" justify="start">
        <Image style={styles.goodsImg} source={{ uri: item.small_image }} />
        <Flex direction="column" style={styles.goodsInfo} align="start">
          <Text numberOfLines={2} style={styles.goodsName}>{item.name}</Text>
          <Flex direction="row" style={{ marginTop: Rem(10) }}>
            <Text style={styles.xl}>销量: {item.salenum}</Text>
            <Text style={styles.kc}>库存: {item.inventory}</Text>
          </Flex>
          <Text style={styles.sjsj}>上架时间：  {item.on_off_time}</Text>
          <Text style={styles.jg}>¥{item.out_price}元</Text>
        </Flex>
      </Flex>
    );
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <FlatList
          data={this.state.list}
          renderItem={this.renderItem}
          keyExtractor={item => item.id}
        />
      </View>
    );
  }}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#fff' },
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
  list: {
    width: 150,
    height: 1000,
    backgroundColor: '#F7F7F7'
  },
  listItem: {
    width: 150,
    height: 100,
    flexWrap: 'nowrap'
  },
  classifyName: {
    color: '#333',
    fontSize: 28,
  },
  goodsItem: {
    borderWidth: 1,
    borderColor: '#F1F1F1',
    paddingTop: 20,
    paddingBottom: 20,
  },
  goodsImg: {
    height: 210,
    width: 210,
    borderRadius: 16,
    marginLeft: 26,
  },
  goodsName: {
    color: '#0C0C0C',
    fontWeight: '400',
    fontSize: 30,
  },
  sjsj: {
    color: '#999999',
    fontSize: 24,
    marginTop: 10
  },
  goodsInfo: {
    marginLeft: 20,
    marginRight: 43,
    flex: 1
  },
  xl: {
    color: '#999999',
    fontSize: 24,
  },
  kc: {
    color: '#999999',
    fontSize: 24,
    marginLeft: 50,
  },
  jg: {
    fontSize: 34,
    color: '#FF7200',
    marginTop: 15,
  }
};
styles = createStyles(styles);

export default GoodsRec;