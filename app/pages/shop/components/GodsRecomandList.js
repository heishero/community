import React, { Component } from 'react';
import  CircleMycirleCell from '../../../pages/circle/components/CirCleMycirleCell';
import { View, FlatList, TouchableOpacity } from 'react-native';
import { getGroupList } from '../../../service/group';
import Storage from '../../../utils/storage';
import PageFn from '@/utils/page';

export default class GodsRecomandList extends Component {

  constructor() {
    super();
    this.state = {
      listArray: [],
      page: 1,
      pageSize: 10,
      hasMore: false,
    };
  }

  componentDidMount() {
    this.init();
  }
  async init() {
    try {
      const token = await Storage.load('token');
      const groupData = await getGroupList({ token });
      this.refreshing = false;
      this.setState({ listArray: groupData.data });
    } catch (error) {
      console.log(error);
    }
  }

  selectedItem=({ item }) => {
    console.log(item);
    PageFn.push(this.props.componentId, 'CircleDetail', { id: item.id });
  }

  render() {
    return (
      <FlatList
        scrollEnabled={false}
        data={this.state.listArray}
        style={{ flex: 1 }}
        keyExtractor={(item, index) => String(item.id)}
        showsVerticalScrollIndicator={false}
        renderItem={(item) => {
          return (
            <TouchableOpacity onPress={this.selectedItem.bind(this, item)}>
              <CircleMycirleCell
                contentColor="#FDAC42"
                imgUrl={item.item.headimgurl}
                numPeople={`${item.item.member}人参加`}
                titleName={item.item.classify_name}
                content={item.item.introduce}
              />
              <View style={{ height: 1, backgroundColor: '#EFEFEF' }}></View>
            </TouchableOpacity>
          );
        }}
      />
    );
  }
}
