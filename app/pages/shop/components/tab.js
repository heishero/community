import React from 'react';
import { View, TouchableHighlight, Text, Image } from 'react-native';
import { Flex, } from 'antd-mobile-rn';
import { createStyles, Rem } from '../../../utils/view';
import Rate from './rate';
class Tab extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      goodsDetail: this.props.value,
      naviBarStatus: 0,
      tab1Style: { 'color': '#F5CC00', 'borderBottomWidth': 2, 'borderBottomColor': '#F5CC00' },
      tab2Style: {}
    };
    console.log(this.state.goodsDetail);
  }

  onNaviBarPress(aNum) {
    if (aNum === 0) {
      this.setState({
        tab1Style: { 'color': '#F5CC00', 'borderBottomWidth': 2, 'borderBottomColor': '#F5CC00' },
        tab2Style: {}
      });
    } else {
      this.setState({
        tab1Style: {},
        tab2Style: { 'color': '#F5CC00', 'borderBottomWidth': 2, 'borderBottomColor': '#F5CC00' }
      });
    }
  }

  onChangeItem(aNum) {
    if (aNum === 0) {
      return (
        <Flex style={styles.goodsSection} direction="column" align="flex-start">
          {
            this.state.goodsDetail.illustrate.map((data, index) => (
              <Image key={index} style={styles.illustrateImg} source={{ uri: data }} />
            ))
          }
        </Flex>
      );
    } else {
      return (
        <Flex style={styles.goodsSection} direction="column" align="flex-start">
          {
            this.state.goodsDetail.evalue_content.length > 0 && this.state.goodsDetail.evalue_content.map((data, index) => (
              <Flex style={styles.item} key={index} direction="column">
                <Flex style={styles.head} direction="row" justify="between" align="center">
                  <Flex>
                    <Image style={styles.avtor} source={{ uri: data.headimgurl }} />
                    <Flex direction="column" align="start">
                      <Text style={{ color: '#333333', fontSize: Rem(24), marginRight: Rem(22) }}>{data.nick_name}</Text>
                      <Rate value={data.star} maxValue="5" />
                    </Flex>
                  </Flex>
                  <Text style={{ color: '#AAAAAA', fontSize: Rem(22), marginRight: Rem(22) }}>{data.created_at}</Text>
                </Flex>
                <Text style={styles.zw}>{data.evaluate}</Text>
              </Flex>
            ))
          }
        </Flex>
      );
    }
  }
  _goodsprofile() {
    this.onNaviBarPress(0);
    this.setState({
      naviBarStatus: 0
    });
  }

  _goodscommit() {
    this.onNaviBarPress(1);
    this.setState({
      naviBarStatus: 1
    });
  }
  render() {
    return (
      <View style={styles.item}>
        <Flex style={styles.naviRow} direction="row" justify="around">
          <TouchableHighlight onPress={this._goodsprofile.bind(this)}>
            <View styles={styles.tab_btn}>
              <Text style={[styles.text1, this.state.tab1Style]}>商品详情</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight onPress={this._goodscommit.bind(this)}>
            <View styles={styles.tab_btn}>
              <Text style={[styles.text1, this.state.tab2Style]}>商品口碑</Text>
            </View>
          </TouchableHighlight>
        </Flex>
        {this.onChangeItem(this.state.naviBarStatus)}
      </View>
    );
  }
}


let style = {
  item: {
    backgroundColor: 'white',
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 20,
    borderBottomColor: '#888888',
    borderBottomWidth: 1
  },
  head: {
    height: 60,
    width: 720
  },
  avtor: {
    height: 60,
    width: 60,
    borderRadius: 30,
    marginRight: 10
  },
  naviRow: {
    flexDirection: 'row',
    backgroundColor: 'white',
    height: 92,
  },
  tab_btn: {
    height: 45,
    justifyContent: 'center'
  },
  text1: {
    fontSize: 29,
    padding: 15
  },
  illustrateImg: {
    width: 750,
    height: 822,
  },
  zw: {
    marginTop: 15,
    marginBottom: 20
  }
};

const styles = createStyles(style);
export default Tab;