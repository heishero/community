import React from 'react';
import { View, Text } from 'react-native';
import { Rem } from '@/utils/view';
/**
 * 没有见到合适的组件，随手撸一个
 */

class Rate extends React.Component {
  constructor(props) {
    super(props);
    this.iconColor = [];
    this.state = {
      value: this.props.value,
      maxValue: this.props.maxValue,
    };
    this.reach();
  }

  reach() {
    for (let i = 0; i < this.state.maxValue; i++) {
      if (i <= this.state.value) {
        this.iconColor[i] = '#E3393C';
      } else {
        this.iconColor[i] = '#AAAAAA';
      }
    }
  }
  render() {
    return (
      <View style={{ flexDirection: 'row' }}>
        {
          this.iconColor.map((data, index) => (
            <Text key={index} style={{ color: data, fontSize: Rem(28), }}>★</Text>
          ))
        }
      </View>
    );
  }
}

export default Rate;