import React from 'react';
import { View, Image } from 'react-native';
import { Flex, Toast } from 'antd-mobile-rn';
import { connect } from 'react-redux';
import CText from '../../components/CText';
import { createStyles } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../utils/page';
import addressActions from '../../controllers/adderss/actions';
import PayModal from '../common/payModal';
import { createOrder } from '../../service/security';
import { getWxPay } from '../../service/shop';
import Storage from '../../utils/storage';
import * as WeChat from 'react-native-wechat';

class Order extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      goodsNum: 1,
      wxPayInfo: null,
      creatOrderTag:false //创建订单结算标记
    };
  }

  componentDidMount() {
    // 获取收货地址
    this.props.getAddress();
  }

  componentDidDisappear() {
    this.payModal && this.payModal.dismiss();
  }

  address = () => {
    const { checkAddress } = this.props;
    return (
      <Flex direction="row" justify="center" align="center" style={styles.addressBox} onPress={() => { PageFn.push(this.props.componentId, 'Address', { check: true }) }}>
        <Icon name="dizhi" style={styles.dingweiIcon} />
        <View style={styles.addressCenter}>
          {/* 收货人row */}
          <Flex style={styles.nameBox} justify="space-between" align="center">
            <CText size="30" color="#363636">收货人：{checkAddress.name}</CText>
            <CText size="30" color="#363636">{checkAddress.mobile}</CText>
          </Flex>
          <Flex>
            <CText size="24" color="#363636">收货地址：{checkAddress.province}{checkAddress.city}{checkAddress.district}{checkAddress.address}</CText>
          </Flex>
        </View>
        <Icon name="xiang-you" style={styles.chooseIcon} />
      </Flex>
    );
  }

  noAddress = () => (
    <Flex direction="row" justify="center" align="center" style={styles.addressBox} onPress={() => { PageFn.push(this.props.componentId, 'Address', { check: true }) }}>
      <Icon name="dizhi" style={styles.dingweiIcon} />
      <View style={styles.addressCenter}>
        <CText>请添加收货地址</CText>
      </View>
      <Icon name="xiang-you" style={styles.chooseIcon} />
    </Flex>
  )
  //从订单详情跳转
  goodsCopy = () => {
    const { goodsData } = this.props;
    const { goodsNum } = this.state;

    console.log('goodsDataCopy',goodsData)

    return (
      <Flex style={styles.goodsWrapper} direction="column" justify="flex-start" align="flex-start">
        <Flex style={styles.groupTitle} align="center"><CText size="26" color="#363636">圈子名称：{goodsData.c_name}</CText></Flex>
        <Flex style={styles.goodsBox} align="center">
          <Image source={{ uri: goodsData.small_image }} style={styles.goodsImg} />
          <Flex style={styles.goodsContent} direction="column" justify="space-between" align="flex-start">
            <CText color="#3E3E3E" size="22">{goodsData.name}</CText>
            <Flex direction="row" justify="space-between" align="center">
              <CText size="32" color="#FDAC42">¥{goodsData.now_price}</CText>
              <CText size="24" color="#3E3E3E"> x {goodsData.buy_num}</CText>
            </Flex>
          </Flex>
        </Flex>
        <Flex direction="row" justify="space-between" align="center" style={styles.orderRow}>
          <CText size="26" color="#363636">购买数量</CText>
          <Flex>
            <Icon style={styles.minusIcon}  name ="jianhao" onPress={this.minNum} />
            <CText size="30" color="#3E3E3E">{goodsNum}</CText>
            <Icon style={styles.addIcon} name="tianjia" onPress={this.addNum} />
          </Flex>
        </Flex>
        <Flex direction="row" justify="space-between" align="center" style={styles.orderRow}>
          <CText size="26" color="#363636">配送方式</CText>
          <CText size="26" color="#363636">快递 包邮</CText>
        </Flex>
      </Flex>
    );
  }
  //从商品详情跳转
  goods = () => {
    const { goodsData } = this.props;
    const { goodsNum } = this.state;

    console.log('goodsData',goodsData)

    return (
      <Flex style={styles.goodsWrapper} direction="column" justify="flex-start" align="flex-start">
        <Flex style={styles.groupTitle} align="center"><CText size="26" color="#363636">圈子名称：{goodsData.c_name}</CText></Flex>
        <Flex style={styles.goodsBox} align="center">
          <Image source={{ uri: goodsData.img[0].img_url }} style={styles.goodsImg} />
          <Flex style={styles.goodsContent} direction="column" justify="space-between" align="flex-start">
            <CText color="#3E3E3E" size="22">{goodsData.show_title}</CText>
            <Flex direction="row" justify="space-between" align="center">
              <CText size="32" color="#FDAC42">¥{goodsData.now_price}</CText>
              <CText size="24" color="#3E3E3E"> x {goodsNum}</CText>
            </Flex>
          </Flex>
        </Flex>
        <Flex direction="row" justify="space-between" align="center" style={styles.orderRow}>
          <CText size="26" color="#363636">购买数量</CText>
          <Flex>
            <Icon style={styles.minusIcon}  name ="jianhao" onPress={this.minNum} />
            <CText size="30" color="#3E3E3E">{goodsNum}</CText>
            <Icon style={styles.addIcon} name="tianjia" onPress={this.addNum} />
          </Flex>
        </Flex>
        <Flex direction="row" justify="space-between" align="center" style={styles.orderRow}>
          <CText size="26" color="#363636">配送方式</CText>
          <CText size="26" color="#363636">快递 包邮</CText>
        </Flex>
      </Flex>
    );
  }

  addNum = () => {
    const { goodsNum } = this.state;
    this.setState({
      goodsNum: goodsNum + 1
    });
  }

  minNum = () => {
    const { goodsNum } = this.state;
    if (goodsNum === 1) return;
    this.setState({
      goodsNum: goodsNum - 1
    });
  }

  footer = () => {
    const { goodsNum } = this.state;
    const { goodsData } = this.props;
    return (
      <Flex direction="row" justify="flex-start" style={styles.bottom}>
        <Flex style={styles.bottom_left}>
          <CText>合计：¥{goodsNum * goodsData.now_price}</CText>
        </Flex>
        <Flex style={styles.bottom_right} justify="center" align="center" onPress={this.showBottomView}>
          <CText size="40" color="#fff">结算</CText>
        </Flex>
      </Flex>
    );
  }
    showBottomView =()=>{
      const { checkAddress, goodsData } = this.props;
      if (checkAddress === undefined) {
        Toast.fail('请先选择收货地址');
        return false;
      }
      if (this.payModal) {
        this.payModal.show();
      }
     if(this.state.creatOrderTag === false){
      this.createOrder()
     }   
   }

    //结算订单
  createOrder = async () => {
    const token = await Storage.load('token');
    const { goodsNum } = this.state;
    const { checkAddress, goodsData } = this.props;
    try {
        const { data } = await createOrder({
          crowd_id: goodsData.crowd_id,
          service_charge: 0,
          freight: 0,
          g_total_price: goodsNum * goodsData.now_price,
          pay_total: goodsNum * goodsData.now_price,
          g_id: goodsData.id,
          buy_num: goodsNum,
          a_id: checkAddress.id,
          token
        });
        let wxInfo = await getWxPay({ token, order_id: data });
        this.setState({
          creatOrderTag:true,wxPayInfo:wxInfo.data,
        });
      
    } catch (error) {
      console.log(error);
    }
  }

  pay = async () => {
    const { wxPayInfo } = this.state;
    console.log('wxPayInfo=====>',wxPayInfo)
    if (wxPayInfo) {
      WeChat.isWXAppInstalled()
        .then((isInstalled) => {
          if (isInstalled) {
            WeChat.pay({
              partnerId: wxPayInfo.partnerid,
              prepayId: wxPayInfo.prepayid,
              nonceStr: wxPayInfo.noncestr,
              timeStamp: wxPayInfo.timestamp,
              package: wxPayInfo.package,
              sign: wxPayInfo.sign,
            }).then(result => {
              console.log('result====',result)
              if (result.errCode === 0) {        
                Toast.success('支付成功');
                this.payModal.dismiss();
                PageFn.push(this.props.componentId, 'MineOrder');
              } else {
                Toast.fail(result.errStr);
              }
            }).catch(err => { console.log('err=====',err) });
          } else {
            Toast.fail('请安装微信');
          }
        });
    }
  }

  render() {
    const { goodsNum } = this.state;
    const { checkAddress, goodsData,tag} = this.props;

    console.log('  this.props.tag =====>',tag)
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}>
          订单确定
        </NormalHeader>
        {/* 收货地址模块 */}
        {
          checkAddress ? this.address() : this.noAddress()
        }
        {/* 商品模块 */}
        {
           this.props.tag === '1'?this.goods():this.goodsCopy()
        }
        {/* 底部结算 */}
        {
          this.footer()
        }
        {/* 支付 */}
        <PayModal price={goodsNum * goodsData.now_price} pay={this.pay} title={goodsData.show_title} ref={(c) => { this.payModal = c }} />
      </View>
    );
  }}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#F7F7F7' },
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
  addressBox: {
    height: 171,
    backgroundColor: '#fff',
    marginBottom: 20,
  },
  dingweiIcon: {
    fontSize: 29,
    color: '#A0A0A0',
    position: 'absolute',
    left: 18
  },
  chooseIcon: {
    fontSize: 29,
    color: '#A0A0A0',
    position: 'absolute',
    right: 18
  },
  addressCenter: {
    width: 610,
  },
  nameBox: {
    marginBottom: 20
  },
  goodsWrapper: {
  },
  groupTitle: {
    backgroundColor: '#fff',
    height: 82,
    width: 750,
    paddingHorizontal: 24,
  },
  goodsBox: {
    width: 750,
    height: 196,
    paddingHorizontal: 23,
  },
  goodsImg: {
    width: 202,
    height: 163,
    marginRight: 34,
  },
  goodsContent: {
    flex: 1,
    height: 163,
  },
  orderRow: {
    height: 94,
    borderBottomWidth: 1,
    borderColor: '#F6F6F6',
    backgroundColor: '#fff',
    paddingHorizontal: 22,
    width: 750,
  },
  addIcon: {
    fontSize: 32,
    color: '#FDAC42',
    marginLeft: 24,
  },
  minusIcon: {
    fontSize: 32,
    color: '#FDAC42',
    marginRight: 24,
  },
  bottom: {
    height: 98,
    width: 750,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: '#fff',
  },
  bottom_left: {
    height: 98,
    paddingLeft: 50,
    flex: 1,
  },
  bottom_right: {
    width: 300,
    height: 98,
    backgroundColor: '#FDAC42'
  }
};
styles = createStyles(styles);

export default connect((Store) => {
  return { checkAddress: Store.AddressRenderData.checkAddress };
}, { getAddress: addressActions.GetAddress })(Order);