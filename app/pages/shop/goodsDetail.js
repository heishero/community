import React from 'react';
import { Dimensions, View, ScrollView, Image, Text, FlatList } from 'react-native';
import { Flex, Carousel } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles, Rem, isIphoneX } from '../../utils/view';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../utils/page';
import Storage from '../../utils/storage';
import { goodsDetail } from '../../service/shop';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import Rate from './components/rate';
import GoodsRec from './components/goodsRec';
import Shop from '../shop/shop';
import FullImage from '@/pages/common/fullImage';

const ScreenWidth = Dimensions.get('window').width;

class GoodsDetail extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      goodsDetail: null,
      carouselIndex: 0,
    };
  }

  componentDidMount() {
    this.init();
  }

  init = async () => {
    try {
      const token = await Storage.load('token');
      const { data } = await goodsDetail({ id: this.props.id, token });
      this.setState({
        goodsDetail: { ...data }
      });
    } catch (error) {

    }
  }

  renderComment = ({ item }) => {
    return (
      <View style={styles.commentList}>
        <Flex style={styles.commentTitle}>
          <Image resizeMode="stretch" style={styles.commentAvatar} source={{ uri: item.headimgurl }} />
          <Text style={{ color: '#333333', fontSize: Rem(28), lineHeight: Rem(30) }}>{item.nick_name}</Text>
        </Flex>
        <Rate value={item.star} maxValue="5" />
        <Text style={styles.zw}>{item.content}</Text>
        <Text style={{ color: '#8B8B8B', fontSize: Rem(24), bottom: 10, position: 'absolute', right: 20, }}>{item.created_at}</Text>
        {item.img_url.length > 0 && <Flex style={styles.commentImgBox}>
          {item.img_url.map(data => (
            <Image resizeMode="stretch" key={data} source={{ uri: data }} style={styles.commentImg} />
          ))}
        </Flex>}
        <Flex style={styles.commentBottom} align="center"><CText size="16" color="#8B8B8B">{item.goods_name}  {item.create_time}</CText></Flex>
      </View>
    );
  }


  onHorizontalSelectedIndexChange = (index) => {
    this.setState({ carouselIndex: index });
  }

  getIllustrateImg = async (data) => {
    let imgWidth;
    let imgHeight;
    await Image.getSize(data, (width, height) => {
      imgWidth = width;
      imgHeight = height;
    });
    return <Image resizeMode="stretch" key={data} style={{ width: ScreenWidth, height: ScreenWidth * imgWidth / imgHeight }} source={{ uri: data }} />;
  }

  render() {
    const { goodsDetail, carouselIndex } = this.state;
    return (
      goodsDetail ? (
        <View style={styles.wrapper}>
          <Flex onPress={() => { PageFn.pop(this.props.componentId) }} style={styles.back}>
            <Icon style={styles.headerIcon} name="fanhui" />
          </Flex>
          <ScrollView style={styles.scrollWrapper}>
            <Carousel
              style={styles.carouselWrapper}
              selectedIndex={carouselIndex}
              infinite={true}
              afterChange={this.onHorizontalSelectedIndexChange}
            >
              {
                goodsDetail.img.map((data, index) => (
                  <Image resizeMode="stretch" style={styles.carouselImg} key={index} source={{ uri: data.img_url }} />
                ))
              }
            </Carousel>
            {/* 商品信息 */}
            <Flex style={styles.goodsWrapper} direction="column" justify="flex-start" align="flex-start">
              <Flex style={styles.goodsTitle}><CText size="35" color="#393939">{goodsDetail.name}</CText></Flex>
              <Flex style={styles.priceBox} justify="flex-start" align="center">
                <View style={styles.nowPrice}><CText size="48" color="#FF7200">¥{goodsDetail.now_price}</CText></View>
                <Text style={{ fontSize: Rem(25), color: '#AAAAAA', textDecorationLine: 'line-through' }}>原价：¥{goodsDetail.now_price}</Text>
              </Flex>
            </Flex>
            <View style={{ height: 1, backgroundColor: '#EFEFEF' }}></View>
            <Flex
              style={{ height: 30, backgroundColor: 'white' }}
              direction="row">
              <Icon style={{ marginLeft: 20 }} name="zhengping" />
              <View style={{ marginLeft: Rem(10) }}>
                <CText size="24" color="#AAAAAA">100%正品</CText>
              </View>
              <Icon style={{ marginLeft: 30 }} name="baoyou" />
              <View style={{ marginLeft: Rem(10) }}>
                <CText style={{ marginRight: 100 }} size="24" color="#AAAAAA">包邮</CText>
              </View>
            </Flex>
            <View style={{ height: 10, backgroundColor: '#EFEFEF' }}>
            </View>
            <ScrollableTabView
              tabBarUnderlineStyle={{ backgroundColor: '#fff', height: 2, width: (ScreenWidth - 150) / 3, marginLeft: 25 }}
              tabBarActiveTextColor="#2AB6F3"
              tabBarInactiveTextColor="#8C8C8C"
              tabBarBackgroundColor="#FFFFFF"
              tabBarTextStyle={{ fontSize: Rem(25), marginTop: Rem(15) }}
              initialPage={0}
              onChangeTab={(item) => {

              }}>
              <View tabLabel="商品详情">
                {
                  goodsDetail.illustrate.map((data, index) => (
                    <FullImage key={data} source={{ uri: data }} />
                  ))
                }
              </View>
              <View tabLabel="商品评论">
                <FlatList
                  data={this.state.goodsDetail.evalue_content}
                  keyExtractor={item => item.id}
                  renderItem={this.renderComment}
                  ItemSeparatorComponent={() => <View style={{ height: Rem(1), backgroundColor: '#E2E2E2' }}></View>}
                  ListEmptyComponent={<Flex justify="center"><Text style={{ fontSize: 30, color: '#AAAAAA', marginTop: 100 }}>暂无评论</Text></Flex>}
                />
              </View>
              <GoodsRec {...this.props} tabLabel="商品推荐" />
            </ScrollableTabView>
          </ScrollView>
          <Flex style={styles.bottom} onPress={() => { PageFn.push(this.props.componentId, 'Order', { goodsData: goodsDetail, tag: '1' }) }}>
            <Flex style={styles.buyBtn} justify="center" align="center"><CText size="36" color="#fff">立即购买</CText></Flex>
          </Flex>
        </View>
      ) : null
    );
  }}
let styles = {
  wrapper: {
    flex: 1,
    backgroundColor: '#F7F7F7',
    ios: {
      marginTop: isIphoneX ? -88 : -64
    }
  },
  scrollWrapper: {
    flex: 1,
    marginBottom: 100
  },
  back: {
    position: 'absolute',
    top: 33,
    left: 20,
    zIndex: 999,
  },
  headerIcon: {
    fontSize: 36,
    fontWeight: 'bold',
    color: '#fff',
    backgroundColor: '#AAAAAA',
    borderRadius: 35,
    ios: {
      marginTop: isIphoneX ? 120 : 100,
      marginLeft: 30,
    },
    padding: 17,
  },
  carouselImg: {
    height: 700,
    flex: 1,
    width: 750,
  },
  goodsWrapper: {
    height: 200,
    paddingTop: 14,
    paddingHorizontal: 20,
    backgroundColor: '#fff'
  },
  goodsTitle: {
    marginBottom: 10,
  },
  nowPrice: {
    marginRight: 44,
  },

  rowTitle: {
    height: 80,
    paddingHorizontal: 21,
  },
  bottom: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
  },
  buyPrice: {
    flex: 1,
    paddingLeft: 50,
    height: 96,
    backgroundColor: '#F9F7F7'
  },
  buyBtn: {
    flex: 1,
    height: 100,
    backgroundColor: '#FF0000'
  },
  item: {
    backgroundColor: 'white',
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 20,
    width: ScreenWidth,
    borderBottomColor: '#F2F2F2',
    borderBottomWidth: 1
  },
  goodsSection: {
    marginTop: 10,
    backgroundColor: '#fff'
  },
  commentList: {
    backgroundColor: '#fff',
    paddingHorizontal: 24,
  },
  commentTitle: {
    borderBottomWidth: 1,
    borderBottomColor: '#F2F2F2',
    height: 74,
  },
  commentAvatar: {
    height: 58,
    width: 58,
    marginRight: 15,
  },
  commentImgBox: {
    flexWrap: 'wrap',
  },
  commentImg: {
    width: 102,
    height: 102,
    marginRight: 10,
  },
  zw: {
    fontSize: 26,
    lineHeight: 28,
    marginTop: 10,
  },
  commentBottom: {
    height: 39,
  }
};
styles = createStyles(styles);

export default GoodsDetail;