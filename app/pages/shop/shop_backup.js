import React from 'react';
import { View, Text, Image } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../utils/page';
import { goodsMenu, goodsList } from '../../service/shop';
import { Navigation } from 'react-native-navigation';
import Storage from '../../utils/storage';

class Shop extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
    this.state = {
      classify: [],
      classifyActive: 0,
      list: [],
    };
  }

  componentDidMount() {
    this.init();
  }

  init = async () => {
    try {
      const token = await Storage.load('token');
      const menu = await goodsMenu({ crowd_id: this.props.id, token });
      const list = await goodsList({ cate_id: menu.data[0].id, token });
      this.setState({ classify: menu.data, list: list.data });
    } catch (error) {

    }
  }

  classify = () => (
    <Flex direction="column" style={styles.list}>
      {
        this.state.classify.map((data, index) => this.classifyItem(data, index))
      }
    </Flex>
  )

  classifyItem = (data, index) => (
    <Flex key={data.id} onPress={() => this.checkClassify(index)} style={[styles.listItem, index === this.state.classifyActive && { backgroundColor: '#fff' }]} direction="column" justify="center" align="center">
      <Text style={[styles.classifyName, index === this.state.classifyActive && { color: '#FFBB12' }]} numberOfLines={1}>{data.name}</Text>
    </Flex>
  )

  checkClassify = async (index) => {
    const { classify } = this.state;
    console.log(classify[index].id);
    try {
      const token = await Storage.load('token');
      const list = await goodsList({ cate_id: classify[index].id, token });
      this.setState({ classifyActive: index, list: list.data });
    } catch (error) {
      console.log(error);
    }
  }

  goodsList = () => {
    const { list } = this.state;
    return (
      <Flex direction="row" wrap="wrap" style={styles.goodsList}>
        {
          list.map((data) => (
            <Flex onPress={() => { PageFn.push(this.props.componentId, 'GoodsDetail', { id: data.id }) }} key={data.id} style={styles.goodsItem} direction="column" align="center">
              <Image style={styles.goodsImg} source={{ uri: data.small_image }} />
              <View style={styles.goodsName}><CText size="24" color="#3e3e3e">{data.name}</CText></View>
              <CText size="24" color="#F97273">¥{data.out_price}元</CText>
            </Flex>
          ))
        }
      </Flex>
    );
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}>
          商城
        </NormalHeader>
        <Flex style={styles.container} justify="flex-start" align="flex-start">
          {this.classify()}
          {this.goodsList()}
        </Flex>
      </View>
    );
  }}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#fff' },
  headerIcon: {
    fontSize: 32,
    color: '#fff',
  },
  container: {
    flex: 1,
  },
  list: {
    width: 150,
    height: 1000,
    backgroundColor: '#F7F7F7'
  },
  listItem: {
    width: 150,
    height: 100,
    flexWrap: 'nowrap'
  },
  classifyName: {
    color: '#333',
    fontSize: 28,
  },
  goodsItem: {
    width: 266,
    height: 335,
    backgroundColor: '#F5F5F5',
    borderRadius: 16,
    marginLeft: 26,
    marginTop: 27,
  },
  goodsImg: {
    height: 250,
    width: 266,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
  },
  goodsName: {
    marginTop: 5,
  }
};
styles = createStyles(styles);

export default Shop;