/*
 * @Author: yixin
 * @Date: 2019-04-23 22:52:46
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-23 23:00:19
 */

import React from 'react';
import { View, ScrollView } from 'react-native';
import CText from '@/components/CText';
import { createStyles } from '@/utils/view';
import NormalHeader from '@/components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '@/utils/page';

class SecurityAgreement extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}>
          隐私协议
        </NormalHeader>
        <ScrollView>

        </ScrollView>
      </View>
    );
  }}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#F7F7F7' },
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
};
styles = createStyles(styles);

export default SecurityAgreement;