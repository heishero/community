import React from 'react';
import { Dimensions, Image } from 'react-native';

export default class FullImage extends React.PureComponent {
  constructor() {
    super();

    this.state = {
      width: 0,
      height: 0
    };
  }

  componentDidMount() {
    const ScreenWidth = Dimensions.get('window').width;
    Image.getSize(this.props.source.uri, (imgWidth, imgHeight) => {
      this.setState({
        width: ScreenWidth, height: ScreenWidth * imgHeight / imgWidth
      });
    }, error => {
      console.log('error:', error);
    });
  }

  render() {
    return (
      <Image
        key={this.props.key}
        style={{ width: this.state.width, height: this.state.height }}
        source={this.props.source}
        resizeMode="cover"
      />
    );
  }
}