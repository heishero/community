import React from 'react';
import {
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import { Flex } from 'antd-mobile-rn';
import { createStyles } from '../../utils/view';
import Icon from 'react-native-vector-icons/icomoon';
import { Navigation } from 'react-native-navigation';
import PageFn from '@/utils/page';

const goToPage = (index) => {
  Navigation.mergeOptions('BottomTabsId', {
    bottomTabs: {
      currentTabIndex: index
    }
  });
};

const host = (index) => {
  let componentID = '';
  switch (parseInt(index, 10)) {
    case 0:
      componentID = 'Message';
      break;
    case 1:
      componentID = 'Find';
      break;
    case 2:
      componentID = 'Circle';
      break;
    case 3:
      componentID = 'Mine';
      break;
    default:
      break;
  }
  PageFn.push(componentID, 'ReleaseDynamic');
};

const Bottom = ({ index }) => {
  return (
    <View style={styles.wrapper}>
      <Flex onPress={() => goToPage(0)} direction="column" justify="flex-end" alignItems="center" style={styles.item}>
        <Icon name={parseInt(index, 10) === 0 ? 'xiao-xi-1' : 'xingxi'} style={[styles.icon, parseInt(index, 10) === 0 && { color: '#2AB6F3' }]} />
        <Text style={[styles.text, parseInt(index, 10) === 0 && { color: '#2AB6F3' }]}>消息</Text>
      </Flex>
      <Flex onPress={() => goToPage(1)} direction="column" justify="flex-end" alignItems="center" style={styles.item}>
        <Icon name={parseInt(index, 10) === 1 ? 'fa-xian' : 'faxian-wei'} style={[styles.icon, parseInt(index, 10) === 1 && { color: '#2AB6F3' }]} />
        <Text style={[styles.text, parseInt(index, 10) === 1 && { color: '#2AB6F3' }]}>发现</Text>
      </Flex>
      <Flex direction="column" justify="flex-end" alignItems="center" style={styles.item}>
        <TouchableOpacity onPress={() => host(index)} style={styles.middleIconBox} ><Icon style={styles.middleIcon} name="fa-bu" /></TouchableOpacity>
        <Text style={styles.text}>发布</Text>
      </Flex>
      <Flex onPress={() => goToPage(2)} direction="column" justify="flex-end" alignItems="center" style={styles.item}>
        <Icon name={parseInt(index, 10) === 2 ? 'quanzi' : 'quan-zi-dibu'} style={[styles.icon, parseInt(index, 10) === 2 && { color: '#2AB6F3' }]} />
        <Text style={[styles.text, parseInt(index, 10) === 2 && { color: '#2AB6F3' }]}>圈子</Text>
      </Flex>
      <Flex onPress={() => goToPage(3)}direction="column" justify="flex-end" alignItems="center" style={styles.item}>
        <Icon name={parseInt(index, 10) === 3 ? 'wo-de-dibu' : 'wo-de'} style={[styles.icon, parseInt(index, 10) === 3 && { color: '#2AB6F3' }]} />
        <Text style={[styles.text, parseInt(index, 10) === 3 && { color: '#2AB6F3' }]}>我的</Text>
      </Flex>
    </View>
  );
};

let styles = {
  wrapper: {
    height: 100,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    backgroundColor: '#fff',
  },
  item: {
    flex: 1,
    paddingBottom: 4,
  },
  icon: {
    fontSize: 43,
    lineHeight: 45,
    color: '#737373',
    marginBottom: 10,
  },
  text: {
    fontSize: 20,
    lineHeight: 22,
    color: '#737373'
  },
  middleIconBox: {
    position: 'absolute',
    bottom: 48,
    zIndex: 999
  },
  middleIcon: {
    fontSize: 78,
    color: '#2AB6F3',
  }
};
styles = createStyles(styles);
export default Bottom;