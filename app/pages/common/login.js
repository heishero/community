/*
 * @Author: yixin
 * @Date: 2018-10-28 21:39:26
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-23 23:06:42
 * 登录
 */
import React from 'react';
import { View, TouchableOpacity, TextInput, Keyboard, Alert, Platform } from 'react-native';
import { Flex, Toast } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles, Rem } from '../../utils/view';
import Icon from 'react-native-vector-icons/icomoon';
import NormalHeader from '../../components/NormalHeader';
import toolsVerifi from '../../utils/verifi';
import { connect } from 'react-redux';
import userActions from '../../controllers/user/actions';
import messageActions from '../../controllers/message/actions';
import Storage from '../../utils/storage';
import JPushModule from 'jpush-react-native';
import PageFn from '../../utils/page';
import JMessage from 'jmessage-react-plugin';
import * as WeChat from 'react-native-wechat';
import { wxLogin } from '../../service/wx';
import { login, getCode } from '../../service/user';
import { goToMain } from '@/utils/navigate';

class Login extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      },
      statusBar: {
        backgroundColor: 'rgba(0,0,0,0)',
        style: 'dark',
        drawBehind: true,
      },
    };
  }

  constructor(props) {
    super(props);
    this.startTime = 0;
    this.state = {
      mobile: '',
      validate_code: '',
      codeText: '发送验证码'
    };
  }

  // 登录成功操作
  loginSuccess = (data) => {
    this.props.setUserInfo({
      userInfo: { ...data }
    });
    Toast.success('登录成功');
    // 登录jmessage
    JMessage.login({
      username: data.jg_im_username,
      password: data.jg_im_userpassword
    }, () => { console.log('JMessage 登录成功'); this.props.getMessage() }, (error) => { console.log('JMessage登录失败', error) });
    // 绑定jpush alias
    JPushModule.setAlias(`${data.id}`, () => {
      console.log('Set alias succeed');
    }, () => {
      console.log('Set alias failed');
    });

    Storage.save('token', data.auth_token).then(() => {
      if (!data.nick_name) {
        PageFn.push(this.props.componentId, 'PerfectId');
        return false;
      }
      if (!data.mobile) {
        PageFn.push(this.props.componentId, 'BindPhone');
        return false;
      }
      goToMain();
    });
  }

  toLogin = () => {
    let { mobile, validate_code } = this.state;
    if (!toolsVerifi.isPhone(mobile)) {
      return Toast.fail('请输入正确的手机号码');
    }
    login({ mobile, validate_code }).then(data => {
      if (data.status === 'success') {
        this.loginSuccess(data.data);
      }
    });
  }

  /**
   * 发送获取验证码
   */
  onSendCode = () => {
    // 60秒后重新获取
    let { mobile } = this.state;
    mobile = mobile.replace(/ /g, '');
    if (!toolsVerifi.isPhone(mobile)) {
      return Toast.fail('请输入正确的手机号码');
    }

    if (this.startTime === 0) {
      this.startTime = 60;
      this.timeInter = setInterval(() => {
        if (this.startTime > 0) {
          this.setState({ codeText: `${this.startTime}秒` });
          this.startTime--;
        } else {
          this.setState({ codeText: `发送验证码` });
          clearInterval(this.timeInter);
        }
      }, 1000);
      // 发送登录验证码
      getCode(mobile);
    } }

  wx = () => {
    WeChat.isWXAppInstalled()
      .then((isInstalled) => {
        if (isInstalled) {
          // 发送授权请求
          WeChat.sendAuthRequest('snsapi_userinfo', 'chaihe')
            .then(responseCode => {
              // 返回code码，通过code获取access_token
              wxLogin(responseCode.code).then(data => {
                if (data.status === 'success') {
                  this.loginSuccess(data.data);
                }
              }).catch(err => { console.log(err) });
            }).catch(err => {
              Alert.alert('登录授权发生错误：', err.message, [{
                text: '确定'
              }]);
            });
        } else {
          Platform.OS === 'ios' ?
            Alert.alert('没有安装微信', '是否安装微信？', [
              { text: '取消' },
              { text: '确定', onPress: () => this.installWechat() }
            ]) :
            Alert.alert('没有安装微信', '请先安装微信客户端在进行登录', [
              { text: '确定' }
            ]);
        }
      });
  }


  render() {
    const { mobile, validate_code, codeText } = this.state;
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          backgroundColor="#fff"
        ><CText size="38" color="#333">登录注册</CText></NormalHeader>
        <Flex onPress={() => { Keyboard.dismiss() }} style={{ marginTop: Rem(160) }} direction="column" justify="flex-start" align="center">
          <TextInput keyboardType="number-pad" onChangeText={(mobile) => { this.setState({ mobile }) }} clear value={mobile} placeholder="请输入手机号码" TextColor="#333" placeholderTextColor="#333" style={[styles.input, { marginBottom: Rem(46) }]} />
          <Flex>
            <TextInput keyboardType="numeric" style={[styles.input, { marginBottom: Rem(10), width: Rem(385) }]} onChangeText={(validate_code) => { this.setState({ validate_code }) }} clear value={validate_code} placeholder="请输入验证码" placeholderTextColor="#333" />
            <TouchableOpacity onPress={this.onSendCode} style={styles.smsBtn}>
              <CText color="#2AB6F3" size="30">{codeText}</CText>
            </TouchableOpacity>
          </Flex>
          <View style={styles.agreement}>
            <CText size="24" color="#A4A4A4">登录即同意</CText>
            <TouchableOpacity onPress={() => { PageFn.push(this.props.componentId, 'PlatformAgreement') }} style={{ borderBottomWidth: 1, borderColor: '#2AB6F3' }}><CText color="#2AB6F3" size="24">用户协议</CText></TouchableOpacity>
            <CText size="24" color="#A4A4A4">和</CText>
            <TouchableOpacity onPress={() => { PageFn.push(this.props.componentId, 'SecurityAgreement') }} style={{ borderBottomWidth: 1, borderColor: '#2AB6F3' }}><CText color="#2AB6F3" size="24">隐私协议</CText></TouchableOpacity>
          </View>
          <Flex onPress={() => { this.toLogin() }} style={styles.loginBtn} justify="center" align="center">
            <CText size="36" color="#fff">登 录</CText>
          </Flex>
          <Flex style={styles.forgetPass} onPress={() => { PageFn.push(this.props.componentId, 'LoginPwd') }} justify="flex-end"><CText color="#444444" size="28">密码登录</CText></Flex>
          {/* 第三方平台登录 */}
          <Flex style={styles.otherPlatform}>
            <View style={styles.solid}></View>
            <CText size="26" color="#333">第三方平台登录</CText>
            <View style={styles.solid}></View>
          </Flex>
          <TouchableOpacity style={styles.wx} onPress={this.wx}>
            <Icon name="weixing" style={styles.wxIcon} />
          </TouchableOpacity>
        </Flex>
      </View>
    );
  }

}

let styles = {
  wrapper: {
    flex: 1,
    backgroundColor: '#fff',
  },
  back: {
    fontSize: 32,
    color: '#333',
  },
  input: {
    width: 604,
    height: 90,
    borderBottomWidth: 1,
    borderColor: '#E6E6E6',
    paddingHorizontal: 9,
    fontSize: 26,
    color: '#333',
    paddingVertical: 0,
  },
  smsBtn: {
    borderWidth: 1,
    borderColor: '#2AB6F3',
    width: 210,
    height: 90,
    borderRadius: 45,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  agreement: {
    width: 604,
    paddingHorizontal: 9,
    display: 'flex',
    flexDirection: 'row'
  },
  loginBtn: {
    width: 604,
    height: 94,
    backgroundColor: '#2ab6f3',
    borderRadius: 48,
    marginTop: 80,
    marginBottom: 24,
  },
  forgetPass: {
    width: 604,
    paddingRight: 30
  },
  solid: {
    width: 100,
    height: 1,
    backgroundColor: '#444444',
    marginHorizontal: 32,
  },
  otherPlatform: {
    marginTop: 300,
  },
  wx: {
    height: 88,
    width: 88,
    borderRadius: 44,
    backgroundColor: '#fff',
    marginTop: 34,
    justifyContent: 'center',
    alignItems: 'center',
  },
  wxIcon: {
    fontSize: 60,
    color: '#44c6ff',
  }
};
styles = createStyles(styles);

export default connect((Store) => {
  return { };
}, { setUserInfo: userActions.RenderData, getMessage: messageActions.GetMessage })(Login);
