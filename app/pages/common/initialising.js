import React from 'react';
import { View } from 'react-native';
import { createStyles } from '@/utils/view';
import Storage from '@/utils/storage';
import { goToAuth, goToMain } from '@/utils/navigate';
import { getInfo } from '../../service/user';
import SplashScreen from 'react-native-splash-screen';

export default class Initialising extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      },
      topBar: {
        drawBehind: true,
        visible: false
      }
    };
  }

  async componentDidMount() {
    try {
      const token = await Storage.load('token');
      if (token) {
        const userinfo = await getInfo(token);
        if (userinfo.data.nick_name && userinfo.data.mobile) {
          SplashScreen.hide();
          goToMain();
        } else {
          SplashScreen.hide();
          goToAuth();
        }
      } else {
        SplashScreen.hide();
        goToAuth();
      }
    } catch (error) {
      SplashScreen.hide();
      goToAuth();
    }
  }

  render() {
    return (
      <View style={styles.wrapper}>
      </View>
    );
  }}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#F7F7F7' },
};
styles = createStyles(styles);