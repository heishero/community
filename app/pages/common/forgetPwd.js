/*
 * @Author: yixin
 * @Date: 2018-10-28 21:39:26
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-22 23:22:43
 * 忘记密码
 */
import React from 'react';
import { View, TouchableOpacity, TextInput, Keyboard, Text } from 'react-native';
import { Flex, Toast } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles, Rem } from '../../utils/view';
import Icon from 'react-native-vector-icons/icomoon';
import NormalHeader from '../../components/NormalHeader';
import toolsVerifi from '../../utils/verifi';
import { Navigation } from 'react-native-navigation';
import { getCode, forgetPassWord } from '../../service/user';
import MineInput from '../mine/components/mineInput';


class ForgetPwd extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.startTime = 0;
    this.state = {
      mobile: '',
      validate_code: '',
      codeText: '发送验证码',
      password: '',
      twicePassword: '',
    };
  }

  handleChange = (name, value) => {
    this.setState({
      [name]: value
    });
  }

submit=(mobile, validate_code, password) => {

  if (!toolsVerifi.isPhone(mobile)) {
    return Toast.fail('请输入正确的手机号码');
  }
  if (validate_code.length === 0) {
    return Toast.fail('请输入验证码');
  }
  if (this.state.twicePassword !== password) {
    return Toast.fail('两次密码请输入一致');
  }

  forgetPassWord({ mobile, validate_code, password }).then(data => {
    if (data.status === 'success') {
      Toast.success('修改成功');
      Navigation.pop(this.props.componentId);
    }
  });

}
  /**
   * 发送获取验证码
   */
  onSendCode = () => {
    // 60秒后重新获取
    let { mobile } = this.state;
    mobile = mobile.replace(/ /g, '');
    if (!toolsVerifi.isPhone(mobile)) {
      return Toast.fail('请输入正确的手机号码');
    }

    if (this.startTime === 0) {
      this.startTime = 60;
      this.timeInter = setInterval(() => {
        if (this.startTime > 0) {
          this.setState({ codeText: `${this.startTime}秒` });
          this.startTime--;
        } else {
          this.setState({ codeText: `发送验证码` });
          clearInterval(this.timeInter);
        }
      }, 1000);
      // 发送登录验证码
      getCode(mobile);
    } }


  render() {
    const { mobile, validate_code, codeText, password, twicePassword } = this.state;
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          backgroundColor="#fff"
          leftContent={<Icon style={styles.back} name="fanhui" />}
          onLeftPress={() => { Navigation.pop(this.props.componentId) }}
        ><CText size="38" color="#333">忘记密码</CText></NormalHeader>
        <Flex onPress={() => { Keyboard.dismiss() }} style={{ marginTop: Rem(100) }} direction="column" justify="flex-start" align="center">
          <MineInput name="mobile" keyboardType="number-pad" onChangeText={this.handleChange} clear value={mobile} placeholder="请输入手机号码" TextColor="#333" placeholderTextColor="#333" />
          <Flex>
            <TextInput style={[styles.input, { marginBottom: Rem(10), width: Rem(385) }]} secureTextEntry onChangeText={(validate_code) => { this.setState({ validate_code }) }} clear value={validate_code} placeholder="请输入验证码" placeholderTextColor="#333" />
            <TouchableOpacity onPress={this.onSendCode} style={styles.smsBtn}>
              <CText color="#2AB6F3" size="30">{codeText}</CText>
            </TouchableOpacity>
          </Flex>
          <View style={styles.inputRow}><MineInput keyboardType="default" name="password" secureTextEntry onChangeText={this.handleChange} value={password} placeholderTextColor="#333333" placeholder="请设置6～16位登录密码" /></View>
          <MineInput keyboardType="default" name="twicePassword" onSubmitEditing={this.submit} secureTextEntry onChangeText={this.handleChange} value={twicePassword} placeholderTextColor="#333333" placeholder="请重复输入6～16位登录密码" />
          {(password !== twicePassword) && twicePassword ? <Text style={styles.noMatchPWD}>*两次密码输入不一致</Text> : null}
          <Flex onPress={() => { this.submit(this.state.mobile, this.state.validate_code, this.state.password) }} style={styles.loginBtn} justify="center" align="center">
            <CText size="36" color="#fff">确定</CText>
          </Flex>
        </Flex>
      </View>
    );
  }

}

let styles = {
  wrapper: {
    flex: 1,
    backgroundColor: '#fff',
  },
  back: {
    fontSize: 32,
    color: '#333',
  },
  input: {
    width: 604,
    height: 90,
    borderBottomWidth: 1,
    borderColor: '#E6E6E6',
    paddingHorizontal: 9,
    fontSize: 26,
    color: '#333',
    paddingVertical: 0,
  },
  smsBtn: {
    borderWidth: 1,
    borderColor: '#2AB6F3',
    width: 210,
    height: 90,
    borderRadius: 45,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginBtn: {
    width: 604,
    height: 94,
    backgroundColor: '#2ab6f3',
    borderRadius: 48,
    marginTop: 130,
    marginBottom: 24,
  },
  solid: {
    width: 100,
    height: 1,
    backgroundColor: '#444444',
    marginHorizontal: 32,
  },
  noMatchPWD: {
    color: '#FF0000',
    fontSize: 24,
    width: 606,
    marginTop: 10,
  }
};
styles = createStyles(styles);

export default ForgetPwd;
