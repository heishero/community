import React from 'react';
import { View, ScrollView, TextInput,FlatList } from 'react-native';
import { Flex, Toast } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles, Rem } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import { getRegeo, poiSearch } from '../../service/map';
import PageFn from '../../utils/page';
import { Geolocation } from 'react-native-amap-geolocation';

class Map extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      regeocode: {},
      searchText: '',
      searchList: [],
      lat: null,
      lon: null,
    };
  }

  componentDidMount() {
    this.init();
    // navigator.geolocation.getCurrentPosition(
    //   (position) => {
    //     const positionData = position.coords;
    //     // 经度：positionData.longitude
    //     // 纬度：positionData.latitude

    //   // 最后一步 todo：高德 || 百度地图逆地理编码转~~具体就是调个接口把经纬度丢进去就行了
    //   convert({ locations: `${positionData.longitude},${positionData.latitude}`, coordsys: 'gps' }).then(location => {
    //     getRegeo({ location: location.locations, extensions: 'all' }).then(result => {
    //       this.setState({
    //         regeocode: result.regeocode,
    //         searchList: result.regeocode.pois,
    //       });
    //     });
    //   });
    // },
    // (error: any) => {
    //   console.warn('失败：' + JSON.stringify(error.message));
    // }, {
    //   // 提高精确度，但是获取的速度会慢一点
    //   enableHighAccuracy: false,
    //   // 设置获取超时的时间20秒
    //   timeout: 5000,
    //   // 示应用程序的缓存时间，每次请求都是立即去获取一个全新的对象内容
    //   maximumAge: 1000
    // });
  }

  componentWillUnmount() {
    Geolocation.stop();
  }

  async init() {
    Toast.loading('正在加载...',10)
    Geolocation.setOptions({
      interval: 1000000,
      distanceFilter: 10,
      reGeocode: true
    });
    Geolocation.start();
    Geolocation.addLocationListener(location =>
    {
      getRegeo({ location: `${location.longitude},${location.latitude}`, extensions: 'all' }).then(result => {
        Toast.hide()
        this.setState({
          lat: location.latitude,
          lon: location.longitude,
          regeocode: result.regeocode,
          searchList: result.regeocode.pois,
          searchText: location.address
        });
      }); }
    );
  }

search = (text) => {
  this.setState({ searchText: text });
  this.timer && clearTimeout(this.timer);
  Toast.loading('正在加载...',10)
  if (this.state.regeocode.addressComponent) {
    this.timer = setTimeout(() => {
      poiSearch({ keywords: text, city: this.state.regeocode.addressComponent.adcode, citylimit: true }).then(result => {
        Toast.hide()
        this.setState({
          searchList: result.pois,
          address: text,
          lat: result.pois[0].location.split(',')[0],
          lon: result.pois[0].location.split(',')[1]
        });
      });
    }, 500);
  }

}

render() {
  const { regeocode, searchText, searchList, lat, lon } = this.state;
  return (
    <View style={styles.wrapper}>
      <NormalHeader
        leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
        onLeftPress={() => { PageFn.pop(this.props.componentId) }}
        rightContent={<CText color="#000">确定</CText>}
        onRightPress={() => { this.props.setAddress({
          address: searchText,
          lat: lat,
          lon: lon
        });
        PageFn.pop(this.props.componentId); }}
      >
        地理位置
      </NormalHeader>
      <Flex direction="row" style={styles.topBox}>
        <Flex justify="center" align="center" style={styles.city}>
          <CText>{regeocode.addressComponent && regeocode.addressComponent.city}</CText>
        </Flex>
        <TextInput clearButtonMode="while-editing" onChangeText={(text) => { this.search(text) }} value={searchText} style={styles.input} placeholder="输入地址" />
      </Flex>
      <ScrollView>
        <View style={styles.list}>
          {
            (searchList.length > 0) ? (searchList.map((data) => (
              <Flex onPress={() => { this.search(data.name) }} key={data.id} direction="column" justify="center" align="flex-start" style={styles.item}>
                <View style={{ marginBottom: Rem(12) }}><CText>{data.name}</CText></View>
                <CText size="26" color="#AAAAAA">{data.address}</CText>
              </Flex>
            ))) : (
              <FlatList
                refreshing={true}
                style={{ flex: 1 }}
              >
              </FlatList>
            )
          }
        </View>
      </ScrollView>
    </View>
  );
}}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#fff' },
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
  headerIconRight: {
    fontSize: 32,
  },
  topBox: {
    height: 100,
    borderBottomWidth: 1,
    borderColor: '#AEAEAE'
  },
  city: {
    width: 200,
    height: 100,
  },
  input: {
    width: 500,
    height: 80,
    borderWidth: 1,
    borderColor: '#f0f0f0',
    paddingLeft: 26,
    paddingVertical: 0,
  },
  list: {
    paddingHorizontal: 26
  },
  item: {
    height: 160,
    borderBottomWidth: 1,
    borderColor: '#AEAEAE',
    flex: 1,
  }
};
styles = createStyles(styles);

export default Map;