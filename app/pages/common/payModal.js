/*
 * @Author: yixin
 * @Date: 2019-01-13 16:33:12
 * @Last Modified by: yixin
 * @Last Modified time: 2019-02-22 17:05:11
 * 支付通用组件
 */
import React from 'react';
import { Modal, View } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../components/CText';
import Icon from 'react-native-vector-icons/icomoon';
import { createStyles } from '../../utils/view';

export default class PayModal extends React.Component {
  state = {
    visible: false
  }

  show() {
    this.setState({
      visible: true
    });
  }

  dismiss() {
    this.setState({
      visible: false
    });
  }

  render() {
    const { title, price, pay } = this.props;
    return (<Modal
      transparent={true}
      visible={this.state.visible}
      onRequestClose={() => this.dismiss()}
    >
      <View style={styles.wrapper}>
        <View style={styles.payBox}>
          <Flex style={styles.title} justify="center" align="center">
            <Icon onPress={() => this.dismiss()} name="guanbi" style={styles.closeIcon} />
            <CText size="30" color="#2E2E2E">确认付款</CText>
          </Flex>
          <Flex justify="center" align="center" style={styles.price}>
            <CText size="65" color="#2E2E2E">¥ {price}元</CText>
          </Flex>
          <Flex style={styles.row} justify="space-between">
            <CText color="#969696" size="30">订单信息：</CText>
            <CText size="30" color="#2E2E2E">{title}</CText>
          </Flex>
          <Flex style={styles.row} justify="space-between">
            <CText color="#969696" size="30">付款方式：</CText>
            <CText size="30" color="#2E2E2E">微信</CText>
          </Flex>
        </View>
        <Flex style={styles.pay} justify="center" align="center" onPress={pay}>
          <CText size="36" color="#fff">立即付款</CText>
        </Flex>
      </View>
    </Modal>);
  }
}

let styles = {
  wrapper: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.47)',
  },
  payBox: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    height: 697,
    backgroundColor: '#fff',
  },
  title: {
    height: 90,
    borderBottomWidth: 1,
    borderColor: '#CBCBCB'
  },
  closeIcon: {
    fontSize: 52,
    color: '#969696',
    position: 'absolute',
    left: 30,
  },
  price: {
    marginVertical: 50,
  },
  row: {
    height: 90,
    paddingHorizontal: 30,
    borderBottomWidth: 1,
    borderColor: '#CBCBCB'
  },
  pay: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: '#2ab6f3',
    height: 90,
  }
};
styles = createStyles(styles);
