/*
 * @Author: yixin
 * @Date: 2018-11-12 09:56:59
 * @Last Modified by: yixin
 * @Last Modified time: 2019-01-29 11:56:14
 *
 */
import React from 'react';
import { View, ScrollView, TextInput, Image, TouchableOpacity, Text } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles, Rem } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../utils/page';

class Draw extends React.Component {

static navigatorStyle = {
  navBarHidden: true,
  navBarNoBorder: true,
}

render() {
  return (
    <ScrollView style={styles.wrapper}>
      <NormalHeader
        leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
        onLeftPress={() => { PageFn.pop(this.props.componentId) }}>
        提现
      </NormalHeader>
      <View style={styles.totolMoney}>
        <View style={{ marginBottom: Rem(30) }}><CText size="82" color="#5d5d5d">23415.67</CText></View>
        <CText size="28" color="#5d5d5d">总收益</CText>
      </View>
      <Flex style={styles.tip} justify="center" align="center">
        <CText size="24" color="#9D9D9D">每次兑换金额不得低于100元，余额低于100时不可提现</CText>
      </Flex>
      <Flex align="center" style={styles.inputBox}>
        <CText size="30" color="#454545">提现金额：</CText>
        <TextInput style={styles.textinput} placeholder="输入提现金额" />
      </Flex>
      <View style={styles.payList}>
        <CText size="24" color="#8F8F8F">提现到：</CText>
        <Flex justify="space-between" align="center" style={{ marginTop: Rem(25) }}>
          <Flex>
            <Image style={styles.img} />
            <CText>提现到微信</CText>
          </Flex>
          <View style={styles.check}></View>
        </Flex>
      </View>
      <TouchableOpacity style={styles.submit}>
        <CText size="36" color="#fff">提 现</CText>
      </TouchableOpacity>
      <View style={styles.bottomDesc}>
        <View style={{ marginBottom: Rem(20) }}><CText size="24" color="#6F6F6F">提现提示：</CText></View>
        <View style={{ marginBottom: Rem(10) }}><Text style={styles.descText}>1. 点击申请提现后，长按下方财务二维码，并添加财务微信进行提现转账</Text></View>
        <View style={{ marginBottom: Rem(10) }}><Text style={styles.descText}>2. 请各位会员添加客服，以便微信不回复时备用。 </Text></View>
      </View>
    </ScrollView>
  );
}}

let styles = {
  wrapper: { flex: 1 },
  headerIcon: {
    fontSize: 32,
    color: '#fff',
  },
  totolMoney: {
    height: 293,
    flexDirection: 'column',
    alignItems: 'center',
    paddingTop: 72,
  },
  tip: {
    height: 80,
    backgroundColor: '#f4f4f4'
  },
  inputBox: {
    height: 120,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#CBCBCB',
    paddingLeft: 41,
  },
  textinput: {
    flex: 1,
    paddingVertical: 0,
  },
  payList: {
    paddingHorizontal: 41,
    paddingTop: 28,
    paddingBottom: 30,
    borderBottomWidth: 1,
    borderColor: '#CBCBCB'
  },
  img: {
    backgroundColor: '#FDAC42',
    width: 80,
    height: 80,
    marginRight: 25
  },
  check: {
    width: 36,
    height: 36,
    backgroundColor: '#FDAC42',
    borderRadius: 18,
  },
  submit: {
    width: 710,
    height: 100,
    borderRadius: 10,
    backgroundColor: '#FDAC42',
    marginLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 98,
    marginBottom: 85,
  },
  bottomDesc: {
    marginLeft: 75,
    width: 600,
  },
  descText: {
    fontSize: 20,
    color: '#9D9D9D',
    lineHeight: 30,
  }
};
styles = createStyles(styles);

export default Draw;