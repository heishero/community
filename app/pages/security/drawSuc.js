/*
 * @Author: yixin 
 * @Date: 2018-11-13 09:36:48 
 * @Last Modified by: yixin
 * @Last Modified time: 2018-12-05 01:02:24
 * 提现成功
 */
import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../utils/page';

class DrawSuc extends React.Component {

static navigatorStyle = {
  navBarHidden: true,
  navBarNoBorder: true,
}

render() {
  return (
    <View style={styles.wrapper}>
      <NormalHeader
        leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
        onLeftPress={() => { PageFn.pop(this.props.componentId) }}>
        提现成功
      </NormalHeader>
      <Flex direction="column" align="center">
        <Icon name="dui" style={styles.statusIcon} />
        <CText size="38" color="#FDB64A">提现成功</CText>
        <View style={styles.timeMargin}><CText size="28" color="#9D9D9D">预计10秒到账</CText></View>
        <Flex>
          <TouchableOpacity><CText color="#9D9D9D" size="30">转账记录</CText></TouchableOpacity>
          <View style={styles.solid}></View>
          <TouchableOpacity><CText color="#9D9D9D" size="30">继续转账</CText></TouchableOpacity>
        </Flex>
      </Flex>
    </View>
  );
}}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#F7F7F7' },
  headerIcon: {
    fontSize: 32,
    color: '#fff',
  },
  statusIcon: {
    fontSize: 140,
    marginTop: 210,
    marginBottom: 64,
    color: '#FDB64A'
  },
  timeMargin: {
    marginTop: 18,
    marginBottom: 40
  },
  solid: {
    width: 2,
    height: 38,
    backgroundColor: '#FDB64A',
    marginHorizontal: 33,
  }
};
styles = createStyles(styles);

export default DrawSuc;
