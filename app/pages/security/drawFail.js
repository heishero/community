/*
 * @Author: yixin
 * @Date: 2018-11-13 09:36:48
 * @Last Modified by: yixin
 * @Last Modified time: 2018-12-05 01:02:00
 * 提现成功
 */
import React from 'react';
import { View, Text } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../utils/page';

class DrawFail extends React.Component {

static navigatorStyle = {
  navBarHidden: true,
  navBarNoBorder: true,
}

render() {
  return (
    <View style={styles.wrapper}>
      <NormalHeader
        leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
        onLeftPress={() => { PageFn.pop(this.props.componentId) }}>
        提现失败
      </NormalHeader>
      <Flex direction="column" align="center">
        <Icon name="shibai" style={styles.statusIcon} />
        <View style={styles.timeMargin}><CText size="38" color="#FDB64A">提现失败</CText></View>
        <View style={styles.reason}><Text style={styles.reasonText}>失败原因：2008年9月6开始，我两次心肌梗塞，一次病危，四次心血管介入造影手术</Text></View>
      </Flex>
    </View>
  );
}}

let styles = {
  wrapper: { flex: 1 },
  headerIcon: {
    fontSize: 32,
    color: '#fff',
  },
  statusIcon: {
    fontSize: 140,
    marginTop: 210,
    marginBottom: 64,
    color: '#FDB64A'
  },
  timeMargin: {
    marginBottom: 32
  },
  reason: {
    width: 566,
  },
  reasonText: {
    color: '#9D9D9D',
    fontSize: 24,
    lineHeight: 30
  }
};
styles = createStyles(styles);

export default DrawFail;
