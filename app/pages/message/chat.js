import React, { Component } from 'react';
import {
  View,
  Dimensions,
  Platform,
  Modal,
  StatusBar
} from 'react-native';
import { Toast } from 'antd-mobile-rn';
import IMUI from 'aurora-imui-react-native';
import JMessage from 'jmessage-react-plugin';
import { createStyles, Rem } from '@/utils/view';
import { connect } from 'react-redux';
import { PageFn } from '@/utils/page';
import ImageViewer from 'react-native-image-zoom-viewer';
import ImagePicker from 'react-native-image-crop-picker';

let InputView = IMUI.ChatInput;
let MessageListView = IMUI.MessageList;
const AuroraIController = IMUI.AuroraIMUIController;
const window = Dimensions.get('window');

class Chat extends Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      },
      topBar: {
        drawBehind: true,
        visible: false
      }
    };
  }

  constructor(props) {
    super(props);
    let initHeight;
    if (Platform.OS === 'ios') {
      initHeight = Rem(106);
    } else {
      initHeight = Rem(106);
    }
    this.state = {
      inputLayoutHeight: initHeight,
      messageListLayout: { flex: 1, width: window.width, paddingTop: 10, },
      inputViewLayout: { width: window.width, height: initHeight, zIndex: 999 },
      isAllowPullToRefresh: true,
      navigationBar: {},
      chatIndex: 0,
      chatHasMore: true,
      // 点击图片放大
      imgShow: false,
      zoomImgs: []
    };
    this.inputRefs = null;
    this.ChatInput = null;
    this.MessageList = null;
    this.messageListDidLoadEvent = this.messageListDidLoadEvent.bind(this);
  }

  componentDidMount() {
    /**
     * Android only
     * Must set menu height once, the height should be equals with the soft keyboard height so that the widget won't flash.
     * 在别的界面计算一次软键盘的高度，然后初始化一次菜单栏高度，如果用户唤起了软键盘，则之后会自动计算高度。
     */
    if (Platform.OS === 'android') {
      this.ChatInput.setMenuContainerHeight(Rem(316));
    }
    this.resetMenu();
    // ui加载完成获取聊天记录
    AuroraIController.addMessageListDidLoadListener(this.messageListDidLoadEvent);
    // message监听
    JMessage.addReceiveMessageListener(this.listener); // 添加监听
    // 进入会话

    JMessage.addReceiptMessageListener(this.receiptListener); // 添加监听

    JMessage.getMyInfo((UserInf) => {
      if (UserInf.username === 'undefine') {
        // 未登录
      } else {
        // 已登录
      }
    });

  }

  componentWillUnmount() {
    JMessage.removeReceiptMessageListener(this.listener);
    AuroraIController.removeMessageListDidLoadListener(this.messageListDidLoadEvent);
    JMessage.removeReceiveMessageListener(this.receiptListener); // 移除监听(一般在 componentWillUnmount 中调用)
  }

  messageListDidLoadEvent() {
    this.getHistoryMessage();
  }

  unReceiptMessage=(message) => {

  }

  listener = (message) => {
    // 收到的消息会返回一个消息对象. 对象字段可以参考对象说明
    let messages = [];
    if (message.type !== 'event' && message.target.id === this.props.id) {
      messages.push(this.formatMessage(message));
      AuroraIController.appendMessages(messages);
      AuroraIController.scrollToBottom(true);
    }
  }

  receiptListener = (message) => {
    // 已读消息callback

  }

  getHistoryMessage = () => {
    let messages = [];
    // 当是group的情况
    JMessage.getHistoryMessages({ type: this.props.type, groupId: this.props.id, username: this.props.username, from: this.state.chatIndex, limit: 20, appKey: '9fea82812ba31bdd5fa7fd7b' },
      (conversation) => {
        // do something.
        console.log('getHistoryMessages', conversation);
        this.setState({ chatHasMore: conversation.length === 20 });
        conversation.map((data, index) => {
          let formatData = this.formatMessage(data);
          if (data.type !== 'event') {
            messages.push(formatData);
            // 下载头像
            JMessage.downloadThumbUserAvatar({ username: data.from.username, appKey: '9fea82812ba31bdd5fa7fd7b' },
              (result) => { data.from.avatarThumbPath = result.filePath },
              (err) => { console.log('下载用户头像失败:', err) });
          }
          if (data.type === 'image') {
            JMessage.downloadOriginalImage({
              type: this.props.type,
              username: 'username',
              groupId: this.props.id,
              messageId: data.id
            },
            (result) => {
              let tempData = JSON.parse(JSON.stringify(formatData));
              tempData.mediaPath = result.filePath;
              AuroraIController.updateMessage(tempData);
            }, (error) => {
              let code = error.code;
              let desc = error.description;
            });
          }
        });
        AuroraIController.appendMessages(messages);
        AuroraIController.scrollToBottom(true);
        // this.cansendMessage();
      }, (error) => {
        console.log(error);
      }
    );
  }

  onPullToRefresh = () => {
    if (!this.state.chatHasMore) { this.MessageList.refreshComplete(); return false }
    let messages = [];
    let chatIndex = this.state.chatIndex + 20;
    JMessage.getHistoryMessages({ type: this.props.type, groupId: this.props.id, username: this.props.username, from: chatIndex, limit: 10 },
      (conversation) => {
        // do something.
        this.setState({ chatHasMore: conversation.length === 20, chatIndex });
        conversation.map((data, index) => {
          if (data.type !== 'event') {
            messages.push(this.formatMessage(data));
          }
        });
        AuroraIController.insertMessagesToTop(messages);
        this.MessageList.refreshComplete();
      }, (error) => {
        console.log(error);
        this.MessageList.refreshComplete();
      }
    );
  }

  formatMessage = (data) => {
    let message = {};
    message.msgId = data.createTime.toString();
    message.status = 'send_succeed';
    message.msgType = data.type;
    message.isOutgoing = (this.props.userInfo.jg_im_username === data.from.username);
    message.fromUser = {
      userId: data.from.username,
      displayName: data.from.nickname,
      avatarPath: data.from.avatarThumbPath || 'images',
    };
    if (data.type === 'text') { message.text = data.text }
    if (data.type === 'image') { message.mediaPath = data.thumbPath }
    if (data.type === 'voice') { message.mediaPath = data.path; message.duration = data.duration }
    if (data.type === 'file') { message.mediaPath = data.path; message.msgType = 'video'; message.duration = parseFloat(data.extras.duration) || 0 }
    let date = new Date(data.createTime);
    let minute = date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();
    message.timeString = date.getHours() + ':' + minute;
    return message;
  }

  // 自己的消息格式化，默认type为text
  ownMessage = (type) => {
    let message = {};
    message.msgId = new Date().getTime().toString();
    message.status = 'send_going';
    message.msgType = 'text';
    message.isOutgoing = true;

    message.fromUser = {
      userId: '',
      displayName: this.props.userInfo.nick_name,
      avatarPath: this.props.userInfo.headimgurl
    };
    let date = new Date();
    let minute = date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();
    message.timeString = date.getHours() + ':' + minute;

    return message;
  }

  resetMenu() {
    if (Platform.OS === 'android') {
      this.ChatInput.showMenu(false);
      this.setState({
        messageListLayout: { flex: 1, width: window.width, paddingTop: 10, },
        navigationBar: { height: 64, justifyContent: 'center' },
      });
      this.forceUpdate();
    } else {
      AuroraIController.hidenFeatureView(true);
    }
  }

  /**
   * Android need this event to invoke onSizeChanged
   */
  onTouchEditText = () => {
    this.ChatInput.showMenu(false);
  }

  // 隐藏软键盘
  hiddenKeyBord = () => {
    if (this.props.locked) {
      this.props.setLocked && this.props.setLocked(false);
    }
    AuroraIController.hidenFeatureView(false);
  }

  onInputViewSizeChange = (size) => {
    if (this.state.inputLayoutHeight !== size.height) {
      this.setState({
        inputLayoutHeight: size.height,
        inputViewLayout: { width: window.width, height: size.height + 10 },
        messageListLayout: { flex: 1, width: window.width, paddingTop: 20, }
      });
    }
  }

  onSwitchToGalleryMode = () => {
    this.props.setLocked && this.props.setLocked(true);
    AuroraIController.scrollToBottom(true);
  }

  // 是否可以发送消息(不是好友或者不再同一个群就不能发送消息)
  cansendMessage = () => {
    if (this.props.type === 'single' && !this.props.isFriend) {
      let message = this.ownMessage();
      message.text = '你们已经不是好友关系';
      message.msgType = 'event';
      AuroraIController.appendMessages([message]);
      return false;
    }
    if (this.props.type === 'group' && this.props.isJoin === 0) {
      let message = this.ownMessage();
      message.text = '您已不再此圈请重新加入';
      message.msgType = 'event';
      AuroraIController.appendMessages([message]);
      return false;
    }
    return true;
  }

  onSendText = (text) => {
    if (!text.trim()) return false;
    let uiMessage = this.ownMessage();
    uiMessage.text = text;
    let jMessage = {
      type: this.props.type,
      username: this.props.username,
      groupId: this.props.id,
      appKey: '9fea82812ba31bdd5fa7fd7b',
      text: text,
      extras: {
        type: this.props.type + ' ',
        username: this.props.username + ' ',
        groupId: this.props.id + ' ',
      },
      messageType: 'text',
    };
    this.sendMessage({ uiMessage, jMessage });
  }

  // 发送图片
  onSendGalleryFiles = (mediaFiles) => {
    mediaFiles.map(data => {
      // aurora数据
      let uiMessage = this.ownMessage();
      if (data.mediaType === 'image') {
        uiMessage.msgType = 'image';
      } else {
        Toast.fail('请选择图片', 1);
        return false;
        uiMessage.msgType = 'video';
        uiMessage.duration = data.duration;
      }
      uiMessage.mediaPath = data.mediaPath;
      // jmessage数据
      let jMessage = {
        type: this.props.type,
        messageType: data.mediaType === 'image' ? 'image' : 'file',
        groupId: this.props.id,
        username: this.props.username,
        path: data.mediaPath,
        appKey: '9fea82812ba31bdd5fa7fd7b',
        extras: {
          type: String(this.props.type),
          username: String(this.props.username),
          groupId: String(this.props.id),
          // duration: String(data.duration),
        }
      };
      this.sendMessage({ uiMessage, jMessage });
    });
    this.resetMenu();

  }

  // 录音
  onFinishRecordVoice = (mediaPath, duration) => {
    // aurora数据
    let uiMessage = this.ownMessage();
    uiMessage.msgType = 'voice';
    uiMessage.mediaPath = mediaPath;
    uiMessage.duration = duration;
    // jmessage数据
    let jMessage = {
      type: this.props.type,
      messageType: 'voice',
      groupId: this.props.id,
      username: this.props.username,
      path: mediaPath,
      appKey: '9fea82812ba31bdd5fa7fd7b',
      extras: {
        type: this.props.type + ' ',
        username: this.props.username + ' ',
        groupId: this.props.id + ' ',
      }
    };
    this.sendMessage({ uiMessage, jMessage });
  }

  // 发送消息
  sendMessage = ({ uiMessage, jMessage }) => {
    if (!this.cansendMessage()) return false;
    AuroraIController.appendMessages([uiMessage]);
    AuroraIController.scrollToBottom(true);
    try {
      JMessage.createSendMessage(jMessage, (message) => {
        JMessage.sendMessage({ id: message.id, type: this.props.type, groupId: this.props.id, username: this.props.username, appKey: '9fea82812ba31bdd5fa7fd7b' }, (result) => {
          // 成功回调
          console.log('发送消息成功', result);
          let tempUiMessage = JSON.parse(JSON.stringify(uiMessage));
          tempUiMessage.status = 'send_succeed';
          AuroraIController.updateMessage(tempUiMessage);
        }, (error) => {
          // 失败回调
          console.log('发送消息失败', error);
          let tempUiMessage = JSON.parse(JSON.stringify(uiMessage));
          tempUiMessage.status = 'send_failed';
          AuroraIController.updateMessage(tempUiMessage);
        });
      });
    } catch (error) {
    }
  }

  onCancelRecordVoice = () => {
    console.log('on cancel record voice');
  }

  onStartRecordVideo = () => {
    console.log('on start record video');
  }

  // 点击菜单栏麦克风按钮触发，锁定左右移动切换tab
  onSwitchToMicrophoneMode = () => {
    this.props.setLocked && this.props.setLocked(true);
  }

  // 点击头像查看详情
  onAvatarClick = (message) => {
    PageFn.push(this.props.componentId ? this.props.componentId : 'Message', 'ChaterInfo', { id: message.fromUser.userId });
  }

  onMsgClick = (message) => {
    // 点击图片放大
    if (message.msgType === 'image' && message.mediaPath) {
      this.setState({ imgShow: true, zoomImgs: [{ url: 'file:' + message.mediaPath }], });
      StatusBar.setBackgroundColor('#000');
    }
  }

  chooseImage = () => {
    ImagePicker.openPicker({
      // width: 300,
      // height: 300,
      cropping: false,
      cropperCircleOverlay: false,
      // compressImageMaxWidth: 1000,
      // compressImageMaxHeight: 1000,
      takePhotoButtonTitle: true,
      freeStyleCropEnabled: true,
      compressImageQuality: 0.7,
      loadingLabelText: '加载中',
      mediaType: 'photo',
    }).then(async image => {
      let imageUrl = image.path.replace('file://', '');
      let uiMessage = this.ownMessage();
      uiMessage.msgType = 'image';
      uiMessage.mediaPath = imageUrl;
      // jmessage数据
      let jMessage = {
        type: this.props.type,
        messageType: 'image',
        groupId: this.props.id,
        username: this.props.username,
        path: imageUrl,
        appKey: '9fea82812ba31bdd5fa7fd7b',
        extras: {
          type: String(this.props.type),
          username: String(this.props.username),
          groupId: String(this.props.id),
          // duration: String(data.duration),
        }
      };
      this.sendMessage({ uiMessage, jMessage });
      // this.resetMenu();
    });
  }

  render() {
    return (
      <View style={styles.container} >
        <MessageListView style={this.state.messageListLayout}
          ref={(el) => { this.MessageList = el }}
          isAllowPullToRefresh={true}
          isShowDisplayName={true}
          onTouchMsgList={this.hiddenKeyBord}
          onBeginDragMessageList={this.onBeginDragMessageList}
          onPullToRefresh={this.onPullToRefresh}
          onAvatarClick={this.onAvatarClick}
          onMsgClick={this.onMsgClick}
          avatarSize={{ width: Rem(84), height: Rem(84) }}
          avatarCornerRadius={Rem(42)}
          messageListBackgroundColor={'#EDEDED'}
          sendBubbleTextSize={Rem(30)}
          receiveBubbleTextSize={Rem(30)}
          sendBubbleTextColor={'#000000'}
          sendBubble={Platform.OS === 'android' ? { imageName: 'send_msg', padding: { left: Rem(28), top: 0, right: Rem(40), bottom: 0 }} : { padding: { left: Rem(28), top: 0, right: Rem(40), bottom: 0 }}}
          receiveBubble={Platform.OS === 'android' ? { imageName: 'receive_msg', padding: { left: Rem(28), top: 0, right: Rem(40), bottom: 0 }} : { padding: { left: Rem(28), top: 0, right: Rem(40), bottom: 0 }}}
          datePadding={{ left: 5, top: 5, right: 5, bottom: 5 }}
          dateBackgroundColor={'#EDEDED'}
          dateTextColor={'#999999'}
          photoMessageRadius={5}
          maxBubbleWidth={0.7}
          videoDurationTextColor={'#ffffff'}
        />
        <InputView style={this.state.inputViewLayout}
          ref={(el) => { this.ChatInput = el }}
          onSendText={this.onSendText}
          onStartRecordVoice={this.onStartRecordVoice}
          onFinishRecordVoice={this.onFinishRecordVoice}
          onCancelRecordVoice={this.onCancelRecordVoice}
          onSendGalleryFiles={this.onSendGalleryFiles}
          onSwitchToMicrophoneMode={this.onSwitchToMicrophoneMode}
          onSwitchToGalleryMode={this.onSwitchToGalleryMode}
          onSwitchToCameraMode={this.onSwitchToCameraMode}
          onTouchEditText={this.onTouchEditText}
          onSizeChange={this.onInputViewSizeChange}
          showSelectAlbumBtn={true}
          onClickSelectAlbum={this.chooseImage}
          showRecordVideoBtn={false}
          galleryScale={0.8}// default = 0.5
          compressionQuality={0.6}
          customLayoutItems={{
            left: [],
            right: ['send'],
            bottom: ['voice', 'gallery', 'emoji']
          }}
        />
        <Modal visible={this.state.imgShow} transparent={true}>
          <ImageViewer onClick={() => { this.setState({ imgShow: false }); StatusBar.setBackgroundColor('rgba(0,0,0,0)') }} imageUrls={this.state.zoomImgs} onRequestClose={() => this.setState({ imgShow: false })} />
        </Modal>
      </View>
    );
  }
}

const styles = createStyles({
  back: {
    color: '#000',
    fontSize: 36,
  },
  container: {
    flex: 1,
    backgroundColor: '#EDEDED'
  },
  btnStyle: {
    marginTop: 20,
    borderWidth: 1,
    borderColor: '#3e83d7',
    borderRadius: 16,
    backgroundColor: '#3e83d7'
  },
  inputBox: {
    height: 126,
    backgroundColor: '#F7F7F7',
    paddingHorizontal: 19,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  bottomInput: {
    width: 600,
    height: 80,
    backgroundColor: '#fff',
    borderRadius: 20,
    paddingVertical: 0,
    ios: {
      height: 80,
      paddingHorizontal: 20,
    },
  },
  subIcon: {
    fontSize: 68,
    color: '#262626',
    marginLeft: 10,
  },
  sendTextBtn: {
    width: 88,
    height: 80,
    borderRadius: 20,
    backgroundColor: '#47cefa'
  }
});

export default connect((Store) => {
  return { userInfo: Store.UserRenderData.userInfo };
})(Chat);