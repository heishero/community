/*
 * @Author: yixin
 * @Date: 2019-04-04 22:21:25
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-15 22:57:13
 * 群成员选项组件
 */
import React from 'react';
import {
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import { createStyles } from '@/utils/view';
import CText from '@/components/CText';
import Icon from 'react-native-vector-icons/icomoon';


export default class MemberOption extends React.PureComponent {

  render() {
    const { data, index, memberCheck } = this.props;
    return (
      <TouchableOpacity style={styles.wrapper} onPress={() => { memberCheck(index) }}>
        {data.active ? <Icon name="cheng-gong" style={styles.icon} /> : <View style={styles.option} />}
        {data.user.avatarThumbPath ? <Image style={styles.head} source={{ uri: 'file:' + data.user.avatarThumbPath }} /> : <View style={styles.head} />}
        <CText size="30" color="#000">{data.groupNickname || data.user.nickname}</CText>
      </TouchableOpacity>
    );
  }
}

let styles = {
  wrapper: {
    height: 134,
    paddingLeft: 26,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  icon: {
    fontSize: 40,
    color: '#00BAFF'
  },
  option: {
    height: 40,
    width: 40,
    borderRadius: 20,
    borderWidth: 2,
    borderColor: '#4C92F8',
  },
  head: {
    width: 86,
    height: 86,
    backgroundColor: '#f5f5f5',
    borderRadius: 43,
    marginHorizontal: 29,
  }
};
styles = createStyles(styles);
