/*
 * @Author: yixin
 * @Date: 2018-10-24 22:16:17
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-27 00:59:43
 * 自定义消息列表
 */
import React from 'react';
import { View, Text, Image } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '@/components/CText';
import { createStyles, Rem } from '@/utils/view';
import Icon from 'react-native-vector-icons/icomoon';
import { messageTimeFormat } from '@/utils/timestampFormat';

class CustomMessage extends React.PureComponent {

  render() {
    const { data } = this.props;
    return (
      <View style={styles.wrapper} key={data.title}>
        <Flex style={styles.item}>
          {data.type === 'system' ? <Image style={[styles.picBox, { backgroundColor: '#fff' }]} resizeMode="center" source={require('../../../assets/img/logo.png')} /> : <Flex style={styles.picBox} justify="center" align="center">
            <Icon style={{ fontSize: Rem(68), color: '#fff' }} name="tianjiahaoyou" />
          </Flex>}
          <Flex style={[styles.content, data.type !== 'system' && { borderBottomWidth: 1 }]}>
            <Flex direction="column" justify="flex-start" align="flex-start">
              <View style={{ marginBottom: Rem(15) }}><CText size="32" color="#1E1E1E">{data.title}</CText></View>
              <Text numberOfLines={1} style={styles.messageText} >{data.text}</Text>
            </Flex>
            {data.time && <Flex style={styles.timeBox}>
              <CText color="#BEBEBE" size="22">{messageTimeFormat(data.time)}</CText>
            </Flex>}
          </Flex>
        </Flex>
      </View>
    );
  }
}

let styles = {
  wrapper: {
    height: 146,
    backgroundColor: '#fff',
    paddingVertical: 22,
    paddingLeft: 30,
    paddingRight: 10
  },
  item: {
    flex: 1,
    paddingBottom: 14,
  },
  picBox: {
    width: 94,
    height: 94,
    borderRadius: 16,
    marginRight: 23,
    backgroundColor: '#2D9FF0',
  },
  content: {
    height: 138,
    flex: 1,
    borderColor: '#EDEDED',
  },
  messageText: {
    fontSize: 24,
    color: '#A3A3A3',
  },
  timeBox: {
    position: 'absolute',
    top: 26,
    right: 0,
  },
};
styles = createStyles(styles);

export default CustomMessage;