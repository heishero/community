/*
 * @Author: yixin
 * @Date: 2018-10-24 22:16:17
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-23 22:13:29
 * 消息列表
 */
import React from 'react';
import { View, Image, Text } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '@/components/CText';
import { createStyles, Rem } from '@/utils/view';
import PageFn from '@/utils/page';
import { messageTimeFormat } from '@/utils/timestampFormat';
import Icon from 'react-native-vector-icons/icomoon';

class MessageList extends React.PureComponent {

  render() {
    const { data, componentId } = this.props;

    let text = '';
    if (data.latestMessage.type === 'text') { text = data.latestMessage.text }
    if (data.latestMessage.eventType === 'group_member_added') {
      text = '有新用户加入';
    }
    if (data.latestMessage.type === 'image') { text = `${data.latestMessage.from.nickname}：[图片]` }
    if (data.latestMessage.type === 'voice') { text = `${data.latestMessage.from.nickname}：[语音]` }
    if (data.latestMessage.type === 'file') { text = `${data.latestMessage.from.nickname}：[视频]` }
    return (
      <View style={styles.wrapper} key={data.title}>
        <Flex style={styles.item} onPress={() => {
          PageFn.push(componentId, data.conversationType === 'group' ? 'ChatGroup' : 'ChatSingle', { type: data.conversationType, id: data.target.id, username: data.target.username });
        }}>
          {data.target.avatarThumbPath ?
            <Image style={styles.img} source={{ uri: 'file:' + data.target.avatarThumbPath }} /> :
            <View style={styles.img}></View>}
          {parseInt(data.unreadCount, 10) > 0 ?
            (!data.target.isNoDisturb ? <View style={styles.numBox}><Text style={{ color: '#fff', fontSize: 10 }}>{data.unreadCount}</Text></View> :
            <View style={styles.noDisturbNum} />)
            : null}
          <Flex style={styles.content}>
            <Flex direction="column" justify="flex-start" align="flex-start">
              <View style={{ marginBottom: Rem(15) }}><CText size="32" color="#1E1E1E">{data.title}</CText></View>
              <Text numberOfLines={1} style={styles.messageText} >{text}</Text>
            </Flex>
            <Flex style={styles.timeBox}>
              <CText color="#BEBEBE" size="22">{messageTimeFormat(data.latestMessage.createTime / 1000)}</CText>
            </Flex>
            {data.target.isNoDisturb && <Icon style={styles.noDisturb} name="miandarao" />}
          </Flex>
        </Flex>
      </View>
    );
  }
}

let styles = {
  wrapper: {
    height: 146,
    backgroundColor: '#fff',
    paddingVertical: 22,
    paddingHorizontal: 30,
  },
  item: {
    flex: 1,
    paddingBottom: 14,
  },
  img: {
    width: 94,
    height: 94,
    borderRadius: 16,
  },
  content: {
    left: 20,
    height: 138,
    flex: 1,
    borderBottomWidth: 1,
    borderColor: '#EDEDED',
  },
  messageText: {
    fontSize: 24,
    color: '#A3A3A3',
  },
  timeBox: {
    position: 'absolute',
    top: 18,
    right: 0,
  },
  numBox: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: 30,
    height: 30,
    borderRadius: 15,
    top: -12,
    left: 70,
    backgroundColor: 'red',
    position: 'absolute'
  },
  noDisturb: {
    fontSize: 27,
    color: '#E8E8E8',
    position: 'absolute',
    top: 60,
    right: 0
  },
  noDisturbNum: {
    height: 16,
    width: 16,
    borderRadius: 8,
    backgroundColor: '#FF0000',
    position: 'absolute',
    top: -10,
    left: 82,
  }
};
styles = createStyles(styles);

export default MessageList;