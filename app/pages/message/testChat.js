import React, { Component } from 'react';
import {
  View,
  Dimensions,
  Platform,
} from 'react-native';
import IMUI from 'aurora-imui-react-native';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import JMessage from 'jmessage-react-plugin';
import { createStyles, Rem } from '../../utils/view';
import { connect } from 'react-redux';
import { PageFn } from '../../utils/page';
import { convertGroupId } from '../../service/group';
import Storage from '../../utils/storage';

let InputView = IMUI.ChatInput;
let MessageListView = IMUI.MessageList;
const AuroraIController = IMUI.AuroraIMUIController;
const window = Dimensions.get('window');

class Chat extends Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      },
      topBar: {
        drawBehind: true,
        visible: false
      }
    };
  }

  constructor(props) {
    super(props);
    let initHeight;
    if (Platform.OS === 'ios') {
      initHeight = Rem(106);
    } else {
      initHeight = 100;
    }
    this.state = {
      inputLayoutHeight: initHeight,
      messageListLayout: { flex: 1, width: window.width, paddingTop: 10, },
      inputViewLayout: { width: window.width, height: initHeight },
      isAllowPullToRefresh: true,
      navigationBar: {},
      chatIndex: 0,
      chatHasMore: true,
      groupId: null,
      title: ''
    };
    this.inputRefs = null;
    this.messageListDidLoadEvent = this.messageListDidLoadEvent.bind(this);
  }

  componentDidMount() {
    /**
     * Android only
     * Must set menu height once, the height should be equals with the soft keyboard height so that the widget won't flash.
     * 在别的界面计算一次软键盘的高度，然后初始化一次菜单栏高度，如果用户唤起了软键盘，则之后会自动计算高度。
     */
    if (Platform.OS === 'android') {
      this.refs['ChatInput'].setMenuContainerHeight(Rem(316));
    }
    this.resetMenu();
    // ui加载完成获取聊天记录
    AuroraIController.addMessageListDidLoadListener(this.messageListDidLoadEvent);
    // JMessage.addSyncRoamingMessageListener(this.listener);
    // message监听
    // messageid转换为groupid
    this.convertId();
    JMessage.addReceiveMessageListener(this.listener); // 添加监听
    // 进入会话
    JMessage.enterConversation({ type: 'group', groupId: this.props.id },
      (conversation) => {
        // do something.
      }, (error) => {
        console.log(error);
      });

    JMessage.getMyInfo((UserInf) => {
      if (UserInf.username === 'undefine') {
        // 未登录
      } else {
        // 已登录
      }
    });
  }

  componentWillUnmount() {
    AuroraIController.removeMessageListDidLoadListener(this.messageListDidLoadEvent);
    JMessage.removeReceiveMessageListener(this.listener); // 移除监听(一般在 componentWillUnmount 中调用)
    JMessage.exitConversation();
  }

  convertId = async () => {
    try {
      const token = await Storage.load('token');
      const { data } = await convertGroupId({ token, jg_im_gid: this.props.id });
      this.setState({ groupId: data.id, title: `${data.name}(${data.member})` });
    } catch (error) {

    }
  }

  messageListDidLoadEvent() {
    this.getHistoryMessage();
  }

  listener = (message) => {
    // 收到的消息会返回一个消息对象. 对象字段可以参考对象说明
    let messages = [];
    if (message.type !== 'event') {
      messages.push(this.formatMessage(message));
      AuroraIController.appendMessages(messages);
      AuroraIController.scrollToBottom(true);
    }
  }

  getHistoryMessage() {
    let messages = [];
    JMessage.getHistoryMessages({ type: 'group', groupId: this.props.id, from: this.state.chatIndex, limit: 20, isDescend: 'true' },
      (conversation) => {
        // do something.
        console.log('getHistoryMessage', conversation);
        this.setState({ chatHasMore: conversation.length === 10 });
        conversation.map((data, index) => {
          if (data.type !== 'event') {
            messages.push(this.formatMessage(data));
          }

        });
        AuroraIController.appendMessages(messages);
        AuroraIController.scrollToBottom(true);
      }, (error) => {
        let code = error.code;
        let desc = error.description;
        console.log(error);
      }
    );
  }

  onPullToRefresh = () => {
    if (!this.state.chatHasMore) { this.refs['MessageList'].refreshComplete(); return false }
    let messages = [];
    this.state.chatIndex += 10;
    JMessage.getHistoryMessages({ type: 'group', groupId: this.props.id, from: this.state.chatIndex, limit: 10 },
      (conversation) => {
        // do something.
        this.setState({ chatHasMore: conversation.length === 10 });
        conversation.map((data, index) => {
          if (data.type !== 'event') {
            messages.push(this.formatMessage(data));
          }
        });
        AuroraIController.insertMessagesToTop(messages);
      }, (error) => {
        let code = error.code;
        let desc = error.description;
        console.log(error);
      }
    );
    // AuroraIController.insertMessagesToTop(messages);
    if (Platform.OS === 'android') {
      this.refs['MessageList'].refreshComplete();
    }

  }

  formatMessage = (data) => {
    let message = {};
    message.msgId = data.createTime.toString();
    message.status = 'send_succeed';
    message.msgType = data.type;
    message.isOutgoing = (this.props.userInfo.jg_im_username === data.from.username);
    message.fromUser = {
      userId: data.from.username,
      displayName: data.from.nickname,
      avatarPath: data.from.avatarThumbPath || 'images',
    };
    if (data.type === 'text') { message.text = data.text }
    if (data.type === 'image') { message.mediaPath = data.thumbPath }
    if (data.type === 'voice') { message.mediaPath = data.path; message.duration = data.duration }
    let date = new Date(data.createTime);
    message.timeString = date.getHours() + ':' + date.getMinutes();
    return message;
  }

  // 自己的消息格式化，默认type为text
  ownMessage = (type) => {
    let message = {};
    message.msgId = new Date().getTime().toString();
    message.status = 'send_succeed';
    message.msgType = 'text';
    message.isOutgoing = true;

    message.fromUser = {
      userId: '',
      displayName: this.props.userInfo.nick_name,
      avatarPath: this.props.userInfo.headimgurl
    };
    let date = new Date();
    message.timeString = date.getHours() + ':' + date.getMinutes();

    return message;
  }

  resetMenu() {
    if (Platform.OS === 'android') {
      this.refs['ChatInput'].showMenu(false);
      this.setState({
        messageListLayout: { flex: 1, width: window.width, paddingTop: 10, },
        navigationBar: { height: 64, justifyContent: 'center' },
      });
      this.forceUpdate();
    } else {
      AuroraIController.hidenFeatureView(true);
    }
  }

  /**
   * Android need this event to invoke onSizeChanged
   */
  onTouchEditText = () => {
    this.refs['ChatInput'].showMenu(false);
  }

  onFullScreen = () => {
    this.setState({
      messageListLayout: { flex: 0, width: 0, height: 0 },
      inputViewLayout: { flex: 1, width: window.width, height: window.height },
      navigationBar: { height: 0 }
    });
  }

  // 隐藏软键盘
  hiddenKeyBord = () => {
    AuroraIController.hidenFeatureView(false);
  }

  onInputViewSizeChange = (size) => {
    if (this.state.inputLayoutHeight !== size.height) {
      this.setState({
        inputLayoutHeight: size.height,
        inputViewLayout: { width: window.width, height: size.height + 10 },
        messageListLayout: { flex: 1, width: window.width, paddingTop: 20, }
      });
    }
  }

  onSwitchToGalleryMode = () => {
    AuroraIController.scrollToBottom(true);
  }

  // 发送文本
  onSendText = (text) => {
    console.log('onSendText', text);
    if (!text.trim()) return false;
    let message = this.ownMessage();
    message.text = text;
    AuroraIController.appendMessages([message]);
    JMessage.sendTextMessage({
      type: 'group', groupId: this.props.id, text: text, extras: {
        type: 'Chat',
        id: this.props.id
      }
    },
    (msg) => {
    // do something.
    }, (error) => {
      let code = error.code;
      let desc = error.description;
    });
  }

  // 发送图片
  onSendGalleryFiles = (mediaFiles) => {
    mediaFiles.map(data => {
      let message = this.ownMessage();
      if (data.mediaType === 'image') {
        message.msgType = 'image';
      } else {
        message.msgType = 'video';
        message.duration = data.duration;
      }
      message.mediaPath = data.mediaPath;
      AuroraIController.appendMessages([message]);
      AuroraIController.scrollToBottom(true);
      // 发送图片到服务端
      JMessage.sendImageMessage({
        type: 'group', groupId: this.props.id, path: data.mediaPath,
        extras: { type: 'Chat', id: this.props.id }
      },
      (msg) => {
        // do something.

      }, (error) => {
        let code = error.code;
        let desc = error.description;
      });
    });
    this.resetMenu();

  }

  // 录音
  onFinishRecordVoice = (mediaPath, duration) => {
    let message = this.ownMessage();
    message.msgType = 'voice';
    message.mediaPath = mediaPath;
    message.duration = duration;
    // 发送录音到服务端
    JMessage.sendVoiceMessage({
      type: 'group', groupId: this.props.id, path: mediaPath,
      extras: { type: 'Chat', id: this.props.id }
    },
    (msg) => {
    // do something.

    }, (error) => {
      let code = error.code;
      let desc = error.description;
    });
    AuroraIController.appendMessages([message]);
  }

  // 点击头像查看详情
  onAvatarClick = (message) => {
    PageFn.push(this.props.componentId, 'ChaterInfo', { id: message.fromUser.userId });
  }

  toShop = () => {
    if (this.state.groupId) {
      PageFn.push(this.props.componentId, 'Shop', { id: this.state.groupId });
    }
  }

  toArticle = () => {
    PageFn.push(this.props.componentId, 'ArticleScreen', { id: this.state.groupId });
  }

  toDynamic = () => {
    PageFn.push(this.props.componentId, 'Activity', { id: this.state.groupId });
  }

  toJieshao = () => {
    PageFn.push(this.props.componentId, 'CircleDetail', { id: this.state.groupId });
  }


  render() {
    return (
      <View style={styles.container} >
        {/* <NormalHeader
          leftContent={<Icon style={styles.back} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}
        >{this.state.title}</NormalHeader> */}
        <MessageListView style={this.state.messageListLayout}
          ref="MessageList"
          isAllowPullToRefresh={true}
          isShowDisplayName={true}
          onTouchMsgList={this.hiddenKeyBord}
          onBeginDragMessageList={this.onBeginDragMessageList}
          onPullToRefresh={this.onPullToRefresh}
          onAvatarClick={this.onAvatarClick}
          avatarSize={{ width: Rem(84), height: Rem(84) }}
          avatarCornerRadius={Rem(42)}
          messageListBackgroundColor={'#EDEDED'}
          sendBubbleTextSize={Rem(30)}
          receiveBubbleTextSize={Rem(30)}
          sendBubbleTextColor={'#000000'}
          sendBubble={Platform.OS === 'android' ? { imageName: 'send_msg', padding: { left: Rem(28), top: 0, right: Rem(40), bottom: 0 }} : { padding: { left: Rem(28), top: 0, right: Rem(40), bottom: 0 }}}
          receiveBubble={Platform.OS === 'android' ? { imageName: 'receive_msg', padding: { left: Rem(28), top: 0, right: Rem(40), bottom: 0 }} : { padding: { left: Rem(28), top: 0, right: Rem(40), bottom: 0 }}}
          datePadding={{ left: 5, top: 5, right: 5, bottom: 5 }}
          dateBackgroundColor={'#EDEDED'}
          dateTextColor={'#999999'}
          photoMessageRadius={5}
          maxBubbleWidth={0.7}
          videoDurationTextColor={'#ffffff'}
        />
        <InputView style={this.state.inputViewLayout}
          ref="ChatInput"
          onSendText={this.onSendText}
          onStartRecordVoice={this.onStartRecordVoice}
          onFinishRecordVoice={this.onFinishRecordVoice}
          onCancelRecordVoice={this.onCancelRecordVoice}
          onSendGalleryFiles={this.onSendGalleryFiles}
          onSwitchToMicrophoneMode={this.onSwitchToMicrophoneMode}
          onSwitchToGalleryMode={this.onSwitchToGalleryMode}
          onSwitchToCameraMode={this.onSwitchToCameraMode}
          onTouchEditText={this.onTouchEditText}
          onSizeChange={this.onInputViewSizeChange}
          showSelectAlbumBtn={true}
          showRecordVideoBtn={false}
          onClickSelectAlbum={this.onClickSelectAlbum}
          galleryScale={0.6}// default = 0.5
          compressionQuality={0.6}
          customLayoutItems={{
            left: ['voice'],
            right: ['gallery', 'send'],
          }}
        />
      </View>
    );
  }
}

const styles = createStyles({
  back: {
    color: '#000',
    fontSize: 36,
  },
  container: {
    flex: 1,
    backgroundColor: '#EDEDED'
  },
  btnStyle: {
    marginTop: 20,
    borderWidth: 1,
    borderColor: '#3e83d7',
    borderRadius: 16,
    backgroundColor: '#3e83d7'
  },
  inputBox: {
    height: 126,
    backgroundColor: '#F7F7F7',
    paddingHorizontal: 19,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  bottomInput: {
    width: 600,
    height: 80,
    backgroundColor: '#fff',
    borderRadius: 20,
    paddingVertical: 0,
    ios: {
      height: 80,
      paddingHorizontal: 20,
    },
  },
  subIcon: {
    fontSize: 68,
    color: '#262626',
    marginLeft: 10,
  },
  sendTextBtn: {
    width: 88,
    height: 80,
    borderRadius: 20,
    backgroundColor: '#47cefa'
  }
});

export default connect((Store) => {
  return { userInfo: Store.UserRenderData.userInfo };
})(Chat);