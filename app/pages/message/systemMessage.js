import React from 'react';
import { View, FlatList, Text, Image, TouchableOpacity } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../utils/page';
import { fetchSystemInform } from '@/service/system';
import Storage from '@/utils/storage';
import { messageTimeFormat } from '@/utils/timestampFormat';

class SystemMessage extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      list: [],
      loading: false,
      hasMore: true,
    };
    this.pageSize = 5;
    this.messageList = null;
  }

  componentDidMount() {
    this.init();
  }

  init = async () => {
    try {
      this.setState({ loading: true });
      const token = await Storage.load('token');
      const { data } = await fetchSystemInform({ token, page: 1, pageSize: this.pageSize });
      console.log('getSystemInform', data);
      this.setState({
        list: data,
        loading: false,
        hasMore: data.length === this.pageSize,
      }, () => { setTimeout(() => { this.messageList.scrollToEnd() }, 1) });
    } catch (error) {
      this.setState({ loading: false });
    }
  }

  loadMore = async () => {
    if (!this.state.hasMore) return false;
    this.setState({ loading: true });
    try {
      let page = this.state.page + 1;
      const token = await Storage.load('token');
      const { data } = await fetchSystemInform({ token, page: page, pageSize: this.pageSize });
      this.setState({
        list: data.concat(this.state.list),
        page: page,
        loading: false,
        hasMore: data.length === this.pageSize,
      });
    } catch (error) {
      this.setState({ loading: false });
    }
  }

  _renderItem = ({ item }) => {
    return (
      <Flex direction="column" align="center" style={styles.messageBox}>
        <Flex style={styles.messageTime} justify="center" align="center">
          <CText size="22" color="#969696">{messageTimeFormat(item.created_at)}</CText>
        </Flex>
        {item.type === 0 ? this._textMessage(item) : this._imageMessage(item)}
      </Flex>
    );
  }

  _textMessage = (item) => (
    <View style={styles.textMessage}>
      <CText color="#333333" size="33">{item.title}</CText>
      <Text style={styles.textContent} numberOfLines={2}>{item.cover_content}</Text>
    </View>
  )

  _imageMessage = (item) => (
    <TouchableOpacity onPress={() => { PageFn.push(this.props.componentId, 'SystemInfo', { id: item.id }) }} style={styles.imageMessage}>
      <Image style={styles.image} source={{ uri: item.img_url }} resizeMode="stretch" />
      <View style={styles.imageContent}>
        <CText color="#000000" size="28">{item.title}</CText>
        <Text style={styles.imageText} numberOfLines={1}>{item.cover_content}</Text>
      </View>
    </TouchableOpacity>
  )

  render() {
    console.log(this.state.list);
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}>
          平台消息
        </NormalHeader>
        <FlatList
          style={{ marginTop: 8 }}
          data={this.state.list}
          keyExtractor={item => String(item.id)}
          renderItem={this._renderItem}
          onRefresh={this.loadMore}
          refreshing={this.state.loading}
          ref={e => { this.messageList = e }}
        />
      </View>
    );
  }}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#f4f4f4' },
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
  messageBox: {
    marginBottom: 49,
  },
  messageTime: {
    width: 152,
    height: 32,
    borderRadius: 15,
    backgroundColor: '#d8d8d8',
    marginBottom: 29,
  },
  textMessage: {
    paddingVertical: 28,
    paddingHorizontal: 32,
    backgroundColor: '#fff',
    borderLeftWidth: 8,
    borderColor: '#22B7F8',
    width: 710
  },
  textContent: {
    fontSize: 28,
    color: '#909090',
    lineHeight: 48,
  },
  imageMessage: {
    borderRadius: 20,
    borderWidth: 1,
    borderColor: '#E8E8E8',
  },
  image: {
    height: 242,
    width: 710,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
  },
  imageContent: {
    height: 101,
    paddingHorizontal: 25,
    paddingVertical: 10,
    backgroundColor: '#fff'
  },
  imageText: {
    fontSize: 22,
    color: '#616161'
  }
};
styles = createStyles(styles);

export default SystemMessage;