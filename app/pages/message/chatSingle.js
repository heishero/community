/*
 * @Author: yixin
 * @Date: 2019-04-08 09:53:06
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-22 22:22:14
 * 单聊
 */
import React from 'react';
import {
  View,
  Alert
} from 'react-native';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import { createStyles } from '../../utils/view';
import PageFn from '../../utils/page';
import Chat from './chat';
import JMessage from 'jmessage-react-plugin';

export default class ChatSingle extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      },
      topBar: {
        drawBehind: true,
        visible: false
      },
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      title: '',
      unreadCount: 0,
      isFriend: true,
    };
  }

  componentDidMount() {
    this.init();
  }

  componentWillUnmount() {
    JMessage.exitConversation();
  }

  delete = () => {
    Alert.alert(
      '确定删除该好友吗？',
      '',
      [
        { text: '取消', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        {
          text: '确定', onPress: () => {
            // 解除好友关系
            JMessage.removeFromFriendList({ username: this.props.username, appKey: '9fea82812ba31bdd5fa7fd7b' },
              () => {
                // 删除聊天
                JMessage.deleteConversation({ type: 'single', username: this.props.username, appKey: '9fea82812ba31bdd5fa7fd7b' },
                  (conversation) => {
                    PageFn.pop(this.props.componentId);
                  }, (error) => {
                    console.log(error);
                    PageFn.pop(this.props.componentId);
                  });
              }, (error) => {
                console.log(error);
                // 删除聊天
                JMessage.deleteConversation({ type: 'single', username: this.props.username, appKey: '9fea82812ba31bdd5fa7fd7b' },
                  (conversation) => {
                    PageFn.pop(this.props.componentId);
                  }, (error) => {
                    console.log(error);
                    PageFn.pop(this.props.componentId);
                  });
              });
          }
        },
      ],
      { cancelable: false }
    );
  }

  // 查看群信息
  async init() {
    try {
      // 获取会话对象
      JMessage.getConversation({ type: 'single', username: this.props.username, appKey: '9fea82812ba31bdd5fa7fd7b' },
        (conversation) => {
          // do something.
          this.setState({ title: conversation.title, unreadCount: conversation.unreadCountY, isFriend: conversation.target.isFriend });
          console.log('conversation', conversation);
        }, (error) => {
          console.log(error);
        });
      // 进入聊天不再接受当前会话的消息通知
      JMessage.enterConversation({ type: 'single', username: this.props.username, appKey: '9fea82812ba31bdd5fa7fd7b' },
        (conversation) => {
        }, (error) => {
          console.log(error);
        });
    } catch (error) {}
  }


  render() {
    return (
      <View style={{ flex: 1 }}>
        <NormalHeader
          backgroundColor="#EDEDED"
          leftContent={<Icon style={styles.back} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}
          rightContent={<Icon style={styles.setting} name="shanchu" />}
          onRightPress={() => { this.delete() }}
        >{this.state.title}
        </NormalHeader>
        <Chat isFriend={this.state.isFriend} username={this.props.username} componentId={this.props.componentId} type="single" />
      </View>
    );
  }
}

let styles = {
  back: {
    color: '#000',
    fontSize: 36,
    fontWeight: 'bold',
    paddingRight: 100,
  },
  setting: {
    fontSize: 36,
    color: '#3E3E3E',
    paddingLeft: 100,
    fontWeight: 'bold',
  },
};
styles = createStyles(styles);