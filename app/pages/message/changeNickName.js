/*
 * @Author: yixin
 * @Date: 2019-04-03 22:02:19
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-03 23:33:08
 * 更改群昵称
 */
import React, { Component } from 'react';
import {
  View,
  TextInput
} from 'react-native';
import CText from '../../components/CText';
import JMessage from 'jmessage-react-plugin';
import { createStyles } from '../../utils/view';
import { PageFn } from '../../utils/page';
import Icon from 'react-native-vector-icons/icomoon';
import NormalHeader from '../../components/NormalHeader';
import WingBlank from '../../components/WingBlank';

export default class ChangeNickName extends Component {

  constructor(props) {
    super(props);
    this.state = {
      inputValue: ''
    };
  }

  save = () => {
    if (this.state.inputValue.trim()) {
      JMessage.setGroupNickname({ groupId: this.props.jid, username: this.props.username, nickName: this.state.inputValue },
        () => {
          // do something.
          PageFn.pop(this.props.componentId);
        }, (error) => {
          console.log('error', error);
        });
    }
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}
          rightContent={<CText size="28" color="#1D1D1D">保存</CText>}
          onRightPress={this.save}
        >更改昵称 </NormalHeader>
        <WingBlank>
          <TextInput value={this.state.inputValue} onChangeText={(text) => { this.setState({ inputValue: text }) }} style={styles.input} underlineColorAndroid="transparent" />
          <CText size="26" color="#919191">改一个有个性的昵称！</CText>
        </WingBlank>
      </View>
    );
  }
}

let styles = {
  wrapper: {
    flex: 1
  },
  headerIcon: {
    fontSize: 32,
    color: '#1D1D1D'
  },
  input: {
    height: 80,
    borderBottomWidth: 1,
    borderColor: '#F8AA4A',
    paddingHorizontal: 28,
    marginVertical: 20,
  },
};
styles = createStyles(styles);
