import React from 'react';
import { View, Image, Text, TouchableOpacity, FlatList } from 'react-native';
import { Flex, Toast } from 'antd-mobile-rn';
import CText from '@/components/CText';
import { createStyles, Rem } from '@/utils/view';
import NormalHeader from '@/components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '@/utils/page';
import JMessage from 'jmessage-react-plugin';
import { reqFriendList, friendRes } from '@/service/user';
import Storage from '@/utils/storage';
class FriendInvatation extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      invatations: [],
      isloading: false,
    };
    this.accept = this.accept.bind(this);
    this.sayHello = this.sayHello.bind(this);
    this.renderItem = this.renderItem.bind(this);
  }

  componentDidMount() {
    this.getFriendReq();
  }

  getFriendReq = async () => {
    try {
      this.setState({ isloading: true });
      const token = await Storage.load('token');
      const { data } = await reqFriendList({ token, page: 1, pageSize: 999, });
      let invatations = data.map(item => ({
        img: item.headimgurl,
        title: item.nick_name,
        text: `请求添加你为圈友`,
        invatationName: item.jg_im_username,
        id: item.id,
        status: item.status
      }));
      this.setState({
        invatations: invatations,
        isloading: false,
      });
    } catch (error) {
      this.setState({
        isloading: false,
      });
    }
  }

  async accept(id, username) {
    try {
      const token = await Storage.load('token');
      const data = await friendRes({ token, status: 1, user_friend_id: id });
      if (data.status === 'success') {
        Toast.success('添加成功');
        this.sayHello(username);
      }
    } catch (error) {
      console.log(error);
    }
  }

  sayHello(username) {
    JMessage.sendTextMessage({
      type: 'single', username: username,
      text: '我们已经成为好友',
    },
    (msg) => {
      PageFn.pop(this.props.componentId);
    }, (error) => {
      console.log(error);
    });
  }

  renderItem({ item }) {
    return (
      <View style={styles.list}>
        <Flex style={styles.item}>
          <Image style={styles.img} source={{ uri: item.img }} />
          <Flex justify="space-between" style={styles.content}>
            <Flex direction="column" justify="flex-start" align="flex-start">
              <View style={{ marginBottom: Rem(15) }}><CText size="32" color="#1E1E1E">{item.title}</CText></View>
              <Text numberOfLines={1} style={styles.messageText} >{item.text}</Text>
            </Flex>
            {
              item.status === 0 ? <TouchableOpacity onPress={() => this.accept(item.id, item.invatationName)} style={styles.btn}><CText size="26" color="#fff">接受</CText></TouchableOpacity>
              : <View style={styles.btnText}><CText color="#717171" size="26">已接受</CText></View>
            }
          </Flex>
        </Flex>
      </View>
    );
  }

  render() {
    const { invatations, isloading } = this.state;
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}>
          新的圈友
        </NormalHeader>
        <View style={styles.container}>
          <FlatList
            data={invatations}
            keyExtractor={item => item.id}
            renderItem={this.renderItem}
            onRefresh={this.getFriendReq}
            refreshing={isloading}
          />
        </View>
      </View>
    );
  }}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#F7F7F7' },
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
  container: {
    flex: 1,
    borderTopWidth: 1,
    borderColor: '#D5D5D5'
  },
  list: {
    height: 138,
    backgroundColor: '#fff',
    paddingVertical: 22,
    paddingHorizontal: 30,
  },
  item: {
    flex: 1,
    paddingBottom: 14,
  },
  img: {
    width: 94,
    height: 94,
    borderRadius: 16,
    marginRight: 23,
  },
  content: {
    height: 138,
    flex: 1,
    borderBottomWidth: 1,
    borderColor: '#EDEDED',
  },
  messageText: {
    fontSize: 24,
    color: '#A3A3A3',
  },
  btn: {
    width: 114,
    height: 58,
    borderRadius: 6,
    backgroundColor: '#2AB6F3',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    width: 114,
    height: 58,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  }
};
styles = createStyles(styles);

export default FriendInvatation;