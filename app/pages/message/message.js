/*
 * @Author: yixin
 * @Date: 2018-10-19 23:40:27
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-27 00:58:38
 * 发现
 */
import React from 'react';
import { View, FlatList, TouchableOpacity, StatusBar } from 'react-native';
import { createStyles, Rem } from '../../utils/view';
import { Navigation } from 'react-native-navigation';
import JMessage from 'jmessage-react-plugin';
import MessageList from './components/messageList';
import { connect } from 'react-redux';
import messageActions from '../../controllers/message/actions';
import PageFn from '../../utils/page';
import NormalHeader from '../../components/NormalHeader';
import Bottom from '../common/bottom';
import CustomMessage from './components/customMessage';
import { reqFriendList } from '@/service/user';
import { fetchSystemInform } from '@/service/system';
import Storage from '@/utils/storage';

class Message extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      },
    };
  }

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
    this.state = {
      messageList: [],
      friendInvitation: null,
      systemMessage: null
    };
  }

  componentDidMount() {
    JMessage.addClickMessageNotificationListener(this.notifyListener); // 添加点击消息监听
    JMessage.addSyncRoamingMessageListener(this.syncListener); // 获取离线消息
    JMessage.addContactNotifyListener(this.addListener);  // 好友相关通知
    JMessage.addReceiveMessageListener(this.messageListener); // 收到消息监听
  }

  componentWillUnmount() {
    JMessage.removeClickMessageNotificationListener(this.notifyListener);
    JMessage.removeSyncRoamingMessageListener(this.syncListener);
    JMessage.removeContactNotifyListener(this.addListener);
    JMessage.removeReceiveMessageListener(this.messageListener); // 收到消息监听
  }

  componentDidAppear() {
    StatusBar.setTranslucent(true);
    this.props.getMessage();
    this.getFreindReq();
    this.getSystemInform();
  }

  messageListener = (event) => {
    console.log('收到消息:', event);
    this.props.getMessage();
    // 回调参数 result = {'conversation': {}}
  }

  syncListener = (event) => {
    console.log('消息漫游：', event);
    // 回调参数 result = {'conversation': {}}
  }

  notifyListener = ({ message }) => {
    let pageName;
    if (message.extras.type === 'single') pageName = 'ChatSingle';
    if (message.extras.type === 'group') pageName = 'ChatGroup';
    if (message.extras.type === 'system') pageName = 'SystemMessage';
    if (pageName) {
      PageFn.push(this.props.componentId, pageName, { ...message.extras });
    }
  }

  getFreindReq = async () => {
    const token = await Storage.load('token');
    const { data } = await reqFriendList({ token, page: 1, pageSize: 1 });
    if (data[0] && data[0].status !== 1) {
      this.setState({
        friendInvitation: {
          title: data[0].nick_name,
          text: `请求添加你为圈友`,
          invatationName: data[0].jg_im_username,
          id: data[0].id,
          status: data[0].status,
          time: data[0].create_time
        }
      });
    }
  }

  getSystemInform = async () => {
    try {
      const token = await Storage.load('token');
      const { data } = await fetchSystemInform({ token, page: 1, pageSize: 1 });
      this.setState({
        systemMessage: {
          type: 'system',
          title: '系统消息',
          text: data[0].title,
          time: data[0].created_at
        }
      });
    } catch (error) {

    }

  }



  addListener = (event) => {
    this.getFreindReq();
  }

  renderItem = ({ item }) => {
    if ((item.conversationType === 'group' || item.conversationType === 'single' || item.conversationType === 'file') && item.latestMessage) {
      return <MessageList componentId={this.props.componentId} key={item.target.id} data={item} />;
    }
  }

  render() {
    const { messageList } = this.props;
    const { friendInvitation, systemMessage } = this.state;
    return (
      <View style={styles.wrapper}>
        <NormalHeader>消息</NormalHeader>
        {systemMessage && <TouchableOpacity onPress={() => { PageFn.push(this.props.componentId, 'SystemMessage') }}>
          <CustomMessage data={systemMessage} />
          <View style={{ height: Rem(20), backgroundColor: '#EDEDED' }}></View>
        </TouchableOpacity>}
        {friendInvitation && <TouchableOpacity onPress={() => { PageFn.push(this.props.componentId, 'FriendInvatation') }}>
          <CustomMessage data={friendInvitation} />
        </TouchableOpacity>}
        <FlatList
          data={messageList}
          keyExtractor={item => JSON.stringify(item.target)}
          renderItem={this.renderItem}
          onRefresh={() => { this.props.getMessage(); this.getFreindReq() }}
          refreshing={this.props.loading}
        />
        <Bottom index="0" />
      </View>
    );
  }
}

let styles = {
  wrapper: {
    flex: 1,
    backgroundColor: '#fff'
  },
};
styles = createStyles(styles);

export default connect((Store) => {
  return { messageList: Store.MessageRenderData.messageList, loading: Store.MessageRenderData.loading, userInfo: Store.UserRenderData.userInfo, };
}, { getMessage: messageActions.GetMessage })(Message);
