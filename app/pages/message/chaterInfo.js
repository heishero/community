/*
 * @Author: yixin
 * @Date: 2019-03-30 17:55:15
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-09 22:22:49
 */
import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity
} from 'react-native';
import { Flex, Toast } from 'antd-mobile-rn';
import { connect } from 'react-redux';
import CText from '../../components/CText';
import JMessage from 'jmessage-react-plugin';
import { createStyles } from '../../utils/view';
import { PageFn } from '../../utils/page';
import Icon from 'react-native-vector-icons/icomoon';
import { friendInvatation } from '@/service/user';
import Storage from '@/utils/storage';
class ChaterInfo extends Component {

  static options(passProps) {
    return {
      statusBar: {
        style: 'light'
      },
    };
  }
  constructor(props) {
    super(props);
    this.state = {
      userInfo: null
    };
  }
  componentDidMount() {
    JMessage.getUserInfo({ username: this.props.id },
      (userInfo) => {
        console.log('getUserInfo', userInfo);
        this.setState({ userInfo });
      }, (error) => {
        console.log(error);
      });
  }

  bottomClick = async () => {
    if (this.state.userInfo.isFriend) {
      PageFn.push(this.props.componentId, 'ChatSingle', { username: this.props.id });
    } else {
      try {
        const token = await Storage.load('token');
        const data = await friendInvatation({ token, friend: this.props.id });
        if (data.status === 'success') {
          JMessage.sendInvitationRequest({ username: this.props.id, reason: '请求添加好友' },
            () => {
              Toast.success('请求发送成功');
            }, (error) => {
              console.log(error);
            });
        }
      } catch (error) {
        console.log(error);
      }
      //
    }

  }

  render() {
    const { userInfo } = this.state;

    console.log('userInfo==',userInfo)

    return (
      <View style={{ flex: 1 }}>
       {userInfo &&<Image resizeMode="stretch" style={styles.topBg}  source={{uri:'file:'+userInfo.avatarThumbPath}} defaultSource = {require('../../assets/img/groupbg.png')}></Image>}
        <Flex onPress={() => { PageFn.pop(this.props.componentId) }} style={styles.backBox}><Icon style={styles.back} name="fanhui" /></Flex>
        {userInfo && <Flex direction="column" style={styles.header} align="center">
          {<Image style={styles.headImg} resizeMode="cover"  defaultSource = {require('../../assets/img/avatar.png')} source={{uri:'file:'+userInfo.avatarThumbPath}}/>}
          <CText size="36" color="#2C2C2C">{userInfo.nickname}</CText>
          <Flex style={styles.desc}>
            <Text style={styles.descText}>
              {userInfo.signature}
            </Text>
          </Flex>
        </Flex>}
        {userInfo && (this.props.id !== this.props.userInfo.jg_im_username)
          && <TouchableOpacity style={styles.btn} onPress={this.bottomClick}>
            <CText size="36" color="#fff">{userInfo.isFriend ? '发消息' : '加为好友'}</CText>
          </TouchableOpacity>}
      </View>
    );
  }

}

let styles = {
  topBg: {
    height: 449,
    width: 750,
  },
  backBox: {
    position: 'absolute',
    top: 65,
    left: 26,
    paddingRight: 20,
    paddingBottom: 20,
  },
  back: {
    color: '#fff',
    fontSize: 36,
  },
  header: {
    position: 'absolute',
    top: 333,
    width: 750,
  },
  headImg: {
    height: 194,
    width: 194,
    borderRadius: 97,
    marginBottom: 26,
  },
  desc: {
    width: 680,
    marginTop: 48,
  },
  descText: {
    color: '#646464',
    fontSize: 26,
    lineHeight: 40,
  },
  btn: {
    backgroundColor: '#2AB6F3',
    height: 106,
    width: 750,
    position: 'absolute',
    bottom: 0,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  }
};
styles = createStyles(styles);

export default connect((Store) => {
  return { userInfo: Store.UserRenderData.userInfo };
})(ChaterInfo);