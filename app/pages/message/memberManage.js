import React from 'react';
import { View, TextInput, FlatList, TouchableOpacity, Alert } from 'react-native';
import { Flex, Toast } from 'antd-mobile-rn';
import CText from '@/components/CText';
import { createStyles, isIphoneX } from '@/utils/view';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '@/utils/page';
import JMessage from 'jmessage-react-plugin';
import MemberOption from './components/memberOption';
import { removeMember } from '@/service/group';
import Storage from '@/utils/storage';

export default class MemberManage extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      search: '',
      groupMember: [],
      checkMember: {},
    };
    this.checkStatus = true;
    this.checkAll = this.checkAll.bind(this);
    this.memberCheck = this.memberCheck.bind(this);
  }

  componentDidMount() {
    try {
      JMessage.getGroupMembers({ id: this.props.jid },
        (groupMemberInfoArray) => {  // 群成员数组
          this.setState({ groupMember: groupMemberInfoArray });
        }, (error) => {
          console.log('error', error);
        });
    } catch (error) {
      console.log(error);
    }
  }

  checkAll = () => {
    this.checkStatus = !this.checkStatus;
    let tempGroupMember = JSON.parse(JSON.stringify(this.state.groupMember));
    let tempCheckMember = {};
    if (this.checkStatus) {
      for (const member of tempGroupMember) {
        member.active = false;
      }
    } else {
      for (const member of tempGroupMember) {
        member.active = true;
        tempCheckMember[member.user.username] = true;
      }
    }
    this.setState({ groupMember: tempGroupMember, checkMember: tempCheckMember });
  }

  memberCheck(index) {
    let tempGroupMember = this.state.groupMember.slice();
    let tempMember = { ...tempGroupMember[index] };
    // let tempGroupMember = JSON.parse(JSON.stringify(this.state.groupMember));
    let tempCheckMember = { ...this.state.checkMember };
    // 如果是选中状态则取消选中
    if (tempMember.active) {
      tempMember.active = false;
      delete tempCheckMember[tempMember.user.username];
    } else {
      tempMember.active = true;
      tempCheckMember[tempMember.user.username] = true;
    }
    tempGroupMember[index] = tempMember;
    this.setState({ groupMember: tempGroupMember, checkMember: tempCheckMember });
  }

  // 删除群成员
  isDeleteMember = () => {
    if (Object.keys(this.state.checkMember).length === 0) {
      return Toast.fail('没有选中成员', 1.5);
    }
    Alert.alert(
      '你确定要删除该群成员吗？',
      '',
      [
        { text: '取消', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        {
          text: '确定', onPress: () => {
            this.removeMember();
          }
        },
      ],
      { cancelable: false }
    );
  }

  async removeMember() {
    const token = await Storage.load('token');
    let memberKeys = Object.keys(this.state.checkMember);
    let removeMembers = [];
    for (const memberName of memberKeys) {
      if (memberName) {
        removeMembers.push(memberName);
      }
    }
    Toast.loading('删除中', 8);
    removeMember({ token, crowd_jg_id: this.props.jid, member_jg_id: removeMembers }).then(() => {
      JMessage.getGroupMembers({ id: this.props.jid },
        (groupMemberInfoArray) => {  // 群成员数组
          console.log('groupMemberInfoArray', groupMemberInfoArray);
          Toast.hide();
          this.setState({ groupMember: groupMemberInfoArray });
        }, (error) => {
          Toast.hide();
          console.log('error', error);
        });
    }).catch(() => { Toast.hide() });
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <Flex direction="row" align="center" style={styles.header}>
          <Icon onPress={() => PageFn.pop(this.props.componentId)} name="fanhui" style={styles.headerIcon} />
          <TextInput
            style={styles.search}
            onChange={(text) => { this.setState({ search: text }) }}
            value={this.state.search}
            underlineColorAndroid="transparent"
            placeholder="搜索"
          />
        </Flex>
        <FlatList
          data={this.state.groupMember}
          renderItem={({ item, index }) => {
            if (item.memberType !== 'owner') {
              return <MemberOption memberCheck={this.memberCheck} data={item} index={index} />;
            }
          }}
          keyExtractor={item => item.user.username}
          ItemSeparatorComponent={() => <View style={styles.separator}></View>}
        />
        <Flex style={styles.bottom} justify="space-between" align="center">
          <TouchableOpacity onPress={this.checkAll} style={styles.bottomBtn}><CText size="36" color="#00BAFF">全 选</CText></TouchableOpacity>
          <TouchableOpacity onPress={this.isDeleteMember} style={styles.bottomBtn}><CText size="36" color="#00BAFF">删 除</CText></TouchableOpacity>
          <TouchableOpacity style={styles.bottomBtn} onPress={() => PageFn.pop(this.props.componentId)}><CText size="36" color="#00BAFF">取 消</CText></TouchableOpacity>
        </Flex>
      </View>
    );
  }}

let styles = {
  wrapper: { flex: 1 },
  header: {
    height: 90,
    marginTop: isIphoneX ? 60 : 30,
    borderBottomWidth: 1,
    borderColor: '#DEDEDE',
    paddingHorizontal: 26,
  },
  headerIcon: {
    fontSize: 32,
    color: '#1D1D1D',
  },
  search: {
    width: 580,
    height: 60,
    borderRadius: 30,
    backgroundColor: '#F0F0F0',
    marginLeft: 40,
    paddingHorizontal: 30,
    paddingVertical: 0
  },
  separator: {
    width: 572,
    height: 1,
    backgroundColor: '#E4E4E4',
    marginLeft: 178,
  },
  bottom: {
    height: 98,
    paddingHorizontal: 56
  },
  bottomBtn: {
    width: 178,
    height: 70,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: '#00BAFF',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  }
};
styles = createStyles(styles);