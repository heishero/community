import React from 'react';
import { View, ScrollView, Text, Image } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '@/components/CText';
import { createStyles, Rem } from '@/utils/view';
import NormalHeader from '@/components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '@/utils/page';
import Storage from '@/utils/storage';
import HTMLView from 'react-native-htmlview';
import { fetchSystemDetail } from '@/service/system';

class SystemInfo extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      info: null
    };
  }

  componentDidMount() {
    this.init();
  }

  async init() {
    try {
      const token = await Storage.load('token');
      const { data } = await fetchSystemDetail({ token, id: this.props.id });
      this.setState({
        info: data
      });
    } catch (error) {
      console.log(error);
    }
  }

toPage = () => {
  PageFn.push(this.props.componentId, 'CommentList', { id: this.props.id });
}

renderNode(node, index, siblings, parent, defaultRenderer) {
  if (node.name === 'img') {
    const a = node.attribs;
    return (<Image key={index} style={{ width: Rem(750), height: Rem(300), padding: 0, margin: 0 }} source={{ uri: a.src }} />);
  }
}


render() {
  const { info } = this.state;
  if (!info) return null;
  return (
    <View style={styles.wrapper}>
      <NormalHeader
        leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
        onLeftPress={() => { PageFn.pop(this.props.componentId) }}
      >文章</NormalHeader>
      <ScrollView>
        <Flex justify="center">
          <View style={{ fontSize: Rem(42), fontWeight: 'bold', marginLeft: Rem(12), marginRight: Rem(12), marginTop: Rem(30) }}>
            {info.title && <Text style={{ fontSize: Rem(36), fontWeight: 'bold', marginLeft: Rem(12), marginRight: Rem(12), marginBottom: 15 }}>{info.title}</Text>}
          </View>
        </Flex>
        <Flex justify="space-between" align="center" style={{ marginHorizontal: Rem(27) }}>
          <CText size="24" color="#7F7F7F">上传者：平台</CText>
          <CText size="24" color="#7F7F7F">{info.updated_at}</CText>
        </Flex>
        <View style={{ height: this.state.height, marginLeft: Rem(27), marginRight: Rem(27), marginTop: Rem(30) }}>
          {info.content && <HTMLView value={info.content} renderNode={this.renderNode} />}
        </View>
      </ScrollView>
    </View>
  );
}}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#F7F7F7' },
  headerIcon: {
    fontSize: 32,
    color: '#000',
    paddingHorizontal: 10
  },
};
styles = createStyles(styles);

export default SystemInfo;