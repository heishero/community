import React from 'react';
import {
  View,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import JMessage from 'jmessage-react-plugin';
import { createStyles, Rem } from '../../utils/view';
import PageFn from '../../utils/page';
import { convertGroupId } from '../../service/group';
import Storage from '../../utils/storage';
import Chat from './chat';
import Shop from '../shop/shop';
import Article from '../article/article';
import Activity from '../activity/activity';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import CustomeTabBar from '@/components/customeTabBar';

export default class ChatGroup extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      },
      topBar: {
        drawBehind: true,
        visible: false
      },
      // statusBar: {
      //   drawBehind: false,
      // },
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      title: '',
      groupId: null,
      menuActive: 0,
      locked: false,
      isJoin: 1,
      introduce: ''
    };
    this.setLocked = this.setLocked.bind(this);
    Navigation.events().bindComponent(this);
  }

  componentDidMount() {
    // 进入聊天不再接受当前会话的消息通知
    JMessage.enterConversation({ type: 'group', groupId: this.props.id },
      (conversation) => {
        // do something.
      }, (error) => {
        console.log(error);
      });
  }

  componentWillUnmount() {
    JMessage.exitConversation();
  }

  componentDidAppear() {
    this.init();
  }

  // 查看群信息
  async init() {
    try {
      const token = await Storage.load('token');
      const { data } = await convertGroupId({ token, jg_im_gid: this.props.id });
      this.setState({
        groupId: data.id,
        title: `${data.name}(${data.member})`,
        menuActive: this.props.menuActive ? this.props.menuActive : 0,
        isJoin: data.is_join,
        introduce: data.introduce
      });
    } catch (error) {

    }
  }

  toShop = () => {
    if (this.state.groupId) {
      PageFn.push(this.props.componentId, 'Shop', { id: this.state.groupId });
    }
  }

  toDynamic = () => {
    PageFn.push(this.props.componentId, 'Activity', { id: this.state.groupId });
  }

  toJieshao = () => {
    PageFn.push(this.props.componentId, 'CircleDetail', { id: this.state.groupId });
  }

  toStting = () => {
    if (this.state.isJoin) {
      PageFn.push(this.props.componentId, 'GroupSetting', { id: this.state.groupId, jid: this.props.id });
    } else {
      PageFn.push(this.props.componentId, 'CircleDetail', { id: this.state.groupId, jg_im_gid: this.props.id, introduce: this.state.introduce });
    }

  }

  setLocked(status) {
    this.setState({ locked: status });
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <NormalHeader
          backgroundColor="#EDEDED"
          leftContent={<Icon style={styles.back} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}
          rightContent={<Icon style={styles.setting} name="shezhi" />}
          onRightPress={() => { this.toStting() }}
        >{this.state.title}
        </NormalHeader>
        {!!this.state.title && <ScrollableTabView
          ref={(e) => { this.scrollTab = e }}
          tabBarUnderlineStyle={{ height: 0 }}
          tabBarActiveTextColor="#000"
          tabBarInactiveTextColor="#8C8C8C"
          tabBarBackgroundColor="#E8E8E8"
          tabBarTextStyle={{ fontSize: Rem(30) }}
          locked={this.state.locked}
          renderTabBar={() =>
            <CustomeTabBar />}>
          <Chat isJoin={this.state.isJoin} locked={this.state.locked} setLocked={this.setLocked} componentId={this.props.componentId} type="group" tabLabel="互动" id={this.props.id} />
          {!!this.state.isJoin && <Shop tabLabel="福利" id={this.state.groupId} componentId={this.props.componentId} />}
          {!!this.state.isJoin && <Activity tabLabel="活动" id={this.state.groupId} componentId={this.props.componentId} />}
          {!!this.state.isJoin && <Article tabLabel="文化" id={this.state.groupId} componentId={this.props.componentId} />}
        </ScrollableTabView>}
      </View>
    );
  }
}

let styles = {
  back: {
    color: '#000',
    fontSize: 36,
    paddingRight: 100,
  },
  setting: {
    fontSize: 36,
    color: '#3E3E3E',
    paddingLeft: 100,
  },
  menuWrapper: {
    height: 88,
    backgroundColor: '#E8E8E8',
  },
  menuItem: {
    flex: 1,
  },
  menuText: {
    fontSize: 30,
  }
};
styles = createStyles(styles);