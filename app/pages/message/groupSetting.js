import React from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  ImageBackground,
  Switch,
  Modal,
  TouchableOpacity
} from 'react-native';
import { Flex, Toast } from 'antd-mobile-rn';
import Icon from 'react-native-vector-icons/icomoon';
import JMessage from 'jmessage-react-plugin';
import { createStyles, Rem, isIphoneX } from '@/utils/view';
import PageFn from '@/utils/page';
import CText from '@/components/CText';
import WindBlank from '@/components/WingBlank';
import Storage from '@/utils/storage';
import { getGroupDetail, outGroup, setNodisturb, removeNodisturb } from '@/service/group';
import CirclePriceFlag from '../circle/components/circlePriceFlag';
import { connect } from 'react-redux';
import { Navigation } from 'react-native-navigation';

class GroupSetting extends React.Component {

  static options(passProps) {
    return {
      statusBar: {
        style: 'light'
      },
    };
  }

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
    this.state = {
      groupDetail: {},
      groupMember: [],
      nickName: '',
      isNoDisturb: false,
      modalVisible: false,
    };
  }

  componentDidAppear() {
    this.init();
  }

  init = async () => {
    console.log('init');
    try {
      const token = await Storage.load('token');
      // 获取群信息
      let { data } = await getGroupDetail({ id: this.props.id, token });
      this.setState({ groupDetail: data });

      // 获取极光群信息
      JMessage.getGroupInfo(
        { id: this.props.jid },
        (result) => {
          this.setState({ isNoDisturb: result.isNoDisturb });
          console.log('jgroupInfo', result);
        }, (error) => {
          /**
           * error {Object} {code:Number,desc:String}
           */
        }
      );
      // 获取群成员信息
      JMessage.getGroupMembers({ id: this.props.jid },
        (groupMemberInfoArray) => {  // 群成员数组
          let nickName = this.props.userInfo.nick_name;
          for (const info of groupMemberInfoArray) {
            if (info.user.username === this.props.userInfo.jg_im_username) {
              nickName = info.groupNickname;
            }
            groupMemberInfoArray.map((info, index) => {
              // 下载头像
              JMessage.downloadThumbUserAvatar({ username: info.user.username, appKey: '9fea82812ba31bdd5fa7fd7b' },
                (result) => { info.user.avatarThumbPath = result.filePath },
                (err) => { console.log('下载用户头像失败:', err) });
            });
          }
          this.setState({ groupMember: groupMemberInfoArray, nickName });
        }, (error) => {
          console.log('error', error);
        });
    } catch (error) {

    }
  }

  outGroup = async () => {
    try {
      const token = await Storage.load('token');
      await outGroup({ crowd_id: this.props.id, token });
      this.setState({ modalVisible: false });
      Toast.success('退出成功');
      PageFn.popToRoot(this.props.componentId);
    } catch (error) {
    }
  }

  // 设置消息免打扰
  setNoDisturb = async (isNoDisturb) => {
    const token = await Storage.load('token');
    try {
      if (this.state.isNoDisturb) {
        this.setState({ isNoDisturb: false });
        await removeNodisturb({ token, crowd_id: this.props.id });
      } else {
        this.setState({ isNoDisturb: true });
        await setNodisturb({ token, crowd_id: this.props.id });
      }
    } catch (error) {
      this.setState({ isNoDisturb: isNoDisturb });
    }
  }

  // 头部banner
  header = () => (
    <ImageBackground style={styles.headBg} resizeMode="stretch" source={{ uri: this.state.groupDetail.crowd.headimgurl }}>
      <Flex style={styles.header} justify="space-between" align="center">
        <Icon onPress={() => { PageFn.pop(this.props.componentId) }} style={styles.headIcon} name="fanhui" />
        <CText size="36" color="#fff">圈子设置</CText>
        {this.state.groupDetail.crowd.login_user_is_admin ?
          <Icon style={styles.headIcon_right} onPress={() => { PageFn.push(this.props.componentId, 'MemberManage', { jid: this.props.jid }) }} name="shanchu" /> :
          <Icon style={styles.headIcon_right} onPress={() => { this.setState({ modalVisible: true }) }} name="tuichuqun" />
        }
      </Flex>
    </ImageBackground>
  )

  // 群信息
  info = () => (
    <WindBlank>
      <Flex style={styles.info} justify="space-between" align="center">
        <View>
          <View style={{ marginBottom: Rem(26) }}><CText size="48" color="#3E3E3E">{this.state.groupDetail.crowd.name}</CText></View>
          <CText size="28" color="#828282">{this.state.groupDetail.crowd.userData.nick_name}...等{this.state.groupDetail.crowd.member}人已参加</CText>
        </View>
        <CirclePriceFlag style={styles.flag} price={this.state.groupDetail.crowd.charge} size="28" />
      </Flex>
    </WindBlank>
  )

  // 群成员
  member = () => (
    <Flex style={styles.member}>
      {this.state.groupMember.map((data) => (
        <TouchableOpacity style={{ width: '20%' }} onPress={() => { PageFn.push(this.props.componentId, 'ChaterInfo', { id: data.user.username }) }} key={data.user.username} >
          <Flex direction="column" justify="center" align="center">
            {data.user.avatarThumbPath ? <Image style={styles.memberImg} source={{ uri: 'file:' + data.user.avatarThumbPath }} /> : <View style={styles.memberImg} />}
            <Text style={styles.memberName} numberOfLines={1}>{data.groupNickname || data.user.nickname}</Text>
          </Flex>
        </TouchableOpacity>
      ))}
    </Flex>
  )

  closeModal = () => {
    this.setState({ modalVisible: false });
  }

  render() {
    const { groupMember, nickName, isNoDisturb } = this.state;
    const { crowd } = this.state.groupDetail;
    if (!this.state.groupDetail.crowd) return null;
    return (
      <ScrollView style={styles.wrapper}>
        {this.header()}
        {crowd && this.info()}
        <View style={styles.dash}></View>
        {groupMember.length > 0 && this.member()}
        <View style={styles.dash}></View>
        <WindBlank>
          <Flex style={styles.row} onPress={() => { PageFn.push(this.props.componentId, 'ChangeNickName', { jid: this.props.jid, username: this.props.userInfo.jg_im_username }) }} justify="space-between" align="center">
            <CText color="#2C2C2C" size="30">我在圈子的昵称</CText>
            <CText color="#828282" size="30">{nickName}</CText>
          </Flex>
        </WindBlank>
        <WindBlank>
          <Flex style={styles.row} justify="space-between" align="center">
            <CText color="#2C2C2C" size="30">消息免打扰</CText>
            <Switch onValueChange={this.setNoDisturb} value={isNoDisturb} style={styles.switch} />
          </Flex>
        </WindBlank>
        {/* 退出群modal */}
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={this.closeModal}
        >
          <View style={styles.modalWrapper}>
            <View style={styles.modal}>
              <Flex style={styles.modalTitle}>
                <CText color="#1C1C1C" size="30">您确定要退出该群吗？</CText>
              </Flex>
              {crowd && <Flex style={styles.modalInfo}>
                <Image resizeMode="cover" style={styles.modalImg} source={{ uri: crowd.headimgurl }} />
                <Flex style={styles.modalContent} direction="column" justify="space-between" align="flex-start">
                  <CText size="30" color="#262626" fontWeight="700">{crowd.name}</CText>
                  <CText size="24" color="#767676">群介绍：{crowd.detailed}</CText>
                  <Flex>
                    <View style={styles.modalTag}><CText color="#C2C2C2" size="22">{crowd.member}人</CText></View>
                    <View style={styles.modalTag}><CText color="#C2C2C2" size="22">{crowd.goods}件</CText></View>
                  </Flex>
                </Flex>
              </Flex>}
              <Flex style={styles.modalBottom}>
                <TouchableOpacity onPress={this.closeModal} style={styles.modalBtn}><CText size="30" color="#3A3A3A">取消</CText></TouchableOpacity>
                <TouchableOpacity onPress={this.outGroup} style={[styles.modalBtn, { backgroundColor: '#F11515', borderBottomRightRadius: Rem(20) }]}><CText size="30" color="#fff">退群</CText></TouchableOpacity>
              </Flex>
            </View>
          </View>
        </Modal>
      </ScrollView>
    );
  }
}

let styles = {
  wrapper: {
    flex: 1,
  },
  // head
  headIcon: {
    color: '#fff',
    fontSize: 39,
    paddingRight: 100,
  },
  headIcon_right: {
    color: '#fff',
    fontSize: 39,
    paddingLeft: 100,
  },
  headBg: {
    ios: {
      marginTop: isIphoneX ? -88 : 0
    },
    height: 430,
  },
  header: {
    height: 90,
    paddingHorizontal: 26,
    ios: {
      marginTop: isIphoneX ? 60 : 30
    },
    android: {
      marginTop: 30
    }
  },
  // info
  info: {
    height: 173,
  },
  flag: {
    width: 140,
    height: 56,
    borderRadius: 20,
  },
  dash: {
    height: 20,
    backgroundColor: '#F2F2F2'
  },
  // member
  member: {
    flexWrap: 'wrap',
    marginBottom: 50,
  },
  memberImg: {
    height: 108,
    width: 108,
    borderRadius: 10,
    marginBottom: 20,
    marginTop: 26,
    backgroundColor: '#f5f5f5'
  },
  memberName: {
    fontSize: 24,
    color: '#8A898A',
  },
  // 底部row
  row: {
    height: 90,
    borderBottomWidth: 1,
    borderColor: '#F2F2F2',
  },
  switch: {
    width: 88,
    height: 46,
  },
  // 退出modal
  modalWrapper: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,.5)'
  },
  modal: {
    width: 665,
    height: 415,
    backgroundColor: '#fff',
    borderRadius: 26,
  },
  modalTitle: {
    height: 99,
    borderBottomWidth: 8,
    paddingHorizontal: 34,
    borderColor: '#E9E9E9',
    marginBottom: 26,
  },
  modalInfo: {
    paddingHorizontal: 34,
    flex: 1,
  },
  modalImg: {
    height: 184,
    width: 138,
    marginRight: 23,
  },
  modalContent: {
    height: 184,
    flex: 1,
    paddingVertical: 8,
  },
  modalTag: {
    width: 90,
    height: 40,
    borderRadius: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#C2C2C2',
    marginRight: 30,
  },
  modalBottom: {
    height: 100,
  },
  modalBtn: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
  }
};
styles = createStyles(styles);
export default connect((Store) => {
  return { userInfo: Store.UserRenderData.userInfo };
})(GroupSetting);