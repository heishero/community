import React from 'react';
import { View, Image,  } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import { Navigation } from 'react-native-navigation';
import CText from '../../components/CText';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import { createStyles, Rem } from '../../utils/view';
import { PageFn } from '../../utils/page';

class ActivitySuccess extends React.Component {
  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.back} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}
        >提交成功</NormalHeader>
        <Flex direction="column" justify="center" align="center" style={{ flex: 1 }}>
          <Flex direction="column" justify="center" align="center">
            <Image
              source={require('./img/success.png')}
              style={{ width: Rem(140), height: Rem(140) }}
            />
          </Flex>
          <Flex direction="column" justify="center" align="center" style={{ marginTop: Rem(58) }}>
            <CText color="#FDB64A" size="38" >提交成功</CText>
          </Flex>
          <Flex direction="column" justify="center" align="center" style={{ marginTop: Rem(19) }}>
            <CText color="black" size="24" style={{ marginTop: Rem(218) }} >活动订单提交成功，请静静等待活动的开始</CText>
          </Flex>
        </Flex>
      </View>
    );
  }
}
let styles = {
  back: {
    color: '#000',
    fontSize: 32,
  },
  fabiaoIcon: {
    fontSize: 45,
  },
  wrapper: {
    flex: 1,
    backgroundColor: '#fff'
  },
};
styles = createStyles(styles);
export default ActivitySuccess;