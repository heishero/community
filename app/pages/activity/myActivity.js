import React from 'react';
import { RefreshControl,View, Image, ImageBackground, ScrollView, Text,TouchableOpacity} from 'react-native';
import { Flex, } from 'antd-mobile-rn';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import { createStyles, Rem } from '../../utils/view';
import Storage from '../../utils/storage';
import { PageFn } from '../../utils/page';
import { Navigation } from 'react-native-navigation';
import CText from '../../components/CText';
import { myActivity } from '../../service/activity';

class MyActivity extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }
  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
    this.state = {
      activity_list: [],
      loadingState:false,
    };
  }
  componentDidMount() {
    this.init();
  }
  // componentDidAppear() {
  //   // BackHandler.addEventListener('hardwareBackPress', this.onBackAndroid);
  //   this.init();
  // }
  _onRefresh = () => {
      this.init();
  }

  redItemCell=(item,index)=>{
    return(
      <TouchableOpacity activeOpacity={0.8 } key={index} onPress={() => { PageFn.push(this.props.componentId, 'ActivityDetail', { id: item.id,value:1 }) }}>
      <View style={{paddingBottom:10,marginTop:10,marginLeft:10,marginRight:10,borderRadius:10,backgroundColor:'#FCFAFA',shadowOffset: {width: 0, height: 0},shadowColor: '#666',
       shadowOpacity:0.5,overflow:'hidden'}}>
      <Image  style={styles.image} source={{ uri: item.imgurl}} resizeMode="stretch" ></Image>
      <View style={{marginLeft:10}}>
      <Text style={{marginTop:10,fontSize:18,color:'black'}}>{item.title}</Text>
      <Text style={{marginTop:10,fontSize:14,color:'#616161'}}>地点:{item.address}</Text>
      <Text style={{marginTop:10,fontSize:14,color:'#616161'}}>活动时间:{item.start_time}—{item.end_time}</Text>
       </View>
       </View>
      </TouchableOpacity>
    )
  }

  init = async () => {
    this.setState({
      loadingState:true
    })
    const token = await Storage.load('token');
    const data = await myActivity({ token });
    this.setState({ activity_list: data.data,loadingState:false});
  }

  render() {



    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.back} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}
        >我的活动</NormalHeader>
        {/* <Flex justify="start" align="start" style={{marginBottom: Rem(10) }}> */}
          <ScrollView
          
          refreshControl={
            <RefreshControl
              refreshing={this.state.loadingState}
              onRefresh={this._onRefresh}
            />
          }
          >
            {
              this.state.activity_list.length > 0 && this.state.activity_list.map((item, index) => (

    
                this.redItemCell(item,index)
                // <Flex key={index} style={styles.item} direction="column" justify="start" align="start">
                //   <View style={{ marginTop: Rem(20)}}>
                //     <ImageBackground source={require('./img/bg4.png')} style={{marginLeft:Rem(20),height:Rem(374),width:Rem(680)}} >
                //       {/* <CText size="20" color="#8B8B8B" style={{ paddingBottom: Rem(20) }}>{item.name}</CText> */}
                //       <Flex direction="column"  onPress={() => { PageFn.push(this.props.componentId, 'ActivityDetail', { id: item.id, value: 1 }) }} >
                //         <Image style={styles.img} source={{ uri: item.imgurl }} />
                //         <Flex direction="column" align="start" justify="start" style={{ marginLeft: Rem(15), marginTop: Rem(20) }}>
                //           <Text style={{ fontSize: Rem(28), color: 'black', fontWeight: '400', width: Rem(490) }} numberOfLines={1} >{item.title}</Text>
                //           <CText style={{marginTop:Rem(10),}} color="#FF8D27" size="24">活动时间：{item.start_time}—{item.end_time}</CText>
                //           <Text style={{ color: '#616161', fontSize: Rem(24), width: Rem(490) }} numberOfLines={1} >地点:{item.address}</Text>
                //           {
                //             item.charge === 0 ? (<Text></Text>) : (
                //               <Flex direction="row" justify="between" align="baseline" style={{ width: Rem(490) }}>
                //                 <CText color="#FF8E32" size="24">￥{item.charge}</CText>
                //                 <CText color="#979797" size="24">x1</CText>
                //               </Flex>
                //             )  
                //           }
                //         </Flex>
                //       </Flex>
                //     </ImageBackground>
                //   </View>
                // </Flex>
              ))
            }
          </ScrollView>
        {/* </Flex> */}
      </View>
    );
  }
}

let styles = {
  image:{
   height:242,
  },
  back: {
    color: '#000',
    fontSize: 32,
  },
  fabiaoIcon: {
    fontSize: 45,
  },
  wrapper: {
    flex: 1,
    backgroundColor: '#fff'
  },
  item: {
    marginLeft: 10,
    marginRight: 10,
    height: 240,
    borderRadius: 20,
    marginTop: 10,
    marginBottom: 30
  },
  img: {
    width:Rem(750),
    height: 144,
    borderRadius: 20,
    backgroundColor: '#8B8B8B'
  },

};

styles = createStyles(styles);
export default MyActivity;