import React from 'react';
import { View, Image,  } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import { Navigation } from 'react-native-navigation';
import CText from '../../components/CText';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import { createStyles, Rem } from '../../utils/view';
import { PageFn } from '../../utils/page';

class ActivityFailed extends React.Component {
  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.back} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}
        >提交失败</NormalHeader>
        <Flex direction="column" justify="center" align="center" style={{ flex: 1 }}>
          <Image
            style={{ width: Rem(140), height: Rem(140), }}
            source={require('./img/failed.png')}
          />
          <CText color="#FDB64A" size="38" style={{ marginTop: Rem(63), }}>提交失败</CText>
          <CText color="#9D9D9D" size="28" style={{ marginTop: Rem(18) }} >活动订单提交失败，请检查网络连接</CText>
        </Flex>
      </View>
    );
  }
}
let styles = {
  back: {
    color: '#000',
    fontSize: 32,
  },
  fabiaoIcon: {
    fontSize: 45,
  },
  wrapper: {
    flex: 1,
    backgroundColor: '#fff'
  },
};
styles = createStyles(styles);
export default ActivityFailed;