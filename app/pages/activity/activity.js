import React from 'react';
import { View, Image, FlatList, Text, TouchableOpacity } from 'react-native';
import { Flex, } from 'antd-mobile-rn';
import { createStyles, Rem } from '../../utils/view';
import Storage from '../../utils/storage';
import { PageFn } from '../../utils/page';
import CText from '../../components/CText';
import { activityList } from '../../service/activity';

class Activity extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      activityList: [],
      loading: false
    };
  }

  componentDidMount() {
    this.init();
  }

  init = async () => {
    const token = await Storage.load('token');
    const data = await activityList({ token: token, crowd_id: this.props.id, loading: true });
    this.setState({ activityList: data.data, loading: false });
  }

  renderItem = ({ item }) => (
    <TouchableOpacity style={styles.item} key={item.id} onPress={() => {
      PageFn.push(this.props.componentId, 'ActivityDetail', { id: item.id });
    }}>
      <Image style={styles.img} resizeMode="stretch" source={{ uri: item.imgurl }} />
      <View style={styles.content}>
        <View style={{ marginTop: Rem(14) }}><Text style={{ color: 'black', fontSize: Rem(28), width: Rem(550), lineHeight: Rem(30) }} numberOfLines={1}>{item.title}</Text></View>
        <Flex style={{ marginTop: Rem(7) }} justify="space-between">
          <Text style={{ color: '#616161', fontSize: Rem(22), lineHeight: Rem(24) }}>地点  {item.address}</Text>
          <View><CText color="#616161" size="22">{item.start_time}</CText></View>
        </Flex>
      </View>
    </TouchableOpacity>
  )


  render() {
    return (
      <View style={styles.wrapper}>
        <FlatList
          data={this.state.activityList}
          keyExtractor={item => JSON.stringify(item.id)}
          renderItem={this.renderItem}
          refreshing={this.state.loading}
          onRefresh={this.init}
        />
      </View>
    );
  }
}
let styles = {
  wrapper: {
    flex: 1,
    backgroundColor: '#fff'
  },
  item: {
    marginTop: 26,
    borderRadius: 20,
    borderWidth: 2,
    borderColor: '#EEEEEE',
    marginLeft: 20,
    marginRight: 20,
    display: 'flex',
    flexDirection: 'column'
  },
  img: {
    height: 242,
    width: 710,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
  },
  content: {
    height: 101,
    width: 710,
    paddingHorizontal: 24,
  },
};

styles = createStyles(styles);

export default Activity;