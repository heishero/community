import React from 'react';
import { View, ScrollView, } from 'react-native';
import { Flex, Toast } from 'antd-mobile-rn';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import { createStyles, Rem } from '../../utils/view';
import Storage from '../../utils/storage';
import { PageFn } from '../../utils/page';
import { Navigation } from 'react-native-navigation';
import CText from '../../components/CText';
import { activityDetail, activityTake } from '../../service/activity';
import HTMLView from 'react-native-htmlview';
import Input from 'antd-mobile-rn/lib/input-item/Input.native';
import PayModal from '@/pages/common/payModal';
import * as WeChat from 'react-native-wechat';
import { getWxPay } from '@/service/shop';

class ActivityDetail extends React.Component {
  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
    this.state = {
      activityDetail: {},
      mobile: '',
      name: '',
      token: '',
    };
    this.payModal = null;
  }

  componentDidAppear() {
    this.init();
  }

  init = async () => {
    try {
      const token = await Storage.load('token');
      const data = await activityDetail({ token, id: this.props.id });
      this.setState({ activityDetail: data.data, token });
    } catch (error) {

    }
  }

  submit = async () => {
    const token = await Storage.load('token');
    let myreg = /^[1][3,4,5,7,8][0-9]{9}$/;
    const { mobile, name } = this.state;
    if (name === '') {
      Toast.fail('名字不能为空');
      return;
    }
    if (mobile === '') {
      Toast.fail('手机号不能为空');
      return;
    }
    if (!myreg.test(mobile)) {
      Toast.fail('请输入正确的手机号');
      return;
    }
    Toast.loading('请等待', 8);
    activityTake({ token, mobile, name, activity_id: this.props.id }).then(result => {
      Toast.hide();
      if (result.status === 'success') {
        // 如果返回有订单id则进行支付
        if (result.data.order_id && this.state.activityDetail.charge > 0) {
          this.wxPay(result.data.order_id);
        }
        // PageFn.push(this.props.componentId, 'ActivitySuccess');
      }
      else {
        Toast.fail(result.failedMsg);
      }
    }).catch(err => {
      Toast.fail(err);
    });
  }

  wxPay = async (order_id) => {
    const token = await Storage.load('token');
    let wxInfo = await getWxPay({ token, order_id: order_id, type: 'activity' });
    this.setState({
      wxPayInfo: wxInfo.data,
    });
    console.log(wxInfo);
    if (this.payModal && wxInfo) {
      this.payModal.show();
    }
  }

  pay = async () => {
    const { wxPayInfo } = this.state;
    if (wxPayInfo) {
      WeChat.isWXAppInstalled()
        .then((isInstalled) => {
          if (isInstalled) {
            WeChat.pay({
              partnerId: wxPayInfo.partnerid,
              prepayId: wxPayInfo.prepayid,
              nonceStr: wxPayInfo.noncestr,
              timeStamp: wxPayInfo.timestamp,
              package: wxPayInfo.package,
              sign: wxPayInfo.sign,
            }).then(result => {
              console.log('wxpay', result);
              this.payModal.dismiss();
              if (result.errCode === 0) {
                Toast.success('加入活动成功');
                this.init();
              } else {
                Toast.fail(result.errStr);
              }
            }).catch(err => { console.log(err) });
          } else {
            Toast.fail('请安装微信');
          }
        });
    }
  }

  render() {
    const { activityDetail } = this.state;
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.back} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}
        >活动详情</NormalHeader>
        <ScrollView style={{ flex: 1 }} ref={(view) => { this.scrollView = view }}>
          <Flex direction="column" justify="start" align="start" style={{ marginTop: Rem(20) }}>
            <Flex style={{ marginLeft: Rem(19), marginRight: Rem(19) }}>
              <CText color="#353535" size="32" fontWeight="400">{activityDetail.title}</CText>
            </Flex>
            <Flex style={{ marginLeft: Rem(27), marginRight: Rem(27), marginTop: Rem(54) }} direction="column" align="start" justify="start">
              <CText size="28" color="#3C3C3C">活动详情</CText>
              <CText size="24" color="#626262">活动时间：{activityDetail.start_time} - {activityDetail.end_time}</CText>
              <CText size="24" color="#626262">活动地点：{activityDetail.address}</CText>
              <CText size="24" color="#626262">活动人数：{activityDetail.take_num}</CText>
            </Flex>
            <View style={{ marginTop: Rem(36) }}>
              <HTMLView value={activityDetail.introduce} style={{ marginLeft: Rem(27), marginRight: Rem(27) }} />
            </View>
          </Flex>
          <View>
            <View style={styles.line} />
            <View style={{ flex: 1, width: Rem(698), padding: Rem(25), height: Rem(295), marginLeft: Rem(20), marginBottom: Rem(40) }} justify="center" align="center">
              <Flex direction="column" justify="start" align="start" style={{ flex: 1, marginTop: Rem(0) }}>
                <CText color="#515151" size="30" >报名信息</CText>
                <View style={styles.line2} />
                <Flex direction="row" align="center">
                  <Flex style={{ marginTop: 20 }}>
                    <CText size="28" color="#515151" >姓        名</CText>
                  </Flex>
                  <Input
                    placeholder={activityDetail.isTake === 2 ? activityDetail.name : ''}
                    editable={activityDetail.isTake === 2 ? false : true}
                    style={{ paddingBottom: 2, borderBottomWidth: Rem(2), marginTop: 10, borderBottomColor: '#E1E1E1', marginLeft: Rem(22), width: Rem(500) }}
                    value={this.state.name}
                    onChangeText={(Text) => {
                      this.setState({
                        name: Text
                      });
                    }}
                  />
                </Flex>

                <Flex direction="row" align="center" >
                  <Flex style={{ marginTop: 20 }}>
                    <CText size="28" color="#515151">手机号码</CText>
                  </Flex>
                  <Input
                    editable={activityDetail.isTake === 2 ? false : true}
                    style={{ paddingBottom: 2, borderBottomWidth: Rem(2), marginTop: 10, borderBottomColor: '#E1E1E1', marginLeft: Rem(22), width: Rem(500) }}
                    placeholder={activityDetail.isTake === 2 ? activityDetail.mobile : ''}
                    value={this.state.mobile}
                    onChangeText={(Text) => {
                      this.setState({
                        mobile: Text
                      });
                    }}
                  />
                </Flex>
              </Flex>
            </View>
          </View>
          <View style={{ height: 50 }} />
        </ScrollView>

        <Flex style={{ height: Rem(90), backgroundColor: activityDetail.isTake === 2 ? '#E3E3E3' : '#2ab6f3' }} direction="row" justify="center" align="center" onPress={activityDetail.isTake === 2 ? null : this.submit}>
          <CText color="white" size="32" fontWeight="400">{activityDetail.isTake === 2 ? '已报名' : '立即报名'}</CText>
          <CText color="white" size="28" fontWeight="400">({activityDetail.charge}元)</CText>
        </Flex>

        {/* 支付 */}
        {activityDetail && <PayModal price={activityDetail.charge} pay={this.pay} title="立即报名" ref={(c) => { this.payModal = c }} />}
      </View>
    );
  }
}

let styles = {
  back: {
    color: '#000',
    fontSize: 32,
  },
  fabiaoIcon: {
    fontSize: 45,
  },
  wrapper: {
    flex: 1,
    backgroundColor: '#fff'
  },
  line: {
    height: 20,
    flex: 1,
    backgroundColor: '#F7F6F6',
    marginBottom: 20
  },
  line2: {
    height: 2,
    backgroundColor: '#E1E1E1',
    width: 634,
    marginLeft: 2,
    marginRight: 2,
    marginTop: 30
  }
};
styles = createStyles(styles);
export default ActivityDetail;