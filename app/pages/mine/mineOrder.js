import React from 'react';
import { RefreshControl, View, Image, Alert, FlatList, TouchableOpacity, Text } from 'react-native';
import { Flex, Toast } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles, Rem } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../utils/page';
import { Navigation } from 'react-native-navigation';
import { myOrder } from '../../service/security';
import Storage from '../../utils/storage';
import { postCancel } from '../../service/shop';

class MineOrder extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
    this.state = {
      orderList: [],
      cancelId: 0,
      cancelStatus: 0,
      loadingState: false,
    };
  }

  componentDidAppear() {
    this.init();
  }

  _onRefresh = () => {
    this.init();
  }
  init = async () => {
    this.setState({
      loadingState: true
    });
    const token = await Storage.load('token');
    const { data } = await myOrder({ token });
    this.setState({ orderList: data, loadingState: false });
  }
  // 状态
  GetState=(status) => {
    let stateString;
    switch (status) {
      case 0:
        stateString = '待支付';
        break;
      case 1:
        stateString = '确认收货';
        break;
      case 2:
        stateString = '确认收货';
        break;
      case 3:
        stateString = '评价';
        break;
      case 4:
        stateString = '已评价';
        break;
      case 5:
        stateString = '退款中';
        break;
      case 6:
        stateString = '已退款';
        break;
      case 7:
        stateString = '已取消';
        break;
      default:
        break;
    }
    return stateString;
  }
  renderItem = (item) => {
    return (
      <TouchableOpacity style={styles.itemWrapper} activeOpacity={0.8} onPress={() => { PageFn.push(this.props.componentId, 'MyOrderDetail', { order_id: item.id }) }}>
        <Flex style={styles.itemGoods}>
          <Image style={styles.goodsImg} source={{ uri: item.small_image == null ? '' : item.small_image }} />
          <Flex style={styles.goodsInfo} direction="row" justify="space-between" align="flex-start">
            <View style={styles.goodsTitle}><Text numberOfLines={2} style={styles.title}>{item.name}</Text></View>
            <Flex style={styles.goodsPrice} direction="column" align="flex-end">
              <CText color="#3E3E3E" size="28">¥{item.now_price}</CText>
              <CText color="#3E3E3E" size="24">x{item.buy_num}</CText>
            </Flex>
          </Flex>
        </Flex>
        <Flex style={styles.goodsNum} justify="flex-end" align="center">
          <CText color="#363636" size="24">共{item.buy_num}件商品 合计：¥{item.pay_total}（含运费 ¥{item.freight}）</CText>
        </Flex>
        <Flex justify="flex-end" align="center" style={styles.itemBottom}>
          {
            this.showItem(item.id, item.status, item.message, item)
          }
        </Flex>
      </TouchableOpacity>
    );
  }

  showItem(id, status, message, item) {
    let string = this.GetState(status);
    return (
      <Flex style={styles.itemStatus} justify="center" align="center" onPress={() => { status === 3 ? PageFn.push(this.props.componentId, 'GoodsComment', { goodsImformation: item }) : PageFn.push(this.props.componentId, 'MyOrderDetail', { order_id: item.id }) }}>
        <CText size="28" color="#646464">{string}</CText>
      </Flex>
    );
  }

  cancel = (id, status) => {
    this.setState({
      cancelId: id,
      cancelStatus: status
    });
    Alert.alert(
      '',
      '确定要取消该订单吗？',
      [
        {
          text: '确认', onPress: async () => {
            let id = this.state.cancelId;
            let status = this.state.cancelStatus;
            const token = await Storage.load('token');
            console.log(id, status, token);
            await postCancel({ token, order_id: id, status });
            this.init();
          }
        },
        { text: '取消', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
      ],
      { cancelable: false }
    );
  }

  render() {
    const { orderList } = this.state;
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}
        >
          我的订单
        </NormalHeader>
        <Flex direction="column" align="center" style={{ marginBottom: Rem(100) }}>
          {orderList.length > 0 &&
          <FlatList
            refreshControl={
              <RefreshControl
                refreshing={this.state.loadingState}
                onRefresh={this._onRefresh}
              />}
            data={orderList}
            renderItem={({ item }) => this.renderItem(item)}
            keyExtractor={(item) => item.created_at} >
          </FlatList>
          }
        </Flex>
      </View>
    );
  }}
let styles = {
  wrapper: { flex: 1, backgroundColor: '#F7F7F7' },
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
  itemWrapper: {
    width: 698,
    // height: 415,
    paddingHorizontal: 20,
    paddingTop: 30,
    backgroundColor: '#fff',
    marginTop: 20,
    borderRadius: 20,
  },
  itemTitle: {
    marginVertical: 16,
  },
  itemGoods: {
    height: 163,
  },
  goodsImg: {
    width: 202,
    height: 163,
    marginRight: 12,
    backgroundColor: '#F7F7F7',
    borderRadius: 20,
  },
  goodsInfo: {
    flex: 1,
    height: 181,
  },
  goodsTitle: {
    flex: 1,
  },
  title: {
    color: '#3E3E3E',
    fontSize: '26'
  },
  goodsPrice: {
    height: 30,
  },
  goodsNum: {
    height: 70,
    borderBottomWidth: 1,
    borderColor: '#DCDCDC'
  },
  itemBottom: {
    height: 120,
  },
  itemStatus: {
    width: 160,
    height: 60,
    borderWidth: 1,
    borderColor: '#FDAC42',
    borderRadius: 30,
  },
};
styles = createStyles(styles);

export default MineOrder;
// export default connect((Store) => {
//   return {  };
// }, {  })(MineOrder);
