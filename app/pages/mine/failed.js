import React from 'react';
import { View, Image, Text } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import PageFn from '../../utils/page';
import Icon from 'react-native-vector-icons/icomoon';
import NormalHeader from '../../components/NormalHeader';
import { createStyles, Rem } from '../../utils/view';

class TxFailed extends React.Component {
  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  render() {
    return (
      <View>
        <View>
          <NormalHeader
            leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
            onLeftPress={() => { PageFn.pop(this.props.componentId) }}>
            提现失败
          </NormalHeader>
        </View>
        <Flex direction="column" style={styles.main} justify="center" >
          <View>
            <Image source={require('../../assets/img/failed.png')} style={{ width: Rem(140), height: Rem(140) }} />
          </View>
          <Flex direction="column" align="center">
            <Text style={{ color: '#FDB64A', marginTop: Rem(62), fontSize: Rem(43), fontFamily: 'PingFang Regular' }}>提现失败</Text>
            <Text style={{ fontSize: Rem(28), color: '#9D9D9D', marginTop: Rem(16), fontFamily: 'PingFang Regular', marginBottom: Rem(39) }}>失败原因：{this.props.reason}</Text>
          </Flex>
        </Flex>
      </View>
    );
  }
}

let styles = {
  zzjl: {
    borderColor: '#FDB64A',
    color: '#FDB64A'
  },
  jxtx: {
    borderColor: '#A6A6A6'
  },
  draw: {
    marginLeft: 34,
    marginRight: 34,
    height: 38,
    width: 2,
    backgroundColor: '#FDB64A'
  },
  main: {
    alignContent: 'center',
    marginTop: 211
  }
};
styles = createStyles(styles);
export default TxFailed;