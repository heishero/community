/*
 * @Author: yixin 
 * @Date: 2018-11-05 18:16:00 
 * @Last Modified by: yixin
 * @Last Modified time: 2018-12-05 00:56:37
 * 修改个人爱好
 */
import React from 'react';
import { View, ScrollView, TouchableOpacity } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../utils/page';

class MineHobby extends React.Component {

static navigatorStyle = {
  navBarHidden: true,
  navBarNoBorder: true,
}

render() {
  return (
    <View style={styles.wrapper}>
      <NormalHeader
        leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
        onLeftPress={() => { PageFn.pop(this.props.componentId) }}>
        个人喜好
      </NormalHeader>
      <ScrollView style={{ flex: 1 }}>
        <Flex style={styles.title} justify="center" align="center"><CText color="#414141" size="30">选择您的兴趣爱好</CText></Flex>
        <View style={styles.list}>
          <TouchableOpacity style={styles.item}>
            <CText size="30" color="#FDAC42">亲子幼教</CText>
          </TouchableOpacity>
          <TouchableOpacity style={styles.item}>
            <CText size="30" color="#FDAC42">亲子幼教</CText>
          </TouchableOpacity>
          <TouchableOpacity style={styles.item}>
            <CText size="30" color="#FDAC42">亲子幼教</CText>
          </TouchableOpacity>
          <TouchableOpacity style={styles.item}>
            <CText size="30" color="#FDAC42">亲子幼教</CText>
          </TouchableOpacity>
          <TouchableOpacity style={styles.item}>
            <CText size="30" color="#FDAC42">亲子幼教</CText>
          </TouchableOpacity>
        </View>
        <View style={styles.bottom}>
          <CText color="#FEFEFE" size="36">确定选择</CText>
        </View>
      </ScrollView>
    </View>
  );
}}

let styles = {
  wrapper: { flex: 1 },
  headerIcon: {
    fontSize: 32,
    color: '#fff',
  },
  title: {
    marginTop: 40,
    marginBottom: 17,
  },
  list: {
    paddingLeft: 26,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexWrap: 'wrap'
  },
  item: {
    width: 210,
    height: 80,
    borderRadius: 35,
    borderWidth: 1,
    borderColor: '#FDAC42',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 30,
    marginTop: 30,
  },
  bottom: {
    width: 650,
    height: 80,
    borderRadius: 20,
    backgroundColor: '#FDAC42',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 50,
    marginTop: 230
  }
};
styles = createStyles(styles);

export default MineHobby;