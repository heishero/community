import React from 'react';
import { View, TouchableOpacity, Text, ScrollView, Image, Dimensions } from 'react-native';
import { Flex, TextareaItem, Toast } from 'antd-mobile-rn';
import CText from '../../components/CText';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../utils/page';
import NormalHeader from '../../components/NormalHeader';
import { Rem, createStyles } from '../../utils/view';
import Storage from '../../utils/storage';
import ImagePicker from 'react-native-image-crop-picker';
import { takeGoods, commentGoods } from '../../service/security';
import { uploadImg } from '../../service/user';

export default class GoodsComment extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      phone: '',
      address: '',
      check: true,
      val: '',
      imgList: [],
      star: 5,
      data: [{ value: 1 },
        { value: 2 },
        { value: 3 },
        { value: 4 },
        { value: 5 }]
    };
  }

  onChange = (val: any) => {
    this.setState({ val });
  }

  noPicCommentGoods = async () => {
    if (this.state.val.length === 0) {
      return Toast.show('请先输入评价内容');
    }
    try {
      Toast.loading();
      const token = await Storage.load('token');
      await commentGoods({ token: token, order_id: this.props.goodsImformation.id, content: this.state.val, star: this.state.star, goods_id: this.props.goodsImformation.goods_id, goods_name: this.props.goodsImformation.name });
      Toast.hide();
      PageFn.pop(this.props.componentId);
    } catch (error) {
      Toast.fail(error);
    }
  }


  commentGoods = async () => {
    if (this.state.val.length === 0) {
      return Toast.show('请先输入评价内容');
    }
    try {
      Toast.loading();
      const token = await Storage.load('token');
      await commentGoods({ token: token, order_id: this.props.goodsImformation.id, content: this.state.val, image: this.state.imgList, star: this.state.star, goods_id: this.props.goodsImformation.goods_id, goods_name: this.props.goodsImformation.name });
      Toast.success('评价成功！');
      Toast.hide();
      PageFn.pop(this.props.componentId);
    } catch (error) {
      Toast.fail(error);
    }
  }



  // 选择照片
  chooseImage = () => {
    ImagePicker.openPicker({
      width: 400,
      height: 400,
      cropping: true,
      takePhotoButtonTitle: true,
      freeStyleCropEnabled: true,
      compressImageQuality: 1
    }).then(async image => {
      this._fetchImage(image);
    });
  }

  _fetchImage(image) {
    let { imgList } = this.state;
    const file = { uri: image.path, type: 'multipart/form-data', name: 'image.png' };

    let formData = new FormData();
    formData.append('img', file);

    uploadImg(formData).then((response) => {
      this.setState({ imgList: imgList.concat(response.data.img_path) });
    });
  }

  // 商品信息
  goodsImformation=(item) => {
    return (
      <View style={styles.goodsWrapper}>
        <Image style={styles.goodsImage} source={{ uri: item.small_image }}></Image>
        <View style={{ flexDirection: 'column', marginLeft: 10, marginTop: 6, width: '65%' }}>
          <Text numberOfLines={2} style={{ color: '#2D2D2D', fontSize: 14, marginTop: 5 }}>{item.name}</Text>
          <Text style={{ color: '#818181', fontSize: 12, marginTop: 6 }}>{item.b_name}</Text>
          <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
            <View style={{ flexDirection: 'row', marginTop: 8, justifyContent: 'flex-start' }}>
              <Text style={{ color: '#292621', fontSize: 16 }}>￥{item.now_price}</Text>
            </View>
            <Text style={{ color: '#8A857C', fontSize: 12, top: 11, left: 60, position: 'absolute', textDecorationLine: 'line-through' }}>￥{item.pay_total}</Text>
            <Text style={{ color: '#292621', fontSize: 16, marginRight: 10, marginTop: 10 }}>x{item.buy_num}</Text>
          </View>
        </View>
      </View>
    );
  }

   // 商品描述
   goodsDescript =(dataDetail) => {
     return (
       <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', backgroundColor: '#fff', height: Rem(118), marginTop: Rem(20), paddingHorizontal: Rem(30), }}>
         <Text style={styles.descript}>商家描述</Text>
         <Flex>
           {this.state.data.map((item, index) => {
             return (
               item.value > this.state.star ?
                 <TouchableOpacity key={index} onPress={() => {
                   this.setState({
                     star: item.value
                   });
                 }}>
                   <Image style={styles.iconStar} source={require('../../assets/img/kongxingxing.png')} />
                 </TouchableOpacity> :
                 <TouchableOpacity key={index} onPress={() => {
                   this.setState({
                     star: item.value
                   });
                 }}>
                   <Image style={styles.selectedIconStar} source={require('../../assets/img/xingxingIcon.png')} />
                 </TouchableOpacity>
             );
           })}
         </Flex>
       </View>
     );

   }

   render() {
     const dataDetail = this.props.goodsImformation;
     let { imgList } = this.state;
     return (
       <View style={{ flex: 1, backgroundColor: '#F4F4F4' }}>
         <NormalHeader leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
           onLeftPress={() => { PageFn.pop(this.props.componentId);
           }}>评价
         </NormalHeader>
         <ScrollView style={{ flex: 1 }}>
           {this.goodsImformation(dataDetail)}
           <TextareaItem autoHeight={true} placeholder="宝贝满足您的期待吗？说说它的优点和美中不足吧.." style={styles.textArea} value={this.state.val} onChange={this.onChange} />
           <View style={styles.imgBox}>
             <TouchableOpacity style={styles.addImg} onPress={this.chooseImage}>
               <Icon name="chuantupian" style={styles.jiaIcon} />
               <CText color="#5E5E5E" size="24">添加图片</CText>
             </TouchableOpacity>
             {
               imgList.map((data, index) => (
                 <View key={index}>
                   <Image source={{ uri: data }} style={styles.uploadImg} />
                   <TouchableOpacity onPress={() => { this.delete(index) }} style={styles.deleteBox}><Icon name="guanbi" style={styles.deleteIcon} /></TouchableOpacity>
                 </View>
               ))
             }
           </View>
           {this.goodsDescript(dataDetail)}
           <TouchableOpacity onPress={() => { this.state.imgList.length === 0 ? this.noPicCommentGoods() : this.commentGoods() }} activeOpacity={0.8}>
             <View style={styles.commentBtn}>
               <Text style={{ color: '#fff', fontSize: Rem(32) }}>提交</Text>
             </View>
           </TouchableOpacity>
         </ScrollView>
       </View>
     );
   }
}
const styles = createStyles({
  headerIcon: {
    fontSize: 40,
    color: '#000',
  },
  wrapper: {
    flex: 1,
    backgroundColor: '#F7F7F7'
  },
  // 商品
  goodsWrapper: {
    height: 240,
    flexDirection: 'row',
    marginTop: 10,
    backgroundColor: '#fff',
  },
  goodsImage: {
    marginLeft: 24,
    marginVertical: 25,
    width: 185,
    height: 185,
  },
  // 商品描述
  textArea: {
    marginTop: 20,
    fontSize: 30,
    height: 200,
    paddingHorizontal: 30,
  },
  imgBox: {
    marginTop: -2,
    height: 200,
    backgroundColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    paddingHorizontal: 30,
  },
  deleteBox: {
    position: 'absolute',
    right: -12,
    top: -16,
  },
  deleteIcon: {
    fontSize: 34,
    color: '#fc5454',
  },
  addImg: {
    height: 165,
    width: 165,
    backgroundColor: '#fff',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#DDDDDD'
  },
  jiaIcon: {
    fontSize: 50,
    color: '#C1C1C1',
    marginBottom: 10
  },
  uploadImg: {
    height: 165,
    width: 165,
    marginLeft: 26,
    borderRadius: 10,
    marginBottom: 26
  },
  // group
  addGroup: {
    height: 90,
    backgroundColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 30,
    paddingHorizontal: 28
  },
  groupItem: {
    borderBottomWidth: 1,
    borderColor: '#C4C4C4',
    height: 133,
  },
  groupList: {
    paddingHorizontal: 26,
  },
  groupImg: {
    width: 95,
    height: 95,
    borderRadius: 10,
    marginRight: 30,
  },
  headerLeft: {
    position: 'absolute',
    left: 20,
  },
  commentBtn: {
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 75,
    marginRight: 75,
    height: 88,
    backgroundColor: '#22B7F8',
    marginVertical: 100,
  },
  descript: {
    fontSize: 30,
    color: '#333333'
  },
  iconStar: {
    width: 40,
    height: 40,
    marginLeft: 10,
  },
  selectedIconStar: {
    width: 40,
    height: 40,
    marginLeft: 10,
  }

});

