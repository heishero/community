import React from 'react';
import { View, Text, Alert, Image, Keyboard, BackHandler } from 'react-native';
import { Flex, Toast } from 'antd-mobile-rn';
import { createStyles, Rem } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import CText from '../../components/CText';
import MineInput from './components/mineInput';
import MineButton from './components/mineButton';
import ImagePicker from 'react-native-image-crop-picker';
import Storage from '../../utils/storage';
import { uploadImg, updateInfo } from '../../service/user';
import { goToMain } from '@/utils/navigate';

export default class PerfectId extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      headImg: '',
      nickname: '',
      password: '',
      twicePassword: '',
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackAndroid);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackAndroid);
  }

  onBackAndroid() {
    return true;
  }

  handleChange = (name, value) => {
    this.setState({
      [name]: value
    });
  }

  changeImg = () => {
    Alert.alert(
      '更改头像',
      '',
      [
        {
          text: '从相册选择图片', onPress: () => { ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
          }).then(image => {
            this._fetchImage(image);
          }); }
        },
        {
          text: '拍照', onPress: () => {
            ImagePicker.openCamera({
              width: 300,
              height: 400,
              cropping: true
            }).then(image => {
              this._fetchImage(image);
            });
          }
        },
        { text: '取消', onPress: () => console.log('OK Pressed'), style: 'cancel' },
      ],
      { cancelable: false }
    ); }

   _fetchImage = (image) => {
     const file = { uri: image.path, type: 'multipart/form-data', name: 'image.png' };
     let formData = new FormData();
     formData.append('img', file);
     uploadImg(formData).then((response) => {
       this.setState({ headImg: response.data.img_path });
     });
   }

   submit = async () => {
     const { headImg, nickname, password, twicePassword } = this.state;
     if (headImg && nickname && password && password === twicePassword) {
       const token = await Storage.load('token');
       try {
         updateInfo({ headimgurl: headImg, nick_name: nickname, password, token }).then(result => {
           if (result.status === 'success') {
             Toast.success('更新用户信息成功');
             goToMain();
           }
         });
       } catch (error) {
         Toast.fail(error.failedMsg);
       }
     } else {
       Toast.fail('请填写完整信息');
     }
   }

   render() {
     const { headImg, nickname, password, twicePassword } = this.state;
     return (
       <View style={{ backgroundColor: '#fff', flex: 1, }}>
         <NormalHeader>
          身份完善
         </NormalHeader>
         <Flex onPress={() => { Keyboard.dismiss() }} direction="column" justify="flex-start" align="center" style={styles.container}>
           {headImg ? <Image style={styles.logoBox} source={{ uri: headImg }} /> : <Flex onPress={this.changeImg} justify="center" align="center" style={styles.logoBox}>
             <Icon style={styles.logo} name="chuantupian" />
           </Flex>}
           <View style={{ marginBottom: Rem(90) }}><CText size="28">请上传头像</CText></View>
           {/* form */}
           <View style={styles.inputRow}><MineInput name="nickname" onChangeText={this.handleChange} value={nickname} placeholderTextColor="#333333" placeholder="请设置您的昵称" /></View>
           <View style={styles.inputRow}><MineInput name="password" secureTextEntry onChangeText={this.handleChange} value={password} placeholderTextColor="#333333" placeholder="请设置6～16位登录密码" /></View>
           <MineInput name="twicePassword" onSubmitEditing={this.submit} secureTextEntry onChangeText={this.handleChange} value={twicePassword} placeholderTextColor="#333333" placeholder="请设置6～16位登录密码" />
           {password !== twicePassword ? <Text style={styles.noMatchPWD}>*两次密码输入不一致</Text> : null}
         </Flex>
         <Flex style={{ marginTop: Rem(130) }} justify="center"><MineButton onPress={this.submit} title="确定" /></Flex>
       </View>
     );
   }
}

let styles = {
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
  container: {
    marginTop: 30,
    paddingHorizontal: 70,
  },
  logoBox: {
    width: 170,
    height: 180,
    borderRadius: 85,
    backgroundColor: '#F2F2F2',
    marginBottom: 10
  },
  logo: {
    fontSize: 77,
  },
  inputRow: {
    marginBottom: 50,
  },
  noMatchPWD: {
    color: '#FF0000',
    fontSize: 24,
    width: 606,
    marginTop: 10,
  }
};
styles = createStyles(styles);
