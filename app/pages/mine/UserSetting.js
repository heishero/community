import React, { Component } from 'react';
import { View, TouchableOpacity, Text, ScrollView, StyleSheet, Alert } from 'react-native';
import UserCenterCell from '@/pages/mine/components/userCenterCell';
import { Flex } from 'antd-mobile-rn';
import CText from '@/components/CText';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '@/utils/page';
import NormalHeader from '@/components/NormalHeader';
import { Rem } from '@/utils/view';
import Storage from '@/utils/storage';
import userActions from '@/controllers/user/actions';
import { connect } from 'react-redux';
import { loginOut } from '@/service/user';
import { createStyles } from '@/utils/view';


const menu1 = [
  { text: '修改密码', path: 'ForgetPwd' },
  // { text: '我的基因', icon: 'wode-jiying', color: '#589AF9' },
  { text: '修改电话号码', path: 'UserChangePhone' },
  { text: '退出登录', path: '' },

];

class UserSetting extends React.PureComponent {
  constructor(props) {
    super();
    this.state = {
      mobile: ''
    };
  }  

    itemDetailView=(data, title) => {
      return (
        <Flex key={data.text} style={styles.menuItem} onPress={() => { if (data.path) { PageFn.push(this.props.componentId, data.path) } }}>
          <Flex style={{ flex: 1 }} direction="row" justify="space-between" align="center">
            <Flex direction="row" justify="flex-start" align="center">
              <CText color="#585858" size="30" >{data.text}</CText>
            </Flex>
            <Flex style={styles.number}>
              <CText color="#585858" size="30" >{title}</CText>
              <Icon name="xiang-you" style={styles.menuRightIcon} />
            </Flex>
          </Flex>
        </Flex>
      );
    }

    loginOut = async () => {
      const token = await Storage.load('token');
      if (token) {
        try {
          const userinfo = await loginOut(token);
          if (userinfo) {
            this.props.clearUserInfo();
          }
        } catch (error) {
        }
      }
    }


    alertView= () => {

      Alert.alert('提示', '确定要退出登录吗', [
        {
          text: '取消', onPress: () => {
          }
        },
        {
          text: '确定', onPress: () => {
            this.loginOut();
          }
        },
      ]);
    }

    logOutDetailView=(data) => {
      return (
        <Flex key={data.text} style={styles.menuItem} onPress={() => { this.alertView() }}>
          <Flex style={{ flex: 1 }} direction="row"  justify="space-between" align="center">
            <Flex direction="row"  align="center">
              <CText   color="#585858" size="30" >{data.text}</CText>
            </Flex>
            <Icon name="xiang-you" style={styles.menuRightIcon} />
          </Flex>
        </Flex>
      );
    }

    itemCell=(item, title) => {

      return (
        <View>
          <View style={{ marginLeft: -10, height: 1, backgroundColor: '#EFEFEF', width: '100%' }} />
          {this.itemDetailView(item[0], '')}
          <View style={{ marginLeft: -10, height: 1, backgroundColor: '#EFEFEF', width: '100%' }} />
          {this.itemDetailView(item[1], title)}
          <View style={{ marginLeft: -10, height: 1, backgroundColor: '#EFEFEF', width: '100%' }} />
          {this.logOutDetailView(item[2], '')}
          <View style={{ marginLeft: -10, height: 1, backgroundColor: '#EFEFEF', width: '100%' }} />
        </View>
      );
    }
    render() {
      return (
        <View style={{ flex: 1 }}>
          <NormalHeader leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
            onLeftPress={() => { PageFn.pop(this.props.componentId);
            }}>设置</NormalHeader>
          <ScrollView>
            {this.itemCell(menu1, this.props.userInfo.mobile)}
          </ScrollView>
        </View>
      );
    }
}

let styles = {
  headerIcon: {
    fontSize: 30,
    color: '#000',
  },
  menuBox: {
    paddingHorizontal: 10,
    backgroundColor: '#fff'
  },
  menuItem: {
    height: 100,
    paddingHorizontal: 26,
  },
  menuWrapper: {
    width: 25,
    height: 25,
    borderRadius: 10,
    marginRight: 20
  },
  menuRightIcon: {
    color: '#E2E2E2',
    fontSize:30,
  },
  number: {
    color: '#E2E2E2',
    fontSize: 18,
  }
}

styles = createStyles(styles);
export default connect((Store) => {
  return { userInfo: Store.UserRenderData.userInfo };
}, { clearUserInfo: userActions.Clear, setUserInfo: userActions.RenderData })(UserSetting);