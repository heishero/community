
import React, { Component } from 'react';
import { Dimensions, View, TouchableOpacity, Text, ScrollView, StyleSheet, Alert, ImageBackground, Image } from 'react-native';
import UserCenterCell from '@/pages/mine/components/userCenterCell';
import { Flex, Toast } from 'antd-mobile-rn';
import CText from '../../components/CText';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../utils/page';
import NormalHeader from '../../components/NormalHeader';
import { Rem, createStyles } from '../../utils/view';
import Storage from '../../utils/storage';
import { OrderDeTail } from '../../service/security';
import { takeGoods } from '../../service/security';
import * as WeChat from 'react-native-wechat';

export default class myOrderDetail extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      dataDetail: {},
      check: true,
      wxPayInfo: {}
    };
  }
  componentDidMount() {
    this.init();
  }
  // 订单详情
      init = async () => {
        const order_id = this.props.order_id;
        const token = await Storage.load('token');
        const { data } = await OrderDeTail({ id: order_id, token: token });
        this.setState({ dataDetail: data });
      }

      // 确认收货
        getTakeGoods = async () => {
          const token = await Storage.load('token');
          const order_id = this.props.order_id;
          takeGoods({ token: token, order_id: order_id }).then(result => {
            if (result.status === 'success') {
              Toast.success('收货成功');
              this.init();
            }
          }).catch(err => {
            console.log(err);
          });
        }
        // 状态
      topState=(dataDetail) => {
        let stateString;
        switch (dataDetail.status) {
          case 0:
            stateString = '待支付';
            break;
          case 1:
            stateString = '已支付';
            break;
          case 2:
            stateString = '已发货';
            break;
          case 3:
            stateString = '已收货';
            break;
          case 4:
            stateString = '交易成功';
            break;
          case 5:
            stateString = '退款中';
            break;
          case 6:
            stateString = '已退款';
            break;
          case 7:
            stateString = '已取消';
            break;
          default:
            break;
        }
        return stateString;
      }

    GetState=(dataDetail) => {
      let stateString;
      switch (dataDetail.status) {
        case 0:
          stateString = '去支付';
          break;
        case 1:
          stateString = '确认收货';
          break;
        case 2:
          stateString = '确认收货';
          break;
        case 3:
          stateString = '评价';
          break;
        case 4:
          stateString = '已评价';
          break;
        case 5:
          stateString = '退款中';
          break;
        case 6:
          stateString = '已退款';
          break;
        case 7:
          stateString = '已取消';
          break;
        default:
          break;
      }
      return stateString;
    }

  // 圈主介绍
  circleView=(item) => {
    return (
      <Flex style={styles.menuItem} onPress={() => { PageFn.push(this.props.componentId, 'ChaterInfo', { id: item.jg_im_username }) }}>
        <Flex style={styles.cirle} direction="row" justify="space-between">
          <CText color="#585858" size="30">圈主介绍</CText>
          <Icon name="xiang-you" style={styles.menuRightIcon} />
        </Flex>
      </Flex>
    );
  }
  // 订单号信息
      orderDetail = (item) => {
        return (
          <View style={styles.orderDetail}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: 20, marginRight: 20, }}>
              <Text style={{ color: '#292621', fontSize: 14 }}>实付金额:</Text>
              <Text style={{ color: '#292621', fontSize: 14 }}>¥{item.now_price}</Text>
            </View>
            {this.lineView('订单号', item.order_num)}
            {/* {this.lineView('微信交易号',item.order_num)} */}
            {this.lineView('支付时间', item.created_at)}
          </View>
        );
      }
      lineView =(title, oddNum) => {
        return (
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: 20, marginRight: 20, marginTop: 10 }}>
            <Text style={styles.order}>{title}</Text>
            <Text style={styles.order}>{oddNum}</Text>
          </View>
        );
      }

      // 商品信息
       goodsImformation=(item) => {
         return (
           <TouchableOpacity onPress={() => PageFn.push(this.props.componentId, 'GoodsDetail', { id: item.goods_id })} style={styles.goodsDetail}>
             <Image style={styles.img} source={{ uri: item.small_image }}></Image>
             <View style={{ flexDirection: 'column', marginLeft: 10, marginTop: 6, width: '65%' }}>
               <Text numberOfLines={2} style={{ color: '#2D2D2D', fontSize: 14, marginTop: 5 }}>{item.name}</Text>
               <Text style={{ color: '#818181', fontSize: 12, marginTop: 6 }}>{item.b_name}</Text>
               <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                 <View style={{ flexDirection: 'row', marginTop: 8, justifyContent: 'flex-start' }}>
                   <Text style={{ color: '#292621', fontSize: 16 }}>￥{item.now_price}</Text>
                 </View>
                 <Text style={{ color: '#8A857C', fontSize: 12, top: 11, left: 60, position: 'absolute' }}>￥{item.pay_total}</Text>
                 <View style={{ backgroundColor: '#8A857C', height: 1, top: 20, width: 30, left: 60, position: 'absolute' }}></View>
                 <Text style={{ color: '#292621', fontSize: 16, marginRight: 10, marginTop: 10 }}>x{item.buy_num}</Text>
               </View>
             </View>
           </TouchableOpacity>
         );
       }
      // 地址
      address = (checkAddress) => {
        return (
          <Flex direction="row" justify="center" align="center" style={styles.addressBox} onPress={() => { }}>
            <Icon name="dizhi" style={styles.dingweiIcon} />
            <View style={styles.addressCenter}>
              {/* 收货人row */}
              <Flex style={styles.nameBox} justify="space-between" align="center">
                <CText size="30" color="#363636">收货人:{checkAddress.add_name}</CText>
                <CText size="30" color="#363636">电话:{checkAddress.mobile}</CText>
              </Flex>
              <Flex>
                <CText size="24" color="#363636">收货地址:{checkAddress.province} {checkAddress.city} {checkAddress.district} {checkAddress.address}</CText>
              </Flex>
            </View>
          </Flex>
        );
      }
      render() {
        const dataDetail = this.state.dataDetail;
        let string = this.GetState(dataDetail);
        let stringTop = this.topState(dataDetail);
        return (
          <View style={{ flex: 1, backgroundColor: '#F4F4F4' }}>
            <NormalHeader leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
              onLeftPress={() => { PageFn.pop(this.props.componentId);
              }}>订单详情
            </NormalHeader>
            <ScrollView style={{ flex: 1 }}>
              <ImageBackground style={styles.image} resizeMode="stretch" source={require('../../assets/img/orderBack.png')}>
                <Text style={styles.stateText}>{stringTop}</Text>
              </ImageBackground>
              {
                this.address(dataDetail)
              }
              {
                this.goodsImformation(dataDetail)
              }
              {
                this.circleView(dataDetail)
              }
              {
                this.orderDetail(dataDetail)
              }
              <TouchableOpacity onPress={() => {
                if (dataDetail.status == 0) {
                  PageFn.push(this.props.componentId, 'Order', { goodsData: dataDetail });
                }
                if (dataDetail.status == 2 || dataDetail.status == 1) {
                  this.getTakeGoods();
                }
                if (dataDetail.status == 3) {
                  PageFn.push(this.props.componentId, 'GoodsComment', { goodsImformation: dataDetail, tag: '2' });
                } }} activeOpacity={0.8}>
                <View style={styles.commentBtn}>
                  <Text style={{ color: '#2866E4', fontSize: 16 }}>{string}</Text>
                </View>
              </TouchableOpacity>
            </ScrollView>
          </View>
        );
      }
}

let styles = {
  headerIcon: {
    fontSize: 40,
    color: '#000',
  },
  image: {
    height: 200,
  },
  stateText: {
    marginLeft: 40,
    marginVertical: 70,
    color: '#fff',
    fontSize: 32
  },
  cirle: {
    marginLeft: 30,
    width: 680
  },
  chooseIcon: {
    fontSize: 30,
    color: '#A0A0A0',
    position: 'absolute',
    right: 18
  },
  addressBox: {
    height: 180,
    backgroundColor: '#fff',
    marginTop: 20,
  },
  addressCenter: {
    width: 610,
  },
  nameBox: {
    marginBottom: 20
  },
  dingweiIcon: {
    fontSize: 30,
    color: '#A0A0A0',
    position: 'absolute',
    left: 18
  },
  order: {
    color: '#707070',
    fontSize: 26,
  },
  menuItem: {
    marginTop: 20,
    height: 100,
    backgroundColor: '#fff',
    // borderBottomWidth: 1,
    // borderColor: '#D4D4D4'
  },
  menuRightIcon: {
    color: '#E2E2E2',
    fontSize: 36,
  },
  commentBtn: {
    marginVertical: 100,
    marginLeft: 560,
    justifyContent: 'center',
    alignItems: 'center',
    width: 160,
    height: 64,
    borderRadius: 16,
    borderColor: '#2866E4',
    borderWidth: 2,
  },

  img: {
    marginLeft: 24,
    marginVertical: 25,
    width: 185,
    height: 185,
  },
  goodsDetail: {
    height: 240,
    flexDirection: 'row',
    marginTop: 10,
    backgroundColor: '#fff',
  },
  orderDetail: {
    marginTop: 20,
    paddingVertical: 20,
    height: 220,
    backgroundColor: '#fff',
    justifyContent: 'space-between'
  }
};
styles = createStyles(styles);
