/*
 * @Author: yixin
 * @Date: 2018-11-06 11:30:53
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-18 23:04:05
 * 动态详情
 */
import React from 'react';
import { View, ScrollView, FlatList } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import DynamicDetailTop from './components/dynamicDetailTop';
import GroupComment from '../find/components/groupComment';
import PageFn from '../../utils/page';
import { topicInfo, topicComment } from '../../service/topic';

class MineDynamicDetail extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      detail: {},
      list: [],
    };
  }

  componentDidMount() {
    this.init();
  }

  init = async () => {
    try {
      const detail = await topicInfo(this.props.id);
      this.setState({ detail: detail.data });
      const list = await topicComment({ topicComment: 1, page: 1, pageSize: 20 });
      this.setState({ list: list.data });

    } catch (error) {

    }
  }

  render() {
    const { detail, list } = this.state;
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}
          rightContent={<Flex direction="column" justify="center" align="center">
            <View style={{ marginBottom: 4 }}><CText size="24" color="#fff">我也</CText></View><CText size="24" color="#fff">要发</CText></Flex>}
          onRightPress={() => {
            PageFn.push(this.props.componentId, 'ReleaseDynamic');
          }}
        >
         动态详情
        </NormalHeader>
        <ScrollView style={{ flex: 1 }}>
          <Flex style={styles.listBox} direction="column" justify="flex-start" align="center">
            {detail.id && <DynamicDetailTop data={detail} componentId={this.props.componentId} />}
            <View style={styles.chatTitle}><CText size="36" color="#414141">讨论详情</CText></View>
            <FlatList
              data={list}
              keyExtractor={(item, index) => item.id}
              renderItem={({ item, index, section }) => <GroupComment data={item} />}
            />
          </Flex>
        </ScrollView>
      </View>
    );
  }}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#F7F7F7' },
  headerIcon: {
    fontSize: 32,
    color: '#fff',
  },
  listBox: {
    marginTop: 26
  },
  chatTitle: {
    marginTop: 22,
    marginBottom: 20,
    width: 698
  }
};

styles = createStyles(styles);

export default MineDynamicDetail;