/*
 * @Author: yixin
 * @Date: 2018-11-06 11:52:15
 * @Last Modified by: yixin
 * @Last Modified time: 2019-01-23 18:25:09
 * 关于我们
 */
import React from 'react';
import { View, ScrollView } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles, Rem } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../utils/page';

class About extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}>
        关于我们
        </NormalHeader>
        <ScrollView style={{ flex: 1, paddingHorizontal: Rem(24), paddingVertical: Rem(30) }}>
          <CText>关于我们</CText>
        </ScrollView>
      </View>
    );
  }}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#F7F7F7' },
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
};
styles = createStyles(styles);

export default About;
