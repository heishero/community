import React from 'react';
import { View, TextInput } from 'react-native';
import { Flex, Toast } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../utils/page';
import { updateInfo } from '../../service/user';
import Storage from '../../utils/storage';

class Rename extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      name: ''
    };
  }

  update = async () => {
    const token = await Storage.load('token');
    try {
      const data = await updateInfo({ token, nick_name: this.state.name });
      Toast.success('更改成功');
      PageFn.pop(this.props.componentId);
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}
          rightContent={<CText size="30" color="#000">保存</CText>}
          onRightPress={() => { this.update() }}
        >
          更改名称
        </NormalHeader>
        <View style={styles.box}>
          <TextInput placeholder="昵称" style={styles.input} value={this.state.value} onChangeText={(text) => { this.setState({ name: text }) }} />
          <View style={styles.tips}><CText color="#A2A2A2" size="24">好的昵称可以让大家更容易记住你哦</CText></View>
        </View>
      </View>
    );
  }
}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#F7F7F7' },
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
  box: {
    paddingHorizontal: 26,
  },
  input: {
    height: 90,
    borderBottomWidth: 1,
    borderColor: '#FDAC42',
    paddingLeft: 28,
    paddingVertical: 0,
    fontSize: 28,
  },
  tips: {
    marginTop: 19,
  }
};
styles = createStyles(styles);

export default Rename;