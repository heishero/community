/*
 * @Author: yixin
 * @Date: 2018-11-06 14:06:19
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-17 21:42:18
 * 东道主
 */
import React from 'react';
import { View, ScrollView, Image, TextInput, TouchableOpacity, Picker, PermissionsAndroid, Platform } from 'react-native';
import { Flex, Toast } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles, Rem } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import ImagePicker from 'react-native-image-crop-picker';
import { uploadImg } from '../../service/user';
import { getClassify, createGroup } from '../../service/group';
import { connect } from 'react-redux';
import PageFn from '../../utils/page';
import PickerWidget from '../../components/PickerWidget';

class MineHost extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      value: 1,
      classify1: [],
      classifyIndex1: 0,
      classify2: [],
      classifyIndex2: 0,
      // 公开还是私密
      is_private: true,
      // 是否免费
      is_free: true,
      // 图片地址
      imageUrl: null,
      // 圈子昵称
      name: '',
      // 圈子费用
      price: 99,
      // 圈子地址
      address: '',
      // 经度
      lat: '',
      // 维度
      lon: '',
      // 圈子简介
      introduce: '',
      commitCirleTag: true
    };
    this.pickRef = '';
    this.setAddress = this.setAddress.bind(this);
  }

  componentDidMount() {

    if (Platform.OS === 'ios') {
      this.init();
    } else {
      this.checkPermission();
    }

  }

  checkPermission() {
    try {
      // 返回Promise类型
      const granted = PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
      );
      granted.then((data) => {
        if (!data) { this.requestLocationPermission() } else {
          this.init();
        }
      }).catch((err) => {
        console.log(err);
        // this.show(err.toString());
      });
    } catch (err) {
      // this.show(err.toString());
    }
  }

  requestLocationPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          // 第一次请求拒绝后提示用户你为什么要这个权限
          'title': '请允许地理位置授权',
          'message': '因为要定位圈子位置，请您开启地理位置授权'
        }
      );

      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        Toast.success('你已获取了地址查询权限');
        this.init();
      } else {
        Toast.fall('获取地址查询失败');
      }
    } catch (err) {
      Toast.show(err.toString());
    }
  }

  async init() {
    const menuData = await getClassify();
    this.setState({ classify1: menuData.data });
    if (menuData.data[0].id) {
      const menuData2 = await getClassify({ pid: menuData.data[0].id });
      this.setState({ classify2: menuData2.data });
    }
  }

  async changePicker1(value) {
    this.setState({ classifyIndex1: value });
    const menuData = await getClassify({ pid: this.state.classify1[value].id });
    this.setState({ classify2: menuData.data });

  }

  changePicker2(value) {
    this.setState({ classifyIndex2: value });
  }

  chooseImage = () => {
    ImagePicker.openPicker({
      width: 400,
      height: 400,
      cropping: true
    }).then(async image => {
      this._fetchImage(image);
    });
  }

  _fetchImage(image) {
    const file = { uri: image.path, type: 'multipart/form-data', name: 'image.png' };
    let formData = new FormData();
    formData.append('img', file);
    uploadImg(formData).then((response) => {
      this.setState({ imageUrl: response.data.img_path });
    });
  }

  // 跳转到Map
  toMap = () => {
    PageFn.push(this.props.componentId, 'Map', { setAddress: this.setAddress }
    );
  }

  setAddress(params) {
    this.setState({
      address: params.address,
      lat: params.lat,
      lon: params.lon
    });
  }

  // 提交申请
  submit = () => {
    const { imageUrl, classify2, classifyIndex2, is_private, is_free, name, price, address, lat, lon, introduce } = this.state;
    Toast.loading('正在申请中···', 10);
    if (imageUrl && classify2 && address && introduce) {
      if (!lat || !lon) {
        return Toast.fail('请开启位置授权');
      }
      if (this.state.commitCirleTag === false) {
        return Toast.fail('请勿重复提交');
      }
      this.setState({
        commitCirleTag: false
      });
      let params = {
        name: name,
        headimgurl: imageUrl,
        classify_id: classify2[classifyIndex2].id,
        is_free: is_free ? 1 : 0,
        charge: price,
        is_private: is_private ? 1 : 0,
        address: address,
        lat: lat,
        lon: lon,
        introduce: introduce,
        token: this.props.userInfo.auth_token
      };
      createGroup(params).then(result => {
        Toast.success('申请成功');
        this.setState({
          commitCirleTag: true
        });
        Toast.hide();
        PageFn.pop(this.props.componentId);
      }).catch(err => {
        console.log(err);
      });
    } else {
      Toast.fail('请填写完所有数据');
    }

  }

  render() {
    const { imageUrl, classify1, classifyIndex1, classify2, classifyIndex2, is_private, is_free, name, price, address, introduce } = this.state;
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}>
        申请建圈子
        </NormalHeader>
        <ScrollView style={styles.content}>
          {/* 圈子头像 */}
          <Flex direction="column" justify="flex-start" align="center" onPress={this.chooseImage}>
            {<Image resizeMode="stretch" style={styles.avatar} source={imageUrl ? { uri: imageUrl } : require('../../assets/img/avatar.png')} />}
            <CText size="26" color="#888888">圈子头像</CText>
          </Flex>
          {/* 圈子昵称 */}
          <Flex style={styles.name} direction="row" justify="flex-start" align="center">
            <CText size="26" color="#888888">圈子昵称：</CText>
            <TextInput value={name} onChangeText={(text) => { this.setState({ name: text }) }} placeholder="圈子昵称" style={styles.input}></TextInput>
          </Flex>
          <View style={styles.tipBox}><CText color="#FDAC42" size="20">*确定后不可修改</CText></View>
          {/* 圈子属性 */}
          <Flex style={styles.name} direction="row" justify="flex-start" align="center">
            <CText size="26" color="#888888">圈子属性：</CText>
            {Platform.OS === 'android' ? <Flex justify="space-between" align="center" style={styles.pickList}>
              <Picker selectedValue={classifyIndex1} mode="dropdown" style={styles.pickItem} prompt="请选择圈子属性"
                enabled={true}
                itemStyle={{ fontSize: Rem(28), color: 'red', textAlign: 'left', fontWeight: 'bold' }} onValueChange={(value) => { this.changePicker1(value) }}>
                {classify1.map((option, index) => <Picker.Item label={option.name} value={index} key={option.id} />)}
              </Picker>

              <Picker selectedValue={classifyIndex2} mode="dropdown" style={styles.pickItem} prompt="请选择圈子属性"
                enabled={true}
                itemStyle={{ fontSize: Rem(28), color: 'red', textAlign: 'left', fontWeight: 'bold' }}onValueChange={(value) => { this.changePicker2(value) }}>
                {classify2.map((option, index) => <Picker.Item label={option.name} value={index} key={option.id} />)}
              </Picker>
            </Flex> :
            <Flex justify="space-between" align="center" style={styles.pickList}>
              {
                classify1.length > 0 &&
                <TouchableOpacity onPress={() => { this.pickRef.show(this, this.changePicker1) }} style={styles.pickItem}>
                <CText>{classify1[classifyIndex1].name}</CText>
              </TouchableOpacity>}
              {
                classify2.length > 0 &&
                <TouchableOpacity onPress={() => { this.pickRef2.show(this, this.changePicker2) }} style={styles.pickItem}>
                <CText>{classify2[classifyIndex2].name}</CText>
              </TouchableOpacity>}
            </Flex>
            }
          </Flex>
          <View style={styles.tipBox}><CText color="#FDAC42" size="20">*确定后不可修改</CText></View>
          {/* 圈子设置 */}
          <Flex style={styles.name} direction="row" justify="flex-start" align="center">
            <CText size="26" color="#888888">圈子设置：</CText>
            <Flex style={styles.checkBox}>
              <Flex style={{ flex: 1, marginLeft: Rem(7) }} direction="row" onPress={() => { this.setState({ is_private: true }) }}><CText color="#414141" size="28">私密</CText><View style={[styles.checkItem, is_private && styles.checkItemActive]}></View></Flex>
              <Flex style={{ flex: 1, marginLeft: Rem(7) }} direction="row" onPress={() => { this.setState({ is_private: false }) }}><CText color="#414141" size="28">公开</CText><View style={[styles.checkItem, !is_private && styles.checkItemActive]}></View></Flex>
            </Flex>
          </Flex>
          <Flex style={styles.name} direction="row" justify="flex-start" align="center">
            <CText size="26" color="#888888">圈子设置：</CText>
            <Flex style={styles.checkBox}>
              <Flex style={{ flex: 1, marginLeft: Rem(7) }} direction="row" onPress={() => { this.setState({ is_free: true }) }}><CText color="#414141" size="28">免费</CText><View style={[styles.checkItem, is_free && styles.checkItemActive]}></View></Flex>
              <Flex style={{ flex: 1, marginLeft: Rem(7) }} direction="row" onPress={() => { this.setState({ is_free: false }) }}><CText color="#414141" size="28">收费</CText><View style={[styles.checkItem, !is_free && styles.checkItemActive]}></View></Flex>
            </Flex>
          </Flex>
          {!is_free && <Flex style={styles.name} direction="row" justify="flex-start" align="center">
            <CText size="26" color="#888888">圈子设置：</CText>
            <Flex justify="space-between" style={styles.moneyInputRow}>
              <TextInput keyboardType="numeric" value={price} onChangeText={(text) => { let price = text.replace(/[^\d]+/, ''); this.setState({ price }) }} placeholder="请设置圈子费用" style={styles.moneyInput} />
              <CText size="26" color="#FDAC42">元</CText>
            </Flex>
          </Flex>}
          <View style={styles.tipBox}><CText color="#FDAC42" size="20">*确定后不可修改</CText></View>
          {/* 圈子位置 */}
          <Flex onPress={this.toMap} style={styles.name} direction="row" justify="flex-start" align="center">
            <CText size="26" color="#888888">圈子位置：</CText>
            <View style={styles.input}>{address ? <CText>{address}</CText> : <CText size="28" color="#C4C4C4" fontWeight="100" >社圈子具体位置</CText>}</View>
          </Flex>
          <Flex style={styles.introduceBox} direction="row" justify="flex-start" align="flex-start">
            <CText size="26" color="#888888">圈子简介：</CText>
            <TextInput multiline={true} value={introduce} onChangeText={(text) => { this.setState({ introduce: text }) }} placeholder="圈子简介" style={styles.introduceText}></TextInput>
          </Flex>
          <TouchableOpacity onPress={this.submit} style={styles.submit}><CText size="36" color="#fff">提交申请</CText></TouchableOpacity>
        </ScrollView>
        {classify1.length > 0 && <PickerWidget ref={(ref) => { this.pickRef = ref }} defaultVal={classifyIndex1} pickData={classify1} />}
        {classify2.length > 0 && <PickerWidget ref={(ref) => { this.pickRef2 = ref }} defaultVal={classifyIndex2} pickData={classify2} />}
      </View>
    );
  }}

let styles = {
  wrapper: { flex: 1 },
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
  content: {
    flex: 1,
    paddingHorizontal: 26,
  },
  avatar: {
    width: 252,
    height: 252,
    borderRadius: 15,
    marginTop: 32,
    marginBottom: 20,
  },
  name: {
    height: 60,
    marginTop: 30,
  },
  input: {
    height: 80,
    width: 568,
    borderWidth: 1,
    borderColor: '#cbcbcb',
    borderRadius: 10,
    paddingLeft: 20,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 0,
  },
  introduceBox: {
    marginTop: 30,
  },
  introduceText: {
    height: 200,
    width: 568,
    borderWidth: 1,
    borderColor: '#cbcbcb',
    borderRadius: 10,
    paddingLeft: 20,
    flexDirection: 'row',
    alignItems: 'center',
    textAlignVertical: 'top'
  },
  tipBox: {
    marginTop: 16,
    marginLeft: 122
  },
  picker: {
    height: 60,
    width: 274,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#CBCBCB',
    marginLeft: 20,
  },
  pickList: {
    width: 568,
    height: 60,
  },
  pickItem: {
    width: 274,
    height: 60,
    borderWidth: 1,
    borderColor: '#CBCBCB',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  checkBox: {
    width: 568,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginLeft: 10,
  },
  checkItem: {
    width: 30,
    height: 30,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: '#cbcbcb',
    marginLeft: 30
  },
  checkItemActive: {
    borderWidth: 8,
    borderColor: '#FDAC42'
  },
  moneyInputRow: {
    width: 579,
    height: 60,
    borderWidth: 1,
    borderColor: '#cbcbcb',
    borderRadius: 10,
    paddingHorizontal: 30,
  },
  moneyInput: {
    flex: 1,
    height: 60,
    padding: 0,
  },
  submit: {
    width: 698,
    height: 80,
    borderRadius: 20,
    backgroundColor: '#2ab6f3',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 100
  }
};
styles = createStyles(styles);

// export default MineHost;
export default connect((Store) => {
  return { userInfo: Store.UserRenderData.userInfo };
})(MineHost);