import React from 'react';
import { View, Text, TextInput, Switch, Alert } from 'react-native';
import { Flex, Toast } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../utils/page';
import Picker from 'react-native-picker';
import Storage from '../../utils/storage';
import { getAreaData, addAddress, delAddress, saveAddress } from '../../service/user';

class AddAddress extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      phone: '',
      address: '',
      check: true,
      area: [],
      tagKeep:true
    };
  }

  componentDidMount() {
    this.init();
  }

  init = async () => {
    try {
      const token = await Storage.load('token');
      const { data } = await getAreaData(token);
      Picker.init({
        pickerData: data,
        pickerTitleText: '选取地址',
        pickerConfirmBtnText: '确认',
        pickerCancelBtnText: '取消',
        onPickerConfirm: data => {
          console.log(data);
          this.setState({ area: data });
        },
        onPickerCancel: data => {
          console.log(data);
        },
        onPickerSelect: data => {
          console.log(data);
        }
      });
      if (this.props.edit) {
        const { edit } = this.props;
        console.log(edit);
        this.setState({
          name: edit.name,
          phone: edit.mobile,
          address: edit.address,
          check: edit.useing === 1 ? true : false,
          area: [edit.province, edit.city, edit.district]
        });
        Picker.select([edit.province, edit.city, edit.district]);
      }
    } catch (error) {

    }
  }


  add = async () => {
    try {

      const { name, phone, address, area, check } = this.state;
      if (area.length === 0) { Toast.fail('请先选择地址'); return false }
      if (!(phone.length === 11 && (/^[1][3,4,5,6,7,8,9][0-9]{9}$/.test(phone)))) { Toast.fail('请输入正确的手机号码'); return false }
      if(!this.state.tagKeep){
        return Toast.fail('请勿重复保存')
      }
       this.setState({
          tagKeep:false
      })
      const token = await Storage.load('token');
      await addAddress({ name, mobile: phone, province: area[0], city: area[1], district: area[2], address, token, useing: check ? 1 : 0 });
      Toast.success('地址添加成功');
      this.setState({
        tagKeep:true
    })
      PageFn.pop(this.props.componentId);
    } catch (error) {
      console.log(error);
    }
  }

  edit = async () => {
    try {
      const { name, phone, address, area, check } = this.state;
      if (area.length === 0) { Toast.fail('请先选择地址'); return false }
      if (!(phone.length === 11 && (/^[1][3,4,5,6,7,8,9][0-9]{9}$/.test(phone)))) { Toast.fail('请输入正确的手机号码'); return false }
      const token = await Storage.load('token');
      await saveAddress({ name, mobile: phone, province: area[0], city: area[1], district: area[2], address, token, useing: check ? 1 : 0, id: this.props.edit.id });
      Toast.success('地址修改成功');
      PageFn.pop(this.props.componentId);
    } catch (error) {
      console.log(error);
    }
  }

  delete = async () => {
    const token = await Storage.load('token');
    Alert.alert(
      '',
      '确定要删除该地址吗？',
      [
        {
          text: '确认', onPress: () => {
            delAddress({ id: this.props.edit.id, token }).then(() => {
              PageFn.pop(this.props.componentId);
            }).catch((err) => {
              console.log(err);
            });

          }
        },
        { text: '取消', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
      ],
      { cancelable: false }
    );
  }

  render() {
    const { name, phone, address, check, area } = this.state;
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}
          rightContent={<CText size="26" color="#000">保存</CText>}
          onRightPress={() => { this.props.edit ? this.edit() : this.add() }}
        >
          添加收货地址
        </NormalHeader>
        <Flex direction="row" justify="flex-start" align="center" style={styles.row}>
          <Text style={styles.rowTitle}>收货人</Text>
          <TextInput style={styles.rowInput} underlineColorAndroid="transparent"
            numberOfLines={1}
            onChangeText={(text) => { this.setState({ name: text }) }}
            value={name}
          />
        </Flex>
        <Flex direction="row" justify="flex-start" align="center" style={styles.row}>
          <Text style={styles.rowTitle}>手机号码</Text>
          <TextInput style={styles.rowInput} underlineColorAndroid="transparent"
            numberOfLines={1}
            onChangeText={(text) => { this.setState({ phone: text }) }}
            value={phone}
            keyboardType="phone-pad"
          />
        </Flex>
        <Flex direction="row" justify="space-between" align="center" style={styles.row} onPress={() => { console.log('pser'); Picker.show() }}>
          <Text style={styles.rowTitle}>所在地区: {area.length > 0 && <Text>{area[0]} {area[1]} {area[2]}</Text>}</Text>
          <Icon style={styles.headerIcon} name="xiang-you" />
        </Flex>
        <Flex direction="row" justify="flex-start" align="center" style={styles.row}>
          <Text style={styles.rowTitle}>详细地址</Text>
          <TextInput style={styles.rowInput} underlineColorAndroid="transparent"
            numberOfLines={1}
            placeholder="详细地址：如道路、门牌号、小区、楼栋号、单元室等"
            onChangeText={(text) => { this.setState({ address: text }) }}
            value={address}
          />
        </Flex>
        {/* 默认地址 */}
        <Flex direction="row" justify="space-between" align="center" style={[styles.row, styles.defaultRow]} onPress={() => { Picker.show() }}>
          <Text style={styles.rowTitle}>默认地址</Text>
          <Switch value={check} onValueChange={(value) => { this.setState({ check: value }) }} />
        </Flex>
        {/* 删除地址 */}
        {this.props.edit && <Flex direction="row" justify="space-between" align="center" style={styles.row} onPress={this.delete}>
          <CText color="#FF1907" size="28">删除地址</CText>
        </Flex>}
      </View>
    );
  }}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#F7F7F7' },
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
  row: {
    height: 94,
    paddingHorizontal: 24,
    borderBottomWidth: 1,
    borderColor: '#F6F6F6',
    backgroundColor: '#fff'
  },
  rowTitle: {
    fontSize: 28,
    color: '#363636'
  },
  rowInput: {
    flex: 1,
    textAlign: 'right',
    paddingVertical: 0,
  },
  defaultRow: {
    marginVertical: 20,
  }
};
styles = createStyles(styles);

export default AddAddress;

