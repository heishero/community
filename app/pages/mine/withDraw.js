import React from 'react';
import { View, Text, TextInput, Image } from 'react-native';
import { Flex, Radio, Button, Toast } from 'antd-mobile-rn';
import { Navigation } from 'react-native-navigation';
import NormalHeader from '../../components/NormalHeader';
import { createStyles, Rem } from '../../utils/view';
import { withDraw, userBalance } from '../../service/user';
import PageFn from '../../utils/page';
import Icon from 'react-native-vector-icons/icomoon';
import Storage from '../../utils/storage';
import CText from '../../components/CText';

const RadioItem = Radio.RadioItem;

class WithDraw extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      },
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      select: '',
      amount: 0,
      balance: 0
    };
    Navigation.events().bindComponent(this);
  }

  componentDidAppear() {
    this.init();
  }



  init = async () => {
    const token = await Storage.load('token');
    const balance = await userBalance({ token });
    this.setState({ balance: balance.data.balance });
  }

  onPress = async () => {
    let amount = this.state.amount;
    if (this.state.amount < 100) {
      Toast.fail('输入金额必须大于100');
      return 0;
    }
    const token = await Storage.load('token');
    const state = await withDraw({ money: amount, token });
    if (state.status === 'success') {
      Toast.success('提现成功');
      // PageFn.push(this.props.componentId, 'TxSuccess');
      PageFn.showModal('TxSuccess');
    } else {
      Toast.fail('提现失败');
      PageFn.push(this.props.componentId, 'TxFailed', { reason: state.failedMsg });
    }
  }

  render() {
    console.log('id', this.props.componentId);
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          onRightPress={() => { PageFn.showModal('DrawHistory') }}
          rightContent={<CText>提现记录</CText>}
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}>
          我的收益
        </NormalHeader>
        <Flex direction="column" justify="center" align="center" style={{ marginTop: Rem(76), marginBottom: Rem(101) }}>
          <Text style={styles.amount}>{this.state.balance}</Text>
          <Text style={styles.text}>余额</Text>
        </Flex>
        <View style={styles.notice}>
          <Text style={styles.notice_text}>每次兑换金额不得低于100元，余额低于100时不可提现</Text>
        </View>
        <Flex style={{ height: Rem(120) }} direction="row" align="center">
          <Flex direction="row">
            <Text style={styles.with_text}>提现金额:</Text>
            <TextInput
              keyboardType="numeric"
              style={styles.with_input}
              placeholder="输入提现金额"
              placeholderTextColor="#9D9D9D"
              onChangeText={(amount) => { this.setState({ amount }) }}
            />
          </Flex>
        </Flex>
        <Flex direction="column" style={styles.tx} align="start" justify="center">
          <Text style={styles.txtext}>提现到:</Text>
          <Flex direction="row" >
            <View>
              <Image source={require('../../assets/img/wx.png')} style={{ width: Rem(80), height: Rem(80), marginLeft: Rem(41) }} />
            </View>
            <Flex direction="row" justify="between" style={{ flex: 1, marginLeft: Rem(20), marginRight: Rem(60), marginBottom: Rem(5) }}>
              <Text style={{ fontSize: Rem(30) }}>微信</Text>
              <Radio name="wx" defaultChecked />
            </Flex>
          </Flex>
        </Flex>
        <View>
          <Button onClick={() => { this.onPress() }} style={{ marginLeft: Rem(26), marginRight: Rem(26), marginTop: Rem(98), height: Rem(80), backgroundColor: '#FDAC42', borderColor: '#FDAC42' }}><Text style={{ fontSize: Rem(30), fontWeight: 'bold', color: 'white' }}>提现</Text></Button>
          <Flex style={{ marginLeft: Rem(75), marginTop: Rem(55), marginRight: Rem(95) }} direction="column" align="start" >
            <Text style={{ fontSize: Rem(24), color: '#3C3C3C' }}>提现提示</Text>
            <Text style={{ color: '#9D9D9D', fontSize: Rem(20), marginTop: Rem(27), }}>1. 点击申请提现后，长按下方财务二维码，并添加财务微信进行提现转账</Text>
            <Text style={{ color: '#9D9D9D', fontSize: Rem(20), marginTop: Rem(18), }}>2. 请各位会员添加客服，以便微信不回复时备用。 </Text>
          </Flex>
        </View>
      </View>
    );
  }
}
let styles = {
  wrapper: { flex: 1, backgroundColor: '#FFFFFF' },
  amount: {
    fontSize: 82,
    color: '#5D5D5D',
  },
  text: {
    fontSize: 28,
    color: '#202020',
  },
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
  notice_text: {
    fontSize: 24,
    color: '#909090',
    textAlign: 'center',
    marginTop: 20,
  },
  notice: {
    backgroundColor: '#EDEDED',
    height: 80
  },
  with_text: {
    fontSize: 30,
    color: '#3C3C3C',
    marginLeft: 41,
  },
  with_input: {
    fontSize: 28,
    marginLeft: 61,
    paddingVertical: 0,
    flex: 1,
  },
  txtext: {
    fontSize: 24,
    color: '#3C3C3C',
    marginLeft: 41
  },
  tx: {
    borderWidth: 1,
    borderColor: '#CBCBCB',
    height: 145,
  },
  wx_check: {
    fontSize: 30,
    color: '#666666',
    marginLeft: 35,
  }
};
styles = createStyles(styles);
export default WithDraw;