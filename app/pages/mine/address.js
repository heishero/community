import React from 'react';
import { View, Image } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import { connect } from 'react-redux';
import CText from '../../components/CText';
import { createStyles } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../utils/page';
import actions from '../../controllers/adderss/actions';
import { Navigation } from 'react-native-navigation';

class Address extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  componentDidAppear() {
    this.props.getAddress();
  }

  // 订单页更换收货地址
  checkAddress = (data) => {
    if (!this.props.check) return false;
    this.props.checkAddress({ data });
    PageFn.pop(this.props.componentId);
  }

  // 编辑收货地址
  editAddress = (e, data) => {
    e.stopPropagation();
    PageFn.push(this.props.componentId, 'AddAddress', { edit: data });
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}
          rightContent={<CText size="26" color="#000000">   添加新地址</CText>}
          onRightPress={() => { PageFn.push(this.props.componentId, 'AddAddress') }}
        >
          我的收货地址
        </NormalHeader>
        {
          this.props.addressList.map((data) => (
            <Flex key={data.id} style={styles.list} onPress={() => this.checkAddress(data)}>
              <Flex style={styles.content} direction="column" justify="flex-start" align="flex-start">
                <View style={styles.content_top}><CText size="34" color="#363636">{data.name}  {data.mobile}</CText></View>
                <View><CText size="26" color="#363636">{data.province} {data.city} {data.district} {data.address}</CText></View>
              </Flex>
              <Flex style={styles.edit} justify="center" align="center" onPress={(e) => { this.editAddress(e, data) }}>
                <CText size="26" color="#363636">编辑</CText>
              </Flex>
              {!!data.useing && <Image style={styles.defaultIcon} source={require('../../assets/img/defaultAddress.png')} />}
            </Flex>
          ))
        }
      </View>
    );
  }}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#F7F7F7' },
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
  list: {
    height: 170,
    backgroundColor: '#fff',
    marginTop: 30,
    paddingLeft: 25,
  },
  content: {
    flex: 1
  },
  content_top: {
    marginBottom: 16
  },
  edit: {
    width: 99,
    height: 170,
    borderLeftWidth: 1,
    borderColor: '#CACACA',
  },
  defaultIcon: {
    position: 'absolute',
    height: 78,
    width: 99,
    right: 0,
    bottom: 0,
  }
};
styles = createStyles(styles);

export default connect((Store) => {
  return { addressList: Store.AddressRenderData.addressList };
}, { getAddress: actions.GetAddress, checkAddress: actions.CheckAddress })(Address);
