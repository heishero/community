
import React from 'react';
import { View, Text, FlatList, BackHandler } from 'react-native';
import NormalHeader from '../../components/NormalHeader';
import Storage from '../../utils/storage';
import { createStyles, Rem } from '../../utils/view';
import { withdrawList } from '../../service/user';
import PageFn from '../../utils/page';
import Icon from 'react-native-vector-icons/icomoon';

export default class DrawHistory extends React.PureComponent {
     pageIndex = 1;
     refreshing = false;
     hadMore = false;
     pageSize = 20;

     constructor(props) {
       super(props);
       this.state = {
         datas: [],
       };
     }
     // componentDidAppear() {
     //   this.getDatasList()
     // }

     componentDidMount() {
       this.getDatasList();
       BackHandler.addEventListener('hardwareBackPress', this.onBackAndroid);
     }

     componentWillUnmount() {
       BackHandler.removeEventListener('hardwareBackPress', this.onBackAndroid);
     }

     onBackAndroid = () => {
       PageFn.dismissAllModals();
       return true;
     }

      // 提现记录
      getDatasList = async () => {
        const token = await Storage.load('token');
        withdrawList({ token: token, page: this.pageIndex, pageSize: this.pageSize }).then(result => {
          this.refreshing = false;
          let tempRows = result.data.list;
          if (this.pageIndex === 1) {
            this.setState({ datas: tempRows });
          } else {
            this.setState({ datas: this.state.datas.concat(tempRows) });
          }
          this.hadMore = (tempRows.length === this.pageSize);
        }).catch(err => {
          this.refreshing = false;
          console.log(err);
        });
      }

      // 上拉
      _onEndReached = () => {
        if (!this.refreshing && this.hadMore) {
          this.pageIndex++;
          this.getDatasList();
        }
      }
      // 下拉
      _onRefresh= () => {
        this.pageIndex = 1;
        this.refreshing = true;
        this.getDatasList();
      }
      // 底部
      renderFooter = () => {
        if (this.hadMore) {
          return (
            <View style={{
              height: 36,
              backgroundColor: 'white',
              justifyContent: 'center',
              alignItems: 'center'
            }}>
              <Text>{'正在加载....'}</Text>
            </View>
          );
        } else {
          return (
            <View />
          );
        }
      }

    _separator = () => {
      return <View style={{ width: 750, height: 1, backgroundColor: '#EFEFEF' }} />;
    }

    render() {
      return (
        <View style={{ flex: 1 }}>
          <NormalHeader
            leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
            onLeftPress={() => { PageFn.dismissAllModals() }}>
            提现记录
          </NormalHeader>
          <FlatList
            style={{ flex: 1, }}
            renderItem={(item) => {
              return (
                <View>
                  <View style={styles.item}>
                    <View style={styles.itemLeft}>
                      <Text>提现到微信</Text>
                      <Text style={{ marginTop: 5 }}>{item.item.created_at}</Text>
                    </View>
                    <Text style={styles.itemRight}>-{item.item.money}</Text>
                  </View>
                  <View style={{ height: 1, backgroundColor: '#EFEFEF' }} />
                </View>
              );
            }}
            //  ItemSeparatorComponent={this._separator}
            onEndReachedThreshold={0.1}
            onEndReached={this._onEndReached}
            refreshing={this.refreshing}
            onRefresh={this._onRefresh}
            data={this.state.datas}
            ListFooterComponent={this.renderFooter}// 尾巴
            keyExtractor={(item, index) => index}
            showsVerticalScrollIndicator={false}
            ListEmptyComponent={() => {
              return <View style={{ height: '100%', marginTop: 200, alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ fontSize: 20 }}>暂无数据</Text>
              </View>;
            }}
          />
        </View>
      );
    }
}

let styles = {
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
  item: {
    height: 140,
    // width:750,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  itemLeft: {
    flexDirection: 'column',
    marginLeft: 40,
    marginTop: 30
  },
  itemRight: {
    color: 'red',
    marginRight: 40,
    marginTop: 50
  },
};
styles = createStyles(styles);