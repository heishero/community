/*
 * @Author: yixin
 * @Date: 2018-11-09 11:41:08
 * @Last Modified by: yixin
 * @Last Modified time: 2019-03-23 16:30:32
 * 圈子总收益
 */
import React from 'react';
import { View, ScrollView, ImageBackground, TouchableOpacity } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles, Rem } from '../../utils/view';
import Icon from 'react-native-vector-icons/icomoon';
import IncomeTitle from './components/incomeTitle';
import Circle from '../circle/components/circle';

class GroupIncome extends React.Component {

static navigatorStyle = {
  navBarHidden: true,
  navBarNoBorder: true,
}

render() {
  return (
    <View style={styles.wrapper}>
      <ScrollView style={styles.wrapper}>
        <ImageBackground style={styles.headerBg} resizeMode="stretch" source={require('../../assets/img/groupbg.png')}>
          <View style={styles.header}>
            <TouchableOpacity style={styles.backBox}>
              <Icon style={styles.back} name="fanhui" />
              <CText size="30" color="#fff">圈子详情</CText>
            </TouchableOpacity>
            <View style={{ marginLeft: Rem(-100) }}><CText size="30" color="#fff">东道主</CText></View>
            <TouchableOpacity>
              <Icon style={styles.fenxiang} name="fenxiang" />
            </TouchableOpacity>
          </View>
        </ImageBackground>
        <Flex direction="column" justify="center" align="center">
          {/* 我的收益 */}
          <IncomeTitle title="我的收益" path="true" />
          <Flex style={styles.income} justify="center" align="center">
            <Flex style={{ flex: 1 }} direction="column" justify="center" align="center">
              <View style={styles.incomeItem}><CText size="30" color="#5E5E5E">总收入:</CText></View>
              <CText size="36" color="#FDAC42">2345.09</CText>
            </Flex>
            <Flex style={{ flex: 1 }} direction="column" justify="center" align="center">
              <View style={styles.incomeItem}><CText size="30" color="#5E5E5E">昨日收入:</CText></View>
              <CText size="36" color="#FDAC42">103.09</CText>
            </Flex>
          </Flex>
          {/* 精选动态 */}
          <IncomeTitle title="我的圈子" />
          <Circle />
          <Circle />
          <Circle />
          <Circle />
        </Flex>
      </ScrollView>
    </View>
  );
}}

let styles = {
  wrapper: {
    flex: 1,
    backgroundColor: '#F7F7F7'
  },
  headerBg: {
    ios: {
      height: 335,
    },
    android: {
      height: 300,
    },
    marginBottom: 30,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    ios: {
      height: 128,
      paddingTop: 35,
    },
    android: {
      height: 90,
    },
    paddingHorizontal: 26,
  },
  backBox: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  back: {
    fontSize: 36,
    color: '#000',
    marginRight: 16,
  },
  fenxiang: {
    fontSize: 36,
    color: '#fff',
  },
  bottom: {
    height: 98,
    width: 750,
  },
  bottomLeft: {
    width: 129,
    height: 98,
    justifyContent: 'center',
    alignItems: 'center'
  },
  bottomRight: {
    flex: 1,
    height: 98,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FDAC42'
  },
  income: {
    height: 147,
  },
  incomeItem: {
    marginBottom: 15
  }
};
styles = createStyles(styles);

export default GroupIncome;