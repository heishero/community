/*
 * @Author: yixin
 * @Date: 2019-01-10 17:57:14
 * @Last Modified by: yixin
 * @Last Modified time: 2019-01-21 22:55:26
 * 实名验证
 */
import React from 'react';
import { View, Text, TextInput, Image } from 'react-native';
import { Flex, Toast } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../utils/page';
import ImagePicker from 'react-native-image-crop-picker';
import { uploadImg, userCert } from '../../service/user';
import Storage from '../../utils/storage';

class RealName extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      phone: '',
      idCard: '',
      card1: '',
      card2: ''
    };
  }

  chooseImage = (index) => {
    ImagePicker.openPicker({
      width: 400,
      height: 400,
      cropping: true
    }).then(async image => {
      this._fetchImage(image, index);
    });
  }

  _fetchImage(image, index) {
    const file = { uri: image.path, type: 'multipart/form-data', name: 'image.png' };

    let formData = new FormData();
    formData.append('img', file);

    uploadImg(formData).then((response) => {
      if (index === 1) {
        this.setState({ card1: response.data.img_path });
      } else {
        this.setState({ card2: response.data.img_path });
      }
    });
  }

  submit = async () => {
    const { name, phone, idCard, card1, card2 } = this.state;
    if (name && phone && idCard && card1 && card2) {
      try {
        const token = await Storage.load('token');
        await userCert({
          real_name: name,
          mobile: phone,
          id_card: idCard,
          id_card_img_left: card1,
          id_card_img_right: card2,
          token
        });
        Toast.success('实名信息提交成功');
        PageFn.pop(this.props.componentId);
      } catch (error) {

      }
    }
  }

  render() {
    const { name, phone, idCard, card1, card2 } = this.state;
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}>
          实名认证
        </NormalHeader>
        <View style={styles.container}>
          <Flex style={styles.row}>
            <Text style={styles.rowTitle}>姓        名：</Text>
            <TextInput style={styles.rowInput} underlineColorAndroid="transparent"
              numberOfLines={1}
              onChangeText={(text) => { this.setState({ name: text }) }}
              value={name}
              placeholder="请输入您的真实姓名"
            />
          </Flex>
          <Flex style={styles.row}>
            <Text style={styles.rowTitle}>联系电话：</Text>
            <TextInput style={styles.rowInput} underlineColorAndroid="transparent"
              numberOfLines={1}
              onChangeText={(text) => { this.setState({ phone: text }) }}
              value={phone}
              keyboardType="phone-pad"
              placeholder="请输入您的有效联系方式"
            />
          </Flex>
          <Flex style={styles.row}>
            <Text style={styles.rowTitle}>身份证号：</Text>
            <TextInput style={styles.rowInput} underlineColorAndroid="transparent"
              numberOfLines={1}
              onChangeText={(text) => { this.setState({ idCard: text }) }}
              value={idCard}
              keyboardType="phone-pad"
              placeholder="请输入您的身份证号码"
            />
          </Flex>
          <Flex style={styles.row} alignItems="flex-start">
            <Text style={styles.rowTitle}>实名认证：</Text>
            <Flex direction="column">
              {card1 ? <Image style={styles.addImg} source={{ uri: card1 }} /> : <Flex onPress={() => this.chooseImage(1)} direction="column" justify="center" align="center" style={[styles.addImg, { backgroundColor: '#e3e3e3' }]}>
                <Icon style={styles.addIcon} name="tianjia" />
                <CText size="24" color="#5e5e5e">添加身份证正面</CText>
              </Flex>}
              {card2 ? <Image style={styles.addImg} source={{ uri: card2 }} /> : <Flex onPress={() => this.chooseImage(2)} direction="column" justify="center" align="center" style={[styles.addImg, { backgroundColor: '#e3e3e3' }]}>
                <Icon style={styles.addIcon} name="tianjia" />
                <CText size="24" color="#5e5e5e">添加身份证反面</CText>
              </Flex>}
            </Flex>
          </Flex>
          <Flex onPress={this.submit} justify="center" align="center" style={styles.submit}><CText size="36" color="#fff">确定</CText></Flex>
        </View>
      </View>
    );
  }}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#F7F7F7' },
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
  container: {
    paddingHorizontal: 26,
    paddingTop: 63,
  },
  row: {
    marginBottom: 30,
  },
  rowTitle: {
    fontSize: 30,
    color: '#414141'
  },
  rowInput: {
    height: 60,
    width: 548,
    borderWidth: 1,
    borderColor: '#CBCBCB',
    borderRadius: 10,
    paddingVertical: 0,
    paddingLeft: 30,
    fontSize: 24,
  },
  addImg: {
    width: 549,
    height: 226,
    borderRadius: 10,
    marginBottom: 30
  },
  addIcon: {
    fontSize: 63,
    color: '#5e5e5e',
    marginBottom: 30
  },
  submit: {
    height: 80,
    backgroundColor: '#f5ac2f',
    borderRadius: 20,
    marginTop: 80,
  }
};
styles = createStyles(styles);

export default RealName;