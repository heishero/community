import React, { Component } from 'react'
import { ImageBackground,View, TouchableOpacity, Text, ScrollView, StyleSheet, Alert,Image,Dimensions} from 'react-native';
import NormalHeader from '../../components/NormalHeader';
import { Flex,Toast} from 'antd-mobile-rn';
import CText from '../../components/CText';
import Storage from '../../utils/storage';
import userActions from '../../controllers/user/actions';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../utils/page';
import * as WeChat from 'react-native-wechat';
import { wxLogin } from '../../service/wx';
import { Navigation } from 'react-native-navigation';
import { getInfo} from '../../service/user';

 class UserWechatBind extends React.PureComponent {

  constructor(){
    super();
    this.state = {
      moblie:'',
    }
  }
  componentDidMount() {
    this.init();
  }

  init = async () => {
    const token = await Storage.load('token');

    console.log('000000000',token)
    if (token) {
      try {
        const userinfo = await getInfo(token);
        this.props.setUserInfo({
          userInfo: { ...userinfo.data }
        });
        this.setState({
          moblie:userInfo.moblie
        })

        console.log(this.state.moblie)

      } catch (error) {

      }
    }
  }

getBindWechat=()=>{
  let that = this;
   WeChat.sendAuthRequest('snsapi_userinfo', 'chaihe')
            .then(responseCode => {
              // 返回code码，通过code获取access_token
              wxLogin(responseCode.code).then(data => {
                Toast.success('登录成功');
                console.log('6666=>>>',data)
                this.init()
                Storage.save('token', data.data.auth_token);
                Navigation.dismissAllModals();
          
              }).catch(err => { console.log(err) });
            }).catch(err => {
              Alert.alert('登录授权发生错误：', err.message, [{
                text: '确定'
              }]);
            });
}
   unbindWechat=()=>{
    return(
      <Flex  onPress={() =>{this.getBindWechat()}} style={{flexDirection:'row',marginTop:20,marginLeft:20}} justify="space-between">
        <Text style={styles.firstText}>暂未绑定微信</Text>
        <Text style={styles.secondText}>绑定</Text>
      </Flex>
    )
   }

   bindWechat=()=>{
    return(
      <Flex  onPress={() =>{
        this.getBindWechat()
      }}  style={{flexDirection:'row',marginTop:20,marginLeft:20}}  justify="space-around">
         <Icon style={styles.unbindIcon} name="weixing" />
        <Text style={styles.firstText}>{this.state.mobile}</Text>
        <Text style={styles.secondText}>更换</Text>
      </Flex>
    )
  }
  render() {

    return (
      <View style={{backgroundColor:'#EFEFEF',flex:1}}>
         <NormalHeader leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
            onLeftPress={() => { PageFn.pop(this.props.componentId);
            }}>提款资料
        </NormalHeader>
        <ScrollView style={{marginRight:20}}>
            <ImageBackground  style={styles.image} resizeMode="stretch" source={require('../../assets/img/wechatPay.png')} >
            {this.props.userInfo.mobile.length === 0?this.unbindWechat():this.bindWechat()}
           </ImageBackground>
          </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  headerIcon: {
    fontSize: 20,
    color: '#000',
  },
  unbindIcon:{
    width:33,
    height:33,
    marginLeft:10,
    marginTop:20
  },
    image: {
      marginTop:50,
     marginLeft:20,
      borderRadius:10,
      height:150,
    },
    firstText:{
      marginLeft:20,
      color:'#FFFFFF',
      fontSize:18
    },
    secondText:{
      marginRight:20,
      paddingHorizontal:5,
      paddingVertical:5,
      color:'#FFFFFF',
      fontSize:14,
      borderRadius:10,
      borderWidth:1,
      borderColor:'#FFFFFF',
    }
  }
  );
  
  export default connect((Store) => {
    return { userInfo: Store.UserRenderData.userInfo };
  }, { clearUserInfo: userActions.Clear, setUserInfo: userActions.RenderData })(UserWechatBind);