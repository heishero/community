/*
 * @Author: yixin
 * @Date: 2018-11-14 22:17:43
 * @Last Modified by: yixin
 * @Last Modified time: 2019-02-24 17:23:43
 * 圈子成员
 */
import React from 'react';
import { View, ScrollView, Image } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles, Rem } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../utils/page';

class GroupMember extends React.Component {

static navigatorStyle = {
  navBarHidden: true,
  navBarNoBorder: true,
}

constructor(props) {
  super(props);
  this.state = {
    edit: false,
  };
}

render() {
  const { edit } = this.state;
  return (
    <View style={styles.wrapper}>
      <NormalHeader
        leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
        onLeftPress={() => { PageFn.pop(this.props.componentId) }}
        rightContent={<View style={{ paddingLeft: 20, paddingVertical: 10 }}><CText size="26" color="#fff">管理</CText></View>}
        onRightPress={() => { this.setState({ edit: true }) }}>
        圈子成员
      </NormalHeader>
      <ScrollView style={{ flex: 1 }}>
        <Flex style={styles.item}>
          {edit && <View style={styles.check}></View>}
          <Image style={styles.avatar} />
          <CText color="#575757" size="34">成员昵称成员昵称成员昵称成员昵称</CText>
        </Flex>
        <Flex style={styles.item}>
          {edit && <View style={styles.check}></View>}
          <Image style={styles.avatar} />
          <CText color="#575757" size="34">成员昵称成员昵称成员昵称成员昵称</CText>
        </Flex>
        <Flex style={styles.item}>
          {edit && <View style={styles.check}></View>}
          <Image style={styles.avatar} />
          <CText color="#575757" size="34">成员昵称成员昵称成员昵称成员昵称</CText>
        </Flex>
        <Flex style={styles.item}>
          {edit && <View style={styles.check}></View>}
          <Image style={styles.avatar} />
          <CText color="#575757" size="34">成员昵称成员昵称成员昵称成员昵称</CText>
        </Flex>
      </ScrollView>
      {edit && <Flex style={styles.bottom} direction="row" justify="center" align="center">
        <Flex justify="center" align="center" style={styles.bottomItem}>
          <CText size="36" color="#585858">全 选</CText>
        </Flex>
        <Flex justify="center" align="center" style={[styles.bottomItem, { marginHorizontal: Rem(52) }]}>
          <CText size="36" color="#585858">删 除</CText>
        </Flex>
        <Flex onPress={() => { this.setState({ edit: false }) }} justify="center" align="center" style={styles.bottomItem}>
          <CText size="36" color="#585858">取 消</CText>
        </Flex>
      </Flex>}
    </View>
  );
}}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#F7F7F7' },
  headerIcon: {
    fontSize: 32,
    color: '#fff',
  },
  item: {
    height: 118,
    paddingLeft: 26,
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderColor: '#E4E4E4'
  },
  avatar: {
    height: 86,
    width: 86,
    borderRadius: 43,
    marginRight: 28,
    backgroundColor: '#FDAC42',
  },
  check: {
    height: 40,
    width: 40,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: '#FDAC42',
    marginRight: 26,
  },
  bottom: {
    height: 98,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: '#FAF8F8'
  },
  bottomItem: {
    width: 178,
    height: 70,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: '#FDAC42',
    backgroundColor: '#fff'
  }
};
styles = createStyles(styles);

export default GroupMember;
