/*
 * @Author: yixin
 * @Date: 2018-11-05 18:32:21
 * @Last Modified by: yixin
 * @Last Modified time: 2018-12-09 00:49:42
 * 我的动态
 */
import React from 'react';
import { View, ScrollView, FlatList } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import DynamicComment from './components/dynamicComment';
import PageFn from '../../utils/page';
import Storage from '../../utils/storage';
import { topicList } from '../../service/user';

class MineDynamic extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      pageSize: 20,
      token: '',
      topicList: [],
      hasMore: false,
    };
  }

  componentDidMount() {
    this.init();
  }

  init= async () => {
    const { page, pageSize } = this.state;
    try {
      const token = await Storage.load('token');
      const { data } = await topicList({ page, pageSize, token });
      this.setState({ topicList: data, token, hasMore: data.length === pageSize });
    } catch (error) {

    }
  }

  render() {
    const { topicList } = this.state;
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}>
        我的动态
        </NormalHeader>
        <ScrollView style={{ flex: 1 }}>
          <View style={styles.list}>
            <FlatList
              data={topicList}
              keyExtractor={(item, index) => item.id}
              renderItem={({ item, index, section }) => <DynamicComment data={item} componentId={this.props.componentId} path="MineDynamicDetail" />}
            />
          </View>
        </ScrollView>
      </View>
    );
  }}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#F7F7F7' },
  headerIcon: {
    fontSize: 32,
    color: '#fff',
  },
  list: {
    paddingHorizontal: 26,
    marginVertical: 40,
  }
};
styles = createStyles(styles);

export default MineDynamic;
