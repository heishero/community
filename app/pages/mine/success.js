import React from 'react';
import { View, Image, Text } from 'react-native';
import { Flex, Button } from 'antd-mobile-rn';
import PageFn from '../../utils/page';
import Icon from 'react-native-vector-icons/icomoon';
import NormalHeader from '../../components/NormalHeader';
import { createStyles, Rem } from '../../utils/view';

class TxSuccess extends React.Component {
  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  goTx = () => {
    // PageFn.push(this.props.componentId, 'WithDraw');
    PageFn.dismissAllModals();
  }

  goZz = () => {
    // PageFn.push(this.props.componentId, 'DrawHistory');
    PageFn.showModal('DrawHistory');
  }

  render() {
    return (
      <View>
        <View>
          <NormalHeader
            leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
            onLeftPress={() => { PageFn.pop(this.props.componentId) }}>
            提现成功
          </NormalHeader>
        </View>
        <Flex direction="column" style={styles.main} justify="center" >
          <View>
            <Image source={require('../../assets/img/success.png')} style={{ width: Rem(140), height: Rem(140) }} />
          </View>
          <Flex direction="column" align="center">
            <Text style={{ color: '#FDB64A', marginTop: Rem(62), fontSize: Rem(43) }}>提现成功</Text>
            <Text style={{ fontSize: Rem(28), color: '#9D9D9D', marginTop: Rem(16), marginBottom: Rem(39) }}>预计十秒到账</Text>
          </Flex>
          <Flex direction="row">
            <Button style={styles.zzjl} type="ghost" onClick={() => { this.goZz() }}><Text style={{ color: '#FDB64A' }}>提现记录</Text></Button>
            <View style={styles.draw} />
            <Button style={styles.jxtx} type="ghost" onClick={() => { this.goTx() }}><Text style={{ color: '#A6A6A6' }}>继续提现</Text></Button>
          </Flex>
        </Flex>
      </View>
    );
  }
}

let styles = {
  zzjl: {
    borderColor: '#FDB64A',
  },
  jxtx: {
    borderColor: '#A6A6A6'
  },
  draw: {
    marginLeft: 34,
    marginRight: 34,
    height: 38,
    width: 2,
    backgroundColor: '#FDB64A'
  },
  main: {
    alignContent: 'center',
    marginTop: 211
  }
};
styles = createStyles(styles);
export default TxSuccess;