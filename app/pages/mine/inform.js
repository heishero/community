/*
 * @Author: yixin
 * @Date: 2018-11-05 17:08:58
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-13 13:34:42
 * 个人信息
 */
import React from 'react';
import { View, TouchableOpacity, Image, Alert, Platform, KeyboardAvoidingView, TextInput,Keyboard} from 'react-native';
import { Flex, Toast } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import { Rem } from '../../utils/view';
import { connect } from 'react-redux';
import userActions from '../../controllers/user/actions';
import PageFn from '../../utils/page';
import { Navigation } from 'react-native-navigation';
import { getInfo, uploadImg, updateInfo } from '../../service/user';
import Storage from '../../utils/storage';
import ImagePicker from 'react-native-image-crop-picker';
import * as WeChat from 'react-native-wechat';
import { wxBind } from '../../service/wx';

class MineInfo extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      commentInput: '',
      list: [
        { name: '名称', text: '', path: 'Rename', signature: '' },
        // { name: '职业', text: '' },
        // { name: '个人喜好', text: '', path: 'MineHobby' },
      ]
    };
    Navigation.events().bindComponent(this);
  }
  init = async () => {
    const token = await Storage.load('token');
    if (token) {
      try {
        const userinfo = await getInfo(token);

        let list = [].concat(this.state.list);
        list[0].text = userinfo.data.nick_name;
        list[0].signature = userinfo.data.signature;


        this.setState({ list });
        this.props.setUserInfo({
          userInfo: { ...userinfo.data }
        });
      } catch (error) {

      }
    }
  }
  componentDidMount() {
    this.init();
  }

  componentDidAppear() {
    this.init();
  }

  changeImg = () => {
    Alert.alert(
      '更改头像',
      '',
      [
        {
          text: '从相册选择图片', onPress: () => { ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
          }).then(image => {
            this._fetchImage(image);
          }); }
        },
        {
          text: '拍照', onPress: () => {
            ImagePicker.openCamera({
              width: 300,
              height: 400,
              cropping: true
            }).then(image => {
              this._fetchImage(image);
            });
          }
        },
        { text: '取消', onPress: () => console.log('OK Pressed'), style: 'cancel' },
      ],
      { cancelable: false }
    ); }

   _fetchImage = (image) => {
     const file = { uri: image.path, type: 'multipart/form-data', name: 'image.png' };
     let formData = new FormData();
     formData.append('img', file);
     Toast.loading('头像正在上传···',10)
     console.log('头像正在上传··')
     uploadImg(formData).then((response) => {
       updateInfo({ token: this.props.userInfo.auth_token, headimgurl: response.data.img_path }).then((data) => {
         Toast.success('更换头像成功');
         getInfo(this.props.userInfo.auth_token).then((userinfo) => {
          Toast.hide()
           this.props.setUserInfo({
             userInfo: { ...userinfo.data }
           });
         }).catch((err) => { console.log(err) });
       }).catch((err) => { console.log(err) });
     });
   }

  createList = ({ name, text, path }) => {
    return (
      <Flex key="name" onPress={() => { if (path) { PageFn.push(this.props.componentId, path) } }} style={styles.normalItem} justify="space-between" align="center">
        <CText size="30" color="#585858">{name}</CText>
        <Flex justify="flex-end" align="center">
          <CText size="30" color="#585858">{text}</CText>
          <TouchableOpacity onPress={this.changeImg}><Icon name="xiang-you" style={styles.icon} /></TouchableOpacity>
        </Flex>
      </Flex>
    );
  }

  // 保存
submit = async () => {
  const token = await Storage.load('token');
  let signatureName;
  if (this.state.commentInput === '') {
    signatureName = this.state.list[0].signatur;
  }
  else {
    signatureName = this.state.commentInput;
  }
  try {
    updateInfo({ nick_name: this.state.list[0].text, signature: signatureName, token: token, headimgurl: this.props.userInfo.headimgurl, password: '' }).then(result => {
      if (result.status === 'success') {
        Toast.success('更新用户信息成功');
        this.init();
        // this.props.userInfo.signature;
        PageFn.pop(this.props.componentId);
      }
    });
  } catch (error) {
    Toast.fail(error.failedMsg);
  }

}
  bindWx = () => {
    WeChat.isWXAppInstalled()
      .then((isInstalled) => {
        if (isInstalled) {
          // 发送授权请求
          WeChat.sendAuthRequest('snsapi_userinfo', 'chaihe')
            .then(responseCode => {
              // 返回code码，通过code获取access_token
              wxBind({ code: responseCode.code, token: this.props.userInfo.auth_token }).then(data => {
                console.log('绑定微信成功data', data);
                Toast.success('绑定微信成功');
                this.init();
              }).catch(err => { console.log(err) });
            }).catch(err => {
              Alert.alert('登录授权发生错误：', err.message, [{
                text: '确定'
              }]);
            });
        } else {
          Platform.OS === 'ios' ?
            Alert.alert('没有安装微信', '是否安装微信？', [
              { text: '取消' },
              { text: '确定', onPress: () => this.installWechat() }
            ]) :
            Alert.alert('没有安装微信', '请先安装微信客户端在进行登录', [
              { text: '确定' }
            ]);
        }
      });
  }

  render() {
    const { userInfo } = this.props;
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          onRightPress={() => {
            this.submit();
          }}
          rightContent= {<CText>保存</CText>}
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId);
          }}>
        个人信息
        </NormalHeader>
        <View style={styles.topBorder}></View>
        <TouchableOpacity activeOpacity={1} onPress={() => { Keyboard.dismiss()}}>
        <View style={styles.list}>
          <Flex style={styles.imgItem} justify="space-between" align="center" onPress={this.changeImg}>
            <CText size="30" color="#585858">头像</CText>
            <Flex justify="flex-end" align="center">
              <Image source={{ uri: userInfo.headimgurl }} style={styles.img} />
            </Flex>
          </Flex>
          {
            this.state.list.map((data) => (
              this.createList({ ...data })
            ))
          }
          <Flex style={{ marginLeft: Rem(20), marginTop: Rem(40) }}>
            <CText size="30" color="#585858">个人介绍</CText>
          </Flex>
          <KeyboardAvoidingView behavior="padding">
           <TextInput
               multiline={true}
              placeholder={userInfo.signature == null ? '请输入您的个人介绍':userInfo.signature}
              ref={(c) => { this.inputRefs = c }}
              value={this.state.commentInput}
              editable ={true}
              onChangeText={(text) => {
                this.setState({ commentInput: text });
              }}
              // blurOnSubmit={true}
              style={styles.bottomInput}
            />
          </KeyboardAvoidingView>



          {/* 绑定手机号 */}
          {/* {!userInfo.mobile && <Flex onPress={() => { PageFn.push(this.props.componentId, 'BindPhone') }} style={styles.normalItem} key={name} justify="space-between" align="center">
            <CText size="30" color="#585858">绑定手机号</CText>
            <Flex justify="flex-end" align="center">
              <TouchableOpacity onPress={this.changeImg}><Icon name="dianjishuru" style={styles.icon} /></TouchableOpacity>
            </Flex>
          </Flex>}
          {!userInfo.wx_openid && <Flex onPress={this.bindWx} style={styles.normalItem} justify="space-between" align="center">
            <CText size="30" color="#585858">绑定微信号</CText>
            <Flex justify="flex-end" align="center">
              <TouchableOpacity onPress={this.changeImg}><Icon name="dianjishuru" style={styles.icon} /></TouchableOpacity>
            </Flex>
          </Flex>} */}



        </View>
        </TouchableOpacity>
      </View>
    );
  }}

let styles = {
  wrapper: { flex: 1 },
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
  list: {
    paddingHorizontal: 10,
  },
  imgItem: {
    height: 140,
    paddingHorizontal: 18,
    borderBottomWidth: 1,
    borderColor: '#BBBBBB'
  },
  img: {
    height: 100,
    width: 100,
    borderRadius: 10,
  },
  icon: {
    fontSize: 30,
    marginLeft: 22,
  },
  topBorder: {
    height: 20,
    backgroundColor: '#F7F6F6',
  },
  normalItem: {
    height: 90,
    paddingHorizontal: 18,
    borderBottomWidth: 1,
    borderColor: '#BBBBBB'
  },

  bottomInput: {
    textAlignVertical: 'top',
    paddingLeft: 20,
    borderRadius: 6,
    borderWidth: 1,
    // borderBottomWidth: 1,
    borderColor: '#BBBBBB',
    marginLeft: 20,
    marginTop: 50,
    marginRight: 20,
    height: 190
  }

};
styles = createStyles(styles);

// export default MineInfo;
export default connect((Store) => {
  return { userInfo: Store.UserRenderData.userInfo };
}, { setUserInfo: userActions.RenderData })(MineInfo);