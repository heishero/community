import React from 'react';
import { TextInput } from 'react-native';
import { createStyles } from '../../../utils/view';

const TextInputComponent = ({ value, onChangeText, name, ...props }) => {
  return (
    <TextInput
      style={styles.input}
      value={value}
      onChangeText={(value) => onChangeText(name, value)} // ... Bind the name here
      {...props}
    />
  );
};

let styles = {
  input: {
    width: 606,
    height: 90,
    borderBottomWidth: 1,
    borderColor: '#E6E6E6',
    paddingHorizontal: 9,
    fontSize: 26,
    color: '#333',
    paddingVertical: 0,
  }
};
styles = createStyles(styles);

export default TextInputComponent;