import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import { createStyles } from '../../../utils/view';

const MineButton = ({ title, onPress, ...props }) => {
  return (
    <TouchableOpacity style={styles.button} onPress={onPress} {...props}>
      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
  );
};

let styles = {
  button: {
    width: 606,
    height: 94,
    borderRadius: 47,
    backgroundColor: '#2AB6F3',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    fontSize: 36,
    color: '#fff'
  }
};
styles = createStyles(styles);

export default MineButton;