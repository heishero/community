/*
 * @Author: yixin
 * @Date: 2018-10-19 23:42:34
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-04 16:56:01
 * 个人中心-我的收益模块
 */
import React from 'react';
import { View } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../../components/CText';
import { createStyles } from '../../../utils/view';
import PageFn from '../../../utils/page';
import Icon from 'react-native-vector-icons/icomoon';

const MineProfit = ({ componentId, userInfo }) => {
  if (!userInfo) return null;
  return (
    <View style={styles.profit}>
      <Flex style={styles.profitTitle} onPress={() => { PageFn.push(componentId, 'MineProfit') }} justify="space-between" align="center">
        <CText color="#7d7d7d" size="24">我的收益:</CText>
        <CText color="#2AB6F3" size="26">查看详情</CText>
      </Flex>
      <Flex style={styles.profitContent}>
        <Flex style={{ flex: 1 }} direction="column" justify="center" align="center">
          <Icon style={styles.icon} name="yueshoyi" />
          <CText color="#7D7D7D" size="28">月收入：<CText color="#F6A72B" size="28">{userInfo.monthIcom}元</CText></CText>
        </Flex>
        <Flex style={{ flex: 1 }} direction="column" justify="center" align="center">
          <Icon style={styles.icon} name="zuotianshoyi" />
          <CText color="#7D7D7D" size="28">昨日收入：<CText color="#F6A72B" size="28">{userInfo.yesterdayIcom}</CText></CText>
        </Flex>
      </Flex>
    </View>
  );
};

let styles = {
  profit: {
    width: 702,
    height: 235,
    backgroundColor: '#FFFFFF',
    borderRadius: 20,
    paddingHorizontal: 22,
  },
  profitTitle: {
    marginTop: 25,
    height: 40,
    borderColor: '#DADADA'
  },
  profitContent: {
    width: 659,
    height: 140,
    marginTop: 10,
  },
  icon: {
    fontSize: 55,
    color: '#2AB6F3',
    marginBottom: 20,
  },
};
styles = createStyles(styles);

export default MineProfit;