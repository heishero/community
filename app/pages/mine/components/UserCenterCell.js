import React from 'react';
import { Flex, Toast } from 'antd-mobile-rn';
import CText from '@/components/CText';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '@/utils/page';
import { View, Alert } from 'react-native';
import * as WeChat from 'react-native-wechat';
import { wxBind } from '@/service/wx';
import Storage from '@/utils/storage';
import { createStyles, Rem } from '@/utils/view';

export default class userCenterCell extends React.PureComponent {


  DrawMoneyData= () => {

    Alert.alert('提示', '您尚未绑定微信', [
      {
        text: '取消', onPress: () => {
        }
      },
      {
        text: '确定绑定', onPress: () => {
          this.getBindWechat();
        }
      },
    ]);
  }

  getBindWechat = async () => {
    const token = await Storage.load('token');
    WeChat.sendAuthRequest('snsapi_userinfo', 'chaihe')
      .then(responseCode => {
        // 返回code码，通过code获取access_token
        wxBind({ code: responseCode.code, token }).then(data => {
          if (data.status === 'success') {
            Toast.success('绑定成功');
            this.props.init();
          } else {
            Toast.fail(data.failedMsg);
          }
        }).catch(err => { console.log(err) });
      }).catch(err => {
        Alert.alert('登录授权发生错误：', err.message, [{
          text: '确定'
        }]);
      });
  }

  render() {
    const { data, wx_openid } = this.props;
    return (
      <Flex key={data.text} style={styles.menuItem} onPress={() => { if (data.path) { PageFn.push(this.props.componentId, data.path) }
      else {
        if (wx_openid === null) {
          this.DrawMoneyData();
        } else {
          PageFn.push(this.props.componentId, 'WithDraw');
        }
      }
      }}>
        <Flex style={{ flex: 1 }} direction="row" justify="space-between" align="center">
          <Flex direction="row" justify="flex-start" align="center">
            <Flex justify="center" align="center" style={[styles.menuWrapper, { backgroundColor: data.color }]}><Icon style={styles.menuIcon} name={data.icon} /></Flex>
            <View><CText color="#585858" size="30" >{data.text}</CText></View>
          </Flex>
          <Icon name="xiang-you" style={styles.menuRightIcon} />
        </Flex>
      </Flex>
    );
  }
}
const styles = createStyles({
  menuItem: {
    height: 90,
    paddingHorizontal: 26,
  },
  menuIcon: {
    fontSize: 26,
    color: '#fff',
  },
  menuWrapper: {
    width: 40,
    height: 40,
    borderRadius: 10,
    marginRight: 30
  },
  menuRightIcon: {
    marginRight: -15,
    color: '#E2E2E2',
    fontSize: 18,
  }
});
