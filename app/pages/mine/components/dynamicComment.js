import React from 'react';
import { View, TouchableOpacity, Image, Text } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../../components/CText';
import { createStyles, Rem } from '../../../utils/view';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../../utils/page';

const DynamicComment = ({ data, componentId }) => {
  return (
    <TouchableOpacity style={styles.wrapper} onPress={() => { if (componentId) { PageFn.push(componentId, 'MineDynamicDetail', { id: data.id }) } }}>
      <Image source={{ uri: data.headimgurl }} style={styles.img} />
      <View style={styles.content}>
        <Flex style={{ width: Rem(550), marginBottom: Rem(26) }} direction="row" justify="space-between" align="center">
          <CText size="30" color="#414141">{data.nick_name}</CText>
          <CText color="#7F7F7F" size="22">圈子活跃度：<Text style={{ color: '#FDAC42' }}>LEV{data.grade}</Text></CText>
        </Flex>
        <Text style={styles.comment}>{data.content}</Text>
        <View style={styles.groupBox}>
          <Image source={{ uri: data.crowd_headimgurl }} style={styles.groupImg} />
          <Flex direction="column" justify="flex-start" align="flex-start">
            <View style={styles.groupName}><CText size="30" color="#414141">{data.name}</CText></View>
            <CText size="20" color="#434343">{data.introduce}</CText>
          </Flex>
        </View>
        <Flex style={styles.bottomBox} direction="row" justify="flex-end" align="center">
          <Icon name="taolun" style={styles.chatIcon} />
          <CText size="20" color="#7f7f7f">{data.comment}条</CText>
        </Flex>
      </View>
    </TouchableOpacity>
  );
};

let styles = {
  wrapper: {
    width: 698,
    // height: 351,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: '#fff',
    marginBottom: 26,
    padding: 26,
    borderRadius: 20,
    shadowOffset: { width: 6, height: 6 },
    shadowColor: '#000',
    shadowOpacity: 0.14,
    shadowRadius: 8,
  },
  img: {
    width: 70,
    height: 70,
    borderRadius: 35,
    backgroundColor: '#F7F7F7',
    marginRight: 27,
  },
  content: {
    width: 550,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  comment: {
    fontSize: 22,
    color: '#7f7f7f',
    lineHeight: 30,
  },
  groupBox: {
    height: 90,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 20
  },
  groupImg: {
    width: 100,
    height: 90,
    borderRadius: 10,
    backgroundColor: '#FDAC42',
    marginRight: 26
  },
  groupName: {
    marginBottom: 21,
  },
  bottomBox: {
    marginTop: 20,
    width: 550,
    paddingRight: 17
  },
  chatIcon: {
    fontSize: 30,
    color: '#acacac',
    marginRight: 14,
  }
};
styles = createStyles(styles);
export default DynamicComment;
