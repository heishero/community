/*
 * @Author: yixin
 * @Date: 2018-10-19 23:42:34
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-07 21:34:37
 * 用户模块
 */
import React from 'react';
import { TouchableOpacity, Image, View, ImageBackground ,Text} from 'react-native';
import CText from '../../../components/CText';
import { createStyles, Rem } from '../../../utils/view';
import { PageFn } from '../../../utils/page';

const User = ({ componentId, userInfo, exit }) => {
  const { nick_name, headimgurl,signature} = userInfo;
  return (
    <ImageBackground source={require('../../../assets/img/mineBg.png')} resizeMode="stretch" style={styles.wrapper}>
      <View style={styles.box}>
        {!!headimgurl && <View style={styles.avatarBox}><Image source={{ uri: headimgurl }} roundAsCircle={true} resizeMode="cover" style={styles.avatar} /></View>}
        <View>
          <View ><CText size="32" color="#FFFEFE">{nick_name}</CText></View>
          <Text style={{marginTop:signature?Rem(40):0,color:"#FFFEFE"}}>{signature}</Text>
        </View>
        <TouchableOpacity onPress={() => { PageFn.push('Mine', 'MineInfo') }} style={styles.btn}><CText size="24" color="#fff">修改资料</CText></TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

let styles = {
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    backgroundColor: '#fff',
    height: 420,
  },
  box: {
    width: 750,
    height: 148,
    marginTop: 104,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  avatar: {
    width: 148,
    height: 148,
    borderRadius: 74,
    marginLeft: 24,
    marginRight: 30,
  },
  btn: {
    width: 126,
    height: 50,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: '#fff',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    right: 30,
  }
};
styles = createStyles(styles);

export default User;