import React from 'react';
import { TouchableOpacity } from 'react-native';
import CText from '../../../components/CText';
import { createStyles } from '../../../utils/view';
import Icon from 'react-native-vector-icons/icomoon';

const IncomeTitle = ({ title, path }) => {
  return (
    <TouchableOpacity style={styles.wrapper}>
      <CText color="#3D3D3D" size="30">{title}</CText>
      {path && <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}><CText color="#FDAC42" size="24">查看详情 <Icon name="cha-kan-geng-duo" /></CText></TouchableOpacity>}
    </TouchableOpacity>
  );
};

let styles = {
  wrapper: {
    height: 60,
    width: 750,
    paddingHorizontal: 26,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#F0F0F0',
    marginBottom: 26
  }
};
styles = createStyles(styles);
export default IncomeTitle;
