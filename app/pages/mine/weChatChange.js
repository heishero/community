import React, { Component } from 'react';
import { View, TouchableOpacity, Text, ScrollView, StyleSheet, TextInput, Dimensions, Keyboard } from 'react-native';
import UserCenterCell from '@/pages/mine/components/userCenterCell';
import { Flex } from 'antd-mobile-rn';
import CText from '@/components/CText';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '@/utils/page';
import NormalHeader from '@/components/NormalHeader';
import { Rem } from '@/utils/view';
import Storage from '../../utils/storage';
import userActions from '../../controllers/user/actions';
import { connect } from 'react-redux';

const ScreenWidth =  Dimensions.get('window').width;


 class weChatChange extends React.PureComponent {

  constructor() {
    super();
    this.startTime = 0;
    this.state = {
      mobile: '',
      validate_code: '',
      password: '',
      twicePassword: '',
    };

  }

handleChange = (name, value) => {
  this.setState({
    [name]: value
  });
}



  render() {
    const { mobile, validate_code, password, twicePassword } = this.state;

    return (
      <View style={{ flex: 1 }}>
          <NormalHeader leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId);
          }}>修改手机号码 </NormalHeader>
        <ScrollView>
          <Flex onPress={() => { Keyboard.dismiss() }} style={{ marginTop: Rem(100) }} direction="column" justify="flex-start" align="center">
            <TextInput style={{ borderBottomColor: '#E6E6E6', borderBottomWidth: 1, width: ScreenWidth - 30, height: 40 }} name="mobile" keyboardType="number-pad" onChangeText={(mobile) => { this.setState({ mobile }) }} placeholder="请输入手机号码" />
        <Flex>
              <TextInput style={styles.input} onChangeText={(validate_code) => { this.setState({ validate_code }) }} placeholder="请输入微信账号" />
            </Flex>

        <Flex onPress={() => { this.submit() }} style={styles.loginBtn} justify="center" align="center">
              <CText size="36" color="#fff">确定</CText>
            </Flex>
          </Flex>

        </ScrollView>
        </View>
    );
  }
}
const styles = StyleSheet.create({
  headerIcon: {
    fontSize: 20,
    color: '#000',
  },
  wrapper: {
    flex: 1,
    backgroundColor: '#fff',
  },
  back: {
    fontSize: 32,
    color: '#333',
  },
  input: {
    width: ScreenWidth - 150,
    height: 40,
    marginTop: 40,
    borderBottomWidth: 1,
    borderColor: '#E6E6E6',
    fontSize: 15,
    color: '#333',
  },
  smsBtn: {
    borderWidth: 1,
    borderColor: '#2AB6F3',
    marginTop: 30,
    width: 120,
    height: 40,
    borderRadius: 45,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginBtn: {
    width: ScreenWidth - 90,
    height: 48,
    backgroundColor: '#2ab6f3',
    borderRadius: 24,
    marginTop: 130,
  },
  solid: {
    width: 100,
    height: 1,
    backgroundColor: '#444444',
    marginHorizontal: 32,
  },
  noMatchPWD: {
    color: '#FF0000',
    fontSize: 24,
    width: 606,
    marginTop: 10,
  }

});
  

export default connect((Store) => {
    return { userInfo: Store.UserRenderData.userInfo };
  }, { clearUserInfo: userActions.Clear, setUserInfo: userActions.RenderData })(weChatChange);