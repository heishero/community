/*
 * @Author: yixin
 * @Date: 2018-10-28 21:39:26
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-11 23:32:51
 * 注册
 */
import React from 'react';
import { View, TextInput, BackHandler, Keyboard } from 'react-native';
import { Flex, Toast } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles, Rem } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import toolsVerifi from '../../utils/verifi';
import { getCode, phoneBind } from '../../service/user';
import PageFn from '../../utils/page';
import Storage from '../../utils/storage';
import { goToMain } from '@/utils/navigate';

class BindPhone extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.startTime = 0;
    this.state = {
      mobile: '',
      password: '',
      code: '',
      codeText: '发送验证码'
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackAndroid);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackAndroid);
  }

  onBackAndroid() {
    return true;
  }

  /**
   * 发送获取验证码
   */
  onSendCode = () => {
    // 60秒后重新获取
    let { mobile } = this.state;
    mobile = mobile.replace(/ /g, '');
    if (!toolsVerifi.isPhone(mobile)) {
      return Toast.fail('请输入正确的手机号码');
    }

    if (this.startTime === 0) {
      this.startTime = 60;

      this.timeInter = setInterval(() => {
        if (this.startTime > 0) {
          this.setState({ codeText: `${this.startTime}秒后重新获取` });
          this.startTime--;
        } else {
          this.setState({ codeText: `发送验证码` });
          clearInterval(this.timeInter);
        }
      }, 1000);

      // 发送登录验证码
      getCode(mobile);
    } }

  toBind= async () => {
    const { mobile, password, code } = this.state;
    try {
      const token = await Storage.load('token');
      if (mobile && password && code) {
        phoneBind({ mobile, password, validate_code: code, token }).then((data) => {
          if (data.status === 'success') {
            Toast.success('绑定成功');
            goToMain();
          }
        });
      }
    } catch (error) {

    }

  }

  render() {
    const { mobile, password, code, codeText } = this.state;
    return (
      <View style={styles.wrapper} >
        <NormalHeader>手机号绑定</NormalHeader>
        <Flex style={{ marginTop: Rem(40) }} onPress={() => { Keyboard.dismiss() }} direction="column" justify="flex-start" align="center">
          <TextInput keyboardType="number-pad" onChangeText={(mobile) => { this.setState({ mobile }) }} clear value={mobile} placeholder="请输入您的手机号码" TextColor="#606060" placeholderTextColor="#606060" style={styles.input} />
          <TextInput secureTextEntry onChangeText={(password) => { this.setState({ password }) }} clear value={password} placeholder="请设置您的登录密码" placeholderTextColor="#606060" style={styles.input} />
          <Flex style={styles.yzmBox} justify="space-between" align="center" >
            <TextInput onChangeText={(code) => { this.setState({ code }) }} keyboardType="number-pad" value={code} placeholder="请输入短信验证码" placeholderTextColor="#606060" style={styles.yzmInput} />
            <Flex style={styles.yzmBtn} justify="center" align="center" onPress={() => { this.onSendCode() }}><CText size="26" color="#606060">{codeText}</CText></Flex>
          </Flex>
          <Flex style={styles.loginBtn} justify="center" align="center" onPress={() => { this.toBind() }}>
            <CText size="40" color="#fff">确定绑定</CText>
          </Flex>
        </Flex>
      </View>
    );
  }

}

let styles = {
  wrapper: {
    flex: 1,
  },
  back: {
    fontSize: 32,
    color: '#000',
  },
  input: {
    width: 604,
    height: 80,
    borderWidth: 1,
    borderColor: '#606060',
    borderRadius: 16,
    paddingHorizontal: 42,
    marginBottom: 46,
    fontSize: 26,
    color: '#606060',
    marginLeft: 0,
    paddingVertical: 0,
  },
  loginBtn: {
    width: 604,
    height: 80,
    backgroundColor: '#2ab6f3',
    borderRadius: 16,
    marginTop: 53,
    marginBottom: 30
  },
  solid: {
    width: 100,
    height: 1,
    backgroundColor: '#606060',
    marginHorizontal: 32,
  },
  otherPlatform: {
    marginTop: 160,
  },
  yzmBox: {
    width: 604,
    height: 80,
  },
  yzmInput: {
    width: 360,
    height: 80,
    borderRadius: 16,
    borderWidth: 1,
    borderColor: '#606060',
    paddingHorizontal: 37,
    fontSize: 26,
    color: '#606060',
    marginLeft: 0,
    paddingVertical: 0,
  },
  yzmBtn: {
    width: 210,
    height: 80,
    borderRadius: 16,
    borderWidth: 1,
    borderColor: '#606060',
  }
};
styles = createStyles(styles);

export default BindPhone;
