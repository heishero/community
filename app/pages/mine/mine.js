/*
 * @Author: yixin
 * @Date: 2018-10-19 23:40:27
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-22 23:06:57
 * 个人中心
 */
import React from 'react';
import { View, Alert, ScrollView, Dimensions } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles, isIphoneX } from '../../utils/view';
import Icon from 'react-native-vector-icons/icomoon';
import { connect } from 'react-redux';
import userActions from '../../controllers/user/actions';
import Storage from '../../utils/storage';
import { getInfo } from '../../service/user';
import { Navigation } from 'react-native-navigation';
import PageFn from '../../utils/page';
import User from './components/user';
import MineProfit from './components/mineProfit';
import Bottom from '../common/bottom';
import UserCenterCell from '@/pages/mine/components/userCenterCell';


const menu1 = [
  { text: '我的活动', icon: 'wodong-dongtai', color: '#E5CD48', path: 'MyActivity' },
  // { text: '我的基因', icon: 'wode-jiying', color: '#589AF9' },
  { text: '我的订单', icon: 'dingdan', color: '#FFAF60', path: 'MineOrder' },
  { text: '我的动态', icon: 'wode-dongtai', color: '#7AB6EE', path: 'MineFind' },
  { text: '提现资料', icon: 'tixianziliao', color: '#F47C76', path: '' },
  // { text: '我的评论', icon: 'dongtai', color: '#60beff', path: 'MineDynamic' },
  { text: '设置', icon: 'wode-shezhi', color: '#298BEB', path: 'UserSetting' },
  { text: '地址管理', icon: 'dizhi', color: '#FFAF60', path: 'Address' },
  // { text: '创建圈子', icon: 'chengweidongdaozhu', color: '#FDAC42', path: 'MineHost' },
  { text: '关于我们', icon: 'guan-yuwomen', color: '#FDAC42', path: 'About' },
];
const ScreenWidth = Dimensions.get('window').width;

class Mine extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      },
    };
  }
  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
    this.init = this.init.bind(this);
  }

  async init() {
    const token = await Storage.load('token');
    if (token) {
      try {
        const userinfo = await getInfo(token);
        this.props.setUserInfo({
          userInfo: { ...userinfo.data }
        });
      } catch (error) {

      }
    }

  }


  exit = () => {
    Alert.alert('提示', '确定注销登录吗？', [
      {
        text: '确定', onPress: () => {
          this.props.clearUserInfo();
        }
      },
      {
        text: '取消', onPress: () => {

        }
      },
    ]);
  }

  itemCell=(item) => {
    return (
      <View>
        <UserCenterCell componentId={this.props.componentId} data={item[0]} />
        <View style={{ height: 1, backgroundColor: '#EFEFEF', width: ScreenWidth * 2 }} />
        <UserCenterCell componentId={this.props.componentId} data={item[1]} />
        <View style={{ height: 1, backgroundColor: '#EFEFEF', width: ScreenWidth * 2 }} />
        <UserCenterCell componentId={this.props.componentId} data={item[2]} />
        <View style={{ height: 1, backgroundColor: '#EFEFEF', width: ScreenWidth * 2 }} />
        <UserCenterCell init={this.init} wx_openid={this.props.userInfo.wx_openid} componentId={this.props.componentId} data={item[3]} />
        <View style={{ height: 10, backgroundColor: '#EFEFEF', width: ScreenWidth * 2 }} />
        <UserCenterCell componentId={this.props.componentId} data={item[4]} />
        <View style={{ height: 10, backgroundColor: '#EFEFEF', width: ScreenWidth * 2 }} />
        <UserCenterCell componentId={this.props.componentId} data={item[5]} />
        <View style={{ height: 1, backgroundColor: '#EFEFEF', width: ScreenWidth * 2 }} />
        <UserCenterCell componentId={this.props.componentId} data={item[6]} />
      </View>
    );
  }

  componentDidAppear() {
    this.init();
  }

  render() {
    const { userInfo, componentId } = this.props;
    return (
      <View style={styles.wrapper}>
        <ScrollView>
          {/* 个人信息 */}
          <User userInfo={userInfo} componentId={componentId} />
          {/* 我的收益 */}
          <View style={styles.profit}><MineProfit userInfo={userInfo} componentId={componentId} /></View>
          <View style={styles.menuBox}>
            {
              this.itemCell(menu1)
            }
          </View>
          <Flex justify="center" align="center" style={styles.bottom} onPress={() => { PageFn.push(this.props.componentId, 'MineHost') }}>
            <CText size="36" color="white">创建圈子</CText>
          </Flex>
        </ScrollView>
        <Bottom index="3" />
      </View>
    );
  }
}

let styles = {
  wrapper: {
    backgroundColor: '#F4F4F4',
    flex: 1,
    ios: {
      marginTop: isIphoneX ? -88 : -64
    },
    android: {
    }
  },
  profit: {
    marginTop: -118,
    marginLeft: 24,
    marginBottom: 30,
  },
  menuBox: {
    backgroundColor: '#fff'
  },
  solid: {
    height: 20,
    backgroundColor: '#F7F6F6',
  },
  menuWrapper: {
    width: 40,
    height: 40,
    borderRadius: 10,
    marginRight: 20
  },
  menuIcon: {
    fontSize: 24,
    color: '#fff'
  },

  menuRightIcon: {
    color: '#E2E2E2',
    fontSize: 24,
  },
  bottom: {
    marginVertical: 100,
    backgroundColor: '#22B7F8',
    height: 90,
    marginLeft: 100,
    marginRight: 100,
    borderRadius: 10,
  }
};
styles = createStyles(styles);

export default connect((Store) => {
  return { userInfo: Store.UserRenderData.userInfo };
}, { clearUserInfo: userActions.Clear, setUserInfo: userActions.RenderData })(Mine);