import React from 'react';
import { View, ScrollView, Text, TouchableOpacity } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import { showDetail } from '../../service/user';
import { createStyles, Rem } from '../../utils/view';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../utils/page';
import Storage from '../../utils/storage';
import { Navigation } from 'react-native-navigation';

class MineProfit extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      balance: [],
      waste: []
    };
    Navigation.events().bindComponent(this);
  }

  init = async () => {
    const token = await Storage.load('token');
    const balance = await showDetail({ token });

    console.log('balance===>', balance);

    this.setState({ balance: balance.data, waste: balance.data.waste });
  }


  componentDidAppear() {
    this.init();
  }

  render() {
    const balance = this.state.balance;
    const waste = this.state.waste;
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}>
          我的收益
        </NormalHeader>
        <View style={styles.middle_panel}>
          <Flex style={styles.column1} direction="row" justify="between">
            <Flex style={styles.ye} direction="row">
              <Text style={styles.yetext}>余额:</Text>
              <Text style={styles.yevalue}>{balance.balance}</Text>
            </Flex>
            <View style={styles.txbtn}>
              <TouchableOpacity onPress={() => { PageFn.push(this.props.componentId, 'WithDraw') }}><Text style={styles.txtext}>立即提现</Text></TouchableOpacity>
            </View>
          </Flex>
          <View style={styles.br} />
          <Flex style={styles.shouru} direction="row" justify="around">
            <Flex direction="column">
              <View>
                <Text style={styles.shouru_text}>月收入:</Text>
              </View>
              <View>
                <Text style={styles.shouru_value}>{balance.monthIcom}</Text>
              </View>
            </Flex>
            <Flex direction="column">
              <View>
                <Text style={styles.shouru_text}>昨日收入:</Text>
              </View>
              <View>
                <Text style={styles.shouru_value}>{balance.yesterdayIcom}</Text>
              </View>
            </Flex>
          </Flex>
        </View>
        {
          waste.length !== 0 && Object.keys(waste).map((v) => {
            return (
              <ScrollView key={String(v)} style={styles.scroll}>
                <View style={styles.month}>
                  <Text style={styles.month_text}>{v}月</Text>
                </View>
                {
                  waste[v].map((value, key) => {
                    return (
                      <View style={styles.order} key={value.id}>
                        <Flex direction="row" justify="between">
                          <Flex style={{ flex: 1 }} direction="column" align="flex-start">
                            <Text style={styles.name} numberOfLines={1}>{value.name}</Text>
                            <Text style={styles.order_date}>{value.created_at}</Text>
                          </Flex>
                          <Text style={styles.order_value}>+{value.money}</Text>
                        </Flex>
                        <View style={styles.line} />
                      </View>
                    );
                  })
                }
              </ScrollView>
            );
          })
        }
      </View>
    );
  }}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#F7F7F7' },
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
  middle_panel: {
    marginLeft: 26,
    marginRight: 26,
    marginTop: 38,
    width: 698,
    height: 258,
    backgroundColor: '#FFFFFF',
    borderRadius: 20,
  },

  column1: {
    marginTop: 22,
    marginLeft: 28,
    marginRight: 28,
  },
  yetext: {
    color: '#545454',
    fontSize: 26,

  },
  yevalue: {
    color: '#FDAC42',
    fontSize: 32,
    marginLeft: 12
  },
  txbtn: {
    borderRadius: 16,
    borderColor: '#FDAC42',
    width: 111,
    height: 50,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  txtext: {
    color: '#FDAC42',
    fontSize: 22
  },

  br: {
    height: 1,
    width: 670,
    marginLeft: 14,
    backgroundColor: '#E3E3E3',
    marginTop: 25
  },

  shouru_text: {
    color: '#5E5E5E',
    fontSize: 24
  },
  shouru_value: {
    color: '#FDAC42',
    fontSize: 44,
  },
  shouru: {
    marginTop: 30
  },
  scroll: {
    marginTop: 70
  },
  month: {
    backgroundColor: '#EDEDED',
    height: 72,
    marginTop: 27,
    marginBottom: 22,
  },
  month_text: {
    marginLeft: 21,
    color: '#686868',
    fontSize: 28,
    marginTop: 15
  },
  order: {
    marginLeft: 30,
    marginRight: 30,
  },
  order_name: {
    fontSize: 28,
    color: '#C9842D',
    marginTop: 22,
    fontWeight: '100',

  },
  order_date: {
    color: '#BBBBBB',
    fontSize: 24,
    marginTop: 0
  },
  order_value: {
    fontSize: 28,
    color: '#FF8F2B',
  },
  line: {
    height: 1,
    backgroundColor: '#E9E9E9',
    marginTop: 12,
    marginBottom: 12
  },
};
styles = createStyles(styles);

export default MineProfit;