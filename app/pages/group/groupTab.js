/*
 * @Author: yixin
 * @Date: 2018-10-27 15:40:42
 * @Last Modified by: yixin
 * @Last Modified time: 2019-03-19 22:14:17
 * 圈子内容Tab
 */
import React from 'react';
import { View, TouchableOpacity, ImageBackground, FlatList } from 'react-native';
import { Flex, Tabs, Toast } from 'antd-mobile-rn';
import CText from '../../components/CText';
import Icon from 'react-native-vector-icons/icomoon';
import { createStyles } from '../../utils/view';
import { getGroupDetail } from '../../service/group';
import Storage from '../../utils/storage';
import PageFn from '../../utils/page';
import GroupTop from './components/groupTop';
import DynamicItem from '../find/components/dynamicItem';
import GroupArticleJoin from '../find/components/groupArticleJoin';
import { Navigation } from 'react-native-navigation';
import { topicUseful } from '../../service/topic';

const tabs = [
  { title: '动态' },
  { title: '干货' },
];

class GroupTab extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      groupDetail: {},
      token: ''
    };
    Navigation.events().bindComponent(this);
  }

  componentDidAppear() {
    this.init();
  }

  async init() {
    const token = await Storage.load('token');
    let { data } = await getGroupDetail({ id: this.props.id, token });
    this.setState({ groupDetail: data, token });
  }

  toTopicPage = () => {
    PageFn.push(this.props.componentId, 'CommentList', { id: this.props.id });
  }

  useful = async ({ topic_id, index }) => {
    try {
      await topicUseful({ token: this.state.token, topic_id });
      Toast.success('加力成功');
      let topic = [].concat(this.state.groupDetail.topic);
      topic[index].useful++;
      this.setState({ groupDetail: { ...this.state.groupDetail, topic }});
    } catch (error) {

    }
  };

  render() {
    const { groupDetail } = this.state;
    console.log(8888, groupDetail.topic);
    return (
      <View style={styles.wrapper}>
        <ImageBackground style={styles.headerBg} resizeMode="stretch" source={require('../../assets/img/groupbg.png')}>
          <View style={styles.header}>
            <TouchableOpacity onPress={() => { PageFn.pop(this.props.componentId) }} style={styles.backBox}>
              <Icon style={styles.back} name="fanhui" />
            </TouchableOpacity>
            <TouchableOpacity>
              <Icon style={styles.fenxiang} name="fenxiang" />
            </TouchableOpacity>
          </View>
          <Flex justify="center">{groupDetail.crowd && <GroupTop data={groupDetail.crowd} />}</Flex>
        </ImageBackground>
        <Tabs tabs={tabs}
          swipeable={false}
          tabBarUnderlineStyle={{ backgroundColor: '#FDAC42', width: 45 }}
          tabBarActiveTextColor="#FDAC42"
          tabBarInactiveTextColor="#858585">
          {/* tab1 */}
          <View style={{ flex: 1 }}>
            {!!groupDetail.topic && groupDetail.topic.length > 0 && <FlatList
              data={groupDetail.topic}
              renderItem={({ item, index }) => <DynamicItem useful={this.useful.bind(this)} index={index} componentId={this.props.componentId} key={item.id} data={item} />}
            />}
            <TouchableOpacity style={styles.create} onPress={() => {
              PageFn.push(this.props.componentId, 'ReleaseDynamic', { id: this.props.id, reFreshDynamic: this.init });
            }}>
              <CText size="30" color="#fff">发 布</CText>
              <CText size="30" color="#fff">动 态</CText>
            </TouchableOpacity>
          </View>
          {/* tab2 */}
          <View>
            {!!groupDetail.article && groupDetail.article.length > 0 && <FlatList
              data={groupDetail.article}
              renderItem={({ item }) => <GroupArticleJoin key={item.id} data={item} componentId={this.props.componentId} />}
            />}
          </View>
        </Tabs>
      </View>
    );
  }
}

let styles = {
  wrapper: {
    flex: 1,
    backgroundColor: '#F7F7F7'
  },
  headerBg: {
    ios: {
      height: 360,
    },
    android: {
      height: 360,
    },
    marginBottom: 40,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    ios: {
      height: 128,
      paddingTop: 35,
    },
    android: {
      height: 90,
    },
    paddingHorizontal: 26,
  },
  backBox: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  back: {
    fontSize: 36,
    color: '#000',
    marginRight: 16,
  },
  fenxiang: {
    fontSize: 36,
    color: '#fff',
  },
  topBox: {
    position: 'absolute',
    top: 15,
    width: 560,
    left: 95,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  create: {
    height: 120,
    width: 120,
    borderRadius: 60,
    backgroundColor: '#FDAC42',
    position: 'absolute',
    bottom: 194,
    right: 50,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 0,
  }
};
styles = createStyles(styles);

export default GroupTab;