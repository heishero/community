import React from 'react';
import { View, TouchableOpacity, Image, Text } from 'react-native';
import { Flex, Toast } from 'antd-mobile-rn';
import CText from '../../../components/CText';
import { createStyles, Rem } from '../../../utils/view';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '../../../utils/page';
import { topicUseful } from '../../../service/topic';
import Storage from '../../../utils/storage';

const useful = async ({ topic_id, usefulRefresh, index }) => {
  try {
    const token = await Storage.load('token');
    await topicUseful({ token, topic_id });
    Toast.success('加力成功');
    usefulRefresh(index);
  } catch (error) {

  }
};

const GroupListComment = ({ componentId, path, data, usefulRefresh, index }) => {
  return (
    <TouchableOpacity onPress={() => { if (path) { PageFn.push(componentId, 'DynamicDetail', { id: data.id }) } }} style={styles.wrapper}>
      <Image source={{ uri: data.headimgurl }} style={styles.img} />
      <View style={styles.content}>
        <Flex style={{ width: Rem(550), marginBottom: Rem(10) }} direction="row" justify="space-between" align="center">
          <CText size="30" color="#414141">{data.nick_name}</CText>
          <CText color="#7F7F7F" size="22">圈子活跃度：<Text style={{ color: '#FDAC42' }}>LEV{data.active}</Text></CText>
        </Flex>
        <Text style={styles.comment}>{data.content}</Text>
      </View>
      <Flex style={styles.bottom}>
        <Flex style={{ marginRight: Rem(48) }} direction="row" justify="flex-end" alignItems="center">
          <Icon style={styles.icon} name="taolun" />
          <CText color="#C6C6C6" size="24">讨论</CText>
        </Flex>
        <Flex style={{ marginRight: Rem(48) }} onPress={() => { useful({ topic_id: data.id, usefulRefresh, index }) }} direction="row" justify="flex-end" alignItems="center">
          <Icon style={[styles.icon, data.useful > 0 && styles.iconActive]} name="jiali" />
          <CText color="#C6C6C6" size="24">加力</CText>
        </Flex>
      </Flex>
    </TouchableOpacity>
  );
};

let styles = {
  wrapper: {
    width: 698,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: '#fff',
    marginBottom: 24,
    padding: 24,
    borderRadius: 20,
    shadowOffset: { width: 6, height: 6 },
    shadowColor: '#000',
    shadowOpacity: 0.14,
    shadowRadius: 8,
    marginTop: 30
  },
  img: {
    width: 70,
    height: 70,
    borderRadius: 35,
    marginRight: 27,
  },
  content: {
    width: 550,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  comment: {
    fontSize: 22,
    color: '#7f7f7f',
    lineHeight: 30,
    marginBottom: 26,
  },
  bottom: {
    position: 'absolute',
    height: 30,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    bottom: 16,
    left: 26,
    right: 26,
  },
  icon: {
    fontSize: 26,
    color: '#C6C6C6',
    marginRight: 10
  },
  iconActive: {
    color: '#FDAC42'
  }
};
styles = createStyles(styles);
export default GroupListComment;
