/*
 * @Author: yixin
 * @Date: 2018-10-28 22:37:36
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-07 16:29:55
 * 圈子动态
 */
import React from 'react';
import { View, TouchableOpacity, ScrollView } from 'react-native';
import { Flex, TabBar } from 'antd-mobile-rn';
import CText from '../../../components/CText';
import { createStyles } from '../../../utils/view';
import Header from '../../../components/header';
import GroupListComment from './groupListComment';
import PageFn from '../../../utils/page';

class GroupDynamic extends React.Component {

  static navigatorStyle = {
    navBarHidden: true,
    navBarNoBorder: true,
    drawUnderNavBar: true,
    drawUnderTabBar: true,
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header componentId={this.props.componentId} title="圈子昵称" />
        <ScrollView style={styles.wrapper}>
          <Flex direction="column" justify="flex-start" align="center" style={styles.list}>
            <GroupListComment path="DynamicDetail" componentId={this.props.componentId} />
            <GroupListComment path="DynamicDetail" componentId={this.props.componentId} />
            <GroupListComment path="DynamicDetail" componentId={this.props.componentId} />
            <GroupListComment path="DynamicDetail" componentId={this.props.componentId} />
            <GroupListComment path="DynamicDetail" componentId={this.props.componentId} />
            <GroupListComment path="DynamicDetail" componentId={this.props.componentId} />
          </Flex>
        </ScrollView>
        <TouchableOpacity style={styles.create} onPress={() => {
          PageFn.push(this.props.componentId, 'ReleaseDynamic');
        }}>
          <CText size="30" color="#fff">我 也</CText>
          <CText size="30" color="#fff">要 发</CText>
        </TouchableOpacity>
      </View>
    );
  }
}

let styles = {
  wrapper: {
    flex: 1,
    marginBottom: 100,
    backgroundColor: '#F7F7F7'
  },
  list: {
    marginTop: 26,
  },
  create: {
    height: 120,
    width: 120,
    borderRadius: 60,
    backgroundColor: '#FDAC42',
    position: 'absolute',
    bottom: 194,
    right: 50,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 20,
  }
};
styles = createStyles(styles);

export default GroupDynamic;
