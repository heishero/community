/*
 * @Author: yixin 
 * @Date: 2018-12-07 13:58:00 
 * @Last Modified by: yixin
 * @Last Modified time: 2019-02-24 17:22:50
 * 圈子tab头部组件（圈子昵称，圈子介绍）
 */
import React from 'react';
import { View, Image, Text } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../../components/CText';
import { createStyles, Rem } from '../../../utils/view';

const GroupTop = ({ data }) => {
  return (
    <View style={styles.wrapper}>
      <Image style={styles.groupImg} source={{ uri: data.headimgurl }} />
      {/* 圈子昵称 */}
      <CText size="30" color="#fff">{data.name}</CText>
      {/* 圈子人数等级 */}
      <Flex justify="center" align="center" style={styles.groupNumBox}>
        <Flex style={{ flex: 1, marginRight: Rem(100) }} justify="flex-end"><CText size="24" color="#fff">圈子人数：{data.member}</CText></Flex>
        <Flex style={{ flex: 1, marginLefr: Rem(100) }} justify="flex-start"><CText size="24" color="#fff">圈子等级：LEV{data.grade}</CText></Flex>
      </Flex>
      <Text style={styles.introduce}>圈子介绍：{data.introduce}</Text>
    </View>
  );
};

let styles = {
  wrapper: {
    width: 685,
    marginTop: -60,
    marginBottom: 30,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  groupImg: {
    width: 128,
    height: 128,
    borderRadius: 64,
    marginBottom: 10,
  },
  groupNumBox: {
    marginBottom: 10
  },
  introduce: {
    fontSize: 20,
    lineHeight: 30,
    color: '#fff',
  },
};
styles = createStyles(styles);

export default GroupTop;
