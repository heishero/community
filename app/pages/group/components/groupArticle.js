/*
 * @Author: yixin
 * @Date: 2018-10-28 22:37:36
 * @Last Modified by: yixin
 * @Last Modified time: 2019-02-24 17:22:24
 * 圈子福利
 */
import React from 'react';
import { View, TouchableOpacity, ScrollView } from 'react-native';
import { Flex, TabBar } from 'antd-mobile-rn';
import CText from '../../../components/CText';
import { createStyles } from '../../../utils/view';
import Header from '../../../components/header';
import Article from '../../circle/components/groupArticle';

class GroupArticle extends React.Component {

  static navigatorStyle = {
    navBarHidden: true,
    navBarNoBorder: true,
    drawUnderNavBar: true,
    drawUnderTabBar: true,
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header componentId={this.props.componentId} title="圈子福利" />
        <ScrollView style={styles.wrapper}>
          <Flex direction="column" justify="flex-start" align="center" style={styles.list}>
            <Article componentId={this.props.componentId} />
            <Article componentId={this.props.componentId} />
            <Article componentId={this.props.componentId} />
            <Article componentId={this.props.componentId} />
            <Article componentId={this.props.componentId} />
          </Flex>
        </ScrollView>
      </View>

    );
  }
}

let styles = {
  wrapper: {
    flex: 1,
    marginBottom: 100,
    backgroundColor: '#F7F7F7'
  },
  list: {
    marginTop: 26,
  }
};
styles = createStyles(styles);

export default GroupArticle;
