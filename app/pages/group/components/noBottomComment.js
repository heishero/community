import React from 'react';
import { View, Image, Text } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../../components/CText';
import { createStyles, Rem } from '../../../utils/view';

const NoBottomComment = ({ data }) => {
  return (
    <View style={styles.wrapper}>
      <Image source={{ uri: data.headimgurl }} style={styles.img} />
      <View style={styles.content}>
        <Flex style={{ width: Rem(550), marginBottom: Rem(10) }} direction="row" justify="space-between" align="center">
          <CText size="30" color="#414141">{data.nick_name}</CText>
          <CText color="#7F7F7F" size="22">圈子活跃度：<Text style={{ color: '#FDAC42' }}>LEV8</Text></CText>
        </Flex>
        <Text style={styles.comment}>{data.content}</Text>
      </View>
    </View>
  );
};

let styles = {
  wrapper: {
    width: 698,
    // height: 168,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: '#fff',
    marginBottom: 26,
    padding: 26,
    borderRadius: 20,
    shadowOffset: { width: 6, height: 6 },
    shadowColor: '#000',
    shadowOpacity: 0.14,
    shadowRadius: 8,
  },
  img: {
    width: 70,
    height: 70,
    borderRadius: 35,
    backgroundColor: '#F7F7F7',
    marginRight: 27,
  },
  content: {
    width: 550,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  comment: {
    fontSize: 18,
    color: '#7f7f7f',
    lineHeight: 30,
  },
  bottom: {
    position: 'absolute',
    height: 30,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    bottom: 26,
    left: 26,
    right: 26,
  },
  icon: {
    fontSize: 26,
    color: '#C6C6C6',
    marginRight: 10
  },
  imgBox: {
    height: 135,
    width: 671,
    position: 'absolute',
    top: 194,
    paddingHorizontal: 27
  },
  imgItem: {
    width: 135,
    height: 135,
    borderRadius: 10,
    backgroundColor: '#FDAC42',
    marginRight: 26
  }
};
styles = createStyles(styles);
export default NoBottomComment;
