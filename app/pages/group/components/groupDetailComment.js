import React from 'react';
import { View, Image, Text, ScrollView } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../../components/CText';
import { createStyles, Rem } from '../../../utils/view';
import Icon from 'react-native-vector-icons/icomoon';

const GroupDetailComment = ({ data }) => {
  return (
    <View style={styles.wrapper}>
      <Flex direction="row" justify="flex-start" align="flex-start">
        <Image source={{ uri: data.headimgurl }} style={styles.img} />
        <View style={styles.content}>
          <Flex style={{ width: Rem(550), marginBottom: Rem(26) }} direction="row" justify="space-between" align="center">
            <CText size="30" color="#414141">{data.nick_name}</CText>
            <CText color="#7F7F7F" size="22">圈子活跃度：<Text style={{ color: '#FDAC42' }}>LEV{data.active}</Text></CText>
          </Flex>
          <Text style={styles.comment}>{data.content}</Text>
        </View>
      </Flex>
      {
        data.imgs.length > 0 &&
        <ScrollView horizontal={true} style={styles.imgBox}>
          {data.imgs.map((img) => (
            <Image key={img.id} source={{ uri: img.imgurl }} style={styles.imgItem} />
          ))}
        </ScrollView>
      }
      <Flex style={styles.bottom}>
        <Flex style={{ marginRight: Rem(23) }} direction="row" justify="flex-end" alignItems="center">
          <Icon style={styles.icon} name="huoli" />
          <CText color="#C6C6C6" size="24">{data.useful}火力</CText>
        </Flex>
      </Flex>
    </View>
  );
};

let styles = {
  wrapper: {
    width: 698,
    // height: 418,
    backgroundColor: '#fff',
    // marginBottom: 26,
    padding: 26,
    borderRadius: 20,
    shadowOffset: { width: 6, height: 6 },
    shadowColor: '#000',
    shadowOpacity: 0.14,
    shadowRadius: 8,
  },
  img: {
    width: 70,
    height: 70,
    borderRadius: 35,
    backgroundColor: '#F7F7F7',
    marginRight: 27,
  },
  content: {
    width: 550,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginBottom: 30
  },
  comment: {
    fontSize: 22,
    color: '#7f7f7f',
    lineHeight: 30,
  },
  bottom: {
    position: 'absolute',
    height: 30,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    bottom: 26,
    left: 26,
    right: 26,
  },
  icon: {
    fontSize: 26,
    color: '#C6C6C6',
    marginRight: 10
  },
  imgBox: {
    height: 135,
    width: 671,
    paddingLeft: 95,
    paddingRight: 23,
    marginBottom: 28,
  },
  imgItem: {
    width: 135,
    height: 135,
    borderRadius: 10,
    backgroundColor: '#FDAC42',
    marginRight: 26
  }
};
styles = createStyles(styles);
export default GroupDetailComment;
