/*
 * @Author: yixin
 * @Date: 2018-10-28 22:37:36
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-18 23:04:49
 * 动态详情
 */
import React from 'react';
import { View, KeyboardAvoidingView, TouchableOpacity, FlatList, Keyboard } from 'react-native';
import { Flex, InputItem, Toast } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles, Rem } from '../../utils/view';
import Header from '../../components/header';
import GroupDetailComment from './components/groupDetailComment';
import NoBottomComment from './components/noBottomComment';
import { topicInfo, topicComment, createComment } from '../../service/topic';
import Storage from '../../utils/storage';
import Circle from '../circle/components/circle';

class DynamicDetail extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props: any) {
    super(props);
    this.state = {
      info: {},
      page: 1,
      pageSize: 20,
      comment: [],
      content: '',
      hasMore: true,
    };
  }

  componentDidMount() {
    this.init();
  }

  async init() {
    const { page, pageSize } = this.state;
    try {
      const { data } = await topicInfo(this.props.id);
      this.setState({ info: data });
      const comment = await topicComment({ topic_id: this.props.id, page, pageSize });
      this.setState({ comment: comment.data });
    } catch (error) {

    }
  }

  comment = async () => {
    const { content, pageSize } = this.state;
    if (!content) { return Toast.fail('评论内容不能为空') }
    try {
      const token = await Storage.load('token');
      await createComment({ token, topic_id: this.props.id, content });
      this.setState({ page: 1, content: '' });
      Toast.success('评论成功');
      const comment = await topicComment({ topic_id: this.props.id, page: 1, pageSize });
      this.setState({ comment: comment.data });
      Keyboard.dismiss();
    } catch (error) {

    }
  }

  onEndReached = async () => {
    const { page, hasMore, pageSize } = this.state;
    if (!hasMore) return false;
    this.setState({ page: page + 1 }, () => {
      topicComment({ topic_id: this.props.id, page: page + 1, pageSize }).then(data => {
        this.setState({
          comment: this.state.comment.concat(data.data),
          hasMore: data.data.length === pageSize
        });
      });
    });
  }

  render() {
    const { info, comment } = this.state;
    let findData = { id: info.crowd_id, headimgurl: info.crowd_headimgurl, introduce: info.introduce, member: info.member, goods: info.goods, grade: info.grade, classify_name: info.name };
    return (
      <View style={{ flex: 1 }}>
        <Header componentId={this.props.componentId} title="动态详情" />
        <Flex direction="column" justify="flex-start" align="center" style={styles.list}>
          {info.id && <GroupDetailComment data={info} />}
          <FlatList
            ListHeaderComponent={<View>
              <Circle componentId={this.props.componentId} path="CircleDetail" findData={findData} />
              <Flex style={styles.title}><CText color="#7F7F7F" size="30">讨论</CText></Flex>
            </View>}
            style={{ marginBottom: Rem(180) }}
            showsVerticalScrollIndicator={false}
            data={comment}
            keyExtractor={(item, index) => (item.topic_id)}
            renderItem={({ item }) => (<NoBottomComment data={item} />)}
            onEndReachedThreshold={0.1}
            onEndReached={() => { this.onEndReached() }}
          />
        </Flex>
        <KeyboardAvoidingView style={styles.bottom}>
          <InputItem
            clear
            style={styles.input}
            onErrorPress={() => alert('clicked me')}
            value={this.state.content}
            onChangeText={(value: any) => {
              this.setState({
                content: value,
              });
            }}
            placeholder="请输入文字内容"
            onEndReached={this.onEndReached}
            onEndReachedThreshold={0.1}
          />
          <TouchableOpacity style={styles.submit} onPress={this.comment}><CText size="30" color="#fff">发送</CText></TouchableOpacity>
        </KeyboardAvoidingView>
      </View>

    );
  }
}

let styles = {
  wrapper: {
    flex: 1,
    backgroundColor: '#F7F7F7'
  },
  list: {
    marginTop: 26,
  },
  title: {
    height: 60,
    backgroundColor: '#fff',
    marginTop: 20,
    marginBottom: 40,
  },
  bottom: {
    height: 100,
    backgroundColor: '#F0F0F0',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: 25,
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0
  },
  input: {
    width: 570,
    height: 70,
    borderRadius: 20,
    paddingHorizontal: 39,
    fontSize: 30,
    backgroundColor: '#fff'
  },
  submit: {
    width: 109,
    height: 70,
    backgroundColor: '#FDAC42',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
  }
};
styles = createStyles(styles);

export default DynamicDetail;
