/*
 * @Author: yixin
 * @Date: 2018-10-28 21:39:26
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-22 23:56:57
 * 圈子详情含3个Tab
 */
import React from 'react';
import { View, TouchableOpacity, Image, Modal, ScrollView,DeviceEventEmitter } from 'react-native';
import { Flex, TextareaItem, Toast } from 'antd-mobile-rn';
import CText from '../../components/CText';
import { createStyles } from '../../utils/view';
import Icon from 'react-native-vector-icons/icomoon';
import NormalHeader from '../../components/NormalHeader';
import PageFn from '../../utils/page';
import { createTopic } from '../../service/topic';
import { getJoins } from '../../service/group';
import { uploadImg } from '../../service/user';
import ImagePicker from 'react-native-image-crop-picker';
import Storage from '../../utils/storage';

class ReleaseDynamic extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      val: '',
      imgList: [],
      token: '',
      modalVisible: false,
      joinGroups: [],
      checkGroup: null,
    };
    this.canRelease = true;
  }

  async componentDidMount() {
    const token = await Storage.load('token');
    this.setState({ token });
  }

  onChange = (val: any) => {
    this.setState({ val });
  }

  chooseImage = () => {
    ImagePicker.openPicker({
      // compressImageMaxWidth: 800,
      // compressImageMaxHeight: 800,
      cropping: false,
      takePhotoButtonTitle: true,
      freeStyleCropEnabled: true,
      compressImageQuality: 0.7,
      loadingLabelText: '加载中'
    }).then(async image => {
      console.log(image);
      this._fetchImage(image);
    });
  }

  _fetchImage(image) {
    let { imgList } = this.state;
    const file = { uri: image.path, type: 'multipart/form-data', name: 'image.png' };

    let formData = new FormData();
    formData.append('img', file);

    uploadImg(formData).then((response) => {
      this.setState({ imgList: imgList.concat(response.data.img_path) });
    });
  }

  release = async () => {
    console.log('release', this.canRelease);
    Toast.loading('正在发布···',10)
    try {
      if (!this.canRelease) return;
      this.canRelease = false;
      await createTopic({ crowd_id: this.state.checkGroup ? this.state.checkGroup.id : null, content: this.state.val, img: this.state.imgList, token: this.state.token });
      Toast.success('发布成功！');
      Toast.hide()
      DeviceEventEmitter.emit('NoticeCircle')
      PageFn.pop(this.props.componentId);
      this.props.init();
    } catch (error) {
      this.canRelease = true;
    }
  }

  // 删除动态图片
  delete = (index) => {
    let arr = [].concat(this.state.imgList);
    arr.splice(index, 1);
    this.setState({ imgList: arr });
  }

  // 添加群
  addGroup = async () => {
    this.setState({ modalVisible: true });
    try {
      const { data } = await getJoins({ type: 1, token: this.state.token });
      this.setState({ joinGroups: data });
    } catch (error) {

    }

  }

  render() {
    let { imgList, checkGroup } = this.state;
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.back} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}
          rightContent={<CText size="30">发布</CText>}
          onRightPress={this.release}
        >发布动态</NormalHeader>
        <Flex direction="column" justify="flex-start" align="center">
          <TextareaItem autoHeight placeholder="请输入动态内容" style={styles.textArea} value={this.state.val} onChange={this.onChange} />
        </Flex>
        <View style={styles.imgBox}>
          <TouchableOpacity style={styles.addImg} onPress={this.chooseImage}>
            <Icon name="tianjia" style={styles.jiaIcon} />
            <CText color="#5E5E5E" size="24">添加图片</CText>
          </TouchableOpacity>
          {
            imgList.map((data, index) => (
              <View key={index}>
                <Image source={{ uri: data }} style={styles.uploadImg} />
                <TouchableOpacity onPress={() => { this.delete(index) }} style={styles.deleteBox}><Icon name="shibai" style={styles.deleteIcon} /></TouchableOpacity>
              </View>
            ))
          }
        </View>
        <TouchableOpacity style={styles.addGroup} onPress={this.addGroup}>
          <CText size="30" color="#414141">{checkGroup ? `已选择圈子：${checkGroup.name}` : '添加圈子'}</CText>
        </TouchableOpacity>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setState({ modalVisible: false });
          }}
        >
          <View>
            <View style={styles.header}>
              <Flex style={styles.headerLeft} onPress={() => { this.setState({ modalVisible: false }) }}><Icon style={styles.back} name="fanhui" /></Flex>
              <CText size="36" color="#000">选择圈子</CText>
            </View>
            <ScrollView style={styles.groupList}>
              {
                this.state.joinGroups.map(data => (
                  <Flex style={styles.groupItem} onPress={() => { this.setState({ checkGroup: data, modalVisible: false }) }} key={data.id} align="center">
                    <Image style={styles.groupImg} source={{ uri: data.headimgurl }} />
                    <CText color="#4E4E4E" size="36">{data.name}</CText>
                  </Flex>
                ))
              }
            </ScrollView>
          </View>
        </Modal>
      </View>
    );
  }

}

let styles = {
  wrapper: {
    flex: 1,
    backgroundColor: '#F7F7F7'
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderColor: '#DEDEDE',
    height: 88,
    backgroundColor: '#fff',
    zIndex: 9,
  },
  back: {
    fontSize: 32,
    color: '#000',
  },
  textArea: {
    width: 697,
    height: 330,
    marginTop: 26,
    borderRadius: 20,
    fontSize: 24,
    flexDirection: 'row',
  },
  imgBox: {
    marginTop: 26,
    marginLeft: 26,
    width: 722,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
  },
  deleteBox: {
    position: 'absolute',
    right: 10,
    top: -16,
  },
  deleteIcon: {
    fontSize: 34,
    color: '#fc5454',
  },
  addImg: {
    height: 213,
    width: 213,
    backgroundColor: '#E3E3E3',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 25,
  },
  jiaIcon: {
    fontSize: 63,
    color: '#C1C1C1',
    marginBottom: 38
  },
  uploadImg: {
    height: 215,
    width: 215,
    marginRight: 26,
    borderRadius: 10,
    marginBottom: 26
  },
  // group
  addGroup: {
    height: 90,
    backgroundColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 30,
    paddingHorizontal: 28
  },
  groupItem: {
    borderBottomWidth: 1,
    borderColor: '#C4C4C4',
    height: 133,
  },
  groupList: {
    paddingHorizontal: 26,
  },
  groupImg: {
    width: 95,
    height: 95,
    borderRadius: 10,
    marginRight: 30,
  },
  headerLeft: {
    position: 'absolute',
    left: 20,
  }
};
styles = createStyles(styles);

export default ReleaseDynamic;
