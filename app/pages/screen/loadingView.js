import React, { PureComponent } from 'react';
import { Dimensions, Text, View, Modal, ActivityIndicator } from 'react-native';
import { createStyles } from '../../utils/view';
import { colors } from '../../styles/basic';

class LoadingView extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.box}>
          <ActivityIndicator animating={true} color={colors.White} size="large" />
          <Text style={styles.text}>加载中</Text>
        </View>
      </View>
    );
  }

}

const styles = {
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  box: {
    height: 200,
    width: 200,
    borderRadius: 10,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,.3)',
  },
  text: {
    color: colors.White,
    marginTop: 20,
    fontSize: 24,
  }
};
styles = createStyles(styles);

export default LoadingView;