/*
 * @Author: yixin
 * @Date: 2018-02-26 17:52:20
 * @Last Modified by: yixin
 * @Last Modified time: 2018-12-02 16:58:25
 * 页面包裹组件
 */
import React, { PureComponent } from 'react';
import { View } from 'react-native';
import { createStyles } from '../../utils/view';
import { Navigation } from 'react-native-navigation';
// import LoadingView from './loadingView';
import Storage from '../../utils/storage';

class Scene extends PureComponent {

  // constructor(props) {
  //   super(props);
  //   this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  // }

  // onNavigatorEvent(event) {
  //   if (event.type === 'ScreenChangedEvent') {
  //     if (event.id === 'didAppear') {
  //       this.init();
  //     }
  //   }
  // }

  async componentDidMount() {
    const token = await Storage.load('token');
    if (!token) {
      Navigation.showModal({
        component: {
          name: 'Login',
          options: {
            topBar: {
              title: {
                text: '登录'
              }
            }
          }
        }
      });
    }
  }

  render() {
    
    let { isLoading, style, error, children, onRetry } = this.props;
    return (
      <View style={[styles.container, style]}>
        {children}
        {/* <ErrorView text={error} onPress={onRetry} /> */}
        {/* {isLoading && <LoadingView isLoading={isLoading} />} */}
      </View>
    );
  }
}

let styles = {
  container: {
    flex: 1,
  }
};
styles = createStyles(styles);

export default Scene;
