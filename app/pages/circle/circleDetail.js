/*
 * @Author: yixin
 * @Date: 2018-10-27 15:40:42
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-27 18:44:55
 * 圈子详情
 */
import IntroduceModal from './components/cirleIntroduce';
import React from 'react';
import { View, TouchableOpacity, ScrollView, Alert, Image, Text } from 'react-native';
import { Flex, Toast } from 'antd-mobile-rn';
import CText from '../../components/CText';
import Icon from 'react-native-vector-icons/icomoon';
import { createStyles, Rem,  } from '../../utils/view';
import { getGroupDetail, joinGroup, collectGroup, outGroup } from '../../service/group';
import Storage from '../../utils/storage';
import PageFn from '../../utils/page';
import { topicUseful } from '../../service/topic';
import NormalHeader from '../../components/NormalHeader';
import { Navigation } from 'react-native-navigation';
import PayModal from '../common/payModal';
import * as WeChat from 'react-native-wechat';
import { getWxPay } from '../../service/shop';
import HTMLView from 'react-native-htmlview';

class CircleDetailJoin extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
        runNum: false
      }
    };
  }
  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
    this.state = {
      groupDetail: {},
      wxPayInfo: null,
    };
  }
  componentDidAppear() {
    this.init();
  }
  componentDidDisappear() {
    this.payModal && this.payModal.dismiss();
  }
  async init() {
    try {
      const token = await Storage.load('token');
      let { data } = await getGroupDetail({ id: this.props.id, token });
      this.setState({ groupDetail: data });
    } catch (error) {

    }
  }

  async collect() {
    try {
      const token = await Storage.load('token');
      let data = await collectGroup({ crowd_id: this.props.id, token });
      if (data.status === 'success') {
        Toast.success('收藏成功');
      }
    } catch (error) {

    }
  }
  toTopicPage = () => {
    PageFn.push(this.props.componentId, 'CommentList', { id: this.props.id });
  }
  useful = async ({ topic_id, index }) => {
    try {
      const token = await Storage.load('token');
      await topicUseful({ token: token, topic_id });
      Toast.success('加力成功');
      let topic = [].concat(this.state.groupDetail.topic);
      topic[index].useful++;
      this.setState({ groupDetail: { ...this.state.groupDetail, topic }});
    } catch (error) {

    }
  };

  async join() {
    try {
      Toast.loading('请等待', 8);
      const token = await Storage.load('token');
      const { data } = await joinGroup({ crowd_id: this.props.id, token });
      Toast.hide();
      if (this.state.groupDetail.crowd.is_free) {
        Toast.success('加入成功');
        PageFn.push(this.props.componentId, 'ChatGroup', { id: this.state.groupDetail.crowd.jg_im_gid });
      } else {
        let wxInfo = await getWxPay({ token, order_id: data, type: 'joincrowd' });
        this.setState({
          wxPayInfo: wxInfo.data,
        });
        console.log(wxInfo);
        if (this.payModal && wxInfo) {
          this.payModal.show();
        }
      }

    } catch (error) {
      console.log(error);
      Toast.hide();
    }

  }

  pay = async () => {
    const { wxPayInfo } = this.state;
    if (wxPayInfo) {
      WeChat.isWXAppInstalled()
        .then((isInstalled) => {
          if (isInstalled) {
            WeChat.pay({
              partnerId: wxPayInfo.partnerid,
              prepayId: wxPayInfo.prepayid,
              nonceStr: wxPayInfo.noncestr,
              timeStamp: wxPayInfo.timestamp,
              package: wxPayInfo.package,
              sign: wxPayInfo.sign,
            }).then(result => {
              console.log('wxpay', result);
              this.payModal.dismiss();
              if (result.errCode === 0) {
                Toast.success('加入成功');
                PageFn.push(this.props.componentId, 'ChatGroup', { id: this.state.groupDetail.crowd.jg_im_gid });
              } else {
                Toast.fail(result.errStr);
              }
            }).catch(err => { console.log(err) });
          } else {
            Toast.fail('请安装微信');
          }
        });
    }
  }

  outConfirm = () => {
    Alert.alert('提示', '确定要退出圈子吗？', [
      {
        text: '确定', onPress: () => {
          this.out();
        }
      },
      {
        text: '取消', onPress: () => {
        }
      },
    ]);
  }

  onloadEnd = () => {
    if (!this.state.runNum) {
      let script = `location.reload()`;
      this.webView.injectJavaScript(script);
      this.setState({ runNum: true });
    }
  }

  out = async () => {
    try {
      const token = await Storage.load('token');
      await outGroup({ crowd_id: this.props.id, token });
      Toast.success('退出成功');
      PageFn.popToRoot(this.props.componentId);
    } catch (error) {

    }
  }

  renderNode(node, index, siblings, parent, defaultRenderer) {
    if (node.name === 'img') {
      const a = node.attribs;
      return (<Image key={index} style={{ width: Rem(750), height: Rem(300), padding: 0, margin: 0 }} source={{ uri: a.src }} />);
    }
  }

  share = () => {
    Alert.alert(
      '分享',
      '',
      [
        { text: '分享到朋友圈', onPress: () => this.shareTimeLine() },
        { text: '分享给朋友', onPress: () => this.shareSession() },
        { text: '取消', onPress: () => console.log('OK Pressed'), style: 'cancel' },
      ],
      { cancelable: false }
    );
  }

  shareTimeLine = () => {
    WeChat.isWXAppInstalled().then((isInstalled) => {
      if (isInstalled) {
        WeChat.shareToTimeline({
          title: this.state.groupDetail.crowd.name,
          description: this.state.groupDetail.crowd.name,
          thumbImage: 'http://community.img.guoxiaoge.cn/icon.png',
          type: 'news',
          webpageUrl: `https://community.guoxiaoge.cn/h5/group.html?id=${this.state.crowd.id}`,
        })
          .catch((error) => { Toast.error(error.message) }); } else {
        Toast.error('没有安装微信软件，请您安装微信之后再试');
      } });
  }

  shareSession = () => {
    WeChat.isWXAppInstalled().then((isInstalled) => {
      if (isInstalled) {
        WeChat.shareToSession({
          title: this.state.groupDetail.crowd.name,
          description: this.state.groupDetail.crowd.name,
          thumbImage: 'http://community.img.guoxiaoge.cn/icon.png',
          type: 'news',
          webpageUrl: `https://community.guoxiaoge.cn/h5/group.html?id=${this.state.groupDetail.crowd.id}`,
        })
          .catch((error) => { Toast.error(error.message) }); } else {
        Toast.error('没有安装微信软件，请您安装微信之后再试');
      } });
  }

    // 圈主介绍
    circleView=(crowd) => {
      return (
        <Flex style={styles.menuItem} onPress={() => { PageFn.push(this.props.componentId, 'ChaterInfo', { id: crowd.jg_im_username }) }}>
          <Flex style={{ flex: 1 }} direction="row" justify="space-between" align="center">
            <Flex direction="row" justify="flex-start" align="center">
              <CText color="#585858" size="30">圈主介绍</CText>
            </Flex>
            <Icon name="xiang-you" style={styles.menuRightIcon} />
          </Flex>
        </Flex>
      );
    }

    render() {
      const { crowd } = this.state.groupDetail;
      return (
        <View style={{ flex: 1 }}>
          <NormalHeader
            leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
            onLeftPress={() => { PageFn.pop(this.props.componentId) }}
            rightContent={<Icon style={styles.headerIcon} name="fenxiang" />}
            onRightPress={() => { this.share() }}
          >
        圈子详情
          </NormalHeader>
          {crowd && <ScrollView style={styles.wrapper}>

            {/* <IntroduceModal  isVisible={false} ref={Modal=>this.Modal = Modal} /> */}

            <Image style={styles.headerImg} source={{ uri: crowd.headimgurl }} />
            <Flex style={styles.titleBox} direction="column" justify="space-between" align="flex-start">
              <Flex justify="space-between" align="flex-start" style={{ width: Rem(690) }}>
                <Text style={{ fontSize: 22, color: '#3E3E3E' }}>{crowd.name}</Text>
                <Flex style={{ marginTop: Rem(10), marginRight: Rem(20) }}>
                  <CText size="30" color={crowd.is_free ? '#00BAFF' : '#FF0000'}>{crowd.is_free ? '免费入群' : `¥${crowd.charge}元/年`}</CText>
                </Flex>
              </Flex>
              <Flex direction="row">
                <Flex style={styles.joinBox}>
                  {
                    crowd.userList.map((data, index) => (
                      <TouchableOpacity key={data.id} activeOpacity={0.8} onPress={() => { PageFn.push(this.props.componentId, 'ChaterInfo', { id: data.jg_im_username }) }}>
                        <Image source={{ uri: data.headimgurl }} key={index} style={styles.joinImg} />
                      </TouchableOpacity>
                    ))
                  }
                </Flex>
                <CText color="#828282" size="28">{crowd.userData.nick_name}...等{crowd.count}人已参加</CText>
                <Text style={{ color: '#828282', fontSize: Rem(28), paddingLeft: Rem(22), paddingRight: Rem(22), }}>|</Text>
                <CText color="#828282" size="28">{crowd.articleCount}篇文章</CText>
              </Flex>
            </Flex>
            {/* <View style={styles.introBox}> */}
            {this.circleView(crowd)}
            {/* <HTMLView value={crowd.detailed} onLnoadEnd={this.onloadEnd.bind(this)} injectJavaScript={this.state.injectJavaScript} /> */}
            {/* {!this.props.introduce && <HTMLView value={this.props.introduce} renderNode={this.renderNode} />} */}
            {/* </View> */}
            <View style={styles.indroduce}>
              <CText color="#3E3E3E" size="30">{this.props.introduce}</CText>
            </View>
          </ScrollView>}
          {crowd && <Flex style={styles.bottom} justify="flex-start" alignItems="center">
            {/* {crowd && !crowd.is_free && <TouchableOpacity key={1} style={[styles.bottomLeft, { backgroundColor: '#EAEAEA' }]}><CText color="#DF5A5B" size="24">{crowd.charge}元/年</CText></TouchableOpacity>} */}
            {/* {crowd && !crowd.is_collect >= 1 && <TouchableOpacity onPress={this.collect.bind(this)} key={2} style={[styles.bottomLeft, { backgroundColor: '#CBC9C7' }]}>
            <Icon name="dianzanqian" style={{ fontSize: Rem(37), color: '#6B6B6B' }} />
            <CText color="#3E3E3E" size="20">收藏</CText>
          </TouchableOpacity>} */}
            {crowd.is_join > 0 ? <TouchableOpacity onPress={this.outConfirm} style={[styles.bottomRight, { backgroundColor: '#B9B9B9' }]}><CText size="30" color="#FFFFFF">退出圈子</CText></TouchableOpacity> :
            <TouchableOpacity onPress={this.join.bind(this)} style={styles.bottomRight}><CText size="36" color="#FFFFFF">加入圈子{!crowd.is_free && `（${crowd.charge}元/年）`}</CText></TouchableOpacity>}
          </Flex>}
          {/* 支付 */}
          {crowd && <PayModal price={crowd.charge} pay={this.pay} title="加入圈子" ref={(c) => { this.payModal = c }} />}
        </View>

      );
    }
}

let styles = {
  rightText: {

  },
  wrapper: {
    flex: 1,
    backgroundColor: '#F7F7F7'
  },
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
  headerImg: {
    height: 372,
    backgroundColor: '#f5f5f5',
  },
  titleBox: {
    height: 203,
    paddingHorizontal: 24,
    paddingTop: 16,
    paddingBottom: 24,
    backgroundColor: '#fff',
    marginBottom: 20,
  },
  joinBox: {
    marginRight: 14,
  },
  joinImg: {
    width: 52,
    height: 52,
    borderRadius: 26,
    backgroundColor: '#fff'
  },
  introBox: {
    backgroundColor: '#fff',
    paddingHorizontal: 30,
    paddingVertical: 30,
  },
  introTitle: {
    marginBottom: 18
  },
  bottom: {
    height: 98,
    width: 750,
  },
  bottomLeft: {
    width: 129,
    height: 98,
    justifyContent: 'center',
    alignItems: 'center'
  },
  bottomRight: {
    flex: 1,
    height: 98,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2ab6f3'
  },
  menuItem: {
    marginTop: 10,
    height: 100,
    paddingHorizontal: 26,
    backgroundColor: '#fff',
    // borderBottomWidth: 1,
    // borderColor: '#D4D4D4'
  },
  menuRightIcon: {
    color: '#E2E2E2',
    fontSize: 30,
  },
  indroduce: {
    marginTop: 30,
    marginLeft: 30,
    width: 700
  }

};
styles = createStyles(styles);

export default CircleDetailJoin;