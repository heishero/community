import React, { Component } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import Bottom from '../common/bottom';
import NormalHeader from '@/components/NormalHeader';
import CircleState from './circle';
import CircleMyCircle from '@/pages/circle/circleMyCircle';
import CircleFreind from '@/pages/circle/circleFreind';
import Icon from 'react-native-vector-icons/icomoon';
import { PageFn } from '@/utils/page';
import ScrollableTabView, { DefaultTabBar, ScrollableTabBar } from 'react-native-scrollable-tab-view';
import { Rem } from '@/utils/view';

export default class CircleHomeScreen extends Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      },
    };
  }
  constructor() {
    super();
    this.state = {
      selectDate: 0
    };
    this.circleState = null;
    this.circleFriend = null;
    this.circleMyCircle = null;
  }

  selectedItem=(index) => {
    this.setState({
      selectDate: index
    }); }

  titleView = () => {
    return (
      <View style={{ flexDirection: 'row' }}>
        <TouchableOpacity
          onPress={() => {
            this.selectedItem(0);
          }}
          activeOpacity={0.7}
          style={{
            marginVertical: 10,
            borderRadius: 3,
            paddingLeft: 20,
            paddingRight: 20,
          }}>
          <Text style={{ color: this.state.selectDate === 0 ? '#000000' : '#727272', fontSize: this.state.selectDate === 0 ? 18 : 16 }}>动态</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            this.selectedItem(1);
          }}
          activeOpacity={0.7}
          style={{
            marginVertical: 10,
            marginLeft: 10,
            borderRadius: 3,
            paddingLeft: 10,
            paddingRight: 20,
          }}>
          <Text style={{ color: this.state.selectDate === 1 ? '#000000' : '#727272', fontSize: this.state.selectDate === 1 ? 18 : 16 }}>我的圈子</Text>
        </TouchableOpacity>

        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => {
            this.selectedItem(2);
          }}
          style={{
            marginVertical: 10,
            marginLeft: 10,
            borderRadius: 3,
            paddingRight: 20,
          }}>
          <Text style={{ color: this.state.selectDate === 2 ? '#000000' : '#727272', fontSize: this.state.selectDate === 2 ? 18 : 16 }}>圈友</Text>
        </TouchableOpacity>
      </View>
    );
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
        <NormalHeader rightContent={this.state.selectDate === 2 && <Icon name="tianjia" style={{ fontSize: Rem(40), paddingLeft: Rem(100) }} />}
          onRightPress={() => { PageFn.push(this.props.componentId, 'FindFriend') }}>圈子</NormalHeader >
        {
          this.titleView()
        }
        <ScrollableTabView style={{ flex: 1 }}
          tabBarUnderlineStyle={{ height: 0 }}
          renderTabBar={() =>
            <ScrollableTabBar
              style={{
                height: 0,
                borderWidth: 0,
              }}
            />}
          initialPage={0}
          page={this.state.selectDate}
          onChangeTab={(item) => {
            switch (parseInt(item.i, 10)) {
              case 0:
                this.circleState.init();
                break;
              case 1:
                this.circleMyCircle.init();
                break;
              case 2:
                this.circleFriend.init();
                break;
              default:
                break;
            }
            this.setState({
              selectDate: item.i,
            });
          }}>
          <CircleState ref={el => { this.circleState = el }} componentId={this.props.componentId} />
          <CircleMyCircle ref={el => { this.circleMyCircle = el }} componentId={this.props.componentId} />
          <CircleFreind ref={el => { this.circleFriend = el }} componentId={this.props.componentId} />
        </ScrollableTabView>
        <Bottom index="2" />
      </View>


    );
  }
}
