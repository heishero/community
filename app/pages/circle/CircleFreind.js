import React from 'react';
import { View, SectionList, TouchableOpacity, Image, FlatList, Dimensions } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import Icon from 'react-native-vector-icons/icomoon';
import pinyin from 'pinyin';
import CText from '@/components/CText';
import { createStyles, Rem } from '@/utils/view';
import JMessage from 'jmessage-react-plugin';
import PageFn from '@/utils/page';


class CircleFreind extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      sections: null,
      letterArr: [],
      isLoading: false,
    };
  }

  init = () => {
    this.setState({ isLoading: true });
    JMessage.getFriends((friendArr) => {  // 好友用户对象数组。
      console.log('friendArr', friendArr);
      this.setState({ isLoading: false });
      this.formatData(friendArr);
    }, (error) => {
      console.log(error);
      this.setState({ isLoading: false });
    });
  }

  formatData = (friendArr) => {
    let letterArr = []; // 右侧字母数组
    let sections = []; // 分组数据
    // 右侧字母栏处理
    friendArr.map((item, index) => {
      letterArr.push(pinyin(item.nickname.substring(0, 1), {
        style: pinyin.STYLE_FIRST_LETTER,
      })[0][0].toUpperCase());
      letterArr = [...new Set(letterArr)].sort();
    });
    this.setState({ letterArr: letterArr });
    // 分组数据处理
    letterArr.map((item, index) => {
      sections.push({
        title: item,
        data: []
      });
    });
    friendArr.map((frienditem, index) => {
      sections.map(sectionItem => {
        let first = frienditem.nickname.substring(0, 1);
        let letter = pinyin(first, { style: pinyin.STYLE_FIRST_LETTER })[0][0].toUpperCase();
        if (sectionItem.title === letter) {
          sectionItem.data.push({ firstName: first, ...frienditem });
        }
      });
    });
    this.setState({ sections: sections });
  }

  // 新的圈友
  _renderListHeader = () => (
    <TouchableOpacity style={styles.renderItem} onPress={() => { PageFn.push('Circle', 'FriendInvatation') }}>
      <Flex style={styles.listHeaderLeft} justify="center" align="center"><Icon style={styles.listHeaderIcon} name="wo-de-dibu" /></Flex>
      <CText>新的圈友</CText>
    </TouchableOpacity>
  )

  _renderSectionHeader = ({ section }) => {
    return (
      <View style={styles.sectionHeader}>
        <CText>{section.title.toUpperCase()}</CText>
      </View>
    );
  }

  _renderItem = (item, index) => {
    return (
      <TouchableOpacity style={styles.renderItem} onPress={() => { PageFn.push('Circle', 'ChatSingle', { username: item.username }) }}>
        <Image style={styles.itemImage} source={{ uri: 'file:' + item.avatarThumbPath }} />
        <CText>{item.nickname}</CText>
      </TouchableOpacity>
    );
  }

  _letterRender=({ item, index }) => (
    <TouchableOpacity style={styles.letterItem}>
      <CText color="#747474" size="30">{item}</CText>
    </TouchableOpacity>
  )

  render() {
    const { sections, letterArr, isLoading } = this.state;
    const top_offset = (Dimensions.get('window').height - letterArr.length * Rem(50)) / 2;
    return (
      <View style={styles.wrapper}>
        {sections && <SectionList
          ref={(el) => { this.sectionList = el }}
          keyExtractor={(item, index) => index + item.username}
          ListHeaderComponent={this._renderListHeader}
          renderSectionHeader={this._renderSectionHeader}
          sections={sections}
          ItemSeparatorComponent={() => (<View style={styles.separator}></View>)}
          renderItem={({ item, index }) => this._renderItem(item, index)}
          onRefresh={this.init}
          refreshing={isLoading}
        />}
        <View style={[styles.letterWrapper, { top: Rem(100) }]}>
          {letterArr.length > 0 && <FlatList
            data={letterArr}
            keyExtractor={(item, index) => item}
            renderItem={this._letterRender}
          />}
        </View>
      </View>
    );
  }}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#F7F7F7' },
  headerIcon: {
    fontSize: 32,
    color: '#fff',
  },
  listHeaderLeft: {
    height: 77,
    width: 77,
    borderRadius: 10,
    backgroundColor: '#1A82D2',
    marginRight: 33,
  },
  listHeaderIcon: {
    fontSize: 37,
    color: '#fff',
  },
  sectionHeader: {
    height: 60,
    backgroundColor: '#EDEDED',
    paddingHorizontal: 26,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  separator: {
    width: 611,
    height: 2,
    marginLeft: 139,
    backgroundColor: '#E4E4E4'
  },
  renderItem: {
    height: 107,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingLeft: 31,
    flex: 1,
  },
  itemImage: {
    width: 77,
    height: 77,
    borderRadius: 10,
    marginRight: 33,
  },
  letterWrapper: {
    position: 'absolute',
    right: 14,
  },
  letterItem: {
    height: 50,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  }
};
styles = createStyles(styles);

export default CircleFreind;