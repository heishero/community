import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';
import  CircleMycirleCell from '@/pages/circle/components/CirCleMycirleCell';
import { getJoins } from '@/service/group';
import Fetch from '@/NewWorkDatas/FetchNewWork';
import Storage from '@/utils/storage';
import { PageFn } from '@/utils/page';



export default class CircleMyCircle extends Component {


  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      },
    };
  }

  constructor() {
    super();
    this.state = {
      listArray: [],
      page: 1,
      pageSize: 10,
      hasMore: false,
    };
    this.refreshing = false;
  }
  async init() {
    try {
      const token = await Storage.load('token');
      const groupData = await getJoins({ token, type: 1 });
      this.refreshing = false;
      this.setState({ listArray: groupData.data });
    } catch (error) {
      console.log(error);
    }
  }
  _onRefresh= () => {
    this.refreshing = true;
    this.init();
  }
  
  selectedItem=(item) => {
    PageFn.push(this.props.componentId, 'ChatGroup', { id: item.item.jg_im_gid });
  }

  render() {
    const { listArray } = this.state;
    return (
      <FlatList
        data={listArray}
        style={{ flex: 1 }}
        keyExtractor={(item, index) => String(item.id)}
        showsVerticalScrollIndicator={false}
        onRefresh={this._onRefresh}
        ListEmptyComponent={() => {
          return (
            <View style={{ marginTop: 180, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={{ fontSize: 20 }}>暂无数据</Text>
            </View>
          );
        }}
        refreshing={this.refreshing}
        renderItem={(item) => {
          return (
            <TouchableOpacity onPress={this.selectedItem.bind(this, item)}>
              <CircleMycirleCell
                contentColor="#727272"
                imgUrl={item.item.headimgurl}
                numPeople={`${item.item.member}人参加`}
                titleName={item.item.name}
                content={item.item.introduce}
              />
            </TouchableOpacity>
          );
        }}
      />
    );
  }
}
