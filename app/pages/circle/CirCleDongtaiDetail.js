import React, { Component } from 'react';
import { Dimensions, View, Image, TextInput, Modal, Text, FlatList, TouchableOpacity, StyleSheet, KeyboardAvoidingView, StatusBar } from 'react-native';
import  CirCleCommentCell from '@/pages/circle/components/CirCleCommentCell';
import { Flex } from 'antd-mobile-rn';
import { topicInfo, createComment } from '@/service/topic';
import { PageFn } from '@/utils/page';
import ImageViewer from 'react-native-image-zoom-viewer';
import { timestampFormat } from '@/utils/timestampFormat';
import NormalHeader from '@/components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import CText from '@/components/CText';
import Storage from '@/utils/storage';

const ScreenWidth = Dimensions.get('window').width;

export default class CirCleDongtaiDetail extends Component {
  constructor() {
    super();
    this.state = {
      detailData: {},
      imgShow: false,
      imgList: [],
      imgInit: 0,
      commentIndex: 0,
      commentInput: '',
      comment_contentArray: []
    };
  }

  componentDidMount() {
    this.getNetWork();
  }

  comment = async () => {
    const { commentInput } = this.state;
    if (!commentInput) return false;
    try {
      const token = await Storage.load('token');
      const { data } = await createComment({ token, topic_id: this.props.id, content: commentInput });
      let arr = this.state.comment_contentArray.concat(data);
      this.setState({ commentInput: '', commentIndex: null, comment_contentArray: arr });
    } catch (error) {

    }
  }

  async getNetWork() {
    try {
      const { data } = await topicInfo(this.props.id);
      this.setState({ detailData: data, comment_contentArray: data.comment_content });
    } catch (error) {

    }
  }

    // 打开图片缩放
    openImg = (imgs, index) => {
      let imgList = imgs.map((data) => ({ url: data.imgurl }));
      this.setState({ imgShow: true, imgList, imgInit: index });
      StatusBar.setBackgroundColor('#000');
    }

   _headView=(headDetail) => {
     return (
       <View>
         <View style={{ flex: 1 }}>
           <CirCleCommentCell
             UserNamecolor="#566F9A"
             UserNameFont={17}
             userUrl={headDetail.headimgurl === null ? '' : headDetail.headimgurl}
             userNameImageSize={40}
             userName={headDetail.nick_name}
             content={headDetail.content}
             contentColor="#434343"
             contentSize={15}
           >
           </CirCleCommentCell>
           {
             <FlatList
               data={headDetail.imgs}
               style={{ flex: 1, marginLeft: 50 }}
               numColumns={3}
               keyExtractor={(item, index) => index}
               showsVerticalScrollIndicator={false}
               renderItem={(item, index) => {
                 return (
                   <TouchableOpacity onPress={this.ClickImage.bind(this, item)}>
                     <Image source={{ uri: item.item.imgurl }} style={styles.imgItem} />
                   </TouchableOpacity>
                 ); }}
             />}
           <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 15 }}>
             <Text style={{ marginLeft: 50, color: '#7F7F7F', fontSize: 11 }}>{headDetail.created_at === undefined ? '' : timestampFormat(Date.parse(headDetail.created_at.replace(/\-/g, '/')) / 1000)}</Text>
             <Text style={{ marginLeft: 180, color: '#7F7F7F', fontSize: 13 }}>{`评论:${this.state.comment_contentArray.length}`}</Text>
           </View>

           <View style={{ height: 10, backgroundColor: '#EFEFEF', marginTop: 15 }}>
           </View>
         </View>
       </View>
     );
   }

  // 点击图片
  ClickImage=(item) => {
    this.openImg(this.state.detailData.imgs, item.index);
  }

  render() {
    const { imgShow, imgList, imgInit, commentInput } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <NormalHeader
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }} >动态详情</NormalHeader>
        <KeyboardAvoidingView style={{ flex: 1 }}>
          <FlatList
            data={this.state.comment_contentArray}
            style={{ flex: 1 }}
            keyExtractor={(item, index) => index}
            showsVerticalScrollIndicator={false}
            ListHeaderComponent={this._headView(this.state.detailData)}
            ListEmptyComponent={() => {
              return <View style={{ height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                <Text>暂无数据</Text>
              </View>;
            }}
            renderItem={(item) => {
              return (
                <View>
                  <CirCleCommentCell
                    time={item.item.created_at ? item.item.created_at : ''}
                    UserNamecolor="#272727"
                    UserNameFont={14}
                    userUrl={item.item.headimgurl === null ? '' : item.item.headimgurl}
                    userNameImageSize={40}
                    userName={item.item.nick_name === null ? '' : item.item.nick_name}
                    content={item.item.content === null ? '' : item.item.content}
                    contentColor="#6C6C6C"
                    tag={1}
                    contentSize={11}
                  />
                  <View style={{ marginLeft: 60, height: 1, backgroundColor: '#D6D6D6', marginTop: 10 }} />
                </View>
              );
            }}
          />
          <Flex justify="space-between" align="center" style={styles.inputBox}>
            <TextInput
              ref={(c) => { this.inputRefs = c }}
              value={commentInput}
              onChangeText={(text) => {
                this.setState({ commentInput: text });
              }}
              blurOnSubmit={true}
              style={styles.bottomInput}
            />
            <Flex style={styles.sendTextBtn} onPress={this.comment} justify="center" align="center"><CText size="30" color="#fff">发送</CText></Flex>
          </Flex>
        </KeyboardAvoidingView>
        <Modal visible={imgShow} transparent={true}>
          <ImageViewer index={imgInit} onClick={() => { this.setState({ imgShow: false }); StatusBar.setBackgroundColor('rgba(0,0,0,0)'); }} imageUrls={imgList} onRequestClose={() => this.setState({ imgShow: false })} />
        </Modal>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  headerIcon: {
    fontSize: 20,
    color: '#000',
  },
  imgItem: {
    marginTop: 10,
    height: (ScreenWidth - 90) / 3,
    width: (ScreenWidth - 90) / 3,
    borderRadius: 10,
    backgroundColor: '#F3F3F3',
    marginRight: 5,
    marginLeft: 5,
  },
  inputBox: {
    height: 60,
    backgroundColor: '#F7F7F7',
    paddingHorizontal: 24,
  },
  sendTextBtn: {
    marginLeft: 20,
    width: 109,
    height: 40,
    borderRadius: 20,
    backgroundColor: '#47cefa'
  },
  bottomInput: {
    width: ScreenWidth - 160,
    height: 40,
    backgroundColor: '#fff',
    borderRadius: 20,
    paddingLeft: 20,
    // paddingVertical: 0,
  },

});
