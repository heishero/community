import React from 'react';
import { View, TextInput, TouchableOpacity, FlatList, Image } from 'react-native';
import { createStyles } from '@/utils/view';
import NormalHeader from '@/components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import PageFn from '@/utils/page';
import { findFriend } from '@/service/user';
import Storage from '@/utils/storage';
import CText from '@/components/CText';

class FindFriend extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      search: '',
      list: []
    };
    this.search = this.search.bind(this);
  }

  async search() {
    try {
      const token = await Storage.load('token');
      const { data } = await findFriend({ token, userMsg: this.state.search });
      this.setState({ list: data.list });
      console.log(99, data);
    } catch (error) {
      console.log(error);
    }
  }

  _renderItem = ({ item }) => (
    <TouchableOpacity style={styles.renderItem} onPress={() => { PageFn.push(this.props.componentId, 'ChaterInfo', { id: item.jg_im_username }) }}>
      <Image style={styles.itemImage} source={{ uri: item.headimgurl }} />
      <CText>{item.nick_name || item.mobile}</CText>
    </TouchableOpacity>
  )

  render() {
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}>
          添加圈友
        </NormalHeader>
        <View style={styles.inputWrapper}>
          <TextInput
            style={styles.search}
            placeholder="请输入对方的昵称"
            value={this.state.search}
            onChangeText={(text) => this.setState({ search: text })}
            onSubmitEditing={this.search}
          />
          <TouchableOpacity style={styles.searchBtn} onPress={this.search}><Icon name="sosuo" style={styles.inputIcon} /></TouchableOpacity>
        </View>
        <FlatList
          data={this.state.list}
          keyExtractor={(item, index) => String(item.id)}
          renderItem={this._renderItem}
        />
      </View>
    );
  }}

let styles = {
  wrapper: { flex: 1, backgroundColor: '#fff' },
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
  inputWrapper: {
    paddingHorizontal: 36,
    marginTop: 30,
  },
  search: {
    width: 694,
    height: 60,
    backgroundColor: '#f4f4f4',
    borderRadius: 29,
    paddingHorizontal: 40,
    paddingVertical: 0,
  },
  searchBtn: {
    position: 'absolute',
    right: 50,
    marginTop: 14,
  },
  inputIcon: {
    fontSize: 25,
    color: '#888888'
  },
  renderItem: {
    height: 107,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingLeft: 31,
    flex: 1,
    borderBottomWidth: 1,
    borderColor: '#E4E4E4'
  },
  itemImage: {
    width: 77,
    height: 77,
    borderRadius: 10,
    marginRight: 33,
  },
};
styles = createStyles(styles);

export default FindFriend;