import React from 'react';
import { View, BackHandler, Image, Alert, ScrollView, Text, FlatList, KeyboardAvoidingView, TextInput, Modal, TouchableOpacity } from 'react-native';
import { Flex, Toast } from 'antd-mobile-rn';
import CText from '../../components/CText';
import NormalHeader from '../../components/NormalHeader';
import Icon from 'react-native-vector-icons/icomoon';
import { createStyles, Rem } from '../../utils/view';
import { Navigation } from 'react-native-navigation';
import { createComment, unReadList } from '../../service/topic';
import Storage from '../../utils/storage';
import { timestampFormat } from '../../utils/timestampFormat';
import { PageFn } from '../../utils/page';
import ImageViewer from 'react-native-image-zoom-viewer';

class Unread extends React.Component {
  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
    this.state = {
      topicList: [],
      page: 1,
      pageSize: 10,
      refreshing: false,
      hasMore: false,
      commentId: null,
      commentIndex: null,
      commentInput: '',
      imgShow: false,
      imgList: [],
      imgInit: 0,
    };
  }

  componentDidMount() {
    this.init();
  }

  componentDidAppear() {
    // BackHandler.addEventListener('hardwareBackPress', this.onBackAndroid);
    this.init();
  }

  async init() {
    try {
      const { pageSize } = this.state;
      const token = await Storage.load('token');
      this.setState({ refreshing: true });
      const { data } = await unReadList({ token });
      this.setState({ topicList: data, refreshing: false, hasMore: data.length === pageSize, page: 1 });
      console.log('find', data);
    } catch (error) {
      console.log(error);
    }
  }

  onEndReached = async () => {
    const { page, pageSize, refreshing, hasMore, topicList } = this.state;
    if (refreshing || !hasMore) return false;
    const token = await Storage.load('token');
    const { data } = await unReadList({ token });
    this.setState({
      topicList: topicList.concat(data),
      hasMore: data.length === pageSize,
      page: page + 1
    });
  }
  onBackAndroid = () => {
    let res = true;
    Alert.alert('提示', '确定退出APP？', [
      {
        text: '确定', onPress: () => {
          BackHandler.exitApp();
        }
      },
      {
        text: '取消', onPress: () => {
          res = true;
        }
      },
    ]);
    return res;
  };

  comment = async () => {
    const { commentId, commentInput, commentIndex } = this.state;
    if (!commentId || !commentInput) { Toast.fail('评论不能为空'); return }
    try {
      const token = await Storage.load('token');
      const { data } = await createComment({ token, topic_id: commentId, content: commentInput });
      let arr = JSON.parse(JSON.stringify(this.state.topicList));
      arr[commentIndex].comment_content.push(data);
      this.setState({ commentId: null, commentInput: '', commentIndex: null, topicList: arr });
    } catch (error) {

    }
  }
  // 打开图片缩放
  openImg = (imgs, index) => {
    let imgList = imgs.map((data) => ({ url: data.imgurl }));
    this.setState({ imgShow: true, imgList, imgInit: index });
  }

  renderItem = ({ item, index }) => {
    return (
      <Flex style={styles.item} align="flex-start" onPress={() => {
        if (this.state.commentId) {
          this.setState({
            commentInput: '',
            commentId: null,
          });
        }
      }}>
        <Image source={{ uri: item.headimgurl }} style={styles.itemImg} />
        <Flex style={styles.topic} direction="column" justify="flex-start" align="flex-start">
          <Flex direction="row" justify="between" align="center" style={{ width: Rem(580) }}>
            <View>
              <View style={styles.topicName}><CText color="#566f9a" size="34" fontWeight="500">{item.nick_name}</CText></View>
              <View style={styles.topicContent}><CText color="#434343" size="30" lineHeight="44">{item.content}</CText></View>
            </View>
            <TouchableOpacity style={{ margin: Rem(30) }} onPress={() => { this.onDeleteFind(item.id) }} >
              <CText size="30" color="#666666" >删除</CText>
            </TouchableOpacity>
          </Flex>
          {/* 动态图片 */}
          {item.imgs.length > 0 &&
            <ScrollView horizontal={true} style={styles.imgBox}>
              {
                item.imgs.map((data, index) => (
                  <Flex key={data.id} onPress={() => { this.openImg(item.imgs, index) }}>
                    <Image source={{ uri: data.imgurl }} style={styles.imgItem} />
                  </Flex>
                ))
              }
            </ScrollView>
          }
          {/* 点赞评论 */}
          <Flex style={styles.fnRow} justify="space-between" align="center">
            <CText color="#7F7F7F" size="22">{timestampFormat(Date.parse(item.created_at.replace(/\-/g, '/')) / 1000)}</CText>
            <Flex justify="space-between">
              {/* <Flex style={{ marginRight: Rem(50) }} align="center" onPress={() => { item.useful > 0 ? this.cancelUseful(item.id, index) : this.useful(item.id, index) }}><Text style={[styles.itemText, item.useful > 0 && { color: '#F8AA4A' }]}><Icon name={item.useful > 0 ? 'dianzanho' : 'dianzanqian'} /> 赞</Text></Flex> */}
              <Flex align="center" onPress={() => { this.setState({ commentId: item.id, commentIndex: index }, () => { this.inputRefs.focus() }) }}><Text style={styles.itemText}><Icon style={styles.fnText} name="tao-lun" /> 评论</Text></Flex>
            </Flex>
          </Flex>
          {/* 动态评论 */}
          <View style={styles.dynamicBox}>
            {
            // item.UsefulCentent.length > 0 &&
              <Flex style={styles.like}>
                {item.UsefulCentent.map((data, index) => (
                  <Flex key={data.index} style={{ marginRight: Rem(10) }}><Text style={styles.likeText}><Icon name="dianzanqian" /> {data.nick_name}</Text></Flex>
                ))}
              </Flex>
            }
            {
            //  item.comment_content.length > 0 &&
              <View style={styles.comment}>
                {
                  item.comment_content.map(data => (
                    <Flex key={data.created_at} style={styles.commentItem} align="flex-start"><CText size="26" color="#01B3FE" lineHeight="30">{data.nick_name}: </CText><CText lineHeight="30" size="26" color="#7F7F7F">{data.content}</CText></Flex>
                  ))
                }
              </View>
            }
          </View>
        </Flex>
      </Flex>
    );
  }

  render() {
    const { topicList, refreshing, commentId, commentInput, imgShow, imgList, imgInit } = this.state;
    return (
      <View style={styles.wrapper}>
        <NormalHeader
          rightContent={<Icon name="fadongtai" style={styles.fabiaoIcon} />}
          onRightPress={() => { PageFn.push(this.props.componentId, 'ReleaseDynamic') }}
          leftContent={<Icon style={styles.headerIcon} name="fanhui" />}
          onLeftPress={() => { PageFn.pop(this.props.componentId) }}
        >未读消息</NormalHeader>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={topicList}
          keyExtractor={(item, index) => (item.id)}
          renderItem={this.renderItem}
          refreshing={refreshing}
          onEndReachedThreshold={0.1}
          onEndReached={() => { this.onEndReached() }}
        />
        {!!commentId && <KeyboardAvoidingView>
          <Flex justify="space-between" align="center" style={styles.inputBox}>
            <TextInput
              ref={(c) => { this.inputRefs = c }}
              value={commentInput}
              onChangeText={(text) => {
                this.setState({ commentInput: text });
              }}
              blurOnSubmit={true}
              style={styles.bottomInput}
            />
            <Flex style={styles.sendTextBtn} onPress={this.comment} justify="center" align="center"><CText size="30" color="#fff">发送</CText></Flex>
          </Flex>
        </KeyboardAvoidingView>}
        <Modal visible={imgShow} transparent={true}>
          <ImageViewer index={imgInit} onClick={() => { this.setState({ imgShow: false }) }} imageUrls={imgList} onRequestClose={() => this.setState({ imgShow: false })} />
        </Modal>
      </View>
    );
  }
}
let styles = {
  wrapper: {
    flex: 1,
    backgroundColor: '#fff'
  },
  back: {
    color: '#000',
    fontSize: 18,
  },
  headerIcon: {
    fontSize: 32,
    color: '#000',
  },
  fabiaoIcon: {
    fontSize: 45,
  },
  item: {
    paddingHorizontal: 26,
    paddingVertical: 30,
    borderBottomWidth: 1,
    borderColor: '#E5E5E5'
  },
  itemImg: {
    width: 95,
    height: 95,
    borderRadius: 10,
    marginRight: 10,
  },
  topic: {
    flex: 1,
  },
  topicContent: {
    marginBottom: 20
  },
  imgBox: {
    width: 588,
    height: 182,
    marginBottom: 30,
  },
  imgItem: {
    height: 182,
    width: 182,
    borderRadius: 10,
    backgroundColor: '#F3F3F3',
    marginRight: 20,
  },
  fnRow: {
    width: 588,
    height: 30,
    marginBottom: 27
  },
  fnText: {
    fontSize: 26,
    color: '#7B7B7B',
    marginRight: 10,
  },
  dynamicBox: {
    width: 588,
    borderRadius: 10,
    backgroundColor: '#F3F3F3',
    paddingHorizontal: 13,
  },
  like: {
    flex: 1,
    flexWrap: 'wrap',
    borderBottomWidth: 1,
    borderColor: '#D7D7D7',
  },
  likeText: {
    color: '#01B3FE',
    fontSize: 26,
    lineHeight: 50,
  },
  commentItem: {
    flexWrap: 'wrap',
    flex: 1,
    marginVertical: 5,
  },
  inputBox: {
    height: 126,
    backgroundColor: '#F7F7F7',
    paddingHorizontal: 24,
  },
  bottomInput: {
    width: 578,
    height: 80,
    backgroundColor: '#fff',
    borderRadius: 20
  },
  sendTextBtn: {
    width: 109,
    height: 80,
    borderRadius: 20,
    backgroundColor: '#F8AA4A'
  }
};
styles = createStyles(styles);
export default Unread;