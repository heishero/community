/*
 * @Author: yixin
 * @Date: 2018-10-19 23:40:27
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-28 22:07:26
 * 发现
 */
import React from 'react';
import { View, Image, Alert, ScrollView, Text, FlatList, Modal, DeviceEventEmitter, StatusBar } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../components/CText';
import Icon from 'react-native-vector-icons/icomoon';
import { createStyles, Rem } from '../../utils/view';
import { getTopicList, topicUseful, createComment, cancelUseful, delTopic, unRead } from '../../service/topic';
import Storage from '../../utils/storage';
import { timestampFormat } from '../../utils/timestampFormat';
import { PageFn } from '../../utils/page';
import ImageViewer from 'react-native-image-zoom-viewer';

class Circle extends React.Component {

  static options(passProps) {
    return {
      bottomTabs: {
        drawBehind: true,
        visible: false,
      },
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      topicList: [],
      page: 1,
      pageSize: 10,
      refreshing: false,
      hasMore: false,
      commentId: null,
      commentIndex: null,
      commentInput: '',
      imgShow: false,
      imgList: [],
      imgInit: 0,
      count: 0,
      image: '',
    };
    this.inputRefs = null;
    this.canCommit = true;
  }


  componentDidMount() {
    this.listener = DeviceEventEmitter.addListener('NoticeCircle', () => {
      this.init();
    });
    this.init();
  }

  componentWillUnmount() {
    if (this.listener) {
      this.listener.remove();
    }
  }

  componentDidAppear() {
    this.init();
  }

  async init() {
    try {
      const { pageSize } = this.state;
      const token = await Storage.load('token');
      this.setState({ refreshing: true });
      const { data } = await getTopicList({ page: 1, pageSize, token });
      const unread = await unRead({ token });
      this.setState({ topicList: data, refreshing: false, hasMore: data.length === pageSize, page: 1, count: unread.data.message, image: unread.data.headImg });
    } catch (error) {
      console.log(error);
    }
  }


  onEndReached = async () => {
    const { page, pageSize, refreshing, hasMore, topicList } = this.state;
    if (refreshing || !hasMore) return false;
    const token = await Storage.load('token');
    const { data } = await getTopicList({ token, page: page + 1, pageSize });
    this.setState({
      topicList: topicList.concat(data),
      hasMore: data.length === pageSize,
      page: page + 1
    });
  }
  _onRefresh = () => {
    this.init();
  }

  // 点赞
  useful = async (id, index) => {
    try {
      const token = await Storage.load('token');
      const { data } = await topicUseful({ token, topic_id: id });
      let arr = JSON.parse(JSON.stringify(this.state.topicList));
      arr[index].useful++;
      arr[index].UsefulCentent.push(data);
      this.setState({ topicList: arr });
    } catch (error) {

    }
  }

  cancelUseful = async (id, index) => {
    try {
      const token = await Storage.load('token');
      const result = await cancelUseful({ token, topic_id: id });
      let user_id = result.data.user_id;
      let arr = JSON.parse(JSON.stringify(this.state.topicList));
      arr[index].useful--;
      let listIndex;
      arr[index].UsefulCentent.map((data, index) => {
        if (data.user_id === user_id) {
          listIndex = index;
        }
      });
      arr[index].UsefulCentent.splice(listIndex, 1);
      this.setState({ topicList: arr });

    } catch (error) {

    }
  }

  comment = async () => {
    const { commentId, commentInput, commentIndex } = this.state;
    try {
      const token = await Storage.load('token');
      const { data } = await createComment({ token, topic_id: commentId, content: commentInput });
      let arr = JSON.parse(JSON.stringify(this.state.topicList));
      arr[commentIndex].comment_content.push(data);
      this.setState({ commentId: null, commentInput: '', commentIndex: null, topicList: arr });
    } catch (error) {

    }
  }

  // 打开图片缩放
  openImg = (imgs, index) => {
    let imgList = imgs.map((data) => ({ url: data.imgurl }));
    this.setState({ imgShow: true, imgList, imgInit: index });
    StatusBar.setBackgroundColor('#000');
  }

  confirmDelete = (id, index) => {
    Alert.alert('是否删除动态？', '', [
      { text: '取消', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
      { text: '确定', onPress: () => this.deleteComment(id, index) },
    ]);
  }

  // 删除动态
  deleteComment = async (id, index) => {
    console.log('deleteComment', this.state.topicList);
    const token = await Storage.load('token');
    await delTopic({ token, id });
    let arr = JSON.parse(JSON.stringify(this.state.topicList));
    arr.splice(index, 1);
    this.setState({ topicList: arr });
  }

  renderItem = ({ item, index }) => {
    return (
      <Flex style={styles.item} align="flex-start" onPress={() => {
        PageFn.push('Circle', 'CirCleDongtaiDetail', { id: item.id });
      }}>
        {item.headimgurl ? <Image source={{ uri: item.headimgurl }} style={styles.itemImg} /> : <View style={styles.itemImg}></View>}
        <Flex style={styles.topic} direction="column" justify="flex-start" align="flex-start" >
          <View style={styles.topicName}><CText color="#566f9a" size="34" fontWeight="500">{item.nick_name}</CText></View>
          <View style={styles.topicContent}><CText color="#434343" size="30" lineHeight="44">{item.content}</CText>{item.crowd_name !== null && <Flex onPress={() => { PageFn.push(this.props.componentId, 'CircleDetail', { id: item.crowd_id }) }}><CText color="#01B3FE" size="30" lineHeight="44">#{item.crowd_name}#</CText></Flex>}</View>
          {/* 动态图片 */}
          {item.imgs.length > 0 &&
            <ScrollView horizontal={true} style={styles.imgBox}>
              {
                item.imgs.map((data, index) => (
                  <Flex key={data.id} onPress={() => { this.openImg(item.imgs, index) }}>
                    <Image source={{ uri: data.imgurl }} style={styles.imgItem} />
                  </Flex>
                ))
              }
            </ScrollView>
          }
          {/* 点赞评论 */}
          <Flex style={styles.fnRow} justify="space-between" align="center">
            <CText color="#7F7F7F" size="22">{timestampFormat(Date.parse(item.created_at.replace(/\-/g, '/')) / 1000)}</CText>
            <Flex justify="space-between" align="center">
              <Flex align="center" >
                <Text style={{ color: '#7F7F7F' }}>
                  {`评论:${item.comment_content.length}`}
                </Text>
              </Flex>
              {item.can_delete && <Flex style={{ marginLeft: Rem(50) }} align="center" onPress={() => { this.confirmDelete(item.id, index) }}><Text style={styles.itemText}><Icon style={styles.fnText} name="shanchu" /> 删除</Text></Flex>}
            </Flex>
          </Flex>
          <View style={styles.dynamicBox}>
            {/* 评论 */}
            {
              item.comment_content.length <= 1 ?
                <View style={styles.comment}>
                  {
                    item.comment_content.map(data => (
                      <Flex key={data.id} style={styles.commentItem} align="flex-start"><CText size="26" color="#01B3FE" lineHeight="30">{data.nick_name}: </CText><CText lineHeight="30" size="26" color="#7F7F7F">{data.content}</CText></Flex>
                    ))
                  }
                </View> :
                <View style={styles.comment}>
                  <Flex style={styles.commentItem} align="flex-start"><CText size="26" color="#01B3FE" lineHeight="30">{item.comment_content[0].nick_name}: </CText><CText lineHeight="30" size="26" color="#7F7F7F">{item.comment_content[0].content}</CText></Flex>
                  <Flex style={styles.commentItem} align="flex-start"><CText lineHeight="30" size="26" color="#7F7F7F">{`共${item.comment_content.length}条评论`}</CText></Flex>
                </View>
            }
          </View>
        </Flex>
      </Flex>


    );
  }

  render() {
    const { topicList, refreshing, imgShow, imgList, imgInit, } = this.state;
    return (
      <View style={styles.wrapper}>
        <FlatList
          ListEmptyComponent={() => {
            return <View style={{ marginTop: 180, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={{ fontSize: 20 }}>暂无动态</Text>
            </View>;
          }}
          showsVerticalScrollIndicator={false}
          data={topicList}
          keyExtractor={(item) => (`${item.id}`)}
          renderItem={this.renderItem}
          refreshing={refreshing}
          onRefresh={this._onRefresh}
          onEndReachedThreshold={0.1}
          onEndReached={() => { this.onEndReached() }}
        />
        <Modal visible={imgShow} transparent={true}>
          <ImageViewer index={imgInit} onClick={() => { StatusBar.setBackgroundColor('rgba(0,0,0,0)'); this.setState({ imgShow: false }) }} imageUrls={imgList} onRequestClose={() => this.setState({ imgShow: false })} />
        </Modal>
        {/* <Bottom index="2" /> */}
      </View>
    );
  }
}

let styles = {
  wrapper: {
    flex: 1,
    backgroundColor: '#fff'
  },
  back: {
    color: '#000',
    fontSize: 18,
  },
  fabiaoIcon: {
    fontSize: 45,
  },
  item: {
    paddingHorizontal: 26,
    paddingVertical: 30,
    borderBottomWidth: 1,
    borderColor: '#E5E5E5'
  },
  itemImg: {
    width: 95,
    height: 95,
    borderRadius: 10,
    marginRight: 10,
  },
  topic: {
    flex: 1,
  },
  topicContent: {
    marginBottom: 20
  },
  imgBox: {
    width: 588,
    height: 182,
    marginBottom: 30,
  },
  imgItem: {
    height: 182,
    width: 182,
    borderRadius: 10,
    backgroundColor: '#F3F3F3',
    marginRight: 20,
  },
  fnRow: {
    width: 588,
    height: 40,
    marginBottom: 17
  },
  fnText: {
    fontSize: 26,
    color: '#7B7B7B',
    marginRight: 10,
  },
  dynamicBox: {
    width: 588,
    borderRadius: 10,
    backgroundColor: '#F3F3F3',
    paddingHorizontal: 13,
  },
  like: {
    flex: 1,
    flexWrap: 'wrap',
    borderBottomWidth: 1,
    borderColor: '#D7D7D7',
  },
  likeText: {
    color: '#01B3FE',
    fontSize: 26,
    lineHeight: 50,
  },
  commentItem: {
    flexWrap: 'wrap',
    flex: 1,
    marginVertical: 5,
  },
  inputBox: {
    height: 126,
    backgroundColor: '#F7F7F7',
    paddingHorizontal: 24,
  },
  bottomInput: {
    width: 578,
    height: 80,
    backgroundColor: '#fff',
    borderRadius: 20,
    paddingVertical: 0,
  },
  sendTextBtn: {
    width: 109,
    height: 80,
    borderRadius: 20,
    backgroundColor: '#47cefa'
  }
};
styles = createStyles(styles);

export default Circle;