
import React, { Component } from 'react';
import { View, TouchableOpacity, ScrollView, Alert, Image,Text,StyleSheet} from 'react-native';
import Modal from 'react-native-modal';


export default class cirleIntroduce extends Component {
    constructor() {
        super();
        this.state = {
            isVisible: true
        }
    }
    componentWillMount() {
        this.state.isVisible = this.props.isVisible;
    }

  render() {

    return (
      <Modal isVisible={this.state.isVisible}
        animationIn={'slideInLeft'} //zoomInDown,zoomInUp,slideInLeft
        animationOut={'slideOutRight'}
        backdropOpacity={0.4}
        onBackdropPress={() => {
            this.showOrDismiss(false);
        }}>
     <View style={{overflow:'hidden',alignItems:'center',justifyContent:'center',backgroundColor:'#fff',marginLeft:20,marginRight:20,height:400,borderRadius:10,}}>
     <Image style={styles.topImage}>
     </Image>

     <Image style={styles.headImage}>
     </Image>

     <Text style={styles.nickName}>用户昵称</Text>

     <TouchableOpacity activeOpacity={0.8} onPress={() =>{  }}>
         <View style={{top:-40}}>
         <Text style={styles.introduce} numberOfLines={2}>
         会议听取了大会主席团常务主席、大会副秘书长、全国人大常委3333
         <Text style={styles.btnClick}>查看全部</Text>
        </Text>
         </View>
     </TouchableOpacity>

     <TouchableOpacity 
     style={{borderRadius:8,height:50,top:10,backgroundColor:'#2AB6F3',justifyContent:'center',alignItems:'center',}}
      activeOpacity={0.8} 
      onPress={() =>{  }}>
         <View>
         <Text style={{paddingLeft:75,paddingRight:75,color:'#fff',fontSize:18}}>加为圈友</Text>
         </View>
     </TouchableOpacity>


      </View>

    </Modal>
    )
  }

    showOrDismiss = (flag) => {
    this.setState({isVisible: flag});
  } 
}

const styles = StyleSheet.create({
    topImage:{
        top:-120,
        width:'100%',
        height:180,
        backgroundColor:'#F89433',
    },
    headImage:{
        height:90,
        width:90,
        borderRadius:45,
        backgroundColor:'#F89433',
        position:'absolute',
        top:70,
    },
    nickName:{
        top:-60,
        fontSize:16,
        color:'#F89433',
    },
    introduce:{
        color:'#828282',
        fontSize:13,
        marginLeft:23,
        marginRight:30
    },
    btnClick:{
        color:'#2AB6F3',
        fontSize:16,
    },

  });