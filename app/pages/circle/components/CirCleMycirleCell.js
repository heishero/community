import React, { Component } from 'react';
import { View, Image, Text } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import { createStyles } from '../../../utils/view';
import CText from '@/components/CText';

export default class CircleMycirleCell extends Component {

  render() {
    let { numPeople, titleName, content, imgUrl } = this.props;
    return (
      <View style={styles.wrapper}>
        <Image style={styles.img} source={{ uri: imgUrl }} />
        <View style={styles.content}>
          <View><CText lineHeight="32" size="30" color="#2b2b2b">{titleName}</CText></View>
          <Flex style={styles.middleRow} justify="space-between" align="center">
            <CText color="#A3A3A3" size="22">{numPeople}人参加</CText>
          </Flex>
          <Text numberOfLines={2} style={styles.desc} >
            群简介：{content}
          </Text>
        </View>
      </View>
    );
  }
}

let styles = {
  wrapper: {
    width: 724,
    height: 216,
    flexDirection: 'row',
    marginLeft: 26,
    paddingTop: 28,
    borderBottomWidth: 1,
    borderColor: '#DDDDDD'
  },
  img: {
    height: 150,
    width: 196,
    borderRadius: 10,
    marginRight: 10,
  },
  content: {
    marginLeft: 10,
    height: 150,
    flex: 1,
    paddingRight: 26,
  },
  middleRow: {
    marginTop: 10,
  },
  desc: {
    marginTop: 10,
    fontSize: 22,
    width: 408,
    lineHeight: 30,
    color: '#5E5E5E',
  },
};
styles = createStyles(styles);