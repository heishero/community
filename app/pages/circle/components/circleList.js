/*
 * @Author: yixin
 * @Date: 2019-03-23 14:36:10
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-19 23:45:40
 * 圈子列表容器组件
 */

import React from 'react';
import { FlatList } from 'react-native';
import { connect } from 'react-redux';
import { actions as CircleActions } from '../../../controllers/circle';
import Circle from './circle';


class CircleList extends React.PureComponent {

  _onRefresh = () => {
  }

  render() {
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        data={this.props.circles}
        keyExtractor={item => String(item.id)}
        renderItem={({ item }) => <Circle data={item} />}
        onEndReached={this.props.GetGroupsMore}
        onEndReachedThreshold={0.1}
        refreshing={this.props.loading}
        onRefresh={this._onRefresh}
      />
    );
  }
}

export default connect(Store => {
  return { circles: Store.CircleRenderData.groupsList, loading: Store.CircleRenderData.loading };
}, { GetGroups: CircleActions.GetGroups, GetGroupsMore: CircleActions.GetGroupsMore })(CircleList);
