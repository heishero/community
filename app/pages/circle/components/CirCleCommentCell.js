
import React, { Component } from 'react';
import { Dimensions, View, Image, Text } from 'react-native';


const ScreenWidth = Dimensions.get('window').width;

export default class CirCleCommentCell extends Component {

  render() {
    let { time, userName, UserNamecolor, UserNameFont, userUrl, userNameImageSize, content, contentColor, contentSize, tag } = this.props;
    return (
      <View style={{ flex: 1, flexDirection: 'row', width: ScreenWidth, justifyContent: 'flex-start', }}>
        <Image source={{ uri: userUrl }} style={{ width: userNameImageSize, height: userNameImageSize, marginLeft: 13, marginTop: 15, borderRadius: userNameImageSize * 0.5 }} />
        <View style={{ flexDirection: 'column', marginLeft: 10, marginTop: 15 }}>
          <Text style={{ color: UserNamecolor, fontSize: UserNameFont }}>{userName}</Text>
          <Text style={{ marginTop: 6, color: contentColor, fontSize: contentSize, marginRight: 60, lineHeight: contentSize + 6 }}>{content}</Text>
        </View>
        {tag === 1 ? <Text style={{ right: 10, top: 15, color: contentColor, fontSize: 12, position: 'absolute' }}>{time}</Text> : null}
      </View>
    );
  }
}