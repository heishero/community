import React from 'react';
import { Text, Image, View } from 'react-native';
import { Flex, } from 'antd-mobile-rn';
import {  Rem } from '../../../utils/view';
import { unRead } from '../../../service/topic';
import Storage from '../../../utils/storage';

class Popup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      headimg: '',
      count: ''
    };
  }
  componentDidMount() {
    this.init();
  }

  init = async () => {
    const token = await Storage.load('token');
    const data = await unRead({ token });
    this.setState({
      headimg: data.headimg,
      count: data.message
    });
  }

  render() {
    if (this.state.count === 0 || this.state.count === '') {
      return <View />;
    } else {
      return (
        <Flex justify="center" align="center" direction="row" style={{ marginTop: Rem(24), marginBottom: Rem(24), flex: 1, zIndex: 0 }} >
          <Flex direction="row" justify="center" align="baseline" style={{ backgroundColor: '#575757', borderRadius: Rem(10), width: Rem(320), height: Rem(92), }} >
            <Image style={{ height: Rem(68), width: Rem(68), borderRadius: Rem(10) }} source={{ uri: this.state.headimg }} />
            <Text style={{ color: '#FFFFFF', fontSize: Rem(34) }}>{this.state.count}条新消息</Text>
          </Flex>
        </Flex>
      );
    }
  }
}

export default Popup;