/*
 * @Author: yixin 
 * @Date: 2019-03-23 15:54:05 
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-12 23:48:36
 * 圈子列表 群费用flag
 */
import React from 'react';
import { View, Text } from 'react-native';
import { createStyles, Rem } from '../../../utils/view';

const CirclePriceFlag = ({ price, style, size }) => (
  <View style={[styles.wrapper, { borderColor: parseFloat(price)>0 ? '#FF0000' : '#2AB6F3', ...style }]}>
    <Text style={{ fontSize: Rem(size ? size : 22), color: parseFloat(price)>0 ? '#FF0000' : '#2AB6F3' }}>{parseFloat(price)>0 ? `¥${price}元` : '免费入群'}</Text>
  </View>
);

let styles = {
  wrapper: {
    width: 110,
    height: 40,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    // borderWidth: 1,
    // borderRadius: 10,
  }
};
styles = createStyles(styles);

export default CirclePriceFlag;
