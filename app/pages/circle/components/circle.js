/*
 * @Author: yixin
 * @Date: 2018-10-25 15:33:03
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-17 21:57:48
 * 圈子列表子组件
 */

import React, { PureComponent } from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import CText from '../../../components/CText';
import { createStyles } from '../../../utils/view';
import PageFn from '../../../utils/page';
import CirclePriceFlag from './circlePriceFlag';


class Circle extends PureComponent {

  render() {
    const { data } = this.props;
    return (
      <TouchableOpacity style={styles.wrapper} onPress={() => {
        data.isJoin > 0 ? PageFn.push('Find', 'ChatGroup', { id: data.jg_im_gid }) :
          PageFn.push('Find', 'CircleDetail', { id: data.id, jg_im_gid: data.jg_im_gid, introduce: data.introduce });
      }}>
        {data.headimgurl && <Image resizeMode="cover" source={{ uri: data.headimgurl }} style={styles.img} />}
        <View style={styles.content}>
          <View><CText lineHeight="32" size="30" color="#2b2b2b">{data.name}</CText></View>
          <Flex style={styles.middleRow} justify="space-between" align="center">
            <CText color="#A3A3A3" size="22">{data.member}人参加</CText>
            <CirclePriceFlag price={data.charge} />
          </Flex>
          <Text numberOfLines={2} style={styles.desc} >
            群简介：{data.introduce}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

let styles = {
  wrapper: {
    width: 724,
    height: 216,
    flexDirection: 'row',
    marginLeft: 26,
    paddingTop: 28,
    borderBottomWidth: 1,
    borderColor: '#DDDDDD'
  },
  img: {
    height: 150,
    width: 196,
    borderRadius: 10,
    marginRight: 10,
  },
  content: {
    marginLeft: 10,
    height: 150,
    flex: 1,
    paddingRight: 26,
  },
  middleRow: {
    marginTop: 10,
  },
  desc: {
    marginTop: 10,
    fontSize: 22,
    width: 408,
    lineHeight: 30,
    color: '#5E5E5E',
  },
};
styles = createStyles(styles);

export default Circle;
