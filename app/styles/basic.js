export const colors = {
  Red: '#d1443b',
  Blue: '#6b8dff',  //大乐透圈圈
  White: '#fff',
  /**
   * 灰色 背景填充颜色
   */
  cf6f: '#f6f6f6',

  /**
   * 灰色2 背景填充颜色
   */
  f7f9: '#f7f9fa',
  /**
   * 黑灰 字体常用颜色
   */
  c333: '#333',
  /**
   * 灰色 字体title常用颜色
   */
  c666: '#666',
  /**
   * 浅灰 二号字体颜色
   */
  c999: '#999',

  /**
   * 浅灰 分割线
   */
  ce6e: '#e6e6e6',

  /**
   * 浅灰 彩帝赛事列表title
   */
  cccc: '#ccc',

  /**
   * 浅灰 彩帝赛事列表title 背景色
   */
  cf5f: '#f5f5f5',

  /**
   * 浅灰 提款页面
   */
  cgrey: '#a1afb4',

  /**
   * 橙黄色
   */
  orange: '#ffd987',
  orange_active: '#ffb301',

  /**
   * 暗红色
   */
  c5c2101: '#5c2101',

  /**
   * 线条颜色
   */
  cline: '#cecece',

  /**
   * 蓝色
   */
  cblue: '#4096fb',

  /**
   * 计划单表头颜色
   */
  chead: '#898989',

  /**
   * 计划单线条颜色
   */
  cddd: '#dddddd'
};