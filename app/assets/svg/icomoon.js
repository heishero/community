
import { createIconSet } from 'react-native-vector-icons';
// import createIconSet from './lib/create-icon-set';
import glyphMap from './glyphmaps/icomoon.json';

const iconSet = createIconSet(glyphMap, 'icomoon', 'icomoon.ttf');

export default iconSet;

export const Button = iconSet.Button;
export const TabBarItem = iconSet.TabBarItem;
export const TabBarItemIOS = iconSet.TabBarItemIOS;
export const ToolbarAndroid = iconSet.ToolbarAndroid;
export const getImageSource = iconSet.getImageSource;