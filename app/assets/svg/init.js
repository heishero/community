/*
 * @Author: yixin
 * @Date: 2018-05-14 13:50:30
 * @Last Modified by: yixin
 * @Last Modified time: 2018-06-21 15:08:06
 * Icon名称生成
 */

const data = require('./icomoon/selection.json');
const fs = require('fs');
const path = require('path');
const fontList = {};
// icomoon.ttf字体文件
const sourceFile = path.join(__dirname, './icomoon/fonts/icomoon.ttf');
const ttfdestPath = path.join(__dirname, '../../../node_modules/react-native-vector-icons/Fonts/icomoon.ttf');
// icomoon.js文件
const icomoonFile = path.join(__dirname, './icomoon.js');
const icodestPath = path.join(__dirname, '../../../node_modules/react-native-vector-icons/icomoon.js');

// svg的图标名称格式化
data.icons.forEach((obj) => {
  fontList[obj.properties.name] = obj.properties.code;
});
fs.writeFileSync(path.join(__dirname, '../../../node_modules/react-native-vector-icons/glyphmaps/icomoon.json'), JSON.stringify(fontList));
console.log('图标名称格式化完成 ——————OK!');

// 字体文件替换
const readStream = fs.createReadStream(sourceFile);
const writeStream = fs.createWriteStream(ttfdestPath);
readStream.pipe(writeStream);
console.log('字体文件替换完成 ——————OK!');

// icomoon入口文件替换
const readIcoStream = fs.createReadStream(icomoonFile);
const writeIcoStream = fs.createWriteStream(icodestPath);
readIcoStream.pipe(writeIcoStream);
console.log('icomoon入口文件替换完成 ——————OK!');

