/*
 * @Author: yixin
 * @Date: 2018-12-02 18:37:15
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-28 23:20:48
 * page方法
 */
import { Navigation } from 'react-native-navigation';

export const PageFn = {
  push(componentId, name, passProps, options) {
    Navigation.push(componentId, {
      component: {
        name: name,
        passProps: passProps,
        options: options
      }
    });
  },
  pop(id) {
    Navigation.pop(id);
  },
  showModal(name, passProps, options) {
    Navigation.showModal({
      stack: {
        children: [{
          component: {
            name: name,
            passProps: passProps,
            options: options
          }
        }]
      }
    });
  },
  popTo(componentId) {
    Navigation.popTo(componentId);
  },
  popToRoot(componentId) {
    Navigation.popToRoot(componentId);
  },
  dismissModal() {
    Navigation.dismissModal();
  },
  dismissAllModals() {
    Navigation.dismissAllModals();
  }
};

export default PageFn;

