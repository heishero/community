import lodash from 'lodash';

const regexs = {
  // 匹配 max_length(12) => ['max_length',12]
  rule: /^(.+?)\((.+)\)$/,
  // 数字
  numericRegex: /^[0-9]+$/,
  /**
   * @descrition:邮箱规则
   * 1.邮箱以a-z、A-Z、0-9开头，最小长度为1.
   * 2.如果左侧部分包含-、_、.则这些特殊符号的前面必须包一位数字或字母。
   * 3.@符号是必填项
   * 4.右则部分可分为两部分，第一部分为邮件提供商域名地址，第二部分为域名后缀，现已知的最短为2位。最长的为6为。
   * 5.邮件提供商域可以包含特殊字符-、_、.
   */
  email: /^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$/,
  /**
   * [ip ipv4、ipv6]
   * '192.168.0.0'
   * '192.168.2.3.1.1'
   * '235.168.2.1'
   * '192.168.254.10'
   * '192.168.254.10.1.1'
   */
  ip: /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])((\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])){3}|(\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])){5})$/,
  /**
   * @descrition:判断输入的参数是否是个合格的固定电话号码。
   * 待验证的固定电话号码。
   * 国家代码(2到3位)-区号(2到3位)-电话号码(7到8位)-分机号(3位)
   **/
  fax: /^(([0\+]\d{2,3}-)?(0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/,
  /**
   *@descrition:手机号码段规则
   * 13段：130、131、132、133、134、135、136、137、138、139
   * 14段：145、147
   * 15段：150、151、152、153、155、156、157、158、159
   * 17段：170、176、177、178
   * 18段：180、181、182、183、184、185、186、187、188、189
   * 国际码 如：中国(+86)
   */
  phone: /^((\+?[0-9]{1,4})|(\(\+86\)))?(13[0-9]|14[57]|15[012356789]|17[0678]|18[0-9])\d{8}$/,
  /**
   * @descrition 匹配 URL
   */
  url: /[a-zA-z]+:\/\/[^\s]/
};



export default {
  // 验证合法邮箱
  isEmail(str) {
    return this.isNotNull(str) && regexs.email.test(str);
  },
  isNotNull(str) {
    return str !== '' && str != null && str !== undefined && str.trim() !== '';
  },
  // 验证合法 ip 地址
  isIp(str) {
    return this.isNotNull(str) && regexs.ip.test(str);
  },
  // 验证传真
  isFax(str) {
    return this.isNotNull(str) && regexs.fax.test(str);
  },
  // 验证座机
  isTel(str) {
    return this.isNotNull(str) && regexs.fax.test(str);
  },
  // 验证URL
  isUrl(str) {
    return this.isNotNull(str) && regexs.url.test(str);
  },
  // 最大值
  max(str, length) {
    str = parseInt(str);
    return ((!lodash.isNaN(str) && lodash.isNumber(str)) && str <= length);
  },
  // 最小值
  min(str, length) {
    str = parseInt(str);
    return ((!lodash.isNaN(str) && lodash.isNumber(str)) && str >= length);
  },
  // 最大长度
  maxLength(str, length) {
    if (!regexs.numericRegex.test(length)) return false;
    return this.isNotNull(str) && str.length <= length;
  },
  // 最小长度
  minLength(str, length) {
    if (!regexs.numericRegex.test(length)) return false;
    return this.isNotNull(str) && str.length >= length;
  },
  // boolean 类型
  isBoolean(str) {
    return lodash.isBoolean(str);
  },
  //数据类型判断 数组
  isArray(str) {
    return lodash.isArray(str);
  },
  // 日期
  isDate(str) {
    return lodash.isBoolean(str);
  },
  // 对比
  isEqual(str, estr) {
    return lodash.isEqual(str, estr);
  },
  // 函数
  isFunction(str) {
    return lodash.isFunction(str);
  },
  // 包含
  isMatch(str, estr) {
    return lodash.isMatch(str, estr);
  },
  // 是否为 nan
  isNaN(str) {
    return lodash.isNaN(str);
  },
  // 数字 nan isNumber 会返回 true
  isNumber(str) {
    str = parseInt(str);
    return (!lodash.isNaN(str) && lodash.isNumber(str));
  },
  // 正则表达式
  isRegExp(str) {
    return lodash.isRegExp(str);
  },
  // 是否为JSON
  isPlainObject(str) {
    return lodash.isPlainObject(str);
  },
  // 字符串JSON格式
  isStrToJSON(str) {
    str += '';
    if (this.isNotNull(str)) {
      return str.indexOf('{') >= 0 && str.indexOf('}') >= 0;
    } else {
      return false;
    }
  },
  // 是否为JSON格式
  isJSON(obj) {
    return (
      typeof obj === 'object' &&
      Object.prototype.toString.call(obj).toLowerCase() === '[object object]' &&
      !obj.length
    );
  },
  // 是否是正确的手机格式
  isPhone(number) {
    let phone = number.trim();
    return !!(phone.length === 11 && (/^[1][3,4,5,6,7,8,9][0-9]{9}$/.test(phone)));
  }
};
