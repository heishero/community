import { Text, TextInput, Dimensions, PixelRatio, Platform, StyleSheet } from 'react-native';
// rn尺寸缩放

export const deviceIsAndroid = Platform.OS === 'android';

export const deviceRatio = PixelRatio.get();

export const deviceWidth = Dimensions.get('window').width;
export const deviceHeight = Dimensions.get('window').height;
export const devicePixWidth = PixelRatio.getPixelSizeForLayoutSize(deviceWidth);
export let devicePixHeight = PixelRatio.getPixelSizeForLayoutSize(deviceHeight);

// 设计稿尺寸计算缩放
export const deviceWindowSize = { width: 750, height: 1334 };

// 判断是否是iphoneX
const { width, height } = Dimensions.get('window');
export const isIphoneX = (Platform.OS === 'ios' && (Number((String(height / width)).substr(0, 4)) * 100) === 216);

// 字体缩放
export const deviceFontScale = deviceRatio / PixelRatio.getFontScale();
// android 状态栏高度
let Statu = (deviceRatio < 3.5 ? 4 : deviceRatio);
export const deviceStatusbarHeight = 24.1 * Statu;
if (deviceIsAndroid) {
  devicePixHeight = devicePixHeight - deviceStatusbarHeight;
}

export const deviceScale = (1 / (deviceWindowSize.width / devicePixWidth)) / deviceRatio;
export const deviceWidthScale = (1 / (deviceWindowSize.width / devicePixWidth)) / deviceRatio;
export const deviceHeightScale = (1 / (deviceWindowSize.height / devicePixHeight)) / deviceRatio;

export const System = {
  ratio: deviceRatio,
  width: deviceWidth,
  height: deviceHeight,
  pixWidth: devicePixWidth,
  pixHeight: devicePixHeight,
  scale: deviceScale,
  scaleWidth: deviceWidthScale,
  scaleHeight: deviceHeightScale,
  isAndroid: deviceIsAndroid,
  windowWidth: deviceWindowSize.width,
  windowHeight: deviceWindowSize.height,
  windowStatusbarHeight: deviceStatusbarHeight,
  fontScale: deviceFontScale
};

/**
 * ios不受系统字体影响
 * @param {*} WrapComponent
 * @param {*} customProps
 */
export function fixedFontScaling() {
  // Text不随系统字体缩放
  TextInput.defaultProps = Object.assign({}, TextInput.defaultProps, { defaultProps: false });
  Text.defaultProps = Object.assign({}, Text.defaultProps, { allowFontScaling: false });
  // 设置默认字体
  Text.defaultProps.style = { fontFamily: 'PingFangSC-Regular' };
  console.log('defaultProps', Text.defaultProps);
}

const paramStyle = [
  'width',
  'height',
  'top',
  'left',
  'right',
  'bottom',
  'lineHeight',
  'letterSpacing',
  'margin',
  'marginTop',
  'marginRight',
  'marginBottom',
  'marginLeft',
  'marginHorizontal',
  'marginVertical',
  'padding',
  'paddingTop',
  'paddingRight',
  'paddingBottom',
  'paddingLeft',
  'paddingHorizontal',
  'paddingVertical',
  'borderRadius',
  'borderTopLeftRadius',
  'borderTopRightRadius',
  'borderBottomRightRadius',
  'borderBottomLeftRadius',
  'borderWidth',
  'borderTopWidth',
  'borderRightWidth',
  'borderBottomWidth',
  'borderLeftWidth',
  'fontSize'
];

const paramStyleAndroid = [
  'width',
  'top',
  'left',
  'right',
  'bottom',
  'lineHeight',
  'letterSpacing',
  'margin',
  'marginTop',
  'marginRight',
  'marginBottom',
  'marginLeft',
  'marginHorizontal',
  'marginVertical',
  'padding',
  'paddingTop',
  'paddingRight',
  'paddingBottom',
  'paddingLeft',
  'paddingHorizontal',
  'paddingVertical',
  'borderRadius',
  'borderTopLeftRadius',
  'borderTopRightRadius',
  'borderBottomRightRadius',
  'borderBottomLeftRadius',
  'borderWidth',
  'borderTopWidth',
  'borderRightWidth',
  'borderBottomWidth',
  'borderLeftWidth',
  'fontSize'
];

const paramStyleHeight = [
  'height',
];

export function Rem(px) {
  return px * System.scale;
}

export function RemW(px) {
  return px * deviceWidthScale;
}

export function RemH(px) {
  return px * deviceHeightScale;
}

export function createStyles(styles) {
  const platformStyles = {};
  for (let key in styles) {
    let { ios, android, ...style } = { ...styles[key] };
    if (ios && Platform.OS === 'ios') {
      style = { ...style, ...ios };
    }
    if (android && Platform.OS === 'android') {
      style = { ...style, ...android };
    }

    if (Platform.OS === 'ios') {
      paramStyle.map((k) => {
        if (style[k]) {
          style[k] = Rem(style[k]);
        }
      });
    }

    if (Platform.OS === 'android') {
      paramStyleAndroid.map((k) => {
        if (style[k]) {
          style[k] = Rem(style[k]);
        }
      });
      paramStyleHeight.map((k) => {
        if (style[k]) {
          style[k] = RemH(style[k]);
        }
      });


    }

    platformStyles[key] = style;
  }
  return StyleSheet.create(platformStyles);

}

