/*
 * @Author: yixin 
 * @Date: 2018-07-11 15:40:22 
 * @Last Modified by: yixin
 * @Last Modified time: 2018-07-11 16:17:16
 */

var { NativeModules } = require('react-native');
module.exports = NativeModules.UMAnalyticsModule;