/*
 * @Author: yixin 
 * @Date: 2018-07-11 15:40:25 
 * @Last Modified by:   yixin 
 * @Last Modified time: 2018-07-11 15:40:25 
 */
var { NativeModules } = require('react-native');
module.exports = NativeModules.UMPushModule;