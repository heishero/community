import Ionicons from 'react-native-vector-icons/icomoon';

export const initIcons = () => {

  const icons = {
    'xiao-xi-1': [22],
    'quanzi': [22],
    'fa-xian': [22],
    'wo-de-dibu': [22],
  };

  return new Promise((resolve) => {

    Promise
      .all(Object.keys(icons).map(
        iconName => Ionicons.getImageSource(
          iconName, 20)))
      .then((sources) => {
        let iconsMap = {};
        Object.keys(icons).forEach(
          (iconName, idx) => { iconsMap[iconName] = sources[idx] });
        resolve(iconsMap);
      });
  });

};