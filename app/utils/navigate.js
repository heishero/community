import { Navigation } from 'react-native-navigation';
import { initIcons } from './Icons';

export const setDefaultOption = () => {
  Navigation.setDefaultOptions({
    topBar: {
      visible: false,
      drawBehind: true,
    },
    layout: {
      orientation: 'portrait'
    },
    bottomTabs: {
      visible: false,
      animate: false, // Controls whether BottomTabs visibility changes should be animated
      drawBehind: false,
      titleDisplayMode: 'alwaysShow',
      hideShadow: true
    },
    bottomTab: {
      iconColor: '#AAAAAA',
      selectedIconColor: '#44c6ff',
      textColor: '#AAAAAA',
      selectedTextColor: '#44c6ff',
      fontFamily: 'Helvetica',
      fontSize: 10
    },
    statusBar: {
      backgroundColor: 'rgba(0,0,0,0)',
      style: 'dark',
      // drawBehind: true,
    },
    animations: {
      from: 0, // Mandatory, initial value
      to: 1, // Mandatory, end value
      duration: 400, // Default value is 300 ms
      startDelay: 100, // Default value is 0
    }
  });
};

export const goToMain = () => {
  initIcons()
    .then(iconsMap => {
      Navigation.setRoot({
        root: {
          bottomTabs: {
            id: 'BottomTabsId',
            options: {
              bottomTabs: {
                currentTabIndex: 0,
                hideShadow: false,
              }
            },
            children: [
              {
                stack: {
                  children: [{
                    component: {
                      name: 'MessageScreen',
                    }
                  }],
                  options: {
                    bottomTab: {
                      text: '消息',
                      icon: iconsMap['xiao-xi-1'],
                    },
                  },
                  id: 'Message',
                }
              },
              {
                stack: {
                  id: 'Find',
                  children: [{
                    component: {
                      name: 'FindScreen',
                    }
                  }],
                  options: {
                    bottomTab: {
                      text: '发现',
                      icon: iconsMap['fa-xian'],
                    },
                  }
                }
              },
              {
                stack: {
                  id: 'Circle',
                  children: [{
                    component: {
                      name: 'CircleScreen',
                    }
                  }],
                  options: {
                    bottomTab: {
                      text: '圈子',
                      icon: iconsMap['quanzi'],
                    },
                  }
                }
              },
              {
                stack: {
                  id: 'Mine',
                  children: [{
                    component: {
                      name: 'MineScreen',
                    },
                  }],
                  options: {
                    bottomTab: {
                      text: '我的',
                      icon: iconsMap['wo-de-dibu'],
                    },
                  }
                }
              }
            ]
          }
        }
      });
    })
    .catch(err => {
      console.error('APP 启动错误', err);
    });
};

export const goToAuth = () => {
  Navigation.setRoot({
    root: {
      stack: {
        id: 'Auth',
        children: [
          {
            component: {
              name: 'Login'
            }
          }
        ]
      }
    }
  });
};