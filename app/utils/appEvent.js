import { AppState, Platform } from 'react-native';

export default (navigator) => {
  return new Promise((resolve) => {

    // APP状态切换
    AppState.addEventListener('change', (appState) => {
      // 应用正在前台运行
      if (appState === 'active') {
      }

      // 应用正在后台运行。用户既可能在别的应用中，也可能在桌面。
      if (appState === 'background') {
      }

      // 此状态表示应用正在前后台的切换过程中，或是处在系统的多任务视图，又或是处在来电状态中。
      if (appState === 'inactive') {
      }

    });

    // 注册极光推送事件
    if (Platform.OS !== 'android') {
      // JPushModule.setBadge(0, () => {});
    }
    // 获取极光推送ID
    // JPushModule.getRegistrationID((res) => {
    //   console.log('getRegistrationID', res);
    // });

    /*
        // 注册消息事件
        JPushModule.addReceiveOpenNotificationListener((message) => {

          let extras = JSON.parse(message.extras);
          let pageName;
          let pageId;
          if (extras.pageName) pageName = parseInt(extras.pageName);
          if (extras.pageId) pageId = extras.pageId;
          if (pageName || pageId) {
            setTimeout(
                () => {
                    // navToPage(navigator, pageName, pageId);
                },
                1000);
          }

        }); */

  });
};
