import { Alert, Linking, Platform } from 'react-native';
import { getVersion } from '../service/user';
import { Config } from '../config';

export function checkVersion() {
  const version = Config.Version;
  getVersion({ version: version, system: Platform.OS === 'ios' ? 'ios' : 'android' }).then(data => {
    if (data.status === 'success') {
      if (Platform.OS === 'ios') {
        if (data.data.notice) {
          Alert.alert(
            '公告',
            data.data.notice,
            [
              {
                text: '好的', onPress: () => { }
              }
            ]
          );
        }
        if ((parseFloat(version.replace(/\./g, '')) < parseFloat(data.data.version.replace(/\./g, '')))) {
          Alert.alert(
            '版本更新',
            `检测到有新版本v${data.data.version}需要更新`,
            [
              {
                text: '好的', onPress: () => {
                  Linking.openURL(data.data.download_url).catch(err => console.log(err));
                }
              },
            ],
            { cancelable: false }
          );
        }
      } else {
        if ((parseFloat(version.replace(/\./g, '')) < parseFloat(data.data.version.replace(/\./g, '')))) {
          Alert.alert(
            '版本更新',
            `检测到有新版本v${data.data.version}需要更新`,
            [
              {
                text: '好的', onPress: () => {
                  Linking.openURL(data.data.download_url).catch(err => console.log(err));
                }
              },
            ],
            { cancelable: false }
          );
        } else {
          if (data.data.notice && !this.props.needUpdate) {
            Alert.alert(
              '公告',
              data.data.notice,
              [
                {
                  text: '好的', onPress: () => { }
                }
              ]
            );
          }
        }
      }
    }
  });
}