import { Config } from '../config';
import { Toast } from 'antd-mobile-rn';
import Storage from './storage';
import PageFn from './page';
import { goToAuth } from './navigate';

export const utilsPost = (api, body, state = {}, options) => {
  return new Promise((resolve, reject) => {
    state.LETIME = Date.now();
    state.api = api;
    state.body = options === 'pic' ? picPost(body) : (options === 'pay' ? FetchPost(body) : parseParaMeter(body));
    // 枚举option中的属性加入到fetch的body中
    if (options) {
      for (let n in options) {
        if (state.body[n]) {
          state.body[n] = Object.assign(state.body[n], options[n]);
        } else {
          state.body[n] = options[n];
        }
      }
    }
    // Toast.loading('加载中', 12);
    fetch(state.api, state.body)
      .then(response => response.text())
      .then((responseText) => {
        Toast.hide();
        state.LETIME = Date.now() - state.LETIME;
        let res = JSON.parse(responseText);
        state.res = res;
        console.log('httpPOST:', state.api, state.body, res);
        if (res.status !== 'success') {
          // 如果是用户过期
          if (res.failedCode === 'NO_LOGIN') {
            Storage.clearall();
            toLogin();
          } else {
            Toast.fail(res.failedMsg);
          }

        }
        resolve(state);
      })
      .catch((error) => {
        state.LETIME = Date.now() - state.LETIME;
        state.ERROR = error;
        reject(state);
      });

  });
};

export const utilsGet = (api, body, state = {}, options) => {

  return new Promise((resolve, reject) => {

    if (body) {
      let plist = [];
      Object.keys(body).forEach((o) => {
        plist.push(`${o}=${body[o]}`);
      });
      if (plist.length > 0) {
        api = `${api}?${plist.join('&')}`;
      } else {
        api = `${api}`;
      }
    }
    console.log('utilsGet', api, body, options);
    // Toast.loading('加载中', 12);
    state.LETIME = Date.now();
    state.api = api;
    fetch(state.api, options)
      .then(response => response.text())
      .then((responseText) => {
        // Toast.hide();
        state.LETIME = Date.now() - state.LETIME;
        let res = JSON.parse(responseText);
        state.res = res;
        console.log('httpGETRES:', state.api, state.LETIME, res);
        if (res.status !== 'success') {
          // 如果是用户过期
          if (res.failedCode === 'NO_LOGIN') {
            Storage.clearall();
            toLogin();
          } else {
            Toast.fail(res.failedMsg);
          }

        }
        resolve(state);
      })
      .catch((error) => {
        state.LETIME = Date.now() - state.LETIME;
        state.ERROR = error;
        console.warn('httpGETRES_ERROR:', state);
        reject(state);
      });
  });
};

export const utilsGetGD = (api, body, state = {}, options) => {

  return new Promise((resolve, reject) => {

    if (body) {
      let plist = [];
      Object.keys(body).forEach((o) => {
        plist.push(`${o}=${body[o]}`);
      });
      if (plist.length > 0) {
        api = `${api}?${plist.join('&')}`;
      } else {
        api = `${api}`;
      }
    }
    console.log('utilsGet', api, body, options);
    // Toast.loading('加载中', 12);
    state.LETIME = Date.now();
    state.api = api;
    fetch(state.api, options)
      .then(response => response.text())
      .then((responseText) => {
        Toast.hide();
        state.LETIME = Date.now() - state.LETIME;
        let res = JSON.parse(responseText);
        state.res = res;
        console.log('httpGETRES:', state.api, state.LETIME, res);
        if (res.status !== '1') {
          Toast.fail(res.info);
        }
        resolve(state);
      })
      .catch((error) => {
        state.LETIME = Date.now() - state.LETIME;
        state.ERROR = error;
        console.warn('httpGETRES_ERROR:', state);
        reject(state);
      });
  });
};

// 格式化Post参数
export const parseParaMeter = (body) => {
  return {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json',
    },
    'body': JSON.stringify(body),
  };
};
// 上传图片专用post
export const picPost = (body) => {
  return {
    method: 'POST',
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    'body': body,
  };
};
export const FetchPost = (body) => {
  return {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    'body': body,
  };
};
const parseKey = (url, mode, param, isLogin = 0) => {

  let rstring = [];
  rstring.push(mode + '=' + url);
  if (param) {
    Object.keys(param).map((k) => { rstring.push(k + '=' + param[k]) });
    rstring.push('lg' + '=' + isLogin);
    return rstring.join('&').replace(/[_%/]/gim, '&').toLocaleLowerCase();
  }
  return (isLogin).replace(/[_%]/gim, '').toLocaleLowerCase();
};

export const toLogin = () => {
  if (global.lock) {
    goToAuth();
    global.lock = false;
    setTimeout(() => { global.lock = true }, 3000);
  }
};

export const Http = {

  TIME: [],
  TIMECOUNT: 0,

  success(resolve, reject, resData, timeIndex) {
    clearTimeout(this.TIME[timeIndex]);
    let { res } = resData;
    resolve(res);

  },

  error(reject, err, timeIndex) {
    clearTimeout(this.TIME[timeIndex]);
    console.log('err', err);
    reject('接口错误'); // 暂时没有做reject处理 改为resolve null

  },

  // 超时提示
  complete(timeIndex, parseValue) {
    return new Promise((resolve, reject) => {
      this.TIME[timeIndex] = setTimeout(() => {
        reject('API请求超时');
        // Toast.fail('API请求超时', 3);
      }, 12000);

    });
  },

  Post(api, body, options) {
    return new Promise((resolve, reject) => {
      let timeIndex = this.TIMECOUNT++;
      Promise
        .race([
          utilsPost(Config.APIRoot + api, body, {}, options),
          this.complete(
            timeIndex, { url: Config.APIRoot + api, parse: body })
        ])
        .then((res) => this.success(resolve, reject, res, timeIndex))
        .catch((err) => this.error(reject, err, timeIndex));

    });

  },

  Get(api, body, options) {

    return new Promise((resolve, reject) => {
      let timeIndex = this.TIMECOUNT++;
      let urlList = [];

      Promise
        .race([utilsGet(Config.APIRoot + api, body, {}, options),
          this.complete(
            timeIndex, { url: Config.APIRoot + api, parse: body })])
        .then((res) => this.success(resolve, reject, res, timeIndex))
        .catch((err) => this.error(reject, err, timeIndex));

    });

  },

  // 高德地图请求接口
  GetGD(api, body, options) {
    return new Promise((resolve, reject) => {
      let timeIndex = this.TIMECOUNT++;

      Promise
        .race([utilsGetGD('https://restapi.amap.com/v3/' + api, body, {}, options),
          this.complete(
            timeIndex, { url: 'https://restapi.amap.com/v3/' + api, parse: body })])
        .then((res) => this.success(resolve, reject, res, timeIndex))
        .catch((err) => this.error(reject, err, timeIndex));

    });
  },

  GetBack(api, body) {

    return new Promise((resolve, reject) => {
      let timeIndex = this.TIMECOUNT++;
      let urlList = [];

      if (api.indexOf('params') >= 0) {
        urlList.push(utilsGet(Config.APIRoot + api, body));
        urlList.push(utilsGet(Config.APIRoot + api, body));
      }

      Promise
        .race([
          utilsGet(Config.APIRoot + api, body),
          this.complete(
            timeIndex, { url: Config.APIRoot + api, parse: body })
        ])
        .then((res) => this.success(resolve, reject, res, timeIndex))
        .catch((err) => this.error(reject, err, timeIndex));
    });

  },

  // 拼接存储规则
  parseRule(url, mode = 'Get', params = {}, isLogin = 0) {

    let Auth = cacheSession.getSession();
    if (Auth) {
      isLogin = 1;
    }
    mode = mode.toLocaleLowerCase();
    return parseKey(url, mode, params, isLogin);

  },


};
