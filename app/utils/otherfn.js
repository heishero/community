import { Platform } from 'react-native';

/**
 * 判断平台
 */
export const getPlatform = () => {
  let appType = 'wap';
  if (Platform.OS === 'ios') { appType = 'ios' }
  if (Platform.OS === 'android') { appType = 'android' }
  return appType;
};

export const addCommas = (val) => {
  return val && val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};
export function isEmptyObject(obj) {
  return Object.keys(obj).length === 0;
}

export function isjson(obj) {
  return typeof (obj) === 'object' && Object.prototype.toString.call(obj).toLowerCase() == '[object object]' && !obj.length;
}

/**
 * 判断两个数组是否相同
 */
export function isEqual(a, b) {
  // 如果a和b本来就全等
  if (a === b) {
    // 判断是否为0和-0
    return a !== 0 || 1 / a === 1 / b;
  }
  // 判断是否为null和undefined
  if (a == null || b == null) {
    return a === b;
  }
  // 接下来判断a和b的数据类型
  let classNameA = toString.call(a),
    classNameB = toString.call(b);
  // 如果数据类型不相等，则返回false
  if (classNameA !== classNameB) {
    return false;
  }
  // 如果数据类型相等，再根据不同数据类型分别判断
  switch (classNameA) {
    case '[object RegExp]':
    case '[object String]':
    // 进行字符串转换比较
      return String(a) === String(b);
    case '[object Number]':
    // 进行数字转换比较,判断是否为NaN
      if (Number(a) !== Number(a)) {
        return Number(b) !== Number(b);
      }
      // 判断是否为0或-0
      return Number(a) === 0 ? 1 / Number(a) === 1 / b : Number(a) === Number(b);
    case '[object Date]':
    case '[object Boolean]':
      return Number(a) === Number(b);
  }
  // 如果是对象类型
  if (classNameA == '[object Object]') {
    // 获取a和b的属性长度
    let propsA = Object.getOwnPropertyNames(a),
      propsB = Object.getOwnPropertyNames(b);
    if (propsA.length != propsB.length) {
      return false;
    }
    for (let i = 0; i < propsA.length; i++) {
      let propName = propsA[i];
      // 如果对应属性对应值不相等，则返回false
      if (a[propName] !== b[propName]) {
        return false;
      }
    }
    return true;
  }
  // 如果是数组类型
  if (classNameA == '[object Array]') {
    if (a.toString() == b.toString()) {
      return true;
    }
    return false;
  }
}

/**
 * 判断是否手机号
 * @param value
 * @returns {boolean}
 */
export function isphone(value) {
  let reg = /^1[0-9]{10}$/;
  return reg.test(value);
}
/**
 *
 * @param str
 * @returns {XML|string|*}
 */
export function trim(str) {
  let result;
  result = str.replace(/(^\s+)|(\s+$)/g, '');
  result = result.replace(/\s/g, '');
  return result;
}

/**
 * 加
 * @param num1
 * @param num2
 * @returns {number}
 */
export function numAdd(num1, num2) {
  let baseNum,
    baseNum1,
    baseNum2;
  try {
    baseNum1 = num1.toString().split('.')[1].length;
  } catch (e) {
    baseNum1 = 0;
  }
  try {
    baseNum2 = num2.toString().split('.')[1].length;
  } catch (e) {
    baseNum2 = 0;
  }
  baseNum = Math.pow(10, Math.max(baseNum1, baseNum2));
  return (num1 * baseNum + num2 * baseNum) / baseNum;
}
/** 减法
 * @return {number}
 */
export function numSub(num1, num2) {
  let { baseNum1, baseNum2, baseNum } = newFunction();
  let precision;// 精度
  try {
    baseNum1 = num1.toString().split('.')[1].length;
  } catch (e) {
    baseNum1 = 0;
  }
  try {
    baseNum2 = num2.toString().split('.')[1].length;
  } catch (e) {
    baseNum2 = 0;
  }
  baseNum = Math.pow(10, Math.max(baseNum1, baseNum2));
  precision = (baseNum1 >= baseNum2) ? baseNum1 : baseNum2;
  return ((num1 * baseNum - num2 * baseNum) / baseNum).toFixed(precision);

  function newFunction() {
    let baseNum,
      baseNum1,
      baseNum2;
    return { baseNum1, baseNum2, baseNum };
  }
}
/**
 * 除以
 * @return {number}
 */
export function Div(a, b) {
  let n1 = 0,
    n2 = 0;
  let baseNum3,
    baseNum4;
  try {
    n1 += a.toString().split('.')[1].length;
  } catch (e) {
  }
  try {
    n2 += b.toString().split('.')[1].length;
  } catch (e) {
  }
  return (a * Math.pow(10, n1)) / (b * Math.pow(10, n2)) / Math.pow(10, n1 - n2);
}


/**
 * 乘以
 * @return {number}
 */
export function Mul(arg1, arg2) {
  let m = 0,
    s1 = arg1.toString(),
    s2 = arg2.toString();
  try {
    m += s1.split('.')[1].length;
  } catch (e) {
  }
  try {
    m += s2.split('.')[1].length;
  } catch (e) {
  }
  return Number(s1.replace('.', '')) * Number(s2.replace('.', '')) / Math.pow(10, m);
}

export function openIfram(url) {
  // 第一部分
  let dom,
    doc,
    where,
    iframe = document.createElement('iframe');
  iframe.src = 'javascript:false';
  iframe.title = '';
  iframe.role = 'presentation';
  (iframe.frameElement || iframe).style.cssText = 'width: 0; height: 0; border: 0';
  where = document.getElementsByTagName('script');
  where = where[where.length - 1];
  where.parentNode.insertBefore(iframe, where);

  // 第二部分
  try {
    doc = iframe.contentWindow.document;
  } catch (e) {
    // IE下如果主页面修改过document.domain，那么访问用js创建的匿名iframe会发生跨域问题，必须通过js伪协议修改iframe内部的domain
    dom = document.domain;
    iframe.src = "javascript:var d=document.open();d.domain='" + dom + "';void(0);";
    doc = iframe.contentWindow.document;
  }
  doc.open()._l = function () {
    let js = this.createElement('script');
    if (dom) this.domain = dom;
    js.id = 'js-iframe-async';
    js.src = url;
    this.body.appendChild(js);
  };
  doc.write('<body onload="document._l();">');
  doc.close();
}
export function isiOS(u) {
  // noinspection JSDuplicatedDeclaration
  var u = u || window.navigator.userAgent;
  return !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); // ios终端
}

export function toDecimal(x) {
  let f = parseFloat(x);
  if (isNaN(f)) {
    return;
  }
  f = Math.round(x * 100) / 100;
  return f;
}
export function toDecimal2(x) {
  var f = parseFloat(x);
  if (isNaN(f)) {
    return false;
  }
  var f = Math.round(x * 100) / 100;
  let s = f.toString();
  let rs = s.indexOf('.');
  if (rs < 0) {
    rs = s.length;
    s += '.';
  }
  while (s.length <= rs + 2) {
    s += '0';
  }
  return s;
}


export function postcall(url, params, target) {
  let tempform = document.createElement('form');
  tempform.action = url;
  tempform.method = 'post';
  tempform.style.display = 'none';
  if (target) {
    tempform.target = target;
  }

  for (let x in params) {
    var opt = document.createElement('input');
    opt.name = x;
    opt.value = params[x];
    tempform.appendChild(opt);
  }

  var opt = document.createElement('input');
  opt.type = 'submit';
  tempform.appendChild(opt);
  document.body.appendChild(tempform);
  // debugger
  tempform.submit();
  document.body.removeChild(tempform);
}
export function parseDom(arg) {

  let objE = document.createElement('div');
  objE.style.display = 'none';

  objE.innerHTML = arg;

  return objE;

}
export function isWeixinBrowser() {
  return /micromessenger/.test(navigator.userAgent.toLowerCase());
}
export function addEvent(obj, type, fn) {
  if (obj.attachEvent) {
    obj['e' + type + fn] = fn;
    obj[type + fn] = function () {
      obj['e' + type + fn](window.event);
    };
    obj.attachEvent('on' + type, obj[type + fn]);
  } else { obj.addEventListener(type, fn, false) }
}
export function removeEvent(obj, type, fn) {
  if (obj.detachEvent) {
    obj.detachEvent('on' + type, obj[type + fn]);
    obj[type + fn] = null;
  } else { obj.removeEventListener(type, fn, false) }
}

// 随机数据方法
export function done(num, arr) {
  let newArray = [];
  let arro = arr.slice(0); // 解决操作影响arr
  for (let index = 0; index < num; index++) {
    let n = Math.floor(Math.random() * arro.length);
    newArray.push(arro[n]);
    arro.splice(n, 1);
  }
  return newArray;
}

// 数据初始化
export function initDate(start, end, valuetype, type = 'red') {
  let numlis = [];
  for (let ri = start; ri <= end; ri++) {
    let temobj = {};
    temobj.value = valuetype == 6 ? ri : ri < 10 ? '0' + ri : ri.toString();
    temobj.checked = false;
    temobj.lostnum = '';
    temobj.type = type;
    numlis.push(temobj);
  }
  return numlis;
}

// 数组去重
export function unique(arr) {
  let unique = {};
  arr.forEach(function(item) {
    unique[JSON.stringify(item)] = item;// 键名不会重复
  });
  arr = Object.keys(unique).map(function(u) {
  // Object.keys()返回对象的所有键值组成的数组，map方法是一个遍历方法，返回遍历结果组成的数组.将unique对象的键名还原成对象数组
    return JSON.parse(u);
  });
  return arr;
}

// 任选九方法
export function groupSplit(arr, size) {
  let r = []; // result

  function _(t, a, n) { // tempArr, arr, num
    if (n === 0) {
      r[r.length] = t;
      return;
    }
    for (let i = 0, l = a.length - n; i <= l; i++) {
      let b = t.slice();
      b.push(a[i]);
      _(b, a.slice(i + 1), n - 1);
    }
  }

  _([], arr, size);
  return r;
}

// js 数组积
export function arrx(arr) {
  return arr.reduce((x, y) => x * y, 1);
};

// 计算注数
export function countNum(arr) {
  let allzhu = 0;
  arr.map(e => {
    allzhu += e.snum;
  });
  return allzhu;
}

/**
 * @return {number}
 */
export function C(n, m) {
  return factorial(n) / (factorial(m) * factorial(n - m));
}
/**
 * @return {number}
 */
export function P(n, m) {
  return factorial(n) / factorial(n - m);
}

export function factorial(n) {
  let sum = 1;
  for (let i = 1; i <= n; i++) {
    sum *= i;
  }
  return sum;
}
