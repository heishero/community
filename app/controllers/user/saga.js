import { takeEvery, put, call } from 'redux-saga/effects';
import types from './types';
import Storage from '../../utils/storage';
import JPushModule from 'jpush-react-native';
import JMessage from 'jmessage-react-plugin';
import { getInfo } from '../../service/user';
import { channel } from 'redux-saga';

let Sagas = {};
const getConversationsChannel = channel();

/**
 * 清楚用户信息
 */
Sagas['UserSaga'] = function* () {
  yield takeEvery(types.Clear, clear);  // 清除用户信息
  yield takeEvery(types.GetUserInfo, getUserInfo); // 根据token获取用户信息
  yield takeEvery(getConversationsChannel, watchConversations);
};

function* watchConversations(action) {
  yield put(action);
}

function* getUserInfo() {
  try {
    const token = yield Storage.load('token');
    const userInfo = yield call(getInfo, token);
    // 当jmessage未登录则自动登录
    JMessage.getMyInfo((UserInf) => {
      if (!UserInf.username && userInfo.data.jg_im_username) {
        JMessage.login({
          username: userInfo.data.jg_im_username,
          password: userInfo.data.jg_im_userpassword
        }, () => { console.log('JMessage 登录成功');
          JMessage.getConversations(data => {
            getConversationsChannel.put({
              type: types.RenderData, payload: {
                messageList: data
              }
            }); }, (error) => {
            console.log(error);
          }); }, (error) => { console.log('JMessage登录失败', error) });
      }
    });

    yield put({
      type: types.RenderData, payload: {
        userInfo: userInfo.data
      }
    });
  } catch (error) {
    console.log(error);
  }
}

function* clear() {
  let userInfo = { // 用户基础数据
    'id': null,
    'mobile': null,
    'nick_name': null,
    'headimgurl': null,
    'auth_token': null,
    'user_cert_status': null
  };
  yield put({
    type: types.RenderData, payload: {
      userInfo
    }
  });
  Storage.clearall();
  JMessage.logout();
  JPushModule.setAlias(``, () => {
    console.log('Set alias succeed');
  }, () => {
    console.log('Set alias failed');
  });
}

export default Sagas;
