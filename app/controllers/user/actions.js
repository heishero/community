import types from './types';

export default {
  // 获取用户信息
  GetUserInfo(Opt) {
    return { type: types.GetUserInfo, payload: { ...Opt }};
  },
  // 更新页面数据
  RenderData(Opt) {
    return { type: types.RenderData, payload: { ...Opt }};
  },
  // 清空数据
  Clear() {
    return { type: types.Clear };
  },
};
