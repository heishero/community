import types  from './types';

const defaultData = {
  userInfo: { // 用户基础数据
    'id': null,
    'mobile': null,
    'nick_name': null,
    'headimgurl': null,
    'auth_token': null,
    'user_cert_status': null
  }
};

export default {
  RenderData(state = defaultData, action) {
    switch (action.type) {
      case types.RenderData:
        return { ...state, ...action.payload };
      default:
        return state;
    }
  }
};