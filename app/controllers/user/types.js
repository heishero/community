export default {

  // 获取用户信息
  GetUserInfo: 'USER_GETUSERINFO',

  // 更新页面数据
  RenderData: 'USER_RENDERDATA',

  // 清除数据
  Clear: 'USER_CLEAR',

};
