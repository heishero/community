import { channel } from 'redux-saga';
import { put, select, takeEvery } from 'redux-saga/effects';
import types from './types';
import UserAction from '../user/actions';
import JMessage from 'jmessage-react-plugin';

let Sagas = {};

const getConversationsChannel = channel();

/**
 * 登录调用，不会触发页面渲染。
 * 登录成功后调用获取更新用户信息
 */
Sagas['MessageSaga'] = function* () {
  yield takeEvery(types.GetMessage, getMessage);  // 更改tab
  yield takeEvery(getConversationsChannel, watchConversations);
};

function* watchConversations(action) {
  yield put(action);
}

function* getMessage() {
  try {
    const { userInfo } = yield select((state) => (state.UserRenderData));
    if (!userInfo.auth_token) {
      yield put(UserAction.GetUserInfo());
    }
    yield put({
      type: types.RenderData, payload: {
        loading: true
      }
    });
    JMessage.getConversations(data => {
      console.log('getConversations', data);
      for (const item of data) { // 下载用户或者群头像
        if (item.conversationType === 'single') {
          JMessage.downloadThumbUserAvatar({ username: item.target.username, appKey: '9fea82812ba31bdd5fa7fd7b' },
            (result) => { item.target.avatarThumbPath = result.filePath },
            (err) => { console.log('下载用户头像失败:', err) });
        }
        if (item.conversationType === 'group') {
          JMessage.downloadThumbGroupAvatar({ groupId: item.target.id, appKey: '9fea82812ba31bdd5fa7fd7b' },
            (result) => {
              item.target.avatarThumbPath = result.filePath;
            }, (err) => {
              console.log('下载群组头像失败:', err);
            });
        }
      }
      getConversationsChannel.put({
        type: types.RenderData, payload: {
          messageList: data,
          loading: false,
        }
      }); }, (error) => {
      console.log(error);
      getConversationsChannel.put({
        type: types.RenderData, payload: {
          loading: false,
        }
      });
    });
  } catch (error) {
    console.log(error);
    yield put({
      type: types.RenderData, payload: {
        loading: false
      }
    });
  }
}

export default Sagas;
