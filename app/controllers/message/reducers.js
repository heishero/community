import types  from './types';

const defaultData = {
  messageList: [],
  loading: false,
  freind: [],
};

export default {
  RenderData(state = defaultData, action) {
    switch (action.type) {
      case types.RenderData:
        return { ...state, ...action.payload };
      default:
        return state;
    }
  }
};