import types from './types';

export default {
  // 更新页面数据
  RenderData(Opt) {
    return { type: types.RenderData, payload: { ...Opt }};
  },
  /**
   * 获取群列表数据
   * @param {*} p_cid 一级id
   * @param {*} s_cid 二级id
   * @param {*} search
   * @param {*} page
   * @param {*} pageSize
   */
  GetGroups(Opt) {
    return { type: types.GetGroups, payload: { ...Opt }};
  },
  GetGroupsMore() {
    return { type: types.GetGroupsMore };
  },
  // 获取群分类
  GetClassify() {
    return { type: types.GetClassify };
  },
  // 清除数据
  Clear(Opt) {
    return { type: types.Clear };
  }
};