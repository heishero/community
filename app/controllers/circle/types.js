export default {
  // 更新页面数据
  RenderData: 'CIRCLE_RENDERDATA',
  GetGroups: 'CIRCLE_GETGROUPS',
  GetGroupsMore: 'CIRCLE_GETGROUPS_MORE',
  GetClassify: 'CIRCLE_GETCLASSIFY',
  ChangeMenu: 'CIRCLE_CHANGEMENU',
  Clear: 'CIRCLE_CLEAR',
};
