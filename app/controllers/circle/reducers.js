import types  from './types';

const defaultData = {
  page: 1,
  pageSize: 10,
  // 首页群列表
  groupsList: [],
  // 分类页群列表
  classifyList: [],
  hasMore: false,
  // 群分类数据
  menu: [],
  // 一级分类id
  p_cid: null,
  // 二级分类id，可选
  menuActive: null,
  // 根据名字搜索，可选
  search: '',
  loading: false,
};

export default {
  RenderData(state = defaultData, action) {
    switch (action.type) {
      case types.RenderData:
        return { ...state, ...action.payload };
      case types.Clear:
        return { ...state, groupsList: [] };
      default:
        return state;
    }
  }
};