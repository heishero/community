import { call, put, select, takeEvery } from 'redux-saga/effects';
import types from './types';
import Storage from '../../utils/storage';
import { getGroupList, getClassify } from '../../service/group';

let Sagas = {};

/**
 * 登录调用，不会触发页面渲染。
 * 登录成功后调用获取更新用户信息
 */
Sagas['CircleSaga'] = function* () {
  yield takeEvery(types.GetGroups, getGroupsList);
  yield takeEvery(types.GetGroupsMore, getGroupsMore);
  yield takeEvery(types.GetClassify, getClassifyData);
};

/**
 * 获取群分类
 */
function* getClassifyData() {
  try {
    const { data } = yield call(getClassify, { pid: 0 });
    yield put({
      type: types.RenderData, payload: {
        menu: data,
      }
    });
  } catch (error) {
    console.log(error);
  }
}

/**
 * 获取群列表
 */
function* getGroupsList({ payload }) {
  try {
    const { pageSize } = yield select((state) => (state.CircleRenderData));
    yield put({
      type: types.RenderData, payload: {
        loading: true,
      }
    });
    const token = yield Storage.load('token');
    const { data } = yield call(getGroupList, { token, page: 1, pageSize, ...payload });
    yield put({
      type: types.RenderData, payload: {
        groupsList: data,
        hasMore: data.length === pageSize,
        page: 1,
        'p_cid': payload.p_cid,
        'menuActive': payload.s_cid,
        search: payload.search || '',
        loading: false,
      }
    });
  } catch (error) {
    console.log(error);
  }
}

// 所有群列表加载更多
function* getGroupsMore() {
  try {
    const { page, pageSize, groupsList, p_cid, menuActive, search } = yield select((state) => (state.CircleRenderData));
    const token = yield Storage.load('token');
    const { data } = yield call(getGroupList, {
      token,
      page: page + 1,
      pageSize,
      [p_cid && 'p_cid']: p_cid,
      [(p_cid && menuActive) && 's_cid']: menuActive,
      search
    });
    yield put({
      type: types.RenderData, payload: {
        groupsList: groupsList.concat(data),
        hasMore: data.length === pageSize,
        page: page + 1,
      }
    });
  } catch (error) {

  }
}

export default Sagas;
