import types from './types';

export default {
  // 更新页面数据
  RenderData(Opt) {
    return { type: types.RenderData, payload: { ...Opt }};
  },
  // 获取消息列表
  GetAddress() {
    return { type: types.GetAddress };
  },
  // 获取地址
  CheckAddress({ data }) {
    return { type: types.RenderData, payload: { checkAddress: data }};
  }
};