import types  from './types';

const defaultData = {
  addressList: [],
  checkAddress: null,
};

export default {
  RenderData(state = defaultData, action) {
    switch (action.type) {
      case types.RenderData:
        return { ...state, ...action.payload };
      default:
        return state;
    }
  }
};