import { call, takeEvery, put } from 'redux-saga/effects';
import types from './types';
import Storage from '../../utils/storage';
import { getAddress } from '../../service/user';

let Sagas = {};

/**
 * 登录调用，不会触发页面渲染。
 * 登录成功后调用获取更新用户信息
 */
Sagas['MessageSaga'] = function* () {
  yield takeEvery(types.GetAddress, fetchAddress);  // 获取地址
};


function* fetchAddress() {
  try {
    const token = yield Storage.load('token');
    const { data } = yield call(getAddress, token);
    let checkAddress;
    for (let item of data) {
      if (item.useing === 1) {
        checkAddress = item;
        break;
      }
    }
    yield put({ type: types.RenderData, payload: { addressList: data, checkAddress }});
  } catch (error) {
    console.log(error);
  }
}

export default Sagas;
