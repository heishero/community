/*
 * @Author: tanggan
 * 字体组件
 * @param {*} type为蓝色或红色 {red||blue}
 * @param {*} textStyle标签字体的样式
 * @param {*} wrapperStyle标签字体外框的样式
 */

import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { Rem, createStyles } from '../utils/view';
import { colors } from '../styles/basic';

const CircleTag = ({ clickfn, checked = false, number, type = 'red', size = 'md', lostnum = '', lotte = false }) => {
  return <TouchableOpacity activeOpacity={0.6} onPress={() => { clickfn && clickfn() }}>
    <View style={[checked ? styles.viewStyleActive : styles.viewStyle,
      {
        borderColor: type === 'blue' ? colors.Blue : colors.Red,
        backgroundColor: checked ? (type === 'blue' ? colors.Blue : colors.Red) : '#f6f8f7',
        width: size === 'md' ? Rem(80) : Rem(20),
        height: size === 'md' ? Rem(80) : Rem(20),
        borderRadius: size === 'md' ? Rem(40) : Rem(10),
        marginRight: lotte === true ? Rem(10) : Rem(20)
      }]}>
      <Text style={[styles.textStyle,
        { color: checked ? colors.White : (type === 'blue' ? colors.Blue : colors.Red) }]}>{number}</Text>
    </View>
    {(lostnum !== '' && lostnum !== null) && <View><Text style={styles.lostNumStyle}>{lostnum}</Text></View>}
  </TouchableOpacity>;
};

let styles = {
  viewStyle: {
    borderWidth: 4,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    marginRight: 40,
    marginTop: 40,
  },
  viewStyleActive: {
    borderWidth: 4,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 40,
    marginTop: 40,
  },
  textStyle: {
    flex: 1,
    fontSize: 30,
    textAlign: 'center',
    fontWeight: '700',
  },
  lostNumStyle: {
    flex: 1,
    fontSize: 30,
    textAlign: 'center',
    color: colors.c999
  }
};
styles = createStyles(styles);

export default CircleTag;