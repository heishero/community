import React from 'react';
import { View } from 'react-native';
import { Rem } from '../utils/view';

const WingBlank = ({ children }) => (
  <View style={{ paddingHorizontal: Rem(26) }}>
    {children}
  </View>
);

export default WingBlank;