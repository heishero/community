/*
 * @Author: yixin 
 * @Date: 2018-08-10 11:14:20 
 * @Last Modified by: yixin
 * @Last Modified time: 2019-03-23 17:25:54
 * 字体组件
 */

import React from 'react';
import { Text } from 'react-native';
import { Rem } from '../utils/view';
import { colors } from '../styles/basic';

/**
 * 自定义字体组件 按照750的标准
 * @param {*} size 字体大小
 * @param {*} color 字体颜色
 * @param {*} fontWeight 字体粗细
 * @param {*} children 文本
 */
const CText = ({ size = 30, color = colors.c333, fontWeight = '400', children, lineHeight }) => {
  return <Text style={{ fontSize: Rem(size), color: color, fontWeight: fontWeight, lineHeight: lineHeight ? Rem(lineHeight) : null }}>{children}</Text>;
};

export default CText;