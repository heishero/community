/*
 * @Author: tanggan
 * 暂无数据组件
 * @param {*} type为['normal'|'follow'],展示的图片不一样
 * @param {*} infomation图片下的提示，默认为‘暂无数据’
 * @param {*} btnContent是一个集合，如果需要按钮，则需要传入这个参数，集合里面须传title,path，
 */

import React from 'react';
import { Text, View, Image } from 'react-native';
import { Button } from 'antd-mobile-rn';
import PropTypes from 'prop-types';
import { createStyles } from '../utils/view';
import { colors } from '../styles/basic';

export default  class NoData extends React.Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  static propTypes = {
    type: PropTypes.oneOf(['normal', 'follow']),
    infomation: PropTypes.string,
    btnContent: PropTypes.object
  }

  static defaultProps = {
    type: 'normal',
    infomation: '暂无数据',
    btnContent: {}
  }

  render() {
    const { type, infomation, btnContent, navigator } = this.props;
    let btnCompoent = null;
    if (JSON.stringify(btnContent) !== '{}') {
      btnCompoent = <Button style={styles.btn} onClick={() => { navigator.push({
        screen: btnContent.path
      }); }}>{btnContent.title}</Button>;
    }
    return (
      <View style={styles.nodataArea}>
        {type === 'normal' ? <Image
          style={styles.normal}
          source={require('../../assets/img/nodataPic.png')}
        /> : <Image
          style={styles.img}
          source={require('../../assets/img/nodataPic.png')}
        />}
        <Text style={styles.nodataText}>{infomation}</Text>
        {btnCompoent}
      </View>
    );
  }

}

let styles = {
  nodataArea: {
    marginTop: 200,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  nodataText: {
    textAlign: 'center',
    marginTop: 50,
    fontSize: 32
  },
  normal: {
    width: 520,
    height: 320,
  },
  img: {
    width: 580,
    height: 420,
  },
  btn: {
    marginTop: 20
  }
};

styles = createStyles(styles);

