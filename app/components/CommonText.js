 /*
 * @Author: tanggan
 * 字体组件
 * @param {*} size字符类型，字体大小
 * @param {*} styles集合，接收的自定义样式
 */

import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';
import { Rem } from '../utils/view';

const Font = {
    'sm': 24,
    'md': 36,
    'lg': 32
}

const Color = {
    'sm': '#999',
    'md': '#666',
    'lg': '#333'
}

class CommonText extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newStyles: { }
    };
  }

  static propTypes = {
    size: PropTypes.oneOf(['sm','md','lg']),
  }

  static defaultProps = {
    size: 'sm'
  }

  componentDidMount(){
    const { size } = this.props;
    this.setState({
      newStyles: {
        fontSize: Rem(Font[size]),
        color: Color[size]
      }
    })
  }

  render() {
    const { style } = this.props;
    const { newStyles } = this.state;
    return <Text style={[newStyles,style]}>{this.props.children}</Text>
  }
}

export default CommonText;