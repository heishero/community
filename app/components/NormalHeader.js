/*
 * @Author: tanggan
 * 头部公用组件
 * @param {*} children 中间部分的内容，可以是一个React节点，组件，也可以是单纯的文字
 * @param {*} leftContent 导航左边部分的内容，默认返回按钮
 * @param {*} rightContent 导航右边部分的内容，默认返回按钮
 * @param {*} onLeftPress 导航左边部分点击执行的方法
 * @param {*} onRightPress 导航右边部分点击执行的方法
 */
import React from 'react';
import Icon from 'react-native-vector-icons/icomoon';
import { Text, View, TouchableWithoutFeedback } from 'react-native';
import { createStyles, isIphoneX } from '../utils/view';
import PropTypes from 'prop-types';
import { colors } from '../styles/basic';

class NormalHeader extends React.Component {

    static propTypes = {
      leftContent: PropTypes.any,
      rightContent: PropTypes.any,
      onLeftPress: PropTypes.func,
      onRightPress: PropTypes.func,
    }

    static defaultProps = {
      leftContent: undefined,
      rightContent: undefined,
      onLeftPress: () => {},
      onRightPress: () => {},
    }

    componentDidMount() { }

    // 渲染导航栏中间部分内容
    renderContent() {
      let TextNode = null;
      React.Children.map(this.props.children, function (child) {
        switch (typeof child) {
          case 'object':
            TextNode = child;
            break;
          case 'string':
            TextNode = <View><Text style={styles.text}>{child}</Text></View>;
            break;
          default:
            return;
        }
      });
      return TextNode;
    }

    renderNavigation(node) {
      let content = null;
      switch (typeof node) {
        case 'object':
          content = node;
          break;
        case 'string':
          content = <View><Text style={styles.text}>{node}</Text></View>;
          break;
        default:
          return;
      }
      return content;
    }

    render() {
      const { leftContent, rightContent, onLeftPress, onRightPress, backgroundColor } = this.props;
      return (
        <View
          style={[styles.head, backgroundColor && { backgroundColor: backgroundColor }]}
        >
          <TouchableWithoutFeedback onPress={() => {
            onLeftPress && onLeftPress();
          }}>
            <View style={styles.leftContent}>
              <View>{this.renderNavigation(leftContent)}</View>
            </View>
          </TouchableWithoutFeedback>
          <View style={styles.centerContent}>{this.renderContent()}</View>
          <View style={styles.rightContent}>
            <TouchableWithoutFeedback onPress={() => {
              onRightPress && onRightPress();
            }}>
              <View>{this.renderNavigation(rightContent)}</View>
            </TouchableWithoutFeedback>
          </View>
        </View>
      );
    }
}

const common = {
  flex: 1,
  justifyContent: 'center'
};

let styles = {
  head: {
    // borderBottomColor:'#D7D7D7',
    // borderWidth: 0.5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    ios: {
      height: isIphoneX ? 152 : 128,
      paddingTop: isIphoneX ? 64 : 40,
    },
    android: {
      height: 128,
      paddingTop: 40
    },
    backgroundColor: '#fff',
    zIndex: 9,
  },
  text: {
    color: '#000',
    fontSize: 36,
  },
  leftContent: {
    ...common,
    padding: 20,
    alignItems: 'flex-start',
  },
  centerContent: {
    // ...common,
    alignItems: 'center'
  },
  rightContent: {
    ...common,
    paddingRight: 20,
    alignItems: 'flex-end'
  }
};

styles = createStyles(styles);

export default NormalHeader;