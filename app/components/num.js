import React from 'react';
import { Text, View } from 'react-native';
import { createStyles } from '../utils/view';
import { colors } from '../styles/basic';
import { Flex } from 'antd-mobile-rn';

const Num = ({ val, blue, type = 'md' }) => {
  return (
    <Text style={[blue ? styles.blue : styles.red, {
      marginRight: type === 'md' ? 36 : 5
    }]}>{val}</Text>
  );
};

let styles = {
  blue: {
    color: colors.Blue,
    fontSize: 36,
    // marginRight: 5,
    fontWeight: '500',
    // backgroundColor: '#f6f8f7',
  },
  red: {
    color: colors.Red,
    fontSize: 36,
    // marginRight: 10,
    fontWeight: '500',
    // backgroundColor: '#f6f8f7',
  }
};
styles = createStyles(styles);

export default Num;