 /*
 * @Author: tanggan
 * 模态框组件
 * @param {*} visible布尔值，控制框态框的显示
 * @param {*} title模态框标题
 * @param {*} content模态框主体内容，应为React Element
 * @param {*} footer布尔值，当传入值为false时，不显示底部的取消和确定按钮
 * @param {*} cancen传入的应为一个方法
 */

import React from 'react';
import { Text, Animated, View, TouchableWithoutFeedback, Modal, ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import { createStyles, Rem } from '../utils/view';
import { colors } from '../styles/basic';
import { Button } from 'antd-mobile-rn';

class CommonModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  static propTypes = {
    visible: PropTypes.bool,
    cancel: PropTypes.func,
    title: PropTypes.any,
    content: PropTypes.any,
    footer: PropTypes.bool,
  }

  componentDidMount(){ }

  renderTitle(node){
    let content = null;
    switch (typeof node) {
        case 'object':
            content = node;
          break;
        case 'string':
            content = <Text style={styles.title}>{node}</Text>;
          break;
        default: 
          return;
    }
    return content;
  }

  render() {
    const { 
        cancel, 
        visible, 
        title, 
        content, 
        footer 
    } = this.props;
    return (
        <View style={styles.wrapper}>
            <Modal
                visible={visible}
                animationType={"slide"}
                transparent={true}
                >
                <View style={styles.mask}>
                    <View style={styles.container}>
                        {title && <View style={styles.header}>{this.renderTitle(title)}</View>}
                        <View style={styles.content}>{content}</View>
                        {footer && <View style={styles.footer}>
                            {cancel && <View style={styles.modalBtns}>
                                <Button onClick={() => {
                                    cancel && cancel()
                                }} style={[styles.modalBtn,styles.cancel]}>取消</Button>
                            </View>}
                            <View style={styles.modalBtns}>
                                <Button type="primary" style={styles.modalBtn} >确定</Button>
                            </View>
                        </View>}
                    </View>
                </View>
            </Modal>
        </View>
    )
  }
}

let styles = {
    wrapper: {
        flex: 1,
    },
    mask: {
        flex: 1,
        flexDirection: 'column',
        flexWrap: 'wrap',
        backgroundColor: 'rgba(0,0,0,.7)',
    },
    container: {
        flex: 1,
        width: 750,
        height: 872,
        flexDirection: 'column',
        flexWrap: 'wrap',
        justifyContent: 'flex-end'
    },
    header: {
        height: 76,
        backgroundColor: colors.White
    },
    title: {
        textAlign: 'center',
        lineHeight: 76
    },
    content: {
        height: 700,
        backgroundColor: '#f2f2f2',
    },
    footer: {
        height: 88,
        flexWrap: 'wrap',
        flexDirection: 'row',
    },
    modalBtns: {
        flex: 1,
    },
    modalBtn: {
        borderRadius: 0
    },
    cancel: {
        backgroundColor: colors.cccc
    },
}

styles = createStyles(styles);

export default CommonModal;