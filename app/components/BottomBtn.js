/*
 * @Author: yixin
 * @Date: 2018-09-06 14:24:34
 * @Last Modified by: yixin
 * @Last Modified time: 2018-10-11 18:32:46
 * 彩种下单底部
 */
import React, { PureComponent } from 'react';
import Icon from 'react-native-vector-icons/icomoon';
import { TouchableOpacity, Alert } from 'react-native';
import { Flex, Toast } from 'antd-mobile-rn';
import CText from './CText';
import { createStyles } from '../utils/view';

class BottomBtn extends PureComponent {

  render() {
    const { clearfn, topvalue, surefn } = this.props;
    return (
      <Flex styles={styles.wrapper} direction="row" justify="start" align="center">
        <TouchableOpacity style={styles.clearBox} onPress={() => {
          Alert.alert(
            '提示',
            '确定清除所有吗？',
            [
              { text: '取消', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
              { text: '确定', onPress: () => { if (topvalue > 0) { clearfn() } else { Toast.offline('已经没东西了') } } },
            ],
            { cancelable: false }
          );
        }}>
          <Icon style={styles.clearIcon} name="dustbin" />
          <CText size="14">清空</CText>
        </TouchableOpacity>
        <Flex style={styles.centerBox} direction="column" justify="center" align="center">
          {topvalue > 0 ?
            <CText size="30">已经选择<CText size="30" color="#d1443b">{topvalue}</CText>场比赛</CText> :
            <CText size="30">请选择您的投注场比赛</CText>
          }
          <CText size="22" color="#999">
            <Icon name="plaint" />
            开奖结果不包含加时赛及点球大战
          </CText>
        </Flex>
        <TouchableOpacity style={styles.okBtn} onPress={() => { surefn() }}>
          <CText size="36" color="#fff">确定</CText>
        </TouchableOpacity>
      </Flex>
    );
  }
}

let styles = {
  wrapper: {
    height: 100,
    borderTopWidth: 1,
    borderColor: '#f6f6f6'
  },
  clearBox: {
    height: 60,
    width: 105,
    borderRightWidth: 1,
    borderColor: '#e6e6e6',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  clearIcon: {
    fontSize: 44,
  },
  centerBox: {
    height: 78,
    width: 426,
  },
  okBtn: {
    width: 218,
    height: 100,
    backgroundColor: '#d1443b',
    justifyContent: 'center',
    alignItems: 'center'
  }
};
styles = createStyles(styles);
export default BottomBtn;
