/*
 * @Author: tanggan
 * tabs标签页，基于antd-mobile-rn的tabs标签页封装
 * @param {*} initialPage页面初始化时进到的指定的标签页
 * @param {*} tabs标签的title
 * onChange是tab标签切换触发的事件
 */

import React from 'react';
import { Text, View, TouchableWithoutFeedback } from 'react-native';
import { Tabs } from 'antd-mobile-rn';
import PropTypes from 'prop-types';
import { createStyles, Rem } from '../utils/view';

class CommonTabs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 0,
      topLineWidth: 0,
      lineOffset: 0
    };
  }

  static propTypes = {
    tabs: PropTypes.array,
    initialPage: PropTypes.number
  }

  static defaultProps = {
    initialPage: 0
  }

  componentDidMount() {
    const { tabs, initialPage } = this.props;
    const tl = 750 / Number.parseInt(tabs.length);
    const topLineWidth = Rem(tl);
    this.setState({
      topLineWidth,
      activeTab: initialPage
    });
  }

  onChange = (content, key) => {
    const { onChange } = this.props;
    this.setState({ activeTab: key });
    onChange && onChange(content, key);
  }

  render() {
    const { activeTab, topLineWidth } = this.state;
    return (
      <View style={{ flex: 1 }} >
        <Tabs
          {...this.props}
          page={activeTab}
          onChange={this.props.onChange}
          renderTabBar={(props) => {
            return (
              <View style={styles.tabbarStyle}>
                <View style={[styles.topTabbarLine, { width: topLineWidth, left: (topLineWidth * activeTab) }]}></View>
                {!!props.tabs.length && props.tabs.map((item, key) => {
                  return (
                    <TouchableWithoutFeedback key={key} onPress={() => {
                      this.onChange(item, key);
                    }}>
                      <View
                        style={[styles.singleTab, props.activeTab === key && styles.activeSingleTab]}
                      >
                        <Text
                          style={[styles.tabText, props.activeTab === key ? styles.activeTabText : styles.normalTabText]}
                        >{item.title}</Text>
                      </View>
                    </TouchableWithoutFeedback>
                  );
                })}
              </View>
            );
          }}>
          {React.Children.map(this.props.children, function (child) {
            return child;
          })}
        </Tabs>
      </View>
    );
  }
}

let styles = {
  tabbarStyle: {
    width: '100%',
    height: 100,
    backgroundColor: '#f6f7f9',
    flexDirection: 'row',
    flexWrap: 'wrap',
    position: 'relative'
  },
  singleTab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#eee'
  },
  activeSingleTab: {
    borderBottomWidth: 0,
    backgroundColor: '#fff'
  },
  tabText: {
    fontSize: 32
  },
  normalTabText: {
    color: '#88898a'
  },
  activeTabText: {
    color: '#740a01'
  },
  topTabbarLine: {
    height: 10,
    backgroundColor: '#740a01',
    position: 'absolute',
    top: 0,
    zIndex: 99999
  }
};

styles = createStyles(styles);

export default CommonTabs;