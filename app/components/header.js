/*
 * @Author: yixin
 * @Date: 2018-01-25 10:52:16
 * @Last Modified by: yixin
 * @Last Modified time: 2018-12-13 14:34:58
 * 头部公用组件
 */
import React from 'react';
import Icon from 'react-native-vector-icons/icomoon';
import { Text, View } from 'react-native';
import { Flex } from 'antd-mobile-rn';
import { createStyles } from '../utils/view';
import { colors } from '../styles/basic';
import PageFn from '../utils/page';

/**
 *
 * @param {*} title 标题文字
 * @param {*} navigator 如果需要返回按钮则传入navigator
 */
const Header = ({ title, componentId }) => {
  return (
    <View style={styles.head}>
      {
        componentId && <Icon onPress={() => PageFn.pop(componentId)} style={styles.back} name="fanhui" />
      }
      <Text style={styles.text}>{title}</Text>
    </View>
  );
};

let styles = {
  head: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    ios: {
      height: 128,
      paddingTop: 35,
    },
    android: {
      height: 88,
    },
    backgroundColor: '#FDAC42',
  },
  text: {
    color: '#fff',
    fontSize: 36,
    fontWeight: 'bold',
  },
  back: {
    fontSize: 36,
    position: 'absolute',
    left: 30,
    color: '#fff',
    ios: {  
      top: 66,
    },
    paddingRight: 100,
  }
};

styles = createStyles(styles);

export default Header;