 /*
 * @Author: tanggan
 * 字体组件
 * @param {*} type标签类型，只接收homeLabel或planLabel
 * @param {*} textStyle标签字体的样式
 * @param {*} wrapperStyle标签字体外框的样式
 */

import React from 'react';
import { Text, View } from 'react-native';
import PropTypes from 'prop-types';
import { Rem } from '../utils/view';
import { colors } from '../styles/basic';

const LabelStyle = {
    'homeLabel': {
        viewStyle: {
            borderRadius: Rem(16),
            backgroundColor: colors.Red,
            paddingHorizontal: Rem(10),
            paddingVertical: Rem(2),
        },
        textStyle: {
            color: colors.White,
            fontSize: Rem(22),
            textAlign: 'center',
        }
    },
    'planLabel': {
        viewStyle: {
            borderRadius: Rem(9),
            backgroundColor: colors.White,
            paddingHorizontal: Rem(10),
            paddingVertical: Rem(2),
            borderWidth: 1,
            borderColor: colors.Red,
        },
        textStyle: {
            color: colors.Red,
            fontSize: Rem(22),
            textAlign: 'center',
        }
    }
}

class CommonLabel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        viewStyle: {},
        textStyle: {}
    };
  }

  static propTypes = {
    type: PropTypes.oneOf(['homeLabel','planLabel']),
    textStyle: PropTypes.object,
    wrapperStyle: PropTypes.object,
  }

  static defaultProps = {
    type: 'homeLabel',
    textStyle: {},
    wrapperStyle: {}
  }

  componentDidMount(){
    const { type } = this.props;
    this.setState({
        viewStyle: { ...LabelStyle[type].viewStyle },
        textStyle: { ...LabelStyle[type].textStyle }
    })
  }

  render() {
    const { wrapperStyle, fontStyle } = this.props;
    const { textStyle, viewStyle } = this.state;
    return <View style={[viewStyle,wrapperStyle]}>
        <Text style={[textStyle,fontStyle]}>
            {this.props.children}
        </Text>
    </View>
  }

}

export default CommonLabel;