
import React, { Component } from 'react';
import {
  View,
  Animated,
  Easing,
  Dimensions,
  PickerIOS,
  TouchableOpacity,
} from 'react-native';
import { createStyles } from '../utils/view';
import CText from '../components/CText';

const { height } = Dimensions.get('window');
const pheight = 600;

class PickerWidget extends Component {

  constructor(props) {
    super(props);
    this.state = {
      offset: new Animated.Value(0),
      opacity: new Animated.Value(0),
      choice: this.props.defaultVal,
      hide: true,
    };
    this.callback = function () {};// 回调方法
    this.parent = {};
  }

  componentWillUnmount() {
    this.timer && clearTimeout(this.timer);
  }

  // 显示动画
  in() {
    Animated.parallel([
      Animated.timing(
        this.state.opacity,
        {
          easing: Easing.linear,
          duration: 500,
          toValue: 0.8,
        }
      ),
      Animated.timing(
        this.state.offset,
        {
          easing: Easing.linear,
          duration: 500,
          toValue: 1,
        }
      )
    ]).start();
  }

  // 隐藏动画
  out() {
    Animated.parallel([
      Animated.timing(
        this.state.opacity,
        {
          easing: Easing.linear,
          duration: 500,
          toValue: 0,
        }
      ),
      Animated.timing(
        this.state.offset,
        {
          easing: Easing.linear,
          duration: 500,
          toValue: 0,
        }
      )
    ]).start();

    this.timer = setTimeout(
      () => this.setState({ hide: true }),
      500
    );
  }

  cancel(event) {
    if (!this.state.hide) {
      this.out();
    }
  }


  // 选择
  ok() {
    if (!this.state.hide) {
      this.out();
      this.callback.apply(this.parent, [this.state.choice]);
    }
  }

  /**
   * 显示picker
   * @param {*} obj parent this
   * @param {*} callback 回调
   */
  show(obj:Object, callback:Object) {
    this.parent = obj;
    this.callback = callback;
    if (this.state.hide) {
      this.setState({ hide: false }, this.in);
    } }

  render() {
    const { hide } = this.state;
    if (hide) {
      return (<View />);
    } else {
      return (
        <View style={styles.wrapper} >
          <View style={styles.mask}>
            <Animated.View style={[styles.tip, {
              transform: [{
                translateY: this.state.offset.interpolate({
                  inputRange: [0, 1],
                  outputRange: [height, (height - pheight)]
                }),
              }]
            }]}>
              <View style={styles.tipTitle}>
                <TouchableOpacity style={{marginLeft:20}} onPress={() => { this.cancel() }}><CText  color="#333">取消</CText></TouchableOpacity>
                <TouchableOpacity style={{marginRight:20}}  onPress={() => { this.ok() }}><CText   color="#e6454a">确定</CText></TouchableOpacity>
              </View>
              <PickerIOS
                style={styles.picker}
                itemStyle={styles.itempicker}
                selectedValue={this.state.choice}
                onValueChange={choice => this.setState({ choice: choice })}>
                {this.props.pickData.map((option, index) => <PickerIOS.Item label={option.name} value={index} key={option.id} />)}
              </PickerIOS>
            </Animated.View>
          </View>
        </View>
      );
    }
  }
}

let styles = {
  wrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    zIndex: 99,
  },
  mask: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    // alignItems: 'center',
  },
  tip: {
    height: pheight,
    backgroundColor:'#fff',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  tipTitle: {
    backgroundColor:'white',
    marginTop: -300,
    height: 100,
    width: 750,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#f0f0f0',
  },
  picker: {
    backgroundColor:'#fff',
    width: 750
  },
  itemPicker: {
    color: 'white',
    fontSize: 38,
    height: 30,
    width: 750,
  }

};
styles = createStyles(styles);
export default PickerWidget;
