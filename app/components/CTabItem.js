/*
 * @Author: yixin
 * @Date: 2018-08-10 11:14:03
 * @Last Modified by: yixin
 * @Last Modified time: 2018-08-13 18:36:24
 * 自定义tab
 */

import React from 'react';
import { View, Text } from 'react-native';
import { createStyles } from '../utils/view';

const CTabItem = ({ tab, currentAction }) => {
  return (
    <View style={[styles.wrapper, currentAction === tab.action ? styles.cTabActive : styles.cTab]}>
      <Text style={currentAction === tab.action ? styles.cTextActive : styles.CText}>{tab.name}</Text>
    </View>
  );
};

let styles = {
  wrapper: {
    flex: 1,
    height: 90,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  cTab: {
    backgroundColor: '#f6f7f9',
  },
  cText: {
    fontSize: 30,
    color: '#88898a',
  },
  cTabActive: {
    backgroundColor: '#fff',
  },
  cTextActive: {
    fontSize: 30,
    color: '#740a01',
  },
};
styles = createStyles(styles);

export default CTabItem;