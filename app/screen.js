import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';

// 消息
import MessageScreen from './pages/message/message';
import Chat from './pages/message/chat';
import ChatGroup from './pages/message/chatGroup';
import ChatSingle from './pages/message/chatSingle';
import ChaterInfo from './pages/message/chaterInfo';
import GroupSetting from './pages/message/groupSetting';
import ChangeNickName from './pages/message/changeNickName';
import MemberManage from './pages/message/memberManage';
import FriendInvatation from './pages/message/friendInvatation';
import SystemMessage from './pages/message/systemMessage';
import SystemInfo from './pages/message/systemInfo';
// 圈子
import FindScreen from './pages/find/find';
import FindClassify from './pages/find/findClassify';
// 活动
import Activity from './pages/activity/activity';
import ActivityDetail from './pages/activity/activityDetail';
import MyActivity from './pages/activity/myActivity';
import ActivitySuccess from './pages/activity/activitySuccess';
import ActivityFailed from './pages/activity/activityFailed';
// 发现
// import CircleScreen from './pages/circle/circle';
// import CircleScreen from './pages/circle/CirCleDongtaiDe
import CircleScreen from './pages/circle/circleHomeScreen';
import CirCleDongtaiDetail from '@/pages/circle/cirCleDongtaiDetail';
import CircleDetail from './pages/circle/circleDetail';
import Search from './pages/find/search';
import SearchResult from './pages/find/searchResult';
import FindFriend from './pages/circle/findFirend';
// import Unread from './pages/find/unread';
// 干货
import ArticleScreen from './pages/article/article';
import ArticleDetail from './pages/article/articleDetail';
import CommentList from './pages/article/commentList';
import ReleaseComment from './pages/article/releaseComment';
// 我的
import MineScreen from './pages/mine/mine';
import MineInfo from './pages/mine/inform';
import MineHobby from './pages/mine/hobby';
import MineDynamic from './pages/mine/dynamic';
import MineDynamicDetail from './pages/mine/dynamicDetail';
import About from './pages/mine/about';
import MineHost from './pages/mine/host';
import GroupIncome from './pages/mine/groupIncome';
import GroupMember from './pages/mine/groupMember';
import Rename from './pages/mine/rename';
import Address from './pages/mine/address';
import AddAddress from './pages/mine/addAddress';
import RealName from './pages/mine/realname';
import MineOrder from './pages/mine/mineOrder';
import MineProfit from './pages/mine/mineProfit';
import WithDraw from './pages/mine/withDraw';
import TxSuccess from './pages/mine/success';
import TxFailed from './pages/mine/failed';
import MineFind from './pages/mine/mineFind';
import UserSetting from './pages/mine/userSetting';
import UserChangePhone from './pages/mine/userChangePhone';
import UserWechatBind from './pages/mine/userWechatBind';
import WeChatChange from './pages/mine/weChatChange';
import DrawHistory from './pages/mine/drawHistory';


// 圈子Tab
import GroupTab from './pages/group/groupTab';
import ReleaseDynamic from './pages/group/releaseDynamic';
import DynamicDetail from './pages/group/dynamicDetail';
// 支付方面
import Draw from './pages/security/draw';
import DrawSuc from './pages/security/drawSuc';
import DrawFail from './pages/security/drawFail';
// 登录
import Initailising from './pages/common/initialising';
import Login from './pages/common/login';
import LoginPwd from './pages/common/loginPwd';
import Map from './pages/common/map';
import BindPhone from './pages/mine/bindPhone';
import PerfectId from './pages/mine/perfectId';
import ForgetPwd from './pages/common/forgetPwd';
import PlatformAgreement from '@/pages/common/platformAgreement';
import SecurityAgreement from './pages/common/securityAgreement';
// 商城
import Shop from './pages/shop/shop';
import GoodsDetail from './pages/shop/goodsDetail';
import Order from './pages/shop/order';

// 订单
import MyOrderDetail from './pages/mine/myOrderDetail';
import GoodsComment from './pages/mine/goodsComment';

export const registerScreens = (Store) => {

  // 消息
  Navigation.registerComponentWithRedux('MessageScreen', () => MessageScreen, Provider, Store);
  Navigation.registerComponentWithRedux('Chat', () => Chat, Provider, Store);
  Navigation.registerComponentWithRedux('ChatGroup', () => ChatGroup, Provider, Store);
  Navigation.registerComponentWithRedux('ChatSingle', () => ChatSingle, Provider, Store);
  Navigation.registerComponentWithRedux('ChaterInfo', () => ChaterInfo, Provider, Store);
  Navigation.registerComponentWithRedux('GroupSetting', () => GroupSetting, Provider, Store);
  Navigation.registerComponent('ChangeNickName', () => ChangeNickName);
  Navigation.registerComponent('MemberManage', () => MemberManage);
  Navigation.registerComponent('FriendInvatation', () => FriendInvatation);
  Navigation.registerComponent('SystemMessage', () => SystemMessage);
  Navigation.registerComponent('SystemInfo', () => SystemInfo);
  // 发现
  Navigation.registerComponentWithRedux('FindScreen', () => FindScreen, Provider, Store);
  Navigation.registerComponentWithRedux('FindClassify', () => FindClassify, Provider, Store);
  // 圈子;
  Navigation.registerComponent('CircleScreen', () => CircleScreen);
  Navigation.registerComponent('CirCleDongtaiDetail', () => CirCleDongtaiDetail);
  Navigation.registerComponent('CircleDetail', () => CircleDetail);
  Navigation.registerComponentWithRedux('Search', () => Search, Provider, Store);
  Navigation.registerComponent('SearchResult', () => SearchResult);
  Navigation.registerComponent('FindFriend', () => FindFriend);
  // 干货
  Navigation.registerComponent('ArticleScreen', () => ArticleScreen);
  Navigation.registerComponent('ArticleDetail', () => ArticleDetail);
  Navigation.registerComponent('CommentList', () => CommentList);
  Navigation.registerComponent('ReleaseComment', () => ReleaseComment);
  // 我的
  Navigation.registerComponentWithRedux('WeChatChange', () => WeChatChange, Provider, Store);
  Navigation.registerComponentWithRedux('UserWechatBind', () => UserWechatBind, Provider, Store);
  Navigation.registerComponentWithRedux('UserSetting', () => UserSetting, Provider, Store);
  Navigation.registerComponentWithRedux('MineScreen', () => MineScreen, Provider, Store);
  Navigation.registerComponentWithRedux('MineInfo', () => MineInfo, Provider, Store);
  Navigation.registerComponent('MineHobby', () => MineHobby);
  Navigation.registerComponent('MineDynamic', () => MineDynamic);
  Navigation.registerComponent('MineDynamicDetail', () => MineDynamicDetail);
  Navigation.registerComponent('About', () => About);
  Navigation.registerComponentWithRedux('MineHost', () => MineHost, Provider, Store);
  Navigation.registerComponent('GroupIncome', () => GroupIncome);
  Navigation.registerComponent('GroupMember', () => GroupMember);
  Navigation.registerComponent('Rename', () => Rename);
  Navigation.registerComponent('BindPhone', () => BindPhone);
  Navigation.registerComponentWithRedux('Address', () => Address, Provider, Store);
  Navigation.registerComponentWithRedux('AddAddress', () => AddAddress, Provider, Store);
  Navigation.registerComponentWithRedux('RealName', () => RealName, Provider, Store);
  Navigation.registerComponent('MineOrder', () => MineOrder);
  Navigation.registerComponent('MineProfit', () => MineProfit);
  Navigation.registerComponent('WithDraw', () => WithDraw);
  Navigation.registerComponent('TxSuccess', () => TxSuccess);
  Navigation.registerComponent('TxFailed', () => TxFailed);
  Navigation.registerComponent('MineFind', () => MineFind);
  Navigation.registerComponent('PerfectId', () => PerfectId);
  Navigation.registerComponent('UserChangePhone', () => UserChangePhone);
  Navigation.registerComponent('DrawHistory', () => DrawHistory);

  // Navigation.registerComponent('Unread', () => Unread);
  // 圈子Tab
  Navigation.registerComponent('GroupTab', () => GroupTab);
  Navigation.registerComponent('ReleaseDynamic', () => ReleaseDynamic);
  Navigation.registerComponent('DynamicDetail', () => DynamicDetail);
  // 支付
  Navigation.registerComponent('Draw', () => Draw);
  Navigation.registerComponent('DrawSuc', () => DrawSuc);
  Navigation.registerComponent('DrawFail', () => DrawFail);
  // 登录
  Navigation.registerComponent('Initailising', () => Initailising);
  Navigation.registerComponentWithRedux('Login', () => Login, Provider, Store);
  Navigation.registerComponentWithRedux('LoginPwd', () => LoginPwd, Provider, Store);
  Navigation.registerComponentWithRedux('ForgetPwd', () => ForgetPwd, Provider, Store);
  Navigation.registerComponent('Map', () => Map);
  Navigation.registerComponent('PlatformAgreement', () => PlatformAgreement);
  Navigation.registerComponent('SecurityAgreement', () => SecurityAgreement);
  // Shop
  Navigation.registerComponent('Shop', () => Shop);
  Navigation.registerComponent('GoodsDetail', () => GoodsDetail);
  Navigation.registerComponentWithRedux('Order', () => Order, Provider, Store);
  // 活动
  Navigation.registerComponent('Activity', () => Activity);
  Navigation.registerComponent('ActivityDetail', () => ActivityDetail);
  Navigation.registerComponent('MyActivity', () => MyActivity);
  Navigation.registerComponent('ActivitySuccess', () => ActivitySuccess);
  Navigation.registerComponent('ActivityFailed', () => ActivityFailed);

  // 订单
  Navigation.registerComponent('MyOrderDetail', () => MyOrderDetail);
  Navigation.registerComponent('GoodsComment', () => GoodsComment);
};