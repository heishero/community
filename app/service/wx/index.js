import { Http } from '../../utils/http';
/**
 * 微信code
 * @param {*} code
 */
export function wxLogin(code) {
  let params = {};
  params['code'] = code;
  return Http.Get(`login/from/wx`, params);
}

/**
 * 绑定微信
 * @param {*} code
 * @param {*} token
 */
export function wxBind(params) {
  return Http.Get(`user/bind/wx`, params);
}