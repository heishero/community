import { Http } from '../../utils/http';
/**
 * 获取所有群目录
 * @param {*} pid 分类上级id 默认值: 0
 */
export function getClassify(params) {
  return Http.Get(`crowd/classify`, params);
}

/**
 * 获取群列表
 * @param {*} p_cid
 * @param {*} search
 * @param {*} page
 * @param {*} pageSize
 */
export function getGroupList(params) {
  if (params.p_cid === 0) { delete params.p_cid }
  if (!params.search) { delete params.search }
  return Http.Get(`crowd/list`, params);
}

/**
 * 获取群详情
 * @param {*} id 群id
 */
export function getGroupDetail({ id, token }) {
  return Http.Get(`crowd/info`, { id, token });
}

/**
 * 创建群
 * @param {*} name 群名称
 * @param {*} headimgurl 群头像
 * @param {*} classify_id 群分类
 * @param {*} is_free 是否免费
 * @param {*} charge 费用
 * @param {*} is_private 公开还是私密
 * @param {*} address 地址
 * @param {*} lat 经度
 * @param {*} lon 纬度
 * @param {*} introduce 简介
 */
export function createGroup(params) {
  return Http.Post('crowd/create', params);
}

/**
 * 加入群
 * @param {*} crowd_id 群ID
 * @param {*} token
 */
export function joinGroup(params) {
  return Http.Post('user/join/crowd', params);
}

/**
 * 收藏群
 * @param {*} crowd_id 群ID
 * @param {*} token
 */
export function collectGroup(params) {
  return Http.Post('user/collect/crowd', params);
}
/**
 * 退出群
 * @param {*} crowd_id
 * @param {*} token
 */
export function outGroup(params) {
  return Http.Post('user/out/crowd', params);
}

/**
 * 获取消息列表
 * @param {*} token
 * @param {*} page
 * @param {*} pageSize
 */
export function getNews(params) {
  return Http.Get('topic/news', params);
}

/**
 * 我加入的群
 * @param {*} type 我的群类型;1加入的群 2收藏的群
 * @param {*} search 根据名字搜索
 * @param {*} page
 * @param {*} pageSize
 * @param {*} token
 */
export function getJoins(params) {
  return Http.Get('user/crowd/join/list', params);
}

/**
 * 极光id转换为groupid
 * @param {*} token
 * @param {*} jg_im_gid
 */
export function convertGroupId(params) {
  return Http.Get('crowd/info/jgid', params);
}

/**
 * 添加群免打扰
 * @param {*} token
 * @param {*} crowd_id
 */
export function setNodisturb(params) {
  return Http.Post('user/group/nodisturb', params);
}

/**
 * 取消群免打扰
 * @param {*} token
 * @param {*} crowd_id
 */
export function removeNodisturb(params) {
  return Http.Post('user/nodisturb/remove', params);
}

/**
 * 删除群成员
 * @param {*} crowd_jg_id
 * @param {*} member_jg_id
 * @param {*} token
 */
export function removeMember(params) {
  return Http.Post('crowd/remove/member', params);
}