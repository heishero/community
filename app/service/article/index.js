import { Http } from '../../utils/http';

/**
 * 获取文章详情
 * @param {*} id 文章id
 * @param {*} token
 */
export function getArticleDetail(parmas) {
  return Http.Get('article/info', { ...parmas });
}

/**
 * 获取文章评论
 * @param {*} article_id 文章id
 * @param {*} page
 * @param {*} pageSize
 */
export function getArticleComment(parmas) {
  return Http.Get('article/comment', { ...parmas });
}

/**
 * 推荐文章
 * @param {*} article_id 当前打开的文章ID
 */
export function getRecommendArticle(article_id) {
  return Http.Get('article/recommend', { article_id });
}

/**
 * 文章评论
 * @param {*} article_id
 * @param {*} token
 * @param {*} content
 */
export function articleComment(params) {
  return Http.Post('article/create/comment', params);
}

/**
 * 获取圈子文章列表
 * @param {*} crowd_id
 * @param {*} page
 * @param {*} pageSize
 */
export function getGroupArticle(params) {
  return Http.Get('crowd/article', params);
}

/**
 * 文章点赞
 * @param {*} token
 * @param {*} article_id
 */
export function articleUseful(parmas) {
  return Http.Post('article/useful', parmas);
}