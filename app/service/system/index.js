import { Http } from '../../utils/http';

/**
 * 平台消息
 * @param {*} token
 */
export function fetchSystemInform(params) {
  return Http.Post('system/information', { ...params });
}

export function fetchSystemDetail(params) {
  return Http.Post('system/information/info', { ...params });
}