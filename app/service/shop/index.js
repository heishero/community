import { Http } from '../../utils/http';

/**
 * 商品目录
 * @param {*} crowd_id 群ID
 * @param {*} token 用户token
 */
export function goodsMenu(params) {
  return Http.Get('crowd/cate/goods', { ...params });
}

/**
 * 商品目录
 * @param {*} cate_id 群商品分类ID
 * @param {*} token 用户token
 */
export function goodsList(params) {
  return Http.Get('goods/list', { ...params });
}

/**
 * @param {*} id 商品id
 * @param {*} token 用户token
 */
export function goodsDetail(params) {
  return Http.Get('goods/details', { ...params });
}

/**
 * 微信支付参数获取
 * @param {*} order_id
 * @param {*} token
 */
export function getWxPay(params) {
  return Http.Post('wechat/pay', { ...params });
}
/**
 * 取消订单
 */
export function postCancel(params) {
  return Http.Post('orders/cancel', { ...params });
}

/**
 * 商品推荐
 * @param {*} crowd_id
 * @param {*} page
 * @param {*} pageSize
 */
export function goodsRec(params) {
  return Http.Post('goods/goodRecommend', { ...params });
}