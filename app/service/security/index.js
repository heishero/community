import { Http } from '../../utils/http';

/**
 * @param {*} crowd_id 群ID
 * @param {*} service_charge 服务费
 * @param {*} freight 运费
 * @param {*} g_total_price 商品总价
 * @param {*} pay_total 总共需支付的总价
 * @param {*} g_id 商品ID
 * @param {*} buy_num 购买数量
 * @param {*} a_id 用户填写地址ID
 * @param {*} token
 */
export function createOrder(params) {
  return Http.Post('orders/create', { ...params });
}

/**
 * @param {*} status 
 * @param {*} token 
 */
export function myOrder(params) {
  return Http.Post('orders/myorder', { ...params });
}

/**
订单评价
 * @param {*} token 
 */
export function commentGoods(params){
  return Http.Post('orders/addGoodComment', { ...params });
}


/**
确认收货
 * @param {*} token 
 */
export function takeGoods(params){
  return Http.Post('orders/take', { ...params });
}

/**
 * @param {*} status 
 * @param {*} token 
 * //订单详情
 */
export function OrderDeTail(params) {
  return Http.Get('orders/info', { ...params });
}