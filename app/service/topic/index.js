import { Http } from '../../utils/http';

/**
 * 群动态列表
 * @param {*} id 群ID
 * @param {*} page
 * @param {*} pageSize
 */
export function groupTopic({ id = 0, page, pageSize }) {
  return Http.Get('crowd/topic', { id, page, pageSize });
}

/**
 * 动态详情
 * @param {*} id 群ID
 */
export function topicInfo(id) {
  return Http.Get('topic/info', { id });
}

/**
 * 动态评论列表
 * @param {*} topic_id 动态ID
 * @param {*} page
 * @param {*} pageSize
 */
export function topicComment(params) {
  return Http.Get('topic/comment', params);
}

/**
 * 发布动态
 * @param {*} crowd_id
 * @param {*} content
 * @param {*} img
 */
export function createTopic(params) {
  return Http.Post('topic/create', params);
}

/**
 * 评论
 * @param {*} token
 * @param {*} topic_id 动态ID
 * @param {*} content 评论内容
 */
export function createComment(params) {
  return Http.Post('topic/create/comment', params);
}

/**
 * 动态点赞
 * @param {*} token
 * @param {*} topic_id
 */
export function topicUseful(params) {
  return Http.Post('topic/useful', params);
}

/**
 * 取消点赞
 * @param {*} topic_id
 * @param {*} token
 */
export function cancelUseful(params) {
  return Http.Post('topic/cancel/useful', params);
}

/**
 * 获取动态列表
 * @param {*} token
 * @param {*} page
 * @param {*} pageSize
 */
export function getTopicList(params) {
  return Http.Get('topic/list', params);
}

/**
 * 我的动态
 * @param {*} token
 */
export function myTopic(params) {
  return Http.Post('topic/mytopic', params);
}

/**
 * 删除动态
 * @param {*} token
 * @param {*} id
 */
export function delTopic(params) {
  return Http.Post('topic/deltopic', params);
}

/**
 * 未读信息
 * @param {*} token
 */
export function unRead(params) {
  return Http.Post('topic/unread', params);
}
/**
 * 未读消息列表
 * @param {*} token
 */
export function unReadList(params) {
  return Http.Post('topic/unread/list', params);
}