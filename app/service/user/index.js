import { Http } from '../../utils/http';
/**
 * 获取验证码
 * @param {*} mobile
 */
export function getCode(mobile) {
  let params = {};
  params['mobile'] = mobile;
  return Http.Post(`validate/code/form/sms`, params);
}

/**
 * @param {*} mobile 手机号
 * @param {*} password 密码
 * @param {*} validate_code 验证码
 * @param {*} nickname 昵称
 */
export function register(mobile, password, validate_code, nickname) {
  let params = {};
  params['mobile'] = mobile;
  params['password'] = password;
  params['validate_code'] = validate_code;
  params['nickname'] = nickname;
  return Http.Post(`register/from/mobile`, params);
}

/**
 * @param {*} mobile 手机号
 * @param {*} validate_code 验证码
 */
export function login({ mobile, validate_code }) {
  let params = {};
  params['mobile'] = mobile;
  params['validate_code'] = validate_code;
  return Http.Post(`login/from/mobile`, params);
}

/**
 *
 * @param {*} mobile
 * @param {*} validate_code
 * @param {*} password
 */
export function forgetPassWord({ mobile, validate_code, password }) {
  let params = {};
  params['mobile'] = mobile;
  params['validate_code'] = validate_code;
  params['password'] = password;
  return Http.Post(`forget/password`, params);
}

/**
密码登录
 * @param {*} mobile 手机号
 *
 * @param {*} password 密码
 */
export function loginPwd({ mobile, password }) {
  let params = {};
  params['mobile'] = mobile;
  params['password'] = password;
  return Http.Post(`login`, params);
}

/**
 * @param {*} mobile 手机号
 * 修改绑定的手机号
 * @param {*} code 验证码
 */
export function mobileUpdate({ mobile, validate_code, token }) {
  let params = {};
  params['mobile'] = mobile;
  params['validate_code'] = validate_code;
  params['token'] = token;
  return Http.Post(`user/bind/mobile/update`, params);
}

/**
 *  @param {*} img
 */
export function uploadImg(img) {
  return Http.Post('tool/upload/img', img, 'pic');
}

/**
 * 获取自己的信息
 * @param {*} token
 */
export function getInfo(token) {
  return Http.Get('user/my/info', { token });
}

/**
 * 退出登录
 * @param {*} token
 */
export function loginOut(token) {
  return Http.Post('user/logout', token);
}

/**
 * 更新用户信息
 * @param {*} token
 * @param {*} headimgurl
 * @param {*} nick_name
 * @param {*} password 可选
 */
export function updateInfo(params) {
  return Http.Post('user/update/info', params);
}

/**
 * 获取用户动态列表
 * @param {*} page
 * @param {*} pageSize
 * @param {*} token
 */
export function topicList(params) {
  return Http.Get('user/topic', params);
}

/**
 * @param {*} system //系统
 * @param {*} version //当前版本号
 */
export function getVersion(params) {
  return Http.Get('tool/app/isupdate', params);
}

/**
 * 绑定手机
 * @param {*} params
 */
export function phoneBind(params) {
  return Http.Post('user/bind/mobile', params);
}

/**
 * 提交实名资料
 * @param {*} real_name 真实姓名
 * @param {*} mobile 联系电话
 * @param {*} id_card
 * @param {*} id_card_img_left 身份证正面照
 * @param {*} id_card_img_right 身份证反面照
 * @param {*} token
 */
export function userCert(params) {
  return Http.Post('user/cert', params);
}

/**
 * 获取收货地址
 * @param {*} token
 */
export function getAddress(token) {
  return Http.Get('address/myaddress', { token });
}

/**
 * 获取城市列表
 * @param {*} token
 */
export function getAreaData(token) {
  return Http.Get('address/area', { token });
}

/**
 *
 * @param {*} name 收货人姓名
 * @param {*} mobile 收货人电话
 * @param {*} province 省份
 * @param {*} city 市
 * @param {*} district 区、县
 * @param {*} address 详细地址 useing
 * @param {*} useing 默认地址
 * @param {*} token
 */
export function addAddress(params) {
  return Http.Post('address/add', params);
}

/**
 * 删除收货地址
 * @param {*} id 收货地址ID
 * @param {*} token
 */
export function delAddress(params) {
  return Http.Post('address/del', params);
}

/**
 * 修改收货地址
 * @param {*} token
 * @param {*} id 收货地址ID
 * @param {*} name 收货人姓名
 * @param {*} mobile 收货人电话
 * @param {*} province 省份
 * @param {*} city 市
 * @param {*} district 区、县
 * @param {*} address 详细地址 useing
 */

export function saveAddress(params) {
  return Http.Post('address/save', params);
}

/**
 * 查看收益
 * @param {*} token
 */

export function showDetail(params) {
  return Http.Post('user/waste', params);
}

/**
 * 提现
 */
export function withDraw(params) {
  return Http.Post('user/withraw', params);
}

/**
 * 余额查询
 */
export function userBalance(params) {
  return Http.Post('user/balance', params);
}

/**
 * 好友请求列表
 * @param {*} token
 */
export function reqFriendList(params) {
  return Http.Get('user/friend/req-list', params);
}

/**
 * 发送好友请求
 * @param {*} token
 * @param {*} friend
 */
export function friendInvatation(params) {
  return Http.Post('user/friend/req', params);
}

/**
 * 好友请求处理
 * @param {*} token
 * @param {*} status
 * @param {*} user_friend_id
 */
export function friendRes(params) {
  return Http.Post('user/friend/res', params);
}

/**
 * 查找好友
 * @param {*} token
 * @param {*} userMsg
 */
export function findFriend(params) {
  return Http.Post('user/userList', params);
}


/**
 * 提现记录
 * @param {*} token
 */
export function withdrawList(params) {
  return Http.Post('user/withdrawList', { ...params });
}

