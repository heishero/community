import { Http } from '../../utils/http';

// 高德POI key
const key = '6fcc38f487dc55d14c21fa8ba68ff33c';

/**
 * 高德坐标转换
 * @param {*} locations //经度和纬度用","分割，经度在前，纬度在后，经纬度小数点后不得超过6位
 */
export function convert(params) {
  return Http.GetGD(`assistant/coordinate/convert`, { key, ...params });
}

/**
 * 逆地理编码API服务地址
 * @param {*} location //经度在前，纬度在后，经纬度间以“,”分割，经纬度小数点后不要超过 6 位。必填
 * @param {*} radius //radius取值范围在0~3000，默认是1000。单位：米
 */
export function getRegeo(params) {
  return Http.GetGD(`geocode/regeo`, { key, ...params });
}

/**
 * POI搜索
 * @param {*} keywords //查询关键字
 * @param {*} city //可选值：城市中文、中文全拼、citycode、adcode
 * @param {*} citylimit //仅返回指定城市数据
 */
export function poiSearch(params) {
  return Http.GetGD(`place/text`, { key, ...params });
}