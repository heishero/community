import { Http } from '../../utils/http';

/**
 * 获取活动列表
 * @param {*} crowd_id 群id
 * @param {*} token
 */

export function activityList(params) {
  return Http.Get('activity/list', params);
}

/**
 * 获取活动详情
 * @param {*} id 活动ID
 * @param {*} token
 */
export function activityDetail(params) {
  return Http.Get('activity/info', params);
}

/**
 * 获取参与活动
 * @param {*} token
 * @param {*} activity_id 活动ID
 * @param {*} name 参与者姓名
 * @param {*} mobile 参与人手机号
 */
export function activityTake(params) {
  return Http.Post('activity/take',params);
}
/**
 * 我的活动列表
 * @param {*} token
 */

export function myActivity(params) {
  return Http.Post('activity/myactivity', params);
}