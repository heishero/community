

const ApiUrl =  'https://api.diandao.net.cn/api/0.0.1/'


class AppService {

    //不定长度参数
    fetchPost(shortUrl,parma) {
        return fetch(`${ApiUrl}${shortUrl}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
          body:JSON.stringify(parma)
        }).then(async (response) => {
            let result= await response.json();
            // if(result.Error==='未登录''){
            //         if(global.navigation){
            //             global.navigation.navigate('Login');
            //         }
            //     }
            return result;
        }).catch((error)=>{
            console.error(error);
        });
    }

    fetchGet(shortUrl,parma) {
        return fetch(`${ApiUrl}${shortUrl}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            
        }).then(async (response) => {
            // if (response._bodyBlob.size === 0) {
            //     return null
            // }
            return await response.json();
        }).catch((error)=>{
            console.error(error);
        });
    }

}
export default new AppService()