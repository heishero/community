/*
 * @Author: yixin
 * @Date: 2018-06-22 11:30:27
 * @Last Modified by: yixin
 * @Last Modified time: 2019-04-16 22:28:38
 */
import { Navigation } from 'react-native-navigation';
import { Platform } from 'react-native';
import { registerScreens } from './screen';
import { reduxStore } from './controllers';
import registerEvent from './utils/appEvent';
import { fixedFontScaling } from './utils/view';
import JPushModule from 'jpush-react-native';
import JMessage from 'jmessage-react-plugin';
import SplashScreen from 'react-native-splash-screen';
import { PageFn } from './utils/page';
import * as WeChat from 'react-native-wechat';
import { Geolocation } from 'react-native-amap-geolocation';
import { setDefaultOption } from '@/utils/navigate';
import Storage from '@/utils/storage';

export default class App {
  constructor() {
    // 注册reduxStore
    reduxStore({}, Store => {
      // 注册页面
      registerScreens(Store);
      registerEvent(Store);
      // tab初始化
      this.registerAppLaunched();
      // if (Platform.OS === 'android') {
      //   this.toInitialising();
      // }
      // this.toTest();
      this.registerJPush();
      this.registerJMessage();
      this.registerWx();
      this.registerMap();
      Storage.isInit();
      // 登录锁初始化
      global.lock = true;
      // 生产环境去除console
      if (!__DEV__) {
        global.console = {
          info: () => {},
          log: () => {},
          warn: () => {},
          debug: () => {},
          error: () => {}
        };
      }
      // ios不随系统字体影响
      fixedFontScaling();
      if (Platform.OS === 'android') {
        JPushModule.notifyJSDidLoad(data => {
          console.log('JPushModule log:', data);
        });
      }
    });
  }

  registerAppLaunched(res) {
    Navigation.events().registerAppLaunchedListener(() => {
      this.toInitialising();
    });
  }

  toInitialising() {
    setDefaultOption();
    Navigation.setRoot({
      root: {
        stack: {
          children: [{
            component: {
              name: 'Initailising'
            }
          }]
        }
      }
    });
  }

  toTest() {
    SplashScreen.hide();
    Navigation.setRoot({
      root: {
        stack: {
          children: [{
            component: {
              name: 'ChaterInfo'
            }
          }],
          options: {
            topBar: {
              visible: false,
              drawBehind: true,
            }
          }
        }
      }
    });
  }

  // 极光推送注册
  registerJPush() {
    JPushModule.addReceiveNotificationListener((message) => {
      console.log('ANreceive notification: ', message);
    });

    // JPushModule.addReceiveOpenNotificationListener((map) => {
    //   console.log('addReceiveOpenNotificationListener', map, this.props);
    //   if (map.custom) {
    //     if (map.custom.type === 'Chat') {
    //       Navigation.mergeOptions('BottomTabsId', {
    //         bottomTabs: {
    //           currentTabIndex: 0
    //         }
    //       });
    //     }
    //     PageFn.push('Chat', map.custom.type, { id: map.custom.id });
    //   }

    // });

    // JMessage.addClickMessageNotificationListener(({ message }) => {
    //   console.log('addClickMessageNotificationListener', message.target.id);
    //   Navigation.mergeOptions('BottomTabsId', {
    //     bottomTabs: {
    //       currentTabIndex: 0
    //     }
    //   });
    // });
  }

  // 极光im初始化
  registerJMessage() {
    JMessage.init({
      appkey: '9fea82812ba31bdd5fa7fd7b',
      isOpenMessageRoaming: true, // 是否开启消息漫游，默认不开启
      isProduction: false, // 是否为生产模式
    });
    // 开启debug模式
    JMessage.setDebugMode({ enable: true });
  }

  registerWx() {
    try {
      WeChat.registerApp('wxb6dac40496ad4d78');
    } catch (e) {
      console.error(e);
    }
  }

  // 注册高德定位
  registerMap() {
    try {
      Geolocation.init({
        ios: '9bd6c82e77583020a73ef1af59d0c759',
        android: 'cdd4f6af07c2fe279208998c532fd9a6'
      });
    } catch (error) {
      console.log(error);
    }
  }
}
